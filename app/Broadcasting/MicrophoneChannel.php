<?php

namespace App\Broadcasting;

use Berkayk\OneSignal\OneSignalClient;
use NotificationChannels\OneSignal\OneSignalChannel;

class MicrophoneChannel extends OneSignalChannel
{
    public function __construct()
    {
        $oneSignal = new OneSignalClient(env("MICROPHONE_ONESIGNAL_APP_ID"), env("MICROPHONE_ONESIGNAL_REST_API_KEY"),null);
        parent::__construct($oneSignal);
    }
}
