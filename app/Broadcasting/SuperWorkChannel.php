<?php

namespace App\Broadcasting;

use Berkayk\OneSignal\OneSignalClient;
use NotificationChannels\OneSignal\OneSignalChannel;

class SuperWorkChannel extends OneSignalChannel
{
    public function __construct()
    {
        $oneSignal = new OneSignalClient(env("SUPERWORK_ONESIGNAL_APP_ID"), env("SUPERWORK_ONESIGNAL_REST_API_KEY"),null);
        parent::__construct($oneSignal);
    }
}
