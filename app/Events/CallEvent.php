<?php

namespace App\Events;

use App\Models\Messenger\MessengerLogCall;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Symfony\Contracts\EventDispatcher\Event;

class CallEvent extends Event implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $user;
    private $logCall;
    private $resultToken;
    private $userToCall;

    /**
     * Create a new event instance.
     *
     * @param $user
     * @param MessengerLogCall $logCall
     * @param $resultToken
     * @param $userToCall
     */
    public function __construct($user, MessengerLogCall $logCall, $resultToken, $userToCall)
    {
        //
        $this->user = $user;
        $this->logCall = $logCall;
        $this->userToCall = $userToCall;
        $this->resultToken = $resultToken;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     */
    public function broadcastOn()
    {
        return new Channel('get.index.call.' . $this->logCall->to_id);
    }

    public function broadcastAs()
    {
        Log::error("get.index.call." . $this->logCall->to_id);
        return 'get.index.call.' . $this->logCall->to_id;
    }

    public function broadcastWith()
    {
        return ['user' => $this->user, 'log_call' => $this->logCall, 'result_token' => $this->resultToken];
    }
}
