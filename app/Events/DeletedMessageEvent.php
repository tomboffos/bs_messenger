<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Collection;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Symfony\Contracts\EventDispatcher\Event;

class DeletedMessageEvent extends Event implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $messages;
    public $type;
    public $chat;
    public $user;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($messages,string $type,int $chat,$user)
    {
        $this->messages = $messages;
        $this->type = $type;
        $this->chat = $chat;
        $this->user = $user;
    }

    public function broadcastWith(){

        return ["chat" => $this->chat,"messages_id" => $this->messages,"type" => $this->type,'user' => $this->user];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('deleted.message.'.$this->chat);
    }

    public function broadcastAs()
    {
        return 'deleted.message.'.$this->chat;
    }
}
