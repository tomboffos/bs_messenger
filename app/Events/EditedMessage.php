<?php

namespace App\Events;

use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\MessengerMessage;
use App\Services\MessageService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Symfony\Contracts\EventDispatcher\Event;

class EditedMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $chat;
    public $message;
    public $currentChat;
    public $edited;
    public $deleted;
    /**
     * Create a new event instance.
     *
     * @param MessageService $chat
     * @param MessengerMessage $message
     * @param MessengerChat $currentChat
     */
    public function __construct(MessageService $chat,MessengerMessage $message,MessengerChat $currentChat,$edited,$deleted)
    {
        $this->message = $message;
        $this->currentChat = $currentChat;
        $this->chat = $chat->getMessageChat($this->currentChat,null,null,'top');
        $this->edited = $edited;
        $this->deleted = $deleted;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('messages.edit.'.$this->message->chat_id);
    }

    public function broadcastWith(){

        return ["chat" => $this->chat];
    }

    public function broadcastAs()
    {
        return 'messages.edit.'.$this->message->chat_id;
    }
}
