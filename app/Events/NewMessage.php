<?php

namespace App\Events;

use App\Models\Messenger\MessengerMessage;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\URL;

class NewMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;
    public $type;
    public $color;
    public $transfer;
    /**
     * Create a new event instance.
     *
     * @param MessengerMessage $message
     */
    public function __construct(MessengerMessage $message,$type,$color)
    {

        $this->message = $message;
        $this->transfer = $message->transfer;
        $this->type = $type;
        $this->color = $color;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('messages.'.$this->message->chat_id);
    }

    public function broadcastWith(){
        $this->message->load('fromContact');
        $this->message->load('file');
        $this->message->load('map');
        $this->message->load('transfer');
        $this->message->load('color');
        $this->message->load('contact');
        $this->message->load('toContact');
        $this->transfer->load('fromContact');
        $this->transfer->load('file');
        $this->transfer->load('map');

        $this->message->file->map(function ($file){
            $file->full_link =   'https://api.aiocorp.kz/'.$file->file;
            $file->full_preload =   'https://api.aiocorp.kz/'.$file->preload_photo;
        });

        if (!empty($this->message->fromContact->id)){
            $this->message->fromContact->full_link =  URL::to('/').'/'.$this->message->fromContact->avatar;

        }

        return ["message" => $this->message,'type' => $this->type,'color' => $this->color,'transfer' => $this->transfer];
    }

    public function broadcastAs()
    {
        return 'messages.'.$this->message->chat_id;
    }
}
