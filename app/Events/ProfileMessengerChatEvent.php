<?php

namespace App\Events;

use App\Models\Messenger\MessengerChat;
use App\Services\ChatService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ProfileMessengerChatEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $chat;
    public $profile;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(MessengerChat $chat ,ChatService $service)
    {
        $this->chat = $chat;
        $this->profile = $service->showChat($chat);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('profile.chat.'.$this->chat->id);
    }

    public function broadcastAs()
    {
        return 'profile.chat.'.$this->chat->id;
    }
}
