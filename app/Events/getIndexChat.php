<?php

namespace App\Events;

use App\Models\Messenger\MessengerCategoryChat;
use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\MessengerChatSettings;
use App\Models\Messenger\MessengerMessage;
use App\Models\User;
use App\Services\ChatService;

use App\Services\MediaService;
use App\Services\SearchService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Symfony\Contracts\EventDispatcher\Event;


class getIndexChat extends Event implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $chat;
    public $user;
    public $last_message;
    public $category;
    public $settings;
    public $no_read_message;
    public $type;
    public $name;
    public function __construct($chat,User $user, $last_message,$category,$settings,$no_read_message,$type,$name){
        $this->name = $name;
        $this->user = $user;
        $this->chat = $chat;
        $this->last_message = $last_message;
        $this->category = $category;
        $this->settings = $settings;
        $this->no_read_message = $no_read_message;
        $this->type = $type;
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('get.index.'.$this->user->id);
    }

    public function broadcastWith(){
        $chat = New ChatService(null,null);
        $contactService = New SearchService();
        if ($this->chat->is_private == 1){
            $contact = $chat->getMessengerPrivateChatMemberByUser($this->chat,$this->user);

            if (!empty($contact->user_id)) {
                $contactUser = User::where('id', '=', $contact->user_id)
                    ->first();
                $this->chat->name = $contactService->getNicknameContactInterlocutor($this->user,$contactUser) == null ? $contactUser->name : $contactService->getNicknameContactInterlocutor($this->user,$contactUser);
                $this->chat->full_link = URL::to('/') . '/' . $contactUser->avatar;
            } else {
                $this->chat->full_link = null;
            }
        }
        $this->chat->admin_id = $chat->getAdminChatId($this->chat);
        $this->last_message->load('fromContact');
        $this->last_message->load('toContact');
        return ["chat_name" => $this->name,"chat" => $this->chat,'last_message' => $this->last_message,'category_chat' => $this->category,'settings' => $this->settings,'no_read_message' => $this->no_read_message,'type' => $this->type];
   }

    public function broadcastAs()
    {
        return 'get.index.'.$this->user->id;
    }
}
