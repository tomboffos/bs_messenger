<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\MessengerChatMember;
use Illuminate\Http\Request;

class ConferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $chat = MessengerChat::create(array_merge([
            'avatar' => $request->file('avatar') != null ? $request->file('avatar')->store('/messenger/chat/avatar') : '',
            'is_conference' => true
        ], $request->except(['avatar'])));
        MessengerChatMember::create([
            'chat_id' => $chat->id,
            'user_id' => $request->user()->id,
            'show_chat' => 1,
            'role' => 'admin'
        ]);
        return response($chat, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MessengerChat $conference)
    {
        //
        $conference->update([
            'is_paid' => true
        ]);
        return response([], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
