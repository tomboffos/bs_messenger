<?php

namespace App\Http\Controllers\Api\User\Messenger;

use App\Http\Controllers\Controller;
use App\Models\Document;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DocumentController extends Controller
{
    //
    public function index(Request $request)
    {
        return $request->user()->documents;
    }

    public function store(Request $request)
    {
        return Document::create(array_merge($request->except('file'), [
            'file' => $request['file']->storeAs('document', Carbon::now()->day . '-' . Carbon::now()->month . '-' . Carbon::now()->year . '-' . mt_rand(100,999) . '.' . $request->file('file')->getClientOriginalExtension(), ['disk' => 'public']),
            'user_id' => $request->user()->id
        ]));
    }

    public function shareWithOther(Document $document, User $user)
    {
        return Document::create([
            'file' => $document->file,
            'user_id' => $user->id
        ]);
    }


    public function destroy(Document $document)
    {
        $document->delete();

        return response([], 200);
    }
}
