<?php

namespace App\Http\Controllers\Application;

use App\Models\Application;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ApplicationController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Просмотр баланса сервисов'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Application::all();

        return $data;
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Application::create($request->all());

        return $data;
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Application::find($request->id);
        if ($data) {
            $data->update($request->all());
            $data = 'Success';
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }
        return $data;
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Application::destroy($request->id);

        if ($data == 1) {$data = 'Success';} else {$data = 'Error';}

        return $data;
    }
}
