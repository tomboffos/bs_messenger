<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\SimpleException;
use App\Http\Controllers\Controller;
use App\Http\Controllers\User\UserController;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\PhoneRequest;
use App\Models\Messenger\MessengerContactUser;
use App\Models\Messenger\UserContactPhone;
use App\Models\MessengerUserCategoryOrder;
use App\Models\Referral;
use App\Models\User;
use App\Services\MessengerInitService;
use App\Services\TwillioVoiceService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

include "smsc_api.php";

class AuthController extends Controller
{
    public $init;
    public $voice_service;

    public function __construct(MessengerInitService $init)
    {
        $this->init = $init;
        $this->voice_service = new TwillioVoiceService();
    }

    public function createCode(PhoneRequest $request)
    {
        $phone = str_replace(['+',' ','-'], "", $request->phone);
        $user = User::where('phone', '=', '+'.$phone)
            ->first();
        $code = str_pad(rand(0, pow(10, 4) - 1), 4, '0', STR_PAD_LEFT);
        if (!empty($user->phone)) {
            if ($user->id == 2 || $user->id == 3) {
                return $this->returnCode($user->id, $user->phone, '0000', 0);
            }
            User::where('phone', '=', '+'.$phone)
                ->update(
                    [
                        'code' => $user->id == 1 ? '0000' : $code,
                        'attempts' => 0,
                    ]
                );
        } else {
            User::create([
                'phone' => '+'.$phone,
                'code' => $code,
                'attempts' => 0,
            ]);
            $user = User::where('phone', '+'.$phone)->first();
        }
//        send_sms($request->phone, 'Код для входа в AIO: ' . $code);
        $this->voice_service->call($phone,$code);
        return $this->returnCode($user->id,$phone,$code,0);
    }



    public function returnCode($user,$phone,$code,$attempts){
        return response()->json([
            'id' => $user,
            'phone' => $phone,
            'code' => $code,
            'attempts' => $attempts,
        ]);
    }

    public function login(LoginRequest $request) {
        $phone = str_replace(['+',' ','-'], "", $request->phone);
        $user = User::where('phone', '=', '+'.$phone)
            ->first();
        $checkOrder = MessengerUserCategoryOrder::where('user_id','=',$user->id)
            ->first();
        if (empty($checkOrder->user_id)){
            $this->init->OrderCategoryInit($user);
        }
        $this->init->initUserSettings($user);
        $this->init->pushToCityChat($user);
        if (($request->get('code') != $user->code ) && (!Hash::check($request->password, $user->password))){
            return response()->json([
                'type' => 'sing_in_error',
                'message' => 'Error in Login. ' .  'Attempts: ' . $user->attempts,
                'attempts' => $user->attempts,
            ], 400);
        }

        $user->update(['code' => $user->id == 2 || 3 ? '0000' : null, 'attempts' => 0, 'language_id' => $request->get('language_id')]);
        $user->save();

        switch ($request['application_id']) {
            case 1:
                if ($user->roles->contains(2) != 1) {
                    $user->roles()->attach(2);
                }
                $tokenResult = $user->createToken('authToken')->plainTextToken;
                break;
            case 2:
                if ($user->roles->contains(6) != 1) {
                    $user->roles()->attach(6);
                }
                $tokenResult = $user->createToken('tokenSuperWork')->plainTextToken;
                break;
            default:
                $tokenResult = $user->createToken('authToken')->plainTextToken;
                break;
        }

        if ($user->applications->contains($request['application_id']) != 1) {
            if ($request['referral_token']) {
                $referrerStatus = Referral::where('referral_token', $request['referral_token'])->first();
                if ($referrerStatus) {
                    if ($referrerStatus['application_id'] == $request['application_id']) {
                        UserController::activateReferral($user, $request['referral_token']);
                    }
                }
            }
            $user->applications()->attach($request['application_id']);
        }

        $contactPhoneUsers = UserContactPhone::where('phone', $user->phone)->get(); // users who have my contact

        foreach ($contactPhoneUsers as $contactPhoneUser) {
            if ($contactPhoneUser->user_id != $user->id){
                MessengerContactUser::firstOrCreate([
                    'user_id' => $contactPhoneUser->user_id,
                    'contact_id' => $user->id
                ]);
            }
        }
        return response()->json([
            'access_token' => $tokenResult,
            'token_type' => 'Bearer',
            'user' => $user
        ]);
    }


//    public function login(Appeal $request)
//    {
//        try {
//            $request->validate([
//                'email' => 'required',
//                'password' => 'required'
//            ]);
//            $user = User::where('email', $request->email)->first();
//            if (! $user || (!Hash::check($request->password, $user->password))) {
//                throw ValidationException::withMessages([
//                    'login' => ['The provided credentials are incorrect.'],
//                ]);
//            }
//            $tokenResult = $user->createToken('authToken')->plainTextToken;
//            return response()->json([
//                'access_token' => $tokenResult,
//                'token_type' => 'Bearer',
//            ]);
//        } catch (Exception $error) {
//            return response()->json([
//                'message' => 'Error in Login'], 401);
//        }
//    }
}
