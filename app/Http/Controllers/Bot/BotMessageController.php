<?php

namespace App\Http\Controllers\Bot;

use App\Http\Controllers\Controller;
use App\Http\Requests\BotMessageStoreRequest;
use App\Http\Resources\BotMessagesResource;
use App\Models\BotMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class BotMessageController extends Controller
{

    private $secret = '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';

    //
    public function index(Request $request)
    {
        return BotMessagesResource::collection($request->user()->botMessages);
    }

    public function store(BotMessageStoreRequest $request)
    {
        return $this->sendMessageToAI($this->createBotMessage($request));
    }


    private function createBotMessage(BotMessageStoreRequest $request)
    {
        return BotMessage::create(array_merge([
            'user_id' => $request->user()->id,
            'author_id' => $request->user()->id,
        ], $request->validated()));
    }

    private function sendMessageToAI(BotMessage $botMessage)
    {
        $key = [
            'Hello' => 'Hello',
            'How are you?' => 'I am great! Thank you',
            'Who are you?' => 'I am BS bot, your smart assistant',
            'Ha-ha-ha, you are Edman' => 'Ha-ha-ha-ha',
            'Goodbye' => 'Bye!',
            'What are you doing' => 'At the moment i am helping you',
            'Привет' => 'Привет',
            'Как дела?' => 'Отлично! Спасибо',
            'Кто ты?' => 'Я БС Бот, твой умный помощник',
            'Ха-ха-ха, ты Эдман' => 'Ха-ха-ха-ха',
            'Пока' => 'Пока',
            'Что ты делаешь' => 'В данный момент я помогаю вам'
        ];
//        $response = Http::withHeaders([
//            'Authorization' => "Bearer {$this->secret}",
//            'Content-Type' => 'application/json'
//        ])->post('https://api.openai.com/v1/engines/davinci/completions', [
//            "prompt" => $botMessage->message,
//            "temperature" => 0.9,
//            "n" => 2,
//            "frequency_penalty" => 0.0,
//            "presence_penalty" => 0.6,
//            "stop" => ["\n", " Human:", " AI:"]
//
//        ]);

        BotMessage::create([
            'author_id' => null,
            'user_id' => $botMessage->user_id,
            'message' => isset($key[$botMessage->message]) ? $key[$botMessage->message] : 'К сожалению я не могу вам помочь'
        ]);
        return $botMessage;
    }

}
