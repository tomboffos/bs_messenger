<?php

namespace App\Http\Controllers\City;

use App\Models\City;
use App\Models\Country;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CityController extends Controller
{
    public function index()
    {
        $user = Auth::user();

//        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
//            throw new AuthorizationException();
////            return response()->json([
////                'message' => 'Forbidden'], 403);
//        }

        $data = City::with('country')->paginate(100);

        return $data;
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = City::create($request->all());

        return $data;
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = City::find($request->id);
        if ($data) {
            $data->update($request->all());
            $data = 'Success';
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }
        return $data;
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        $record = City::find($request['id']);

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        if ($record) {
            foreach ($record->ads as $item) {
                $item->update(['city_id' => null]);
            }
            foreach ($record->users as $item) {
                $item->update(['city_id' => null]);
            }
//            foreach ($record->translations as $item) {
//                CityTranslation::destroy($item['id']);
//            }

            City::destroy($record['id']);

            $data = response()->json([
                'message' => 'Success'], 200);
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }

        return $data;
    }

    public function import(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $json = json_decode(file_get_contents($request['file']), true);

        foreach ($json as $key => $value) {
            $country = Country::create(['name' => $key]);
            foreach ($value as $city) {
                City::create(['name' => $city, 'country_id' => $country['id']]);
            }
        }

        return 'Success';
    }

    public function getByCountry(Request $request)
    {
        $data = City::where('country_id', $request['country_id'])->orderBy('name')->get();

        return $data;
    }

    public function getById(Request $request)
    {
        $data = City::where('id', $request['id'])->with('country')->first();

        return $data;
    }

    public function search(Request $request)
    {
        $data = Country::whereHas('cities', function ($query) use ($request) {
            $query->where('name', 'iLIKE', $request->search . '%');
        })->with('cities')->paginate($request->limit ?? 20);

        return response()->json($data, 200);
    }
}
