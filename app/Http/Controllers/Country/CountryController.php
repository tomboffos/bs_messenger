<?php

namespace App\Http\Controllers\Country;

use App\Models\Country;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function index()
    {
        $data = Country::all();

        return $data;
    }
}
