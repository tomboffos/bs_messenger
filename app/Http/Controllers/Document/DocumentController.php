<?php

namespace App\Http\Controllers\Document;

use App\Models\Document;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class DocumentController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Document::all();

        return $data;
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Document::create($request->except('file'));
        if ($request->hasFile('file')) {
            Storage::disk('public')->delete($data['file']);
            $path = $request['file']->store('documents', ['disk' => 'public']);

            $data->update(['file' => $path]);
        }

        return $data;
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Document::find($request->id);
        if ($data) {
            if ($request->hasFile('file')) {
                Storage::disk('public')->delete($data['file']);
                $path = $request['file']->store('documents', ['disk' => 'public']);

                $data->update(['file' => $path]);
            }
            $data->update($request->except('file'));
            $data = 'Success';
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }
        return $data;
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        foreach ($request['ids'] as $id) {
            $record = Document::find($id);

            if ((!checkPermissions($user, ['Доступ администратора к базе'])) &&
                !(checkPermissions($user, ['Редактирование информации в профиле']) && $user['id'] == $record['user_id'])) {
                throw new AuthorizationException();
//                return response()->json([
//                    'message' => 'Forbidden'], 403);
            }

            if ($record) {
                if ($record['file']) {
                    Storage::disk('public')->delete($record['file']);
                }
                Document::destroy($record['id']);
            }
        }

        return response()->json(['message' => 'Success'], 200);
    }

    public function publish(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Редактирование информации в профиле'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }


        if ($request['files']) {
            foreach ($request['files'] as $file) {
                $data = Document::create($request->except('file'));

                Storage::disk('public')->delete($data['file']);
                $path = $file->store('documents', ['disk' => 'public']);

                $data->update(['file' => $path, 'user_id' => $user['id']]);
                $data->update(['user_id' => $user['id']]);
            }
        }

        return response()->json(['message' => 'Success'], 200);
    }

    public function getSelf()
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Редактирование информации в профиле'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Document::where('user_id', $user['id'])->get();

        return $data;
    }

    public function getByUser(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Document::where('user_id', $request['user_id'])->get();

        return $data;
    }
}
