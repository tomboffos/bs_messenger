<?php

namespace App\Http\Controllers\Media;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateMediaCategory;
use App\Models\MediaCategory;
use App\Services\MediaService;
use Illuminate\Http\Request;

class MediaCategoryController extends Controller
{
    public $media;

    public function __construct(MediaService $media){
        $this->media = $media;
    }

    public function index(){
        $result = $this->media->getAllMediaCategory();

        return response()->json($result, 200);
    }

    public function create(CreateMediaCategory $request){
        $result = $this->media->create($request);

        return response()->json($result, 200);
    }

    public function show(MediaCategory $category){

    }

    public function update(CreateMediaCategory $request,MediaCategory $category){
        $this->media->update($request,$category);

        return response()->json($category, 200);
    }

    public function delete(MediaCategory $category){
        $result = $this->media->delete($category);

        return response()->json($result, 200);
    }
}
