<?php

namespace App\Http\Controllers\Media;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteMediaRequest;
use App\Http\Requests\NewFileRequest;
use App\Http\Requests\UpdateFileRequest;
use App\Models\Media;
use App\Services\MediaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MediaController extends Controller
{
    public $media;

    public function __construct(MediaService $media){
        $this->media = $media;
    }

    public function index(){
        $result = $this->media->getMediaFull();

        return response()->json($result, 200);
    }

    public function userMedia(){
        $result = $this->media->getMediaUser();

        return response()->json($result, 200);
    }

    public function create(NewFileRequest $request){
        if ($request->hasFile('file')){
            foreach ($request->file as $file){
                $this->media->newFile($file,$request->type);
            }
        }
        return response()->json($this->media->getMediaUser(), 200);
    }

    public function show(Media $media){
        $result = $this->media->showMedia($media);
        return response()->json($result, 200);
    }

    public function update(Media $media, UpdateFileRequest $request){
        if ($request->hasFile('file')) {
            $this->media->updateMedia($media,$request);
        }
        return response()->json($this->media->getMediaUser(), 200);
    }

    public function delete(DeleteMediaRequest $category){
        $mediaId = explode ( ',',$category->media);
        foreach ($mediaId as $media){

            $this->media->deleteMedia($media);
        }
        return response()->json($this->media->getMediaUser(), 200);
    }
}
