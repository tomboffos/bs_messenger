<?php

namespace App\Http\Controllers\Messenger;

use App\Agora\RtcTokenBuilder;
use App\Agora\RtmTokenBuilder;
use App\Events\AgoraCallEvent;
use App\Events\AgoraReceiveCall;
use App\Events\AgoraStreamEvent;
use App\Events\AgoraVideoEvent;
use App\Events\CallEvent;
use App\Events\MainCallEvent;
use App\Events\NewCallEvent;
use App\Events\NewMessageChat;
use App\Http\Controllers\Controller;
use App\Http\Requests\AgoraCallRequest;
use App\Http\Requests\CallRequest;
use App\Http\Requests\ContinueBroadcastByPlanRequest;
use App\Http\Requests\MessageStreamRequest;
use App\Http\Requests\NewConferenceRequest;
use App\Http\Requests\NewStreamRequest;
use App\Http\Requests\StreamRequest;
use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\MessengerLogCall;
use App\Models\Messenger\MessengerStream;
use App\Models\Tariff;
use App\Models\User;
use App\Notifications\Messenger\NewCall;
use App\Notifications\Messenger\NewMessage as NewMessageNotification;
use App\Services\AgoraService;
use App\Services\CallService;
use App\Services\FirebaseService;
use App\Services\SearchService;
use App\Services\TransactionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class AgoraController extends Controller
{
    public $call;
    public $transaction;
    public $agora;
    public $contact;

    public function token(Request $request, $channel)
    {
        $appID = "df885d6ed93c41a19ac79d39220bc8ec";
        $appCertificate = "dbd438ed6b7949dba1bdd534232b5740";
        $channelName = $channel;
        $user = Auth::user()->name;
        $role = RtcTokenBuilder::RoleAttendee;
        $expireTimeInSeconds = 3600;
        $currentTimestamp = now()->getTimestamp();
        $privilegeExpiredTs = $currentTimestamp + $expireTimeInSeconds;

        return RtcTokenBuilder::buildTokenWithUserAccount($appID, $appCertificate, $channelName, $user, $role, $privilegeExpiredTs);
    }


    public function __construct(CallService $call, TransactionService $transaction, AgoraService $agora, SearchService $contact)
    {
        $this->call = $call;
        $this->agora = $agora;
        $this->transaction = $transaction;
        $this->contact = $contact;
    }


    public function RtmToken(Request $request, $channel)
    {
        /*$appID = config('agora.app_id');
        $appCertificate = config('agora.app_certificate');
        $expireTimeInSeconds = config('agora.expire_time');
*/
        return RtmTokenBuilder::buildToken(
            $request->app_id,
            $request->certificate,
            Auth::user()->id,
            2,
            now()->getTimestamp() + $request->expire_time,
            $channel
        );
    }


    public function getLogCallUser(Request $request)
    {
        $result = $this->call->getLogCallUser($request);
        $missCall = $this->call->getMissCall();
        return response()->json(
            [
                'call' => $result,
                'count_miss_call' => $missCall,
            ], 200);
    }

    public function deleteAll(Request $request)
    {
        $calls = MessengerLogCall::where(function ($q) {
            $q->where('from_id', '=', Auth::user()->id);
        })->orWhere(function ($q) {
            $q->where('to_id', '=', Auth::user()->id);
        })
            ->orderByDesc('id')
            ->with('fromContact', function ($query) {
                $query->select('id', 'phone', 'name', 'surname', 'avatar');
            })
            ->with('toContact', function ($query) {
                $query->select('id', 'phone', 'name', 'surname', 'avatar');
            })->get();

        foreach ($calls as $call)
            $call->delete();

        return response([], 200);
    }

    public function deleteCall(MessengerLogCall $call)
    {
        $call->delete();
    }

    public function getBlockUserList(Request $request)
    {
        $result = $this->call->getBlockUserList($request);
        return response()->json(
            [
                'block_user' => $result,
            ], 200);
    }

    public function addBlockUser(Request $request)
    {
        $contacts = explode(',', $request->contact);
        foreach ($contacts as $contact) {
            $user = User::where('id', '=', $contact)
                ->first();
            if (empty($user->id)) {
                $this->call->addBlockUser($user);
            }
        }
        return $this->getBlockUserList($request);
    }

    public function deleteBlockUser(Request $request)
    {
        $contacts = explode(',', $request->contact);
        foreach ($contacts as $contact) {
            $user = User::where('id', '=', $contact)
                ->first();
            if (empty($user->id)) {
                $this->call->deleteBlockUser($user);
            }
        }
        return $this->getBlockUserList($request);
    }

    public function agoraOneOnOneCall(AgoraCallRequest $request)
    {

    }

    public function agoraStreamInit()
    {
        $data = 0;
        broadcast(new AgoraStreamEvent($data, Auth::user()));
    }

    public function videoCall(AgoraCallRequest $request)
    {
        $data['userToCall'] = $request->user_to_call;
        $data['channelName'] = $request->channel_name;
        $data['from'] = Auth::user()->id;
        $this->call->newLogCall($request);

        broadcast(new AgoraVideoEvent($data))->toOthers();
    }

    public function acceptVideoCall(Request $request)
    {
        $video = MessengerLogCall::where('id', '=', $request->id)
            ->first();
        $this->call->updateLogCall($request, $video);
    }

    public function continueVideoCall(ContinueBroadcastByPlanRequest $request)
    {
        $getCall = MessengerLogCall::where('id', '=', $request->get('video'))
            ->first();
        $user = User::where('id', '=', $getCall->from_id)
            ->first();
        $plan = Tariff::where('id', '=', 1)
            ->first();

        return $this->agora->continueVideo($user, $plan);
    }

    public function newStream(NewStreamRequest $request)
    {
        $result = $this->agora->newStream($request);
        broadcast(new AgoraStreamEvent($result, Auth::user()))->toOthers();
        return response()->json($result, 200);
    }

    public function newMessageStream(MessageStreamRequest $request)
    {
        $result = $this->agora->sendMessageStream($request);
        broadcast(new NewMessageChat($result));
        return response()->json($result, 200);
    }

    public function endCall(CallRequest $request)
    {
        $result = $this->call->endCall($request);

        return response()->json($result, 200);
    }


    public function newConference(NewConferenceRequest $request)
    {
        $this->agora->createConference($request);

        return response()->json($this->agora->getLastConference(Auth::user()), 200);
    }

    public function endStream(StreamRequest $request)
    {
        $steam = MessengerStream::where('id', '=', $request->stream_id)
            ->first();

    }

    public function newCall(AgoraCallRequest $request)
    {
        $userToCall = User::where('id', '=', $request->to_id)
            ->first();
        $checkUserBlock = $this->call->checkBlockUser($userToCall);
        if ($checkUserBlock == true) {
            return response()->json('Block user', 200);
        }
        $users = User::where('id', '=', $userToCall->id)
            ->where('id', '!=', Auth::user()->id)
            ->get();
        $contactUser = User::where('id', '=', $request->to_id)
            ->first();
        $channel = $this->generateRandomString();

        $resultToken = $this->token($request, $channel);
        $result = $this->call->newLogCall($request);
        $nickname = $this->contact->getNicknameContactInterlocutor($contactUser, Auth::user()) == null ? Auth::user()->name . ' ' . Auth::user()->surname : $this->contact->getNicknameContactInterlocutor($contactUser, Auth::user());

        Auth::user()->avatar == null ? $avatar = 'https://api.aiocorp.kz/messenger/1.png' : $avatar = URL::to('/') . '/' . Auth::user()->avatar;
//        \Notification::send($users, new NewCall($nickname, $avatar, $resultToken, $channel, $request->type));
        broadcast(new MainCallEvent($request->user(), $result, $resultToken, $userToCall, $channel));
//        \Log::error('data show'. json_encode( [
//                'resultToken' => $resultToken,
//                'channel' => $channel,
//                'userAvatar' => $avatar,
//                'nickname' => $nickname,
//                'type_call' => $result->type_call
//            ]));
        foreach ($users as $user) {
            FirebaseService::send($user->deviceTokens(),[
                'type_call' => $result->type_call,
                'resultToken' => $resultToken,
                'channel' => $channel,
                'userAvatar' => $avatar,
                'nickname' => $nickname,
            ], 'incoming call', $nickname);
        }

        return response()->json(['call' => $result,
            'token' => $resultToken,
            'channelName' => $channel], 200);
    }

    public function makeCallConference(Request $request)
    {
        $channel = $this->generateRandomString();

        $resultToken = $this->token($request, $channel);
        $userIds = MessengerChat::find($request->chat_id)->member()->pluck('user_id')->toArray();

        $users = User::whereIn('id', $userIds)->whereHas('oneSignal', function ($query) {
            $query->where('user_id', '!=', Auth::user()->id);
        })
            ->get();
        MessengerChat::find($request->chat_id)->name == null ? $avatar = 'https://api.aiocorp.kz/messenger/1.png' : $avatar = URL::to('/') . '/' . MessengerChat::find($request->chat_id)->name;

        \Notification::send($users, new NewCall(MessengerChat::find($request->chat_id)->name, $avatar, $resultToken, $channel, $request->type));
        foreach ($users as $user) {
            \Log::error('messages ' . json_encode($user->deviceTokens()));
            FirebaseService::send($user->deviceTokens(), [
                'resultToken' => $resultToken,
                'channel' => $channel,
                'userAvatar' => $avatar,
                'nickname' => MessengerChat::find($request->chat_id)->name,
                'type_call' => $request->type
            ], 'incoming call', MessengerChat::find($request->chat_id)->name);
        }
        return response()->json([
            'token' => $resultToken,
            'channelName' => $channel], 200);
    }

    public function generateRandomString()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 10; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
