<?php

namespace App\Http\Controllers\Messenger;

use App\Events\getIndexChat;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryChatRequest;
use App\Http\Requests\UpdateCategoryChatRequest;
use App\Models\Messenger\MessengerCategoryChat;
use App\Services\ChatService;
use App\Services\MessageService;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ChatCategoryController extends Controller
{
    public $chat;
    public $message;

    public function __construct(ChatService $chat,MessageService $message)
    {
        $this->message = $message;
        $this->chat = $chat;
    }

    public function index(){
        $result = $this->chat->getUserChatCategory();

        return response()->json($result, 200);
    }

    public function create(CreateCategoryChatRequest $request){
        $result = $this->chat->createCategory($request);
        return response()->json($result, 200);
    }

    public function show(MessengerCategoryChat $category,Request $request){

        return response()->json(
            $this->chat->showCategoryChat($category,$request->last_chat_id,$request->limit)
        , 200);
    }

    public function update(UpdateCategoryChatRequest $request,MessengerCategoryChat $category){
        $this->chat->updateCategory($request,$category);

        return response()->json($this->chat->getUserChatCategory(), 200);
    }

    public function delete(MessengerCategoryChat $category){
        $this->chat->deleteCategoryChat($category);

        return response()->json($this->chat->getUserChatCategory(), 200);

    }

    public function reorder(Request $request)
    {
        $request->validate([
            'orders' => 'required|array'
        ]);

        $user = \Auth::user();

        try {
            $this->chat->reorderUserChatCategories($request['orders'], $user);
        } catch (\Exception $exception) {
            throw $exception;
        }

        $categories = $user->messengerCategories()
            ->withPivot(['order'])
            ->orderBy('pivot_order', 'asc')
            ->get();

        $categories->map(function ($chat){
            $chat->no_read_message = $this->message->getUserCategoryNotReadMessage($chat);
        });
        return response()->json(['categories' => $categories], 200);

    }

}
