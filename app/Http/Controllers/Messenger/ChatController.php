<?php

namespace App\Http\Controllers\Messenger;

use App\Events\EditedMessage;
use App\Events\getIndexChat;
use App\Events\NewMessage;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddContactRequests;
use App\Http\Requests\ChatRequest;
use App\Http\Requests\CreateChatRequest;
use App\Http\Requests\MessengerAdRequest;
use App\Http\Requests\TimeSecretChatRequest;
use App\Http\Requests\TransferChatCategoryRequest;
use App\Http\Requests\UpdateChat;
use App\Http\Requests\UpdateSettingsChat;
use App\Http\Requests\UserChatRequest;
use App\Models\City;
use App\Models\Interest;
use App\Models\Messenger\MessengerAdChat;
use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\MessengerChatSettings;
use App\Models\Messenger\MessengerTariffChat;
use App\Models\MessengerAdChatInterest;
use App\Services\ChatService;
use App\Services\MessageService;
use App\Services\SettingsService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class ChatController extends Controller
{
    public $chat;
    public $settings;
    public $message;

    public function __construct(ChatService $chat, SettingsService $settings, MessageService $message)
    {
        $this->chat = $chat;
        $this->message = $message;
        $this->settings = $settings;
    }

    public function show(MessengerChat $chat)
    {
        $current_chat = $this->chat->showChat($chat);
        $memberInChat = $this->chat->getMembersChat($chat);
        if ($chat->is_private == 1) {
            $user = $this->chat->getUserInPrivateChat($chat);
        }
        return response()->json([
            'ad' => $this->chat->getStatisticAdChat($chat),
            'chat' => $current_chat,
            'media' => $this->chat->getMediaChat($chat),
            'settings' => $this->settings->getSettingsChat($chat),
            'count_member' => $memberInChat->count(),
            'role' => $this->chat->getRoleUserChat($chat, Auth::user()),
            'user_in_private_chat' => $user ?? null,
            'members' => $memberInChat,
        ], 200);
    }

    public function create(CreateChatRequest $request)
    {
        $result = $this->chat->create($request);
        return response()->json($result, 200);
    }

    public function leaveFromChat(MessengerChat $chat)
    {
        $this->chat->leaveChat($chat);
        $this->message->newAction($chat, 'LeaveUserChat', null);
        $this->chat->callGetIndexChannel($chat, "LeaveUserChat");
        $message = $this->message->getLastMessageChat($chat->id);
        $this->message->callNewMessageEvent($message, 'NewMessage');
        return $this->show($chat);
    }

    public function updateSettingsChat(MessengerChat $chat, UpdateSettingsChat $request)
    {
        $this->settings->updateSettingsChat($chat, $request);
        $this->chat->callGetIndexChannel($chat, "UpdateSettingsChat");
        return response()->json($this->chat->getSettingsChat($chat), 200);
    }

    public function update(MessengerChat $chat, UpdateChat $request)
    {
        $checkAdmin = $this->chat->checkUserAdminRoleChat($chat);
        if (!$checkAdmin) {
            return response()->json('User not Admin Chat', 401);
        } else {
            $this->chat->updateChat($chat, $request);
            $message = $this->message->getLastMessageChat($chat->id);
            if ($message != null) {
                $this->chat->callGetIndexChannel($chat, "UpdateChat");
            }
            return $this->show($chat);
        }
    }


    public function showFullMembers(MessengerChat $chat, Request $request)
    {
        $members = $this->chat->showMembersChat($chat, $request);

        return response()->json($members, 200);
    }

    public function addContactToChat(MessengerChat $chat, AddContactRequests $request)
    {
        $this->chat->addContactToChat($chat, $request);
        $this->chat->callGetIndexChannel($chat, "AddContactChat");
        $message = $this->message->getLastMessageChat($chat->id);
        $this->message->callNewMessageEvent($message, 'NewMessage');
        return $this->show($chat);
    }

    public function deleteContactFromChat(MessengerChat $chat, AddContactRequests $request)
    {
        $this->chat->deleteContactFromChat($chat, $request);
        $this->chat->callGetIndexChannel($chat, "DeleteContactChat");
        $message = $this->message->getLastMessageChat($chat->id);
        $this->message->callNewMessageEvent($message, 'NewMessage');
        return $this->show($chat);
    }


    public function getIndex()
    {
        $category = $this->chat->getUserChatCategory();
        $chats = $this->chat->getAllUserChat();
        $settingsUser = $this->settings->getSettingUser();

        return response()->json([
            'ad_chat' => $this->chat->getAdChat(),
            'category' => $category,
            'chat' => $chats,
            'settings' => $settingsUser,
            'private_chats' => $this->chat->getChatByPrivateType(1),
            'not_private_chats' => $this->chat->getChatByPrivateType(0),
        ], 200);
    }

    public function getUserChat(Request $request)
    {
        $request->validate([
            'last_chat_id' => 'nullable|numeric|exists:messenger_chats,id',
            'limit' => 'nullable|numeric',
            'type' => ''
        ]);

        $response = $this->chat->getAllUserChatWithPaginate($request['last_chat_id'], $request['limit'], $request['type']);
        return response()->json([
            'chats' => $response['chats'],
            'has_more_results' => $response['hasMoreResults'],
        ], 200);
    }


    public function transferChatUserToOtherCategory(TransferChatCategoryRequest $request)
    {
        $chats = explode(',', $request->chats);
        foreach ($chats as $chat) {
            $currentChat = MessengerChat::where('id', '=', $chat)
                ->first();
            $this->chat->transferChatUserToOtherCategory($chat, $request->new_category);
            if (!empty($currentChat->name)) {
                $this->chat->callGetIndexChannel($currentChat, "TransferChatToCategory",);
            }
        }
        $this->getUserChat($request);

        return $this->getIndex();
    }

    public function joinToChat(MessengerChat $chat)
    {
        $this->chat->joinToChat($chat);
        $this->message->newAction($chat, 'UserJoinChat', null);
        $this->chat->callGetIndexChannel($chat, "UserJoinChat");
        $message = $this->message->getLastMessageChat($chat->id);
        $this->message->callNewMessageEvent($message, 'NewMessage');
        return $this->show($chat);
    }

    public function addTimeToSecretChat(TimeSecretChatRequest $request)
    {
        $chat = MessengerChat::where('id', '=', $request->chat_id)
            ->first();
        if ($request->time_deleted == 0 || $request->time_deleted == null) {
            $result = $this->chat->actionTimeToSecretChat($request, $chat, 'UnsetTimeSecret');
        } else {
            $result = $this->chat->actionTimeToSecretChat($request, $chat, 'SetTimeSecret');
        }

        // event(new NewMessage($result,'AddTimeSecret'));
        $this->chat->callGetIndexChannel($chat, "TimeSecret");
        return $this->getIndex();
    }


    public function lastChat()
    {
        return $this->chat->getUserLastChat(Auth::user());
    }

    public function addAdminRoleChat(UserChatRequest $request)
    {
        $chat = MessengerChat::where('id', '=', $request->chat_id)
            ->first();
        $this->chat->addAdminUser($chat, $request);
        return $this->show($chat);
    }

    public function deleteAdminRoleChat(UserChatRequest $request)
    {
        $chat = MessengerChat::where('id', '=', $request->chat_id)
            ->first();
        $this->chat->deleteAdminUser($chat, $request);
        return $this->show($chat);
    }

    public function getAdminChats()
    {
        return response()->json([
            'chats' => $this->chat->getUserPublicChatAdmin(Auth::user()),
        ], 200);
    }


    public function newAdChat(ChatRequest $request)
    {

        $user = Auth::user();
//        if ($user->balance < $request->total_price) {
//            return response()->json([
//                ['error' => 'Нехватка средств',
//                    'message' => 'Failure'
//                ]
//            ], 405);
//        } else {
//            $user->decrement('balance', $request->total_price);
//        }
        $ad = MessengerAdChat::firstOrCreate(
            [
                'tariff_id' => $request->tarrif_id ?? 1,
                'to_period_date' => Carbon::now()->add(31, 'day'),
                'total_views' => $request->total_views,
                'total_price' => $request->total_price,
            ]
        );
        $cities = explode(',', $request->city_id);
        foreach ($cities as $city) {
            $checkCity = City::where('id', '=', $city)
                ->first();
            if (!empty($checkCity->name)) {
                MessengerTariffChat::firstOrCreate(
                    [
                        'ad_id' => $ad->id,
                        'city_id' => $city,
                        'chat_id' => $request->chat_id,
                    ]
                );
            }

        }
        $interests = explode(',', $request->interests);

        foreach ($interests as $interest)
            MessengerAdChatInterest::create([
                'messenger_ad_chat_id' => $ad->id,
                'interest_id' => $interest
            ]);
        return response()->json([
            ['ad' => $ad,
                'message' => 'Success'
            ]
        ], 200);
    }

    public function getMoney($totalPrice)
    {
        $from = Carbon::now()->format('Y-m-d');
        $to = Carbon::parse($totalPrice->to_period_date);
        $days = $to->diffInDays($from);
        $coefficient = 31 / $days;
        $money = $totalPrice->total_price * $coefficient;
        return $money;
    }


    public function stopAdChat(MessengerAdRequest $request)
    {
        $totalPrice = MessengerAdChat::where('id', '=', $request->ad_id)
            ->first();
        $money = $this->getMoney($totalPrice);
        DB::transaction(function () use ($money) {
            $user = Auth::user();
            $user->increment('balance', $money);
        });
        MessengerAdChat::where('id', '=', $request->ad_id)
            ->delete();
        return response()->json([
            'ad stop'
        ], 200);
    }

    public function getCurrentAdChat(ChatRequest $request)
    {
        $chat = MessengerChat::where('id', '=', $request->chat_id)
            ->first();
        $ad = MessengerAdChat::whereHas('ad', function ($query) use ($chat) {
            $query->where('chat_id', '=', $chat->id);
        })
            ->first();
        return response()->json([
            'data' => MessengerAdChat::with('ad.city')
                ->with('tariff')
                ->whereHas('ad', function ($query) use ($request) {
                    $query->where('chat_id', '=', $request->chat_id);
                })
                ->first(),
            'statistic' => $this->chat->getStatisticAdChat($chat),
            'budget' => $this->getMoney($ad),
        ], 200);

    }
}
