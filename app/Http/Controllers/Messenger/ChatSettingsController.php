<?php

namespace App\Http\Controllers\Messenger;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddContactRequests;
use App\Models\Messenger\MessengerChat;
use App\Models\User;
use App\Services\CallService;
use App\Services\ChatSettingsService;
use Illuminate\Http\Request;

class ChatSettingsController extends Controller
{
    private $chatSettingsService;
    public $user;

    public function __construct(ChatSettingsService $chatSettingsService,CallService $user)
    {
        $this->chatSettingsService = $chatSettingsService;
        $this->user = $user;
    }

    // members
    public function switchSound(Request $request, MessengerChat $chat)
    {
        $request->validate([
            'sound' => 'required|boolean'
        ]);

        $userId = \Auth::id();

        $chatSettings = $this->chatSettingsService->switchSound($chat, $userId, $request['sound']);

        return response()->json(['chat_settings' => $chatSettings], 200);
    }

    public function switchVibration(Request $request, MessengerChat $chat)
    {
        $request->validate([
            'vibration' => 'required|boolean'
        ]);

        $userId = \Auth::id();

        $chatSettings = $this->chatSettingsService->switchVibration($chat, $userId, $request['vibration']);

        return response()->json(['chat_settings' => $chatSettings], 200);
    }

    public function blockUser(AddContactRequests $request){
        $contacts = explode ( ',',$request->contact);
        foreach ($contacts as $user){
            $blockUser = User::where('id','=',$user)
                ->first();
            if (!empty($blockUser->name)){
                $this->user->addBlockUser($blockUser);
            }
        }

        return response()->json($this->user->getBlockUserList($request), 200);
    }

    public function unblockUser(AddContactRequests $request){
        $contacts = explode ( ',',$request->contact);
        foreach ($contacts as $user){
            $blockUser = User::where('id','=',$user)
                ->first();
            if (!empty($blockUser->name)){
                $this->user->deleteBlockUser($blockUser);
            }
        }
        return response()->json($this->user->getBlockUserList($request), 200);
    }
}
