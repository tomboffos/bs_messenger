<?php

namespace App\Http\Controllers\Messenger;

use App\Http\Controllers\Controller;
use App\Models\Messenger\MessengerContactUser;
use App\Models\Messenger\UserContactPhone;
use App\Models\User;
use App\Services\SearchService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;


class ContactController extends Controller
{
    public $contact;

    public function __construct(SearchService $contact)
    {
        $this->contact = $contact;
    }

    public function searchContact(Request $request)
    {
        $result = $this->contact->searchUserByPhoneOrName($request);
        return response()->json($result, 200);
    }

    public function bindContacts(Request $request)
    {
        $request->validate([
            'contacts' => 'required|file',
        ]);

        $user = \Auth::user();

        $file = json_decode(file_get_contents($request['contacts']), true);

        $fileContents = ['contacts' => $file];
        Validator::validate($fileContents, [
            'contacts' => 'required|array',
            'contacts.*.middleName' => 'nullable|string',
            'contacts.*.displayName' => 'nullable|string',
            'contacts.*.phones' => 'present|array',
            'contacts.*.phones.*.mobile' => 'nullable|string'
        ]);

        $contacts = $fileContents['contacts'];

        foreach ($contacts as $contact) {
            if (!empty($contact['phones'])) {

                $phone = isset($contact['phones'][0]['other']) ? str_replace([" ", "-", "(", ")"], ["", "", "", ""], $contact['phones'][0]['other']) : str_replace([" ", "-", "(", ")"], ["", "", "", ""], $contact['phones'][0]['mobile']);
                if (isset($phone[0]) && $phone[0] == '8')
                    $phone = substr_replace($phone, "+7", 0, 1);
                UserContactPhone::updateOrCreate([
                    'user_id' => $user->id,
                    'phone' => $phone
                ], [
                    'nickname' => $contact['displayName'] ?? null,
                    'phone' => $phone
                ]);

                $contactUser = User::where('phone', $phone)->first();
                if ($contactUser) {
                    if ($contactUser) {
                        $checkExistContact = MessengerContactUser::where('user_id', '=', $user->id)
                            ->where('contact_id', '=', $contactUser->id)
                            ->first();
                        if (!empty($checkExistContact->user_id)) {
                            MessengerContactUser::where('user_id', '=', $user->id)
                                ->where('contact_id', '=', $contactUser->id)
                                ->update([
                                    'nickname' => $contact['displayName'] ?? null,
                                ]);

                        } else {
                            MessengerContactUser::create([
                                'user_id' => $user->id,
                                'contact_id' => $contactUser->id,
                                'nickname' => $contact['displayName'] ?? null,
                            ]);
                        }
                    }
                }
            }
        }

        return response()->json([], 200);
    }

    public function parseUserContact(Request $request)
    {

        return response()->json($this->contact->getUserContact($request), 200);
    }

    public function getUserContact(Request $request)
    {

        $result = $this->contact->getUserContact($request);

        return response()->json($result, 200);
    }
}
