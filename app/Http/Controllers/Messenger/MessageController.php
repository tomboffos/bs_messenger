<?php

namespace App\Http\Controllers\Messenger;

use App\Events\DeletedMessageEvent;

use App\Events\NewMessage;
use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteMessageRequest;
use App\Http\Requests\MessengerReadMessageRequest;
use App\Http\Requests\SearchMessageRequest;
use App\Http\Requests\SendMessageRequest;
use App\Http\Requests\SetTopMessageRequest;
use App\Http\Requests\UpdateMessageTextRequest;
use App\Jobs\DeleteSecretMessageJob;
use App\Jobs\MessengerAddNotReadMessage;
use App\Jobs\MessengerDeleteSecretMessage;
use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\MessengerChatMember;
use App\Models\Messenger\MessengerChatSettings;
use App\Models\Messenger\MessengerChatUserSettings;
use App\Models\Messenger\MessengerMessage;
use App\Models\User;
use App\Notifications\Messenger\MicrophoneNewMessage;
use App\Notifications\Messenger\NewMessage as NewMessageNotification;
use App\Notifications\Messenger\SuperworkNewMessage;
use App\Services\CallService;
use App\Services\ChatService;
use App\Services\FirebaseService;
use App\Services\MessageService;
use App\Services\NotificationService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\SearchService;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\Rule;


class MessageController extends Controller
{
    public $message;
    public $chat;
    public $search;
    public $call;
    public $notification;

    public function __construct(MessageService $message, ChatService $chat, SearchService $search, CallService $call, NotificationService $notification)
    {
        $this->message = $message;
        $this->chat = $chat;
        $this->search = $search;
        $this->call = $call;
        $this->notification = $notification;
    }

    public function getChatMessage(Request $request, MessengerChat $chat)
    {
        $request->validate([
            'last_message_id' => 'nullable|numeric|exists:messenger_messages,id',
            'limit' => 'nullable|numeric',
            'type' => ['', 'string', Rule::in(['top', 'bot'])]
        ]);


        $memberInChat = $this->chat->getMembersChat($chat);
        $topMessage = $this->message->getTopMessage($chat);
        $chat = $this->chat->showChat($chat);
        $this->chat->incrementView($chat);
        $response = $this->message->getMessageChat(
            $chat,
            $request['last_message_id'],
            $request['limit'],
            $request['type'] ?? 'top'
        );

        return response()->json([
            'top_message' => $topMessage,
            'chat' => $chat,
            'count_member' => $memberInChat->count(),
            'messages' => $response['messages'],
            'hasMoreResults' => $response['hasMoreResults']
        ]);
    }

    public function getChatMessagesBySearch(Request $request, MessengerChat $chat, MessengerMessage $message)
    {
        $topMessage = $this->message->getTopMessage($chat);
        $chat = $this->chat->showChat($chat);

        $response = $this->message->getChatMessagesBySearch($chat, $message);

        return response()->json([
            'top_message' => $topMessage,
            'chat' => $chat,
            'messages' => $response['messages'],
            'has_more_results_top' => $response['hasMoreResultsTop'],
            'has_more_results_bot' => $response['hasMoreResultsBot'],
        ]);

    }

    public function sendMessage(SendMessageRequest $request, MessengerChat $chat = null)
    {
        if ($chat != null) {
            MessengerChatMember::where('chat_id', '=', $chat->id)
                ->update(
                    [
                        'show_chat' => 1,
                    ]
                );

            $checkUserInChat = MessengerChatMember::where('chat_id', '=', $chat->id)
                ->where('user_id', '=', Auth::user()->id)
                ->first();

            if (empty($checkUserInChat->user_id)) {
                MessengerChatMember::firstOrCreate(
                    [
                        'chat_id' => $chat->id,
                        'user_id' => Auth::user()->id,
                        'role' => 'member',
                        'show_chat' => 1
                    ]
                );
            }
            $settings = MessengerChatSettings::where('chat_id', '=', $chat->id)
                ->first();

            if (($settings->admin_message_send == 1) && ($this->chat->checkUserAdminRoleChat($chat) == false)) {
                return response()->json('User not Admin', 403);
            }
            if (($settings->admin_media_send == 1) && ($this->chat->checkUserAdminRoleChat($chat) == false) && !empty($request->file)) {
                return response()->json('User not Admin', 403);
            }
        }


        if (empty($request->contact_id) && empty($request->text) && empty($request->file) && empty($request->forward) && (empty($request->get('latitude')) && empty($request->get('longitude')))) {
            return response()->json('Clear message', 403);
        }

        if (empty($request->forward)) {
            if (($this->call->checkBlockUserInPrivateChat($chat) == true) && ($chat->is_private == 1)) {
                return response()->json('Block user', 403);
            }
        }


        if (isset($request->chats)) {
            $chats = explode(',', $request->chats);
            foreach ($chats as $chat) {
                $transferChat = MessengerChat::where('id', '=', $chat)
                    ->first();
                if (!empty($transferChat->name)) {
                    $message = $this->bodySendMessage($request, $transferChat);
                }
            }
        } else {
            $message = $this->bodySendMessage($request, $chat);
        }

        return response()->json($message);
    }

    public function setTopMessage(SetTopMessageRequest $request, MessengerChat $chat)
    {
        $checkAdmin = $this->chat->checkUserAdminRoleChat($chat);
        if (!$checkAdmin) {
            return response()->json('User not Admin Chat', 401);
        } else {
            $result = $this->message->setTopMessage($request, $chat);
            $this->message->callNewMessageEvent($result['message'], 'SetTopMessage');
            return $this->getChatMessage($request, $result['chat']);
        }
    }

    public function unsetTopMessage(Request $request, MessengerChat $chat)
    {
        $checkAdmin = $this->chat->checkUserAdminRoleChat($chat);
        if (!$checkAdmin) {
            return response()->json('User not Admin Chat', 401);
        } else {
            $this->message->unsetTopMessage($chat);
            $message = $this->message->getMessageTopChat($chat);
            $this->message->callNewMessageEvent($message, 'unSetTopMessage');
            return $this->getChatMessage($request, $chat);
        }
    }


    public function updateMessage(MessengerMessage $message, UpdateMessageTextRequest $request)
    {
        $this->message->update($message, $request);
        $chat = MessengerChat::where('id', '=', $message->chat_id)
            ->first();
        $this->message->callNewMessageEvent($message, 'updateMessage');
        return response()->json([
            'top_message' => $this->message->getTopMessage($chat),
            'messages' => $this->message->getMessageChat($chat, null, null, 'top'),
        ], 200);
    }

    public function deleteChatMessage(DeleteMessageRequest $request)
    {

        $messages = explode(',', $request->messages);
        $deletedMessage = collect();
        $lastMessageChat = collect();
        foreach ($messages as $message) {
            $messengerMessage = MessengerMessage::where('id', '=', $message)
                ->first();

            $lastMessage = $this->message->getLastMessageChat($messengerMessage->chat_id);
            $lastMessageChat->push($lastMessage->id);
            $deletedMessage->push($messengerMessage->id);


            //$this->message->callNewMessageEvent($messengerMessage,'deleteMessage');

            if (intval($request->for_me) == 1) {
                $this->message->safeDelete($messengerMessage, Auth::user());
            } else {
                $this->message->deleteSecretChat($messengerMessage);
            }
        }
        if (intval($request->for_me) == 1) {
            broadcast(new DeletedMessageEvent($deletedMessage, 'deleteMessageSelf', $request->chat_id, Auth::user()->id));
        } else {
            broadcast(new DeletedMessageEvent($deletedMessage, 'deleteMessage', $request->chat_id, null));
        }


        $chat = MessengerChat::where('id', '=', $request->chat_id)
            ->first();

        return response()->json([
            'top_message' => $this->message->getTopMessage($chat),
            'messages' => $this->message->getMessageChat($chat, null, null, 'top'),
        ], 200);
    }

    public function searchMessageAllChat(SearchMessageRequest $request)
    {
        $result = $this->search->searchMessageAllChat($request);

        return response()->json([
            'messages' => $result,
            'chats' => strlen($request->search) >= 3 ? $this->search->searchNotPrivateGroupChat($request) : []
        ], 200);
    }

    public function searchMessageInChat(SearchMessageRequest $request)
    {
        $result = $this->search->searchMessage($request);

        return response()->json($result, 200);
    }

    public function bodySendMessage($request, $chat)
    {
        $message = $this->message->sendMessage($request, $chat);
        $this->message->callNewMessageEvent($message, 'NewMessage');

        $userIds = $chat->member()->pluck('user_id')->toArray();

        $users = User::whereIn('id', $userIds)
            ->where('id', '!=', Auth::user()->id)
            ->get();

        $usersCall = User::whereIn('id', $userIds)
            ->get();

        foreach ($usersCall as $userEvent) {
            if ($chat->is_private == 1) {
                $contact = $this->chat->getMessengerPrivateChatMemberByUser($chat, $userEvent);
                if (!empty($contact->user_id)) {
                    $contactUser = User::where('id', '=', $contact->user_id)
                        ->first();
                    $chat->name = $this->search->getNicknameContactInterlocutor($userEvent, $contactUser) == null ? $contactUser->name : $this->search->getNicknameContactInterlocutor($userEvent, $contactUser);
                    $chat->full_link = URL::to('/') . '/' . $contactUser->avatar;
                } else {
                    $chat->full_link = null;
                }
            }
            $this->chat->callGetIndexChannel($chat, "NewMessage");
            break;
        }

        $currentUser = \Auth::user();
        MessengerAddNotReadMessage::dispatch($message, $users);
        $currentChat = MessengerChat::where('id', '=', $chat->id)
            ->first();
        $currentChat->avatar == null ? $avatar = 'https://api.aiocorp.kz/messenger/1.png' : $avatar = URL::to('/') . '/' . $currentChat->avatar;
        $text = !empty($message->text) ? $message->text : 'вложение';

        $chatSettings = MessengerChatSettings::where('chat_id', '=', $chat->id)
            ->first();
        if ($chatSettings->sound == 1) {
            foreach ($users as $user)
                FirebaseService::send($user->deviceTokens(), [
                    'ok' => 'ok'
                ], $currentUser->name, $text);
        }
        // \Notification::send($users, new SuperworkNewMessage($currentUser, $text,$avatar ));
        // \Notification::send($users, new MicrophoneNewMessage($currentUser, $text,$avatar ));

        //$this->notification->sendMessage($currentUser, $text,$avatar,$chat);
        return $message;
    }


    public function readMessage(MessengerReadMessageRequest $request)
    {
        MessengerMessage::where('id', '=', $request->message_id)
            ->update(
                [
                    'is_read' => 1
                ]
            );
        $message = MessengerMessage::where('id', '=', $request->message_id)
            ->first();

        $chat = MessengerChat::where('id', '=', $message->chat_id)
            ->first();

        if ($chat->is_private == 1) {
            $contact = $this->chat->getMessengerPrivateChatMember($chat);
            if (!empty($contact->user_id)) {
                $user = User::where('id', '=', $contact->user_id)
                    ->first();
                if (!empty($user->phone)) {
                    $chat->name = $this->search->getNicknameContact($user) == null ? $user->name : $this->search->getNicknameContact($user);
                    $chat->full_link = URL::to('/') . '/' . $user->avatar;
                }
            } else {
                $chat->full_link = null;
            }
        } else {
            $chat->full_link = URL::to('/') . '/' . $chat->avatar;
        }

        if ($message->time_deleted != null) {
            $this->message->callNewMessageEvent($message, 'StartTimerSecretMessage');
        } else {
            $this->message->callNewMessageEvent($message, 'MessageRead');
            $this->chat->callGetIndexChannel($chat, "MessageRead");
        }

        return response()->json($message, 200);
    }

}
