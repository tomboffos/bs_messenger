<?php

namespace App\Http\Controllers\Messenger;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use App\Models\Messenger\MessengerChat;
use App\Models\User;
use App\Services\CallService;
use App\Services\ChatService;
use App\Services\UserService;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class ProfileController extends Controller
{
    public $chat;
    public $block;
    public $user;
    public function __construct(ChatService $chat,CallService $block,UserService $user){
        $this->chat = $chat;
        $this->block = $block;
        $this->user = $user;
    }

    public function getUser(ProfileRequest $request)
    {
        $user = User::where('id','=',$request->user_id)
            ->select('id','name','surname','phone','avatar','status','youtube','whatsapp','instagram','facebook','site','email')
            ->first();


        $user->full_link = URL::to('/').'/'.$user->avatar;
        $block = $this->block->checkBlockUserByProfile($user);
        $user->isBlocked = $block == true ? 1 : 0;
        $user->isOnline = $this->user->checkOnlineUser($user);
        $chat = MessengerChat::where('is_private','=',1)
            ->wherehas('member',function ($query) use ($request){
                $query->where('user_id','=', $request->user_id)
                    ->orWhere('user_id','=', Auth::user()->user_id);
            })
            ->first();

        if (!empty($chat->name)){
          $media =   $this->chat->getMediaChat($chat);
        }

        $equal = MessengerChat::wherehas('member',function ($query) use ($request){
                $query->where('user_id','=',$request->user_id);
                $query->where('show_chat','=',1);
            })
            ->wherehas('member',function ($query) use ($request){
                $query->where('user_id','=',Auth::user()->id);
                $query->where('show_chat','=',1);
            })
            ->get();
        $equal->map(function ($chat){
            $chat->full_link = URL::to('/').'/'.$chat->avatar;
        });
        return response()->json(['user' => $user,'chat' => empty($chat->name) ?  null : $this->chat->showChat($chat) ,'equal_group' => $equal,'media' => isset($media) ? $media : null], 200);
    }
}
