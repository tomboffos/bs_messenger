<?php

namespace App\Http\Controllers\Messenger;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChatRequest;
use App\Http\Requests\MessageRequest;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Messenger\ReportGroup;
use App\Models\Messenger\ReportMessage;
use App\Models\Messenger\MessengerMessage;
use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\ReportUser;


class ReportController extends Controller
{
    public function reportChat(Chatrequest $request){
        $chat = MessengerChat::where('id','=',$request->chat_id)
            ->first();
        ReportGroup::firstOrCreate(
            [
                'chat_id' => $chat->id,
                'comment' => $request->comment
            ]
        );
        return response()->json('report was created', 200);
    }

    public function reportMessage(MessageRequest $request){
        $message = MessengerMessage::where('id','=',$request->message_id)
            ->first();
        ReportMessage::firstOrCreate(
            [
                'message_id' => $message->id,
                'comment' => $request->comment
            ]
        );
        return response()->json('report was created', 200);
    }

    public function reportUser(UserRequest $request){
        $user = User::where('id','=',$request->user_id)
            ->first();
        ReportUser::firstOrCreate(
            [
                'user_id' => $user->id,
                'comment' => $request->comment
            ]
        );
        return response()->json('report was created', 200);
    }
}
