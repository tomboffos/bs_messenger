<?php

namespace App\Http\Controllers\Messenger;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlockUserRequest;
use App\Http\Requests\UpdateUserSettingsRequest;
use App\Models\Tariff;
use App\Models\User;
use App\Services\AgoraService;
use App\Services\CallService;
use App\Services\ChatService;
use App\Services\SettingsService;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public $settings;

    public $call;

    public function __construct(SettingsService $settings,CallService $call)
    {

        $this->settings = $settings;
        $this->call  = $call;
    }

    public function getSettings(){
        $result = $this->settings->getSettingUser();

        return response()->json($result, 200);
    }

    public function updateSettings(UpdateUserSettingsRequest $request){
        $this->settings->updateSettingUser($request);

        return response()->json($this->settings->getSettingUser(), 200);
    }

    public function blockUser(BlockUserRequest $request){
        $user = User::where('id','=',$request->user)
            ->first();
        $this->call->addBlockUser($user);

        return response()->json($this->call->getBlockUserList($request), 200);
    }

    public function unblockUser(BlockUserRequest $request){
        $user = User::where('id','=',$request->user)
            ->first();
        $this->call->deleteBlockUser($user);

        return response()->json($this->call->getBlockUserList($request), 200);
    }

    public function getTariff(){
        return response()->json(Tariff::where('application_id','=',3)
            ->get(), 200);
    }

}
