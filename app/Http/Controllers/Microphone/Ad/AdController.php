<?php

namespace App\Http\Controllers\Microphone\Ad;

use App\Exceptions\SimpleException;
use App\Jobs\MicrophoneAdJob;
use App\Models\Microphone\Ad;
use App\Http\Controllers\Controller;
use App\Models\Microphone\Review;
use App\Models\Tariff;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


class AdController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Ad::with('image')
            ->where('expiration_date_time', '>=', now());

        if ($request['city_id']) {
            $data = $data->where('city_id', $request['city_id']);
        }
        if ($request['sort_by'] && $request['sort_type']) {
            switch ($request['sort_type']) {
                case 'asc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderBy('created_at');
                            break;
                        case 'rating':
                            $data = $data->orderBy('rating');
                            break;
                        case 'views':
                            $data = $data->orderBy('views');
                            break;
                        case 'calls':
                            $data = $data->orderBy('calls');
                            break;
                        default:
                            break;
                    }
                    break;
                case 'desc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByDesc('created_at');
                            break;
                        case 'rating':
                            $data = $data->orderByDesc('rating');
                            break;
                        case 'views':
                            $data = $data->orderByDesc('views');
                            break;
                        case 'calls':
                            $data = $data->orderByDesc('calls');
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->get();

        return $data;
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        if ($request['phone']) {
            $phoneUtil = PhoneNumberUtil::getInstance();
            $numberPrototype = $phoneUtil->parse($request->phone, 'RU');
            $phone = $phoneUtil->format($numberPrototype, PhoneNumberFormat::E164);
            if (mb_strlen($phone) >= 12 && mb_strlen($phone) <= 16) {
                $request['phone'] = $phone;
            } else {
                return response()->json([
                    'type' => 'validation_error',
                    'errors' => [
                        'phone' => [
                            'Phone number is not valid'
                        ]
                    ]
                ], 422);
//                return response()->json([
//                    'message' => 'Phone number is not valid',
//                ], 401);
            }
        }
        $data = Ad::create($request->except('rating'));

        return $data;
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        $data = Ad::findOrFail($request['id']);

        if ((!checkPermissions($user, ['Доступ администратора к базе'])) &&
            !(checkPermissions($user, ['Создание объявлений, услуг, медиафайлов']) && $user['id'] == $data['creator_id'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        if ($request['phone']) {
            $phoneUtil = PhoneNumberUtil::getInstance();
            $numberPrototype = $phoneUtil->parse($request->phone, 'RU');
            $phone = $phoneUtil->format($numberPrototype, PhoneNumberFormat::E164);
            if (mb_strlen($phone) >= 12 && mb_strlen($phone) <= 16) {
                $request['phone'] = $phone;
            } else {
                return response()->json([
                    'type' => 'validation_error',
                    'errors' => [
                        'phone' => [
                            'Phone number is not valid'
                        ]
                    ]
                ], 422);
//                return response()->json([
//                    'message' => 'Phone number is not valid',
//                ], 401);
            }
        }

        if ($data) {
            if($request['media_ids']) {
                foreach ($request['media_ids'] as $item) {
                    if ($data->media->contains($item) != 1) {
                        $data->media()->attach($item);
                    }
                }
            }

            if($request['product_ids']) {
                foreach ($request['product_ids'] as $item) {
                    if ($data->products->contains($item) != 1) {
                        $data->products()->attach($item);
                    }
                }
            }

            $data->update($request->except('rating'));
            $data = 'Success';
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }
        return $data;
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        $record = Ad::find($request['id']);

        if ((!checkPermissions($user, ['Доступ администратора к базе'])) &&
            !(checkPermissions($user, ['Создание объявлений, услуг, медиафайлов']) && $user['id'] == $record['creator_id'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        if ($record) {
            foreach ($record->reviews as $item) {
                Review::destroy($item['id']);
            }

            foreach ($record->favorites as $item) {
                $record->favorites()->detach($item['id']);
            }
            foreach ($record->media as $item) {
                $record->media()->detach($item['id']);
            }
            foreach ($record->products as $item) {
                $record->products()->detach($item['id']);
            }

            Ad::destroy($record['id']);

            $data = response()->json([
                'message' => 'Success'], 200);
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }

        return $data;
    }

    public function addMedia(Request $request)
    {
        $user = Auth::user();

        $ad = Ad::find($request['ad_id']);

        if ((!checkPermissions($user, ['Доступ администратора к базе'])) &&
            !(checkPermissions($user, ['Создание объявлений, услуг, медиафайлов']) && $user['id'] == $ad['creator_id'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        foreach ($request['media_ids'] as $item) {
            if ($ad->media->contains($item) != 1) {
                $ad->media()->attach($item);
            }
        }

        return response()->json('Attach successful', 200);
    }

    public function removeMedia(Request $request)
    {
        $user = Auth::user();

        $ad = Ad::find($request['ad_id']);

        if ((!checkPermissions($user, ['Доступ администратора к базе'])) &&
            !(checkPermissions($user, ['Создание объявлений, услуг, медиафайлов']) && $user['id'] == $ad['creator_id'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        foreach ($request['media_ids'] as $item) {
            if ($ad->media->contains($item) == 1) {
                $ad->media()->detach($item);
            }
        }

        return response()->json('Detach successful', 200);
    }

    public function addProduct(Request $request)
    {
        $user = Auth::user();

        $ad = Ad::find($request['ad_id']);

        if ((!checkPermissions($user, ['Доступ администратора к базе'])) &&
            !(checkPermissions($user, ['Создание объявлений, услуг, медиафайлов']) && $user['id'] == $ad['creator_id'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        foreach ($request['product_ids'] as $item) {
            if ($ad->products->contains($item) != 1) {
                $ad->products()->attach($item);
            }
        }

        return response()->json('Attach successful', 200);
    }

    public function removeProduct(Request $request)
    {
        $user = Auth::user();

        $ad = Ad::find($request['ad_id']);

        if ((!checkPermissions($user, ['Доступ администратора к базе'])) &&
            !(checkPermissions($user, ['Создание объявлений, услуг, медиафайлов']) && $user['id'] == $ad['creator_id'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        foreach ($request['product_ids'] as $item) {
            if ($ad->products->contains($item) == 1) {
                $ad->products()->detach($item);
            }
        }

        return response()->json('Detach successful', 200);
    }

    public function top(Request $request) {
        $data = Ad::where('top_date_time', '>=', now())->where('expiration_date_time', '>=', now())->with('image');

        if ($request['city_id']) {
            $data = $data->where('city_id', $request['city_id']);
        }
        if ($request['sort_by'] && $request['sort_type']) {
            switch ($request['sort_type']) {
                case 'asc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderBy('created_at');
                            break;
                        case 'rating':
                            $data = $data->orderBy('rating');
                            break;
                        case 'views':
                            $data = $data->orderBy('views');
                            break;
                        case 'calls':
                            $data = $data->orderBy('calls');
                            break;
                        default:
                            break;
                    }
                    break;
                case 'desc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByDesc('created_at');
                            break;
                        case 'rating':
                            $data = $data->orderByDesc('rating');
                            break;
                        case 'views':
                            $data = $data->orderByDesc('views');
                            break;
                        case 'calls':
                            $data = $data->orderByDesc('calls');
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->get();

        return $data;
    }

    public function getByCategory(Request $request) {
        if ($request['amount'] > 0) {
            $data = Ad::whereHas('subcategory', function($query) use ($request) {
                $query->where('category_id','=', $request->category_id);
            })
                ->where('expiration_date_time', '>=', Carbon::now())
                ->take($request['amount'])
                ->with('image');
        } else {
            $data = Ad::whereHas('subcategory', function($query) use ($request) {
                $query->where('category_id','=', $request->category_id);
            })
                ->where('expiration_date_time', '>=', Carbon::now())
                ->with('image');
        }

        if ($request['city_id']) {
            $data = $data->where('city_id','=', $request['city_id']);
        }
        if ($request['sort_by'] && $request['sort_type']) {
            switch ($request['sort_type']) {
                case 'asc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderBy('created_at');
                            break;
                        case 'rating':
                            $data = $data->orderBy('rating');
                            break;
                        case 'views':
                            $data = $data->orderBy('views');
                            break;
                        case 'calls':
                            $data = $data->orderBy('calls');
                            break;
                        default:
                            break;
                    }
                    break;
                case 'desc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByDesc('created_at');
                            break;
                        case 'rating':
                            $data = $data->orderByDesc('rating');
                            break;
                        case 'views':
                            $data = $data->orderByDesc('views');
                            break;
                        case 'calls':
                            $data = $data->orderByDesc('calls');
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->get();

        return $data;
    }

    public function getBySubcategory(Request $request) {
        if ($request['amount'] > 0) {
            $data = Ad::where('subcategory_id','=', $request['subcategory_id'])
                ->where('expiration_date_time', '>=', Carbon::now())
                ->take($request['amount'])
                ->with('image');
        } else {
            $data = Ad::where('subcategory_id','=', $request['subcategory_id'])->where('expiration_date_time', '>=', Carbon::now())->with('image');
        }

        if ($request['city_id']) {
            $data = $data->where('city_id', $request['city_id']);
        }
        if ($request['sort_by'] && $request['sort_type']) {
            switch ($request['sort_type']) {
                case 'asc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderBy('created_at');
                            break;
                        case 'rating':
                            $data = $data->orderBy('rating');
                            break;
                        case 'views':
                            $data = $data->orderBy('views');
                            break;
                        case 'calls':
                            $data = $data->orderBy('calls');
                            break;
                        default:
                            break;
                    }
                    break;
                case 'desc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByDesc('created_at');
                            break;
                        case 'rating':
                            $data = $data->orderByDesc('rating');
                            break;
                        case 'views':
                            $data = $data->orderByDesc('views');
                            break;
                        case 'calls':
                            $data = $data->orderByDesc('calls');
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->get();

        return $data;
    }

    public function getByUser(Request $request) {
        $data = Ad::where('creator_id', $request['creator_id'])
            ->where('expiration_date_time', '>=', now())
            ->orderByDesc('created_at')
            ->take($request['amount'])
            ->with('image');

        if ($request['city_id']) {
            $data = $data->where('city_id', $request['city_id']);
        }
        if ($request['sort_by'] && $request['sort_type']) {
            switch ($request['sort_type']) {
                case 'asc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderBy('created_at');
                            break;
                        case 'rating':
                            $data = $data->orderBy('rating');
                            break;
                        case 'views':
                            $data = $data->orderBy('views');
                            break;
                        case 'calls':
                            $data = $data->orderBy('calls');
                            break;
                        default:
                            break;
                    }
                    break;
                case 'desc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByDesc('created_at');
                            break;
                        case 'rating':
                            $data = $data->orderByDesc('rating');
                            break;
                        case 'views':
                            $data = $data->orderByDesc('views');
                            break;
                        case 'calls':
                            $data = $data->orderByDesc('calls');
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->get();

        return $data;
    }

    public function getById(Request $request) {
        $data = Ad::where('id', $request['id'])
            ->with('media')
            ->with('image')
            ->with('products')
            ->withCount('reviews')
            ->with('city', function ($query) {
                $query->with('country');
            })
            ->firstOrFail();

        $data['isFavorite'] = false;

        return $data;
    }

    public function getByIdWithFavorite(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Добавление объявлений в избранное'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Ad::where('id', $request['id'])
            ->with('media')
            ->with('image')
            ->with('products')
            ->withCount('reviews')
            ->with('city', function ($query) {
                $query->with('country');
            })
            ->first();

        if ($data) {
            if ($user['id'] != $data['creator_id']) {
                if ($data['expiration_date_time'] > Carbon::now()) {
                    $data->update(['views' => $data['views'] + 1]);
                } else {
                    return json_encode(null);
                }
            }

            $data['isFavorite'] = false;
            foreach ($user->favorites as $item) {
                if ($item['id'] == $data['id']) {
                    $data['isFavorite'] = true;
                }
            }

            return $data;
        } else {
            return json_encode(null);
        }
    }

    public function search(Request $request) {
        if ($request['category_id']) {
            $data = Ad::where('name', 'LIKE', '%' . $request['search'] . '%')
                ->where('expiration_date_time', '>=', now())
                ->orWhere('description', 'LIKE', '%' . $request['search'] . '%')
                ->whereHas('subcategory', function($query) use ($request) {
                    $query->where('category_id', $request['category_id']);
                })
                ->with('image');
        } else if ($request['subcategory_id']) {
            $data = Ad::where('name', 'LIKE', '%' . $request['search'] . '%')
                ->where('expiration_date_time', '>=', now())
                ->orWhere('description', 'LIKE', '%' . $request['search'] . '%')
                ->where('subcategory_id', $request['subcategory_id'])
                ->with('image');
        } else {
            $data = Ad::where('name', 'LIKE', '%' . $request['search'] . '%')
                ->where('expiration_date_time', '>=', now())
                ->orWhere('description', 'LIKE', '%' . $request['search'] . '%')
                ->with('image');
        }

        if ($request['city_id']) {
            $data = $data->where('city_id', $request['city_id']);
        }
        if ($request['sort_by'] && $request['sort_type']) {
            switch ($request['sort_type']) {
                case 'asc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderBy('created_at');
                            break;
                        case 'rating':
                            $data = $data->orderBy('rating');
                            break;
                        case 'views':
                            $data = $data->orderBy('views');
                            break;
                        case 'calls':
                            $data = $data->orderBy('calls');
                            break;
                        default:
                            break;
                    }
                    break;
                case 'desc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByDesc('created_at');
                            break;
                        case 'rating':
                            $data = $data->orderByDesc('rating');
                            break;
                        case 'views':
                            $data = $data->orderByDesc('views');
                            break;
                        case 'calls':
                            $data = $data->orderByDesc('calls');
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->get();

        return $data;
    }

    public function getSelf(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание объявлений, услуг, медиафайлов'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $ads = $user->ads()->orderByDesc('created_at')->with('image');

        $data = $ads;

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->get();

        return $data;
    }

    public function publish(Request $request) {
        $user = Auth::user();

        $request->validate([
            'subcategory_id' => 'required|numeric|exists:tariffs,subcategory_id'
        ]);

        if (!checkPermissions($user, ['Создание объявлений, услуг, медиафайлов'])) {
            throw new AuthorizationException();
        }

        if ($request['phone']) {
            $phoneUtil = PhoneNumberUtil::getInstance();
            $numberPrototype = $phoneUtil->parse($request->phone, 'RU');
            $phone = $phoneUtil->format($numberPrototype, PhoneNumberFormat::E164);
            if (mb_strlen($phone) >= 12 && mb_strlen($phone) <= 16) {
                $request['phone'] = $phone;
            } else {
                return response()->json([
                    'type' => 'validation_error',
                    'errors' => [
                        'phone' => [
                            'Phone number is not valid'
                        ]
                    ]
                ], 422);

            }
        }

        if ($request['overprice']) {
            $overpriceMultiplier = 1.25;
        } else {
            $overpriceMultiplier = 1;
        }

        if ($request['subcategory_id']) {
            $tariffMain = Tariff::where('action', 'Объявление')->where('application_id', 1)->where('subcategory_id', $request['subcategory_id'])->first()['price'];
        }

        $tariffColor = Tariff::where('action', 'Покраска')->where('application_id', 1)->firstOrFail()['price'];
        $tariffTop = Tariff::where('action', 'Топ')->where('application_id', 1)->firstOrFail()['price'];

        if (!$user['trial_used']) {
            $sum = ($request['top_multiplier'] * $tariffTop + $request['color_multiplier'] * $tariffColor + $request['extend_multiplier'] * $tariffMain) * $overpriceMultiplier;
            if ($user['balance'] >= $sum) {
                $data = Ad::create($request->except('rating', 'creator_id'));

                $expirationDate = date('Y-m-d H:i:s', strtotime(now() . '+1 month'));
                $data->update(['expiration_date_time' => $expirationDate]);

                $data->update(['creator_id' => $user['id']]);

                $_sendRequest = $request;
                $_sendRequest['id'] = $data['id'];

                if ($request['top_multiplier']) {
                    $this->makeTop($_sendRequest);
                }
                if ($request['color_multiplier']) {
                    $this->makeColored($_sendRequest);
                }
                if ($request['extend_multiplier']) {
                    $this->extend($_sendRequest);
                }

                if($request['media_ids']) {
                    foreach ($request['media_ids'] as $item) {
                        if ($data->media->contains($item) != 1) {
                            $data->media()->attach($item);
                        }
                    }
                }

                if($request['product_ids']) {
                    foreach ($request['product_ids'] as $item) {
                        if ($data->products->contains($item) != 1) {
                            $data->products()->attach($item);
                        }
                    }
                }

                $user->update(['trial_used' => true]);

                MicrophoneAdJob::dispatch($data)->delay(Carbon::make($expirationDate));

                return response()->json(['message' =>'Success', 'success' => true], 200);
            } else {
                return response()->json([
                    'type' => 'standard_error',
                    'message' =>'Not enough money on balance',
                    'success' => false
                ], 400);

            }
        } else {
            $sum = ($tariffMain + $request['top_multiplier'] * $tariffTop + $request['color_multiplier'] * $tariffColor + $request['extend_multiplier'] * $tariffMain) * $overpriceMultiplier;
            if ($user['balance'] >= $sum) {
                $data = Ad::create($request->except('rating', 'creator_id'));
                $data->update(['creator_id' => $user['id']]);

                $_sendRequest = $request;
                $_sendRequest['id'] = $data['id'];

                if ($request['top_multiplier']) {
                    $this->makeTop($_sendRequest);
                }
                if ($request['color_multiplier']) {
                    $this->makeColored($_sendRequest);
                }

                if ($data['expiration_date_time'] > now()){
                    $expirationDate = date('Y-m-d H:i:s', strtotime($data['expiration_date_time']));
                } else {
                    $expirationDate = date('Y-m-d H:i:s', strtotime(now()));
                }
                $data->update(['expiration_date_time' => $expirationDate]);

                if ($request['extend_multiplier']) {
                    $this->extend($_sendRequest);
                }

                if($request['media_ids']) {
                    foreach ($request['media_ids'] as $item) {
                        if ($data->media->contains($item) != 1) {
                            $data->media()->attach($item);
                        }
                    }
                }

                if($request['product_ids']) {
                    foreach ($request['product_ids'] as $item) {
                        if ($data->products->contains($item) != 1) {
                            $data->products()->attach($item);
                        }
                    }
                }

                return response()->json(['message' =>'Success', 'success' => true], 200);
            } else {
                return response()->json([
                    'type' => 'standard_error',
                    'message' =>'Not enough money on balance',
                    'success' => false
                ], 400);

            }
        }
    }

    public function extend(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание объявлений, услуг, медиафайлов'])) {
            throw new AuthorizationException();
        }

        if ($request['overprice']) {
            $overpriceMultiplier = 1.25;
        } else {
            $overpriceMultiplier = 1;
        }

        $data = Ad::find($request['id']);
        $tariff = Tariff::where('action', 'Объявление')->where('application_id', 1)->where('subcategory_id', $data['subcategory_id'])->first()['price'];
        $sum = $tariff * $request['extend_multiplier'] * $overpriceMultiplier;
        $length = '+' . 1 * $request['extend_multiplier'] . ' day';
        $creationDate = date('Y-m-d H:i:s', strtotime(now()));

        if ($user['balance'] >= $sum) {
            $balance = $user['balance'] - $sum;
            Transaction::create(['amount' => $sum, 'user_id' => $user['id'], 'action' => 'Ad extend', 'type' => 2, 'application_id' => 1]);

            if ($data['expiration_date_time'] > now()){
                $expirationDate = date('Y-m-d H:i:s', strtotime($data['expiration_date_time'] . $length));
            } else {
                $expirationDate = date('Y-m-d H:i:s', strtotime(now() . $length));
            }
            $data->update(['expiration_date_time' => $expirationDate, 'created_at' => $creationDate, 'updated_at' => $creationDate]);
            $user->update(['balance' => $balance]);

            agentPartPayment($user, $sum, 'Referral Ad extend', 1);

            MicrophoneAdJob::dispatch($data)->delay(Carbon::make($expirationDate));

            return response()->json(['message' =>'Success', 'success' => true], 200);
        } else {
            return response()->json([
                'type' => 'standard_error',
                'message' =>'Not enough money on balance',
                'success' => false
            ], 400);
        }
    }

    public function makeColored(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание объявлений, услуг, медиафайлов'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        if ($request['overprice']) {
            $overpriceMultiplier = 1.25;
        } else {
            $overpriceMultiplier = 1;
        }

        $tariff = Tariff::where('action', 'Покраска')->where('application_id', 1)->first()['price'];
        $sum = $tariff * $request['color_multiplier'] * $overpriceMultiplier;
        $length = '+' . 1 * $request['color_multiplier'] . ' day';

        $creationDate = date('Y-m-d H:i:s', strtotime(now()));

        if ($user['balance'] >= $sum) {
            $balance = $user['balance'] - $sum;
            Transaction::create(['amount' => $sum, 'user_id' => $user['id'], 'action' => 'Ad makeColored', 'type' => 2, 'application_id' => 1]);

            $data = Ad::find($request['id']);

            if ($data['colored_date_time'] > now()){
                $expirationDate = date('Y-m-d H:i:s', strtotime($data['colored_date_time'] . $length));
            } else {
                $expirationDate = date('Y-m-d H:i:s', strtotime(now() . $length));
            }
            $data->update(['colored_date_time' => $expirationDate, 'created_at' => $creationDate, 'updated_at' => $creationDate]);
            $user->update(['balance' => $balance]);

            agentPartPayment($user, $sum, 'Referral Ad makeColored', 1);

            return response()->json(['message' =>'Success', 'success' => true], 200);
        } else {
            return response()->json([
                'type' => 'standard_error',
                'message' =>'Not enough money on balance',
                'success' => false
            ], 400);
//            return response()->json(['message' =>'Not enough money on balance', 'success' => false], 400);
        }
    }

    public function makeTop(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание объявлений, услуг, медиафайлов'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        if ($request['overprice']) {
            $overpriceMultiplier = 1.25;
        } else {
            $overpriceMultiplier = 1;
        }

        $tariff = Tariff::where('action', 'Топ')->where('application_id', 1)->first()['price'];
        $sum = $tariff * $request['top_multiplier'] * $overpriceMultiplier;
        $length = '+' . 1 * $request['top_multiplier'] . ' day';

        $creationDate = date('Y-m-d H:i:s', strtotime(now()));

        if ($user['balance'] >= $sum) {
            $balance = $user['balance'] - $sum;
            Transaction::create(['amount' => $sum, 'user_id' => $user['id'], 'action' => 'Ad makeTop', 'type' => 2, 'application_id' => 1]);

            $data = Ad::find($request['id']);

            if ($data['top_date_time'] > now()){
                $expirationDate = date('Y-m-d H:i:s', strtotime($data['top_date_time'] . $length));
            } else {
                $expirationDate = date('Y-m-d H:i:s', strtotime(now() . $length));
            }
            $data->update(['top_date_time' => $expirationDate, 'created_at' => $creationDate, 'updated_at' => $creationDate]);
            $user->update(['balance' => $balance]);

            agentPartPayment($user, $sum, 'Referral Ad makeTop', 1);

            return response()->json(['message' =>'Success', 'success' => true], 200);
        } else {
            return response()->json([
                'type' => 'standard_error',
                'message' =>'Not enough money on balance',
                'success' => false
            ], 400);
//            return response()->json(['message' =>'Not enough money on balance', 'success' => false], 400);
        }
    }

    public function increaseCalls(Request $request) {
        $data = Ad::find($request['id']);
        $data = $data->update(['calls' => $data['calls'] + 1]);

        if ($data >= 1) {
            return response()->json(['message' =>'Success'], 200);
        } else {
            throw new SimpleException('Error');
//            return response()->json(['message' =>'Error'], 400);
        }
    }

    public function addFavorite(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Добавление объявлений в избранное'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        if ($user->favorites->contains($request['ad_id']) != 1) {
            $user->favorites()->attach($request['ad_id']);
            return response()->json('Attach successful', 200);
        } else {
            throw new SimpleException('Unable to attach');
//            return response()->json('Unable to attach', 400);
        }
    }

    public function removeFavorite(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Добавление объявлений в избранное'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        if ($user->favorites->contains($request['ad_id']) == 1) {
            $user->favorites()->detach($request['ad_id']);
            return response()->json('Detach successful', 200);
        } else {
            throw new SimpleException('Unable to detach');
//            return response()->json('Unable to detach', 400);
        }
    }

    public function getFavorites(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Добавление объявлений в избранное'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = $user->favorites()->where('expiration_date_time', '>=', now())->with('image');

        if ($request['sort_by'] && $request['sort_type']) {
            switch ($request['sort_type']) {
                case 'asc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderBy('created_at');
                            break;
                        case 'rating':
                            $data = $data->orderBy('rating');
                            break;
                        case 'views':
                            $data = $data->orderBy('views');
                            break;
                        case 'calls':
                            $data = $data->orderBy('calls');
                            break;
                        default:
                            break;
                    }
                    break;
                case 'desc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByDesc('created_at');
                            break;
                        case 'rating':
                            $data = $data->orderByDesc('rating');
                            break;
                        case 'views':
                            $data = $data->orderByDesc('views');
                            break;
                        case 'calls':
                            $data = $data->orderByDesc('calls');
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->get();

        return $data;
    }
}
