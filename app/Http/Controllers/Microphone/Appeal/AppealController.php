<?php

namespace App\Http\Controllers\Microphone\Appeal;

use App\Models\Microphone\Ad;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Microphone\Appeal;
use Illuminate\Support\Facades\Notification;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use function PHPUnit\Framework\isEmpty;

class AppealController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Appeal::with('user')->get();

        return $data;
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Appeal::create($request->all());

        return $data;
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        $data = Appeal::findOrFail($request['id']);

        if ((!checkPermissions($user, ['Доступ администратора к базе'])) &&
            !(checkPermissions($user, ['Создание заявок']) && $user['id'] == $data['user_id'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        if ($data) {
            $data->update($request->all());
            $data = 'Success';
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }
        return $data;
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        $record = Appeal::find($request['id']);

        if ((!checkPermissions($user, ['Доступ администратора к базе'])) &&
            !(checkPermissions($user, ['Создание заявок']) && $user['id'] == $record['user_id'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Appeal::destroy($request['id']);

        if ($data == 1) {$data = 'Success';} else {$data = 'Error';}

        return $data;
    }

    public function publish(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание заявок'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $request = Appeal::create($request->except('user_id'));
        $request->update(['user_id' => $user['id']]);

        $users = User::has('ads')
            ->where('city_id', $request['city_id'])
            ->whereHas('ads', function ($query) {
                $query->where('subcategory_id', 1)
                    ->orWhere('subcategory_id', 11)
                    ->orWhere('subcategory_id', 4)
                    ->orWhere('subcategory_id', 9)
                    ->orWhere('subcategory_id', 10);
            })
            ->get();
        foreach ($users as $singleUser) {
            Notification::send($singleUser, new \App\Notifications\Microphone\AppealPublish(['user' => $singleUser]));
        }


        return $request;
    }

    public function getAvailable(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Просмотр заявок'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $ads = Ad::where('creator_id', $user['id'])
            ->where(function ($query) {
                $query
                    ->where('subcategory_id', 1)
                    ->orWhere('subcategory_id', 11)
                    ->orWhere('subcategory_id', 4)
                    ->orWhere('subcategory_id', 9)
                    ->orWhere('subcategory_id', 10)
                    ->orWhere('subcategory_id', 25);
            })
            ->get();

        if (!$ads->isEmpty()) {
            $data = Appeal::whereHas('user', function($query) use ($user) {
                $query->where('city_id', $user['city_id']);
            })
                ->whereHas('user', function($query) use ($user) {
                    $query->where('user_id', '!=', $user['id']);
                })
                ->with(['user' => function($query){
                $query->select('id', 'name', 'surname', 'patronym', 'phone');
            }])
                ->orderByDesc('created_at');

            if ($request['pagination_amount'] && $request['pagination_start']) {
                $data = $data->skip($request['pagination_start'] - 1)
                    ->take($request['pagination_amount']);
            }

            if ($request['city_id']) {
                $data = $data->where('city_id', $request['city_id']);
            }

            $data = $data->get();
        } else {
            $data = 'No results';
        };

        return $data;
    }

    public function getSelf(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание заявок'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Appeal::whereHas('user', function($query) use ($user) {
                $query->where('user_id', $user['id']);
            })
            ->orderByDesc('created_at');

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->get();

        return $data;
    }


}
