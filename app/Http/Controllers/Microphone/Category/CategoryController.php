<?php

namespace App\Http\Controllers\Microphone\Category;

use App\Models\Microphone\Ad;
use App\Models\Microphone\Category;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CategoryController extends Controller
{
    public function index(Request $request)
    {

         $data = Category::with('subcategories')
             ->with('translations')
             ->get();

        $data->map(function ($category) use ($request){
            $countSubsAd = 0;
            foreach ($category->subcategories as $subcategory){
                if (isset($request->city_id)){
                    $countSubsAdFirst = Ad::where('subcategory_id','=',$subcategory->id)
                        ->where('city_id', $request->city_id)
                        ->where('expiration_date_time', '>=', Carbon::now())
                        ->get();
                }else{
                    $countSubsAdFirst = Ad::where('subcategory_id','=',$subcategory->id)
                        ->where('expiration_date_time', '>=', Carbon::now())
                        ->get();
                }
                $countSubsAd +=$countSubsAdFirst->count();
            }

            $category->sub_ads_count = $countSubsAd;
            unset($countSubsAd);
            $category->subcategories->map(function ($subcategories) use ($request) {
                if (isset($request->city_id)){
                    $countAddCategory = Ad::where('subcategory_id','=',$subcategories->id)
                        ->where('city_id', $request['city_id'])
                        ->where('expiration_date_time', '>=', Carbon::now())
                        ->count();
                }else{
                    $countAddCategory = Ad::where('subcategory_id','=',$subcategories->id)
                        ->where('expiration_date_time', '>=', Carbon::now())
                        ->count();
                }

                $subcategories->ads_count = $countAddCategory;
            });
        });

        return $data;
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание подкатегорий'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Category::create($request->all());

        return $data;
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание подкатегорий'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Category::find($request->id);
        if ($data) {
            $data->update($request->all());
            $data = 'Success';
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }
        return $data;
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание подкатегорий'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Category::destroy($request->id);

        if ($data == 1) {$data = 'Success';} else {$data = 'Error';}

        return $data;
    }
}
