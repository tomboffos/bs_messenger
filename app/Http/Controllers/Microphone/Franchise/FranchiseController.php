<?php

namespace App\Http\Controllers\Microphone\Franchise;

use App\Exceptions\SimpleException;
use App\Http\Controllers\Controller;
use App\Models\FranchiseClient;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class FranchiseController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'patronymic' => 'nullable|string',
            'phone' => 'required|string',
            'application_id' => 'required|numeric|exists:applications,id'

        ]);

        FranchiseClient::create($request->only(['first_name', 'last_name', 'patronymic', 'phone', 'application_id']));

        return response()->json(['message' => 'Success'], 200);
    }

    public function index(Request $request)
    {
        $user = \Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
        }

        $request->validate([
            'sort_by' => ['nullable', 'string', Rule::in(['first_name', 'last_name', 'patronymic', 'phone'])],
            'sort_type' => ['nullable', 'string', Rule::in(['asc', 'desc'])],
            'search' => 'nullable|string',
            'page' => 'nullable|numeric',
            'per_page' => 'nullable|numeric',
            'application_id' => 'required|numeric|exists:applications,id'
        ]);

        $query = FranchiseClient::where('application_id', $request->application_id);

        if ($request['sort_by']) {
            $query->orderBy($request['sort_by'], $request['sort_type'] ?? 'asc');
        }

        if ($request['search']) {
            $query->search($request['search']);
        }

        $franchiseClients = $query->paginate($request['per_page'] ?? 10);

        return response()->json(['franchiseClients' => $franchiseClients], 200);
    }

    public function delete(Request $request, FranchiseClient $franchiseClient)
    {
        $franchiseClient->delete();

        return response()->json(['message' => 'Success'], 200);
    }
}
