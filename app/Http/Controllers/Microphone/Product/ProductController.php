<?php

namespace App\Http\Controllers\Microphone\Product;

use App\Http\Controllers\Controller;
use App\Models\Microphone\Media;
use App\Models\Microphone\Product;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ProductController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Product::all();

        return $data;
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Product::create($request->all());

        return $data;
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Product::find($request->id);
        if ($data) {
            $data->update($request->all());
            $data = 'Success';
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }
        return $data;
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        $record = Product::find($request['id']);

        if ((!checkPermissions($user, ['Доступ администратора к базе'])) &&
            !(checkPermissions($user, ['Создание заявок']) && $user['id'] == $record['user_id'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        if ($record) {
            foreach ($record->ads as $item) {
                $record->ads()->detach($item['id']);
            }

            Product::destroy($record['id']);

            $data = response()->json([
                'message' => 'Success'], 200);
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }

        return $data;
    }

    public function publish(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание объявлений, услуг, медиафайлов'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Product::create($request->all());

        $data->update(['user_id' => $user['id']]);

        return $data;
    }

    public function getSelf()
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание объявлений, услуг, медиафайлов'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Product::where('user_id', $user['id'])->get();

        return $data;
    }
}
