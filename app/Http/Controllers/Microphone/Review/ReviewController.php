<?php

namespace App\Http\Controllers\Microphone\Review;

use App\Models\Microphone\Ad;
use App\Http\Controllers\Controller;
use App\Models\Microphone\Product;
use App\Models\Microphone\Review;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ReviewController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Review::all();

        return $data;
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Review::create($request->all());
        $ad = Ad::find($request['ad_id']);
        $rating_average = $ad->reviews->avg('rating');
        $ad->update(['rating' => $rating_average]);

        return $data;
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Review::find($request->id);
        if ($data) {
            $data->update($request->all());
            $ad = Ad::find($request['ad_id']);
            $rating_average = $ad->reviews->avg('rating');
            $ad->update(['rating' => $rating_average]);

            $data = 'Success';
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }

        return $data;
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        $record = Review::find($request['id']);

        if ((!checkPermissions($user, ['Доступ администратора к базе'])) &&
            !(checkPermissions($user, ['Создание заявок']) && $user['id'] == $record['user_id'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Review::destroy($request->id);

        if ($data == 1) {$data = 'Success';} else {$data = 'Error';}

        return $data;
    }

    public function publish(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание отзывов'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $review = Review::where('user_id', $user['id'])->where('ad_id', $request['ad_id'])->first();

        if ($review) {
            $review->update(['body' => $request['body'], 'rating' => $request['rating']]);
            $data = Review::find($review['id']);
        } else {
            $data = Review::create($request->except(['user_id']));
            $data->update(['user_id' => $user['id']]);
        };

        $ad = Ad::find($request['ad_id']);
        $rating_average = $ad->reviews->avg('rating');
        $ad->update(['rating' => $rating_average]);

        $creator = User::find($ad['creator_id']);

        Notification::send($creator, new \App\Notifications\Microphone\ReviewPublish(['user' => $creator]));

        return $data;
    }

    public function getByAd(Request $request) {
        $data = Ad::findOrFail($request['ad_id'])->reviews()->with(array('user' => function($query){
            $query->select('id', 'name', 'surname', 'avatar');
        }));

        if ($request['sort_by'] && $request['sort_type']) {
            switch ($request['sort_type']) {
                case 'asc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderBy('created_at');
                            break;
                        case 'rating':
                            $data = $data->orderBy('rating');
                            break;
                        default:
                            break;
                    }
                    break;
                case 'desc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByDesc('created_at');
                            break;
                        case 'rating':
                            $data = $data->orderByDesc('rating');
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->get();

        return $data;
    }
}
