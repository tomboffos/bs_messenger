<?php

namespace App\Http\Controllers\Microphone\Subcategory;

use App\Http\Controllers\Controller;
use App\Models\Microphone\Subcategory;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SubcategoryController extends Controller
{
    public function index()
    {
        $data = Subcategory::with('translations')->get();

        return $data;
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание подкатегорий'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Subcategory::create($request->all());

        return $data;
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание подкатегорий'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Subcategory::find($request->id);
        if ($data) {
            $data->update($request->all());
            $data = 'Success';
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }
        return $data;
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание подкатегорий'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Subcategory::destroy($request->id);

        if ($data == 1) {$data = 'Success';} else {$data = 'Error';}

        return $data;
    }
}
