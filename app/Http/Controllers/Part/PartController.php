<?php

namespace App\Http\Controllers\Part;

use App\Models\Application;
use App\Http\Controllers\Controller;
use App\Models\Part;
use App\Models\Tariff;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PartController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Просмотр баланса сервисов'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Part::all();

        return $data;
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Задавание стоимости доли'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Part::create($request->all());

        return $data;
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Задавание стоимости доли'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Part::find($request->id);
        if ($data) {
            $data->update($request->all());
            $data = 'Success';
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }
        return $data;
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Задавание стоимости доли'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Part::destroy($request->id);

        if ($data == 1) {$data = 'Success';} else {$data = 'Error';}

        return $data;
    }

    public function buy(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Покупка, продажа долей'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $tariffMinimum = Tariff::where('action', 'Минимальная сумма вклада')->where('application_id', $request['application_id'])->first()['price'];
        $tariffMaximum = Tariff::where('action', 'Максимальная сумма вклада')->where('application_id', $request['application_id'])->first()['price'];

        $application = Application::find($request['application_id']);
        $newPercent = floor($request['price'] * 10000000000 / $application['capitalization']) / 100000000;
        $adminPart = Part::where('user_id', 1)->where('application_id', $request['application_id'])->first();
        $adminPrice = floor(($adminPart['percent'] * $application['capitalization'])) / 100;
        $realPrice = $request['price'];
        $adminPercent = $adminPart['percent'] - $newPercent;
        $minimalAdminPercent = Tariff::where('action', 'Минимальный процент админа')->where('application_id', $request['application_id'])->first()['percent'];

        if (($adminPercent >= $minimalAdminPercent) && ($adminPrice >= $request['price']) && ($request['price'] >= $tariffMinimum) && ($request['price'] <= $tariffMaximum) && ($user['balance'] >= $realPrice)) {
                $adminPart->update(['percent' => $adminPart['percent'] - $newPercent]);
                $user->update(['balance' => $user['balance'] - $realPrice]);
                Transaction::create(['amount' => $realPrice, 'user_id' => $user['id'], 'action' => 'Part buy', 'type' => 2, 'application_id' => $request['application_id']]);

                Part::create(['percent' => $newPercent, 'user_id' => $user['id'], 'application_id' => $request['application_id']]);

                investorsPartPayment($user, $realPrice, 'Referral Part buy', $request['application_id']);
            return response()->json(['message' => $adminPrice . ' left', 'success' => true, 'left' => $adminPrice], 200);
        } else {
            return response()->json(['type' => 'standard_error', 'message' =>'Not enough money on balance or out of range: Enter price between ' . $tariffMinimum . ' and ' . $adminPrice, 'success' => false, 'maximum' => $adminPrice], 400);
        }
    }

    public function calculate(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Покупка, продажа долей'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $tariffMinimum = Tariff::where('action', 'Минимальная сумма вклада')->where('application_id', $request['application_id'])->first()['price'];
        $tariffMaximum = Tariff::where('action', 'Максимальная сумма вклада')->where('application_id', $request['application_id'])->first()['price'];

        $application = Application::find($request['application_id']);
        $newPercent = floor($request['price'] * 10000000000 / $application['capitalization']) / 100000000;
        $adminPart = Part::where('user_id', 1)->where('application_id', $request['application_id'])->first();
        $adminPrice = floor(($adminPart['percent'] * $application['capitalization'])) / 100;
        $realPrice = $request['price'];
        $adminPercent = $adminPart['percent'] - $newPercent;
        $minimalAdminPercent = Tariff::where('action', 'Минимальный процент админа')->where('application_id', $request['application_id'])->first()['percent'];

        if (($adminPercent >= $minimalAdminPercent) && ($adminPrice >= $request['price']) && ($request['price'] >= $tariffMinimum) && ($request['price'] <= $tariffMaximum) && ($user['balance'] >= $realPrice)) {
            $enoughMoney = true;
        } else {
            $enoughMoney = false;
        }

        return response()->json(['percent' => $newPercent, 'amount' => $realPrice, 'amountLeft' => $adminPrice, 'percentLeft' => $adminPart['percent'], 'enoughMoney' => $enoughMoney], 200);
    }

    public function minimalPercent(Request $request) {
        $tariffMinimum = Tariff::where('action', 'Минимальная сумма вклада')->where('application_id', $request['application_id'])->first()['price'];
        $tariffMaximum = Tariff::where('action', 'Максимальная сумма вклада')->where('application_id', $request['application_id'])->first()['price'];

        $application = Application::where('id', $request['application_id'])->first();
        $newPercent = floor($tariffMinimum * 10000000000 / $application['capitalization']) / 10000000000;
        $adminPart = Part::where('user_id', 1)->where('application_id', $request['application_id'])->first();
        $adminPrice = floor(($adminPart['percent'] * $application['capitalization'])) / 100;

        return response()->json(['minimalPercent' => $newPercent, 'tariffMinimum' => $tariffMinimum, 'tariffMaximum' => $tariffMaximum, 'amountLeft' => $adminPrice, 'percentLeft' => $adminPart['percent']], 200);
    }

    public function pay(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Пополнение счёта любому пользователю'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $dateStart = Carbon::parse($request['date_start']);
        $dateEnd = Carbon::parse($request['date_end']);

        $days = $dateEnd->diffInDays($dateStart) + 1;
        $dailyAmount = $request['amount'] / $days;

        $allParts = Part::where('user_id', '!=', 1)->where('application_id', $request['application_id'])->get();

        foreach ($allParts as $singlePart) {
            $datePart = Carbon::parse($singlePart['created_at']);
            if ($datePart <= $dateStart) {
                $partDays = $days;
            } else {
                $partDays = $datePart->diffInDays($dateEnd);
            }
            $partUser = User::find($singlePart['user_id']);
            $sum = floor($dailyAmount * $partDays * $singlePart['percent']) / 100;
            $balance = $partUser['balance'] + $sum;
            Transaction::create(['amount' => $sum, 'user_id' => $partUser['id'], 'action' => 'Part pay', 'type' => 1, 'application_id' => $request['application_id']]);
            $partUser->update(['balance' => $balance]);
            switch ($request['application_id']) {
                case 1:
                    Notification::send($partUser, new \App\Notifications\Microphone\PartPay(['user' => $partUser]));
                    break;
                case 2:
                    Notification::send($partUser, new \App\Notifications\SuperWork\PartPay(['user' => $partUser]));
                    break;
            }
        }

        return $data = 'Success';
    }
}
