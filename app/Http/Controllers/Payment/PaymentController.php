<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Http\Requests\ApplePayRequest;
use App\Services\TransactionService;
use App\Services\UserService;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PaymentController extends Controller
{
    public $transaction;
    public $user;

    public function __construct(TransactionService $transaction, UserService $user){
        $this->transaction = $transaction;
        $this->user = $user;
    }
    public function applePay(ApplePayRequest $request){
        return DB::transaction(function () use ($request) {
            if (Auth::user()->balance >= $request->amount){
                $this->transaction->minusBalanceUser(Auth::user(),$request->amount);
                $this->transactionUser($request);
            }else{
                if ($request->test == 1) {
                    $endPoint = 'https://sandbox.itunes.apple.com/verifyReceipt';
                } else {
                    $endPoint = 'https://buy.itunes.apple.com/verifyReceipt';
                }


                $data = json_decode($this->initCurl($endPoint, $request->get('receipt-data')));
                if (!is_object($data)) {
                    return response()->json($data->status, 400);
                }

                if (!isset($data->status)) {
                    return response()->json($data->status, 400);
                }

                if ($data->status == 21007) {
                    $data = json_decode($this->initCurl($endPoint, $request->get('receipt-data')));
                    $receipt = $data->receipt->in_app[0];
                } elseif ($data->status == 0) {
                    $receipt = $data->receipt->in_app[0];
                }

                if (!is_object($data)) {
                    return response()->json($data->status, 400);
                }

                if ($data->status != 0) {
                    return response()->json($data->status, 400);
                }

               $this->transactionUser($request);
            }

            $user = $this->user->getCurrentUser($request);
            return $user;
        });

    }

    public function transactionUser($request){
        $this->user->becomeAgent($request);
        $this->transaction->addLogTransaction('User becomeAgent', $request->amount, Auth::user(), '1', $request->application_id);
    }


    public static function initCurl($url, $data)
    {

        $json['receipt-data'] = $data;
        $json['password'] = 'f38ad3620d71438bafbe7735da205e34';
        $post = json_encode($json);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);

        $response = curl_exec($ch);
        $errno = curl_errno($ch);
        $error = curl_error($ch);
        curl_close($ch);

        if ($errno != 0) {
            throw new Exception($error, $errno);
        }

        return $response;
    }



}
