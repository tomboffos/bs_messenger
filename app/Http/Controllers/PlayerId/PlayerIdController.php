<?php

namespace App\Http\Controllers\PlayerId;

use App\Http\Controllers\Controller;
use App\Models\Part;
use App\Models\PlayerId;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PlayerIdController extends Controller
{
    public function replace(Request $request)
    {
        $user = Auth::user();

        foreach ($user->playerIds()->where('application_id', $request['application_id'])->get() as $item) {
            PlayerId::destroy($item['id']);
        }

        foreach ($request['player_ids'] as $player_id) {
            PlayerId::create(['player_id' => $player_id, 'user_id' => $user['id'], 'application_id' => $request['application_id']]);
        }

        return response()->json('Replace successful', 200);
    }

    public function store(Request $request)
    {
        $user = Auth::user();

//        if (!checkPermissions($user, ['Редактирование информации в профиле'])) {
//            throw new AuthorizationException();
////            return response()->json([
////                'message' => 'Forbidden'], 403);
//        }

        $data = PlayerId::create(['player_id' => $request['player_id'], 'user_id' => $user['id'], 'application_id' => $request['application_id']]);

        return $data;
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Редактирование информации в профиле'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = PlayerId::where('player_id', $request['player_id'])->first();
        $data = PlayerId::destroy($data['id']);

        if ($data == 1) {
            $data = response()->json([
                'message' => 'Success'], 200);
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }

        return $data;
    }

    public function getSelf() {
        $user = Auth::user();

        $data = $user->playerIds->pluck('player_id');

        return $data;
    }
}
