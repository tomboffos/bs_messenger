<?php

namespace App\Http\Controllers\Role;

use App\Exceptions\SimpleException;
use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class RoleController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Role::all();

        return $data;
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Role::create($request->all());

        return $data;
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Role::find($request->id);
        if ($data) {
            $data->update($request->all());
            $data = 'Success';
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }
        return $data;
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Role::destroy($request->id);

        if ($data == 1) {$data = 'Success';} else {$data = 'Error';}

        return $data;
    }

    public function addPermission(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $role = Role::find($request['role_id']);
        if ($role->permissions->contains($request['permission_id']) != 1) {
            $role->permissions()->attach($request['permission_id']);
            return response()->json('Attach successful', 200);
        } else {
            throw new SimpleException('Unable to attach');
//            return response()->json('Unable to attach', 400);
        }
    }

    public function removePermission(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $role = Role::find($request['role_id']);
        if ($role->permissions->contains($request['permission_id']) == 1) {
            $role->permissions()->detach($request['permission_id']);
            return response()->json('Detach successful', 200);
        } else {
            throw new SimpleException('Unable to detach');
//            return response()->json('Unable to detach', 400);
        }
    }
}
