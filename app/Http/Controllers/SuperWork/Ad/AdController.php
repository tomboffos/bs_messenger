<?php

namespace App\Http\Controllers\SuperWork\Ad;

use App\Exceptions\SimpleException;
use App\Http\Controllers\Controller;
use App\Http\Requests\SuperworkAdUserRequest;
use App\Jobs\SuperworkAdJob;
use App\Models\Superwork\AboutCompany;
use App\Models\SuperWork\Ad;
use App\Models\Tariff;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AdController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Ad::with('image')
            ->where('expiration_date_time', '>=', now());

        if ($request['type']) {
            $data = $data->where('type', $request['type']);
        }
        if ($request['city_id']) {
            $data = $data->where('city_id', $request['city_id']);
        }
        if ($request['sort_by'] && $request['sort_type']) {
            switch ($request['sort_type']) {
                case 'asc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, created_at ASC');
                            break;
                        case 'views':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, views ASC');
                            break;
                        case 'calls':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, calls ASC');
                            break;
                        default:
                            break;
                    }
                    break;
                case 'desc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, created_at DESC');
                            break;
                        case 'views':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, views DESC');
                            break;
                        case 'calls':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, calls DESC');
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->with('city')->get();

        foreach ($data as $ad) {
            $ad['isFavorite'] = false;

            $creator = $ad->creator()->first();
            if ($creator['name'] && !$creator['surname']) {
                $ad['creatorName'] = $creator['name'];
            } else if (!$creator['name'] && $creator['surname']) {
                $ad['creatorName'] = $creator['surname'];
            } else if ($creator['name'] && $creator['surname']) {
                $ad['creatorName'] = $creator['name'] . ' ' . $creator['surname'];
            } else if (!$creator['name'] && !$creator['surname']) {
                $ad['creatorName'] = null;
            }
        }

        if ($user) {
            foreach ($data as $ad) {
                foreach ($user->superWorkFavorites as $item) {
                    if ($item['id'] == $ad['id']) {
                        $ad['isFavorite'] = true;
                    }
                }
            }
        }

        foreach ($data as $ad) {
            if ($ad['yellow_date_time'] >= now()) {
                $ad['color'] = 3;
            } else if ($ad['blue_date_time'] >= now()) {
                $ad['color'] = 2;
            } else if ($ad['purple_date_time'] >= now()) {
                $ad['color'] = 1;
            } else {
                $ad['color'] = 0;
            }
        }

        return $data;
    }

    public function store(Request $request)
    {
        $user = Auth::user();


        if ($request['phone']) {
            $phoneUtil = PhoneNumberUtil::getInstance();
            $numberPrototype = $phoneUtil->parse($request->phone, 'RU');
            $phone = $phoneUtil->format($numberPrototype, PhoneNumberFormat::E164);
            if (mb_strlen($phone) >= 12 && mb_strlen($phone) <= 16) {
                $request['phone'] = $phone;
            } else {
                return response()->json([
                    'type' => 'validation_error',
                    'errors' => [
                        'phone' => [
                            'Phone number is not valid'
                        ]
                    ]
                ], 422);
//                return response()->json([
//                    'message' => 'Phone number is not valid',
//                ], 401);
            }
        }
        $data = Ad::create($request->except('rating'));

        return $data;
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        $data = Ad::find($request['id']);


        if ($request['phone']) {
            $phoneUtil = PhoneNumberUtil::getInstance();
            $numberPrototype = $phoneUtil->parse($request->phone, 'RU');
            $phone = $phoneUtil->format($numberPrototype, PhoneNumberFormat::E164);
            if (mb_strlen($phone) >= 12 && mb_strlen($phone) <= 16) {
                $request['phone'] = $phone;
            } else {
                return response()->json([
                    'type' => 'validation_error',
                    'errors' => [
                        'phone' => [
                            'Phone number is not valid'
                        ]
                    ]
                ], 422);
//                return response()->json([
//                    'message' => 'Phone number is not valid',
//                ], 401);
            }
        }

        if ($data) {
            if($request['media_ids']) {
                foreach ($request['media_ids'] as $item) {
                    if ($data->media->contains($item) != 1) {
                        $data->media()->attach($item);
                    }
                }
            }

            if($request['product_ids']) {
                foreach ($request['product_ids'] as $item) {
                    if ($data->products->contains($item) != 1) {
                        $data->products()->attach($item);
                    }
                }
            }

            $data->update($request->except('id', 'rating', 'creator_id'));

            $data = Ad::where('id', $request['id'])->with('city')->first();
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }
        return $data;
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        $record = Ad::find($request['id']);

        if ((!checkPermissions($user, ['Доступ администратора к базе'])) &&
            !(checkPermissions($user, ['Использование приложения', 2]) && $user['id'] == $record['creator_id'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        if ($record) {
            foreach ($record->favorites as $item) {
                $record->favorites()->detach($item['id']);
            }

            Ad::destroy($record['id']);

            $data = response()->json([
                'message' => 'Success'], 200);
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }

        return $data;
    }

    public function top(Request $request) {
        $user = auth('sanctum')->user();

        $data = Ad::where('top_date_time', '>=', now())->where('expiration_date_time', '>=', now())->with('image');

        if ($request['type']) {
            $data = $data->where('type', $request['type']);
        }
        if ($request['city_id']) {
            $data = $data->where('city_id', $request['city_id']);
        }
        if ($request['category_id']) {
            $data = $data->where('category_id', $request['category_id']);
        }
        if ($request['sort_by'] && $request['sort_type']) {
            switch ($request['sort_type']) {
                case 'asc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, created_at ASC');
                            break;
                        case 'views':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, views ASC');
                            break;
                        case 'calls':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, calls ASC');
                            break;
                        default:
                            break;
                    }
                    break;
                case 'desc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, created_at DESC');
                            break;
                        case 'views':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, views DESC');
                            break;
                        case 'calls':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, calls DESC');
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->with('city')->get();

        foreach ($data as $ad) {
            $ad['isFavorite'] = false;

            $creator = $ad->creator()->first();
            if ($creator['name'] && !$creator['surname']) {
                $ad['creatorName'] = $creator['name'];
            } else if (!$creator['name'] && $creator['surname']) {
                $ad['creatorName'] = $creator['surname'];
            } else if ($creator['name'] && $creator['surname']) {
                $ad['creatorName'] = $creator['name'] . ' ' . $creator['surname'];
            } else if (!$creator['name'] && !$creator['surname']) {
                $ad['creatorName'] = null;
            }
        }

        if ($user) {
            foreach ($data as $ad) {
                foreach ($user->superWorkFavorites as $item) {
                    if ($item['id'] == $ad['id']) {
                        $ad['isFavorite'] = true;
                    }
                }
            }
        }

        foreach ($data as $ad) {
            if ($ad['yellow_date_time'] >= now()) {
                $ad['color'] = 3;
            } else if ($ad['blue_date_time'] >= now()) {
                $ad['color'] = 2;
            } else if ($ad['purple_date_time'] >= now()) {
                $ad['color'] = 1;
            } else {
                $ad['color'] = 0;
            }
        }

        return $data;
    }

    public function getByCategory(Request $request) {
        $user = auth('sanctum')->user();

        if ($request['amount'] > 0) {
            $data = Ad::whereHas('category', function($query) use ($request) {
                $query->where('category_id', $request['category_id']);
            })
                ->where('expiration_date_time', '>=', now())
                ->with('image');
        } else {
            $data = Ad::whereHas('category', function($query) use ($request) {
                $query->where('category_id', $request['category_id']);
            })
                ->where('expiration_date_time', '>=', now())
                ->with('image');
        }

        if ($request['type']) {
            $data = $data->where('type', $request['type']);
        }
        if ($request['city_id']) {
            $data = $data->where('city_id', $request['city_id']);
        }
        if ($request['sort_by'] && $request['sort_type']) {
            switch ($request['sort_type']) {
                case 'asc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, created_at ASC');
                            break;
                        case 'views':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, views ASC');
                            break;
                        case 'calls':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, calls ASC');
                            break;
                        default:
                            break;
                    }
                    break;
                case 'desc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, created_at DESC');
                            break;
                        case 'views':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, views DESC');
                            break;
                        case 'calls':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, calls DESC');
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->with('city')->get();

        foreach ($data as $ad) {
            $ad['isFavorite'] = false;

         /*   $creator = $ad->creator()->first();
            if ($creator['name'] && !$creator['surname']) {
                $ad['creatorName'] = $creator['name'];
            } else if (!$creator['name'] && $creator['surname']) {
                $ad['creatorName'] = $creator['surname'];
            } else if ($creator['name'] && $creator['surname']) {
                $ad['creatorName'] = $creator['name'] . ' ' . $creator['surname'];
            } else if (!$creator['name'] && !$creator['surname']) {
                $ad['creatorName'] = null;
            }*/
        }

        if ($user) {
            foreach ($data as $ad) {
                foreach ($user->superWorkFavorites as $item) {
                    if ($item['id'] == $ad['id']) {
                        $ad['isFavorite'] = true;
                    }
                }
            }
        }

        foreach ($data as $ad) {
            if ($ad['yellow_date_time'] >= now()) {
                $ad['color'] = 3;
            } else if ($ad['blue_date_time'] >= now()) {
                $ad['color'] = 2;
            } else if ($ad['purple_date_time'] >= now()) {
                $ad['color'] = 1;
            } else {
                $ad['color'] = 0;
            }
        }

        return $data;
    }

    public function getByUser(Request $request) {
        $user = auth('sanctum')->user();

        $data = Ad::where('creator_id', $request['creator_id'])
            ->where('expiration_date_time', '>=', now())
            ->orderByDesc('created_at')
            ->take($request['amount'])
            ->with('image');

        if ($request['type']) {
            $data = $data->where('type', $request['type']);
        }
        if ($request['city_id']) {
            $data = $data->where('city_id', $request['city_id']);
        }
        if ($request['sort_by'] && $request['sort_type']) {
            switch ($request['sort_type']) {
                case 'asc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, created_at ASC');
                            break;
                        case 'views':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, views ASC');
                            break;
                        case 'calls':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, calls ASC');
                            break;
                        default:
                            break;
                    }
                    break;
                case 'desc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, created_at DESC');
                            break;
                        case 'views':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, views DESC');
                            break;
                        case 'calls':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, calls DESC');
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->with('city')->get();

        foreach ($data as $ad) {
            $ad['isFavorite'] = false;

            $creator = $ad->creator()->first();
            if ($creator['name'] && !$creator['surname']) {
                $ad['creatorName'] = $creator['name'];
            } else if (!$creator['name'] && $creator['surname']) {
                $ad['creatorName'] = $creator['surname'];
            } else if ($creator['name'] && $creator['surname']) {
                $ad['creatorName'] = $creator['name'] . ' ' . $creator['surname'];
            } else if (!$creator['name'] && !$creator['surname']) {
                $ad['creatorName'] = null;
            }
        }

        if ($user) {
            foreach ($data as $ad) {
                foreach ($user->superWorkFavorites as $item) {
                    if ($item['id'] == $ad['id']) {
                        $ad['isFavorite'] = true;
                    }
                }
            }
        }

        foreach ($data as $ad) {
            if ($ad['yellow_date_time'] >= now()) {
                $ad['color'] = 3;
            } else if ($ad['blue_date_time'] >= now()) {
                $ad['color'] = 2;
            } else if ($ad['purple_date_time'] >= now()) {
                $ad['color'] = 1;
            } else {
                $ad['color'] = 0;
            }
        }
        $data['owner'] = User::where('id','=',$data['creator_id'])
            ->select('id','name','surname','avatar')
            ->first();
        return $data;
    }

    public function getById(Request $request) {
        $user = auth('sanctum')->user();

        $data = Ad::where('id', $request['id'])
            ->with('image')
            ->with('creator')
            ->with('category')
            ->with('companyAbout')
            ->with('form')
            ->with('city', function ($query) {
                $query->with('country');
            })
            ->first();

        if ($user) {
            if ($user['id'] != $data['creator_id']) {
                $data->update(['views' => $data['views'] + 1]);
            }
        }

        if ($user) {
            $data['isFavorite'] = false;
            foreach ($user->superWorkFavorites as $item) {
                if ($item['id'] == $data['id']) {
                    $data['isFavorite'] = true;
                }
            }
            $creator = $data->creator()->first();
            if ($creator['name'] && !$creator['surname']) {
                $data->creatorName = $creator['name'];
            } else if (!$creator['name'] && $creator['surname']) {
                $data->creatorName = $creator['surname'];
            } else if ($creator['name'] && $creator['surname']) {
                $data->creatorName = $creator['name'] . ' ' . $creator['surname'];
            } else if (!$creator['name'] && !$creator['surname']) {
                $data->creatorName = null;
            }
        } else {
            $data['isFavorite'] = false;
        }

        return $data;
    }

    public function search(Request $request) {
        $user = Auth::user();

        if ($request['category_id']) {
            $data = Ad::where(function ($query) use ($request) {
                $query->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                ->orWhere('description', 'LIKE', '%' . $request['search'] . '%');
            })
                ->where('expiration_date_time', '>=', now())
                ->where('category_id', $request['category_id'])
                ->with('image');
        } else {
            $data = Ad::where(function ($query) use ($request) {
                $query->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orWhere('description', 'LIKE', '%' . $request['search'] . '%');
            })
                ->where('expiration_date_time', '>=', now())
                ->with('image');
        }


        if ($request['type']) {
            $data = $data->where('type', $request['type']);
        }
        if ($request['city_id']) {
            $data = $data->where('city_id', $request['city_id']);
        }
        if ($request['sort_by'] && $request['sort_type']) {
            switch ($request['sort_type']) {
                case 'asc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, created_at ASC');
                            break;
                        case 'views':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, views ASC');
                            break;
                        case 'calls':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, calls ASC');
                            break;
                        default:
                            break;
                    }
                    break;
                case 'desc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, created_at DESC');
                            break;
                        case 'views':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, views DESC');
                            break;
                        case 'calls':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, calls DESC');
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }



        $data = $data->with('city')->get();

        foreach ($data as $ad) {
            $ad['isFavorite'] = false;

            $creator = $ad->creator()->first();
            if ($creator['name'] && !$creator['surname']) {
                $ad['creatorName'] = $creator['name'];
            } else if (!$creator['name'] && $creator['surname']) {
                $ad['creatorName'] = $creator['surname'];
            } else if ($creator['name'] && $creator['surname']) {
                $ad['creatorName'] = $creator['name'] . ' ' . $creator['surname'];
            } else if (!$creator['name'] && !$creator['surname']) {
                $ad['creatorName'] = null;
            }
        }

        if ($user) {
            foreach ($data as $ad) {
                foreach ($user->superWorkFavorites as $item) {
                    if ($item['id'] == $ad['id']) {
                        $ad['isFavorite'] = true;
                    }
                }
            }
        }

        foreach ($data as $ad) {
            if ($ad['yellow_date_time'] >= now()) {
                $ad['color'] = 3;
            } else if ($ad['blue_date_time'] >= now()) {
                $ad['color'] = 2;
            } else if ($ad['purple_date_time'] >= now()) {
                $ad['color'] = 1;
            } else {
                $ad['color'] = 0;
            }
        }

        return $data;
    }

    public function getSelf(Request $request) {
        $request->validate(
            [
                'pagination_amount' => 'nullable|numeric',
                'pagination_start' => 'nullable|numeric',
                'expired' => 'nullable|boolean'
            ],

            // Custom messages
            [
                'expired.boolean' => 'The expired field must be 1 or 0.'
            ]
        );

        $user = Auth::user();

        if (!checkPermissions($user, ['Использование приложения', 2])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $query = $user->superWorkAds()->orderByDesc('created_at')->with('image');

        if ($request['expired'] !== null) {
            if (!$request['expired']) {
                $query->where('expiration_date_time', '>', Carbon::now());
            } else {
                $query->where('expiration_date_time', '<=', Carbon::now());
            }
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $query->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $query->with('city')->get();

        foreach ($data as $ad) {
            $ad['isFavorite'] = false;

            $creator = $ad->creator()->first();
            if ($creator['name'] && !$creator['surname']) {
                $ad['creatorName'] = $creator['name'];
            } else if (!$creator['name'] && $creator['surname']) {
                $ad['creatorName'] = $creator['surname'];
            } else if ($creator['name'] && $creator['surname']) {
                $ad['creatorName'] = $creator['name'] . ' ' . $creator['surname'];
            } else if (!$creator['name'] && !$creator['surname']) {
                $ad['creatorName'] = null;
            }
        }

        if ($user) {
            foreach ($data as $ad) {
                foreach ($user->superWorkFavorites as $item) {
                    if ($item['id'] == $ad['id']) {
                        $ad['isFavorite'] = true;
                    }
                }
            }
        }

        foreach ($data as $ad) {
            if ($ad['yellow_date_time'] >= now()) {
                $ad['color'] = 3;
            } else if ($ad['blue_date_time'] >= now()) {
                $ad['color'] = 2;
            } else if ($ad['purple_date_time'] >= now()) {
                $ad['color'] = 1;
            } else {
                $ad['color'] = 0;
            }
        }

        return $data;
    }

    public function publish(Request $request) {
        $user = Auth::user();

        $request->validate([
            'category_id' => 'required|numeric|exists:tariffs,superwork_category_id'
        ]);

        if (!checkPermissions($user, ['Использование приложения', 2])) {
            throw new AuthorizationException();
        }

        if ($request['phone']) {
            $phoneUtil = PhoneNumberUtil::getInstance();
            $numberPrototype = $phoneUtil->parse($request->phone, 'RU');
            $phone = $phoneUtil->format($numberPrototype, PhoneNumberFormat::E164);
            if (mb_strlen($phone) >= 12 && mb_strlen($phone) <= 16) {
                $request['phone'] = $phone;
            } else {
                return response()->json([
                    'type' => 'validation_error',
                    'errors' => [
                        'phone' => [
                            'Phone number is not valid'
                        ]
                    ]
                ], 422);
            }
        }

        if ($request['category_id']) {
            $tariffMain = Tariff::where('action', 'Объявление')
                ->where('application_id', 2)
                ->where('superwork_category_id', $request['category_id'])
                ->first()['price'];
        }

        $tariffBeginner = Tariff::where('action', 'Начальный')->where('application_id', 2)->first()['price'];
        $tariffAdvanced = Tariff::where('action', 'Продвинутый')->where('application_id', 2)->first()['price'];
        $tariffVip = Tariff::where('action', 'VIP')->where('application_id', 2)->first()['price'];

        switch ($request['tariff']) {
            case 0:
                if ($user['superwork_trial_used'] == 0) {
                    $user->update(['superwork_trial_used' => 1]);
                    $expirationDate = date('Y-m-d H:i:s', strtotime(now() . '+' . (7 + $request['extend_multiplier']) . ' day'));
                    $topDate = null;
                    $vipDate = null;
                    $sum = $tariffMain * $request['extend_multiplier'];
                } else {
                    $expirationDate = date('Y-m-d H:i:s', strtotime(now() . '+' . $request['extend_multiplier'] . ' day'));
                    $topDate = null;
                    $vipDate = null;
                    $sum = $tariffMain * $request['extend_multiplier'];
                }
                break;
            case 1:
                $expirationDate = date('Y-m-d H:i:s', strtotime(now() . '+3 day'));
                $topDate = date('Y-m-d H:i:s', strtotime(now() . '+3 day'));
                $vipDate = null;
                $sum = $tariffBeginner;
                break;
            case 2:
                $expirationDate = date('Y-m-d H:i:s', strtotime(now() . '+7 day'));
                $topDate = date('Y-m-d H:i:s', strtotime(now() . '+7 day'));
                $vipDate = null;
                $sum = $tariffAdvanced;
                break;
            case 3:
                $expirationDate = date('Y-m-d H:i:s', strtotime(now() . '+30 day'));
                $topDate = date('Y-m-d H:i:s', strtotime(now() . '+30 day'));
                $vipDate = date('Y-m-d H:i:s', strtotime(now() . '+30 day'));
                $sum = $tariffVip;
                break;
            default:
                if ($user['superwork_trial_used'] == 0) {
                    $user->update(['superwork_trial_used' => 1]);
                    $expirationDate = date('Y-m-d H:i:s', strtotime(now() . '+' . (7 + $request['extend_multiplier']) . ' day'));
                    $topDate = null;
                    $vipDate = null;
                    $sum = $tariffMain * $request['extend_multiplier'];
                } else {
                    $expirationDate = date('Y-m-d H:i:s', strtotime(now() . '+' . $request['extend_multiplier'] . ' day'));
                    $topDate = null;
                    $vipDate = null;
                    $sum = $tariffMain * $request['extend_multiplier'];
                }
                break;
        }

        if ($user['balance'] >= $sum) {
            $balance = $user['balance'] - $sum;
            Transaction::create(['amount' => $sum, 'user_id' => $user['id'], 'action' => 'Ad publish', 'type' => 2, 'application_id' => 2]);
            $user->update(['balance' => $balance]);

            agentPartPayment($user, $sum, 'Referral Ad publish', 2);

            $data = Ad::create($request->except('creator_id'));
            $data->update(['creator_id' => $user['id'], 'expiration_date_time' => $expirationDate, 'top_date_time' => $topDate, 'vip_date_time' => $vipDate]);

            switch($request['tariff']) {
                case 1:
                    $data->update(['purple_date_time' => $expirationDate]);
                    break;
                case 2:
                    $data->update(['blue_date_time' => $expirationDate]);
                    break;
                case 3:
                    $data->update(['yellow_date_time' => $expirationDate]);
                    break;
            }

            if (!Carbon::now()->isSameDay($expirationDate)) {
                SuperworkAdJob::dispatch($data)->delay(Carbon::make($expirationDate));
            }

            return response()->json(['message' =>'Success', 'success' => true], 200);
        } else {
            return response()->json([
                'type' => 'standard_error',
                'message' =>'Not enough money on balance',
                'success' => false
            ], 400);
//            return response()->json(['message' =>'Not enough money on balance', 'success' => false], 400);
        }
    }

    public function extend(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Использование приложения', 2])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Ad::find($request['id']);

        $tariffMain = Tariff::where('action', 'Объявление')
            ->where('application_id', 2)
            ->where('superwork_category_id', $data['category_id'])
            ->first()['price'];

        $tariffBeginner = Tariff::where('action', 'Начальный')->where('application_id', 2)->first()['price'];
        $tariffAdvanced = Tariff::where('action', 'Продвинутый')->where('application_id', 2)->first()['price'];
        $tariffVip = Tariff::where('action', 'VIP')->where('application_id', 2)->first()['price'];

        $creationDate = date('Y-m-d H:i:s', strtotime(now()));

        if ($data['expiration_date_time'] >= now()) {
            $currentExpiration = $data['expiration_date_time'];
        } else {
            $currentExpiration = now();
        }

        if ($data['top_date_time'] >= now()) {
            $currentTop = $data['top_date_time'];
        } else {
            $currentTop = now();
        }

        if ($data['vip_date_time'] >= now()) {
            $currentVip = $data['vip_date_time'];
        } else {
            $currentVip = now();
        }

        switch ($request['tariff']) {
            case 0:
                $expirationDate = date('Y-m-d H:i:s', strtotime($currentExpiration . '+' . $request['extend_multiplier'] . ' day'));
                $topDate = null;
                $vipDate = null;
                $sum = $tariffMain * $request['extend_multiplier'];
                break;
            case 1:
                $expirationDate = date('Y-m-d H:i:s', strtotime($currentExpiration . '+3 day'));
                $topDate = date('Y-m-d H:i:s', strtotime($currentTop . '+3 day'));
                $vipDate = null;
                $sum = $tariffBeginner;
                break;
            case 2:
                $expirationDate = date('Y-m-d H:i:s', strtotime($currentExpiration . '+7 day'));
                $topDate = date('Y-m-d H:i:s', strtotime($currentTop . '+7 day'));
                $vipDate = null;
                $sum = $tariffAdvanced;
                break;
            case 3:
                $expirationDate = date('Y-m-d H:i:s', strtotime($currentExpiration . '+30 day'));
                $topDate = date('Y-m-d H:i:s', strtotime($currentTop . '+30 day'));
                $vipDate = date('Y-m-d H:i:s', strtotime($currentVip . '+30 day'));
                $sum = $tariffVip;
                break;
            default:
                $expirationDate = date('Y-m-d H:i:s', strtotime($currentExpiration . '+' . $request['extend_multiplier'] . ' day'));
                $topDate = null;
                $vipDate = null;
                $sum = $tariffMain * $request['extend_multiplier'];
                break;
        }

        if ($user['balance'] >= $sum) {
            $balance = $user['balance'] - $sum;
            Transaction::create(['amount' => $sum, 'user_id' => $user['id'], 'action' => 'Ad extend', 'type' => 2, 'application_id' => 2]);
            $user->update(['balance' => $balance]);

            agentPartPayment($user, $sum, 'Referral Ad extend', 2);

            $data->update(['expiration_date_time' => $expirationDate, 'top_date_time' => $topDate, 'vip_date_time' => $vipDate, 'created_at' => $creationDate]);

            switch($request['tariff']) {
                case 1:
                    if ($data['purple_date_time'] >= now()) {
                        $colorDate = date('Y-m-d H:i:s', strtotime($data['purple_date_time'] . '+3 day'));
                    } else {
                        $colorDate = date('Y-m-d H:i:s', strtotime(now() . '+3 day'));
                    }
                    $data->update(['purple_date_time' => $colorDate]);
                    break;
                case 2:
                    if ($data['blue_date_time'] >= now()) {
                        $colorDate = date('Y-m-d H:i:s', strtotime($data['blue_date_time'] . '+7 day'));
                    } else {
                        $colorDate = date('Y-m-d H:i:s', strtotime(now() . '+7 day'));
                    }
                        $data->update(['blue_date_time' => $colorDate]);
                    break;
                case 3:
                    if ($data['yellow_date_time'] >= now()) {
                        $colorDate = date('Y-m-d H:i:s', strtotime($data['yellow_date_time'] . '+30 day'));
                    } else {
                        $colorDate = date('Y-m-d H:i:s', strtotime(now() . '+30 day'));
                    }
                        $data->update(['yellow_date_time' => $colorDate]);
                    break;
            }

            if (!Carbon::now()->isSameDay($expirationDate)) {
                SuperworkAdJob::dispatch($data)->delay(Carbon::make($expirationDate));
            }

            return response()->json(['message' =>'Success', 'success' => true], 200);
        } else {
            return response()->json([
                'type' => 'standard_error',
                'message' =>'Not enough money on balance',
                'success' => false
            ], 400);
//            return response()->json(['message' =>'Not enough money on balance', 'success' => false], 400);
        }
    }

    public function increaseCalls(Request $request) {
        $data = Ad::find($request['id']);
        $data = $data->update(['calls' => $data['calls'] + 1]);

        if ($data >= 1) {
            return response()->json(['message' =>'Success'], 200);
        } else {
            throw new SimpleException('Error');
//            return response()->json(['message' =>'Error'], 400);
        }
    }

    public function increaseViews(Request $request) {
        $data = Ad::find($request['id']);
        $data = $data->update(['views' => $data['views'] + 1]);

        if ($data >= 1) {
            return response()->json(['message' =>'Success'], 200);
        } else {
            throw new SimpleException('Error');
//            return response()->json(['message' =>'Error'], 400);
        }
    }

    public function addFavorite(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Использование приложения', 2])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        if ($user->superWorkFavorites->contains($request['ad_id']) != 1) {
            $user->superWorkFavorites()->attach($request['ad_id']);
            return response()->json('Attach successful', 200);
        } else {
            throw new SimpleException('Unable to attach');
//            return response()->json('Unable to attach', 400);
        }
    }

    public function removeFavorite(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Использование приложения', 2])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        if ($user->superWorkFavorites->contains($request['ad_id']) == 1) {
            $user->superWorkFavorites()->detach($request['ad_id']);
            return response()->json('Detach successful', 200);
        } else {
            throw new SimpleException('Unable to detach');
//            return response()->json('Unable to detach', 400);
        }
    }

    public function getFavorites(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Использование приложения', 2])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = $user->superWorkFavorites()->where('expiration_date_time', '>=', now())->with('image');

        if ($request['type']) {
            $data = $data->where('type', $request['type']);
        }
        if ($request['city_id']) {
            $data = $data->where('city_id', $request['city_id']);
        }
        if ($request['sort_by'] && $request['sort_type']) {
            switch ($request['sort_type']) {
                case 'asc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, created_at ASC');
                            break;
                        case 'views':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, views ASC');
                            break;
                        case 'calls':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, calls ASC');
                            break;
                        default:
                            break;
                    }
                    break;
                case 'desc':
                    switch ($request['sort_by']) {
                        case 'date':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, created_at DESC');
                            break;
                        case 'views':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, views DESC');
                            break;
                        case 'calls':
                            $data = $data->orderByRaw('CASE WHEN vip_date_time >= CURRENT_TIMESTAMP THEN 1 ELSE 2 END, calls DESC');
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->with('city')->get();

        foreach ($data as $ad) {
            $ad['isFavorite'] = false;

            $creator = $ad->creator()->first();
            if ($creator['name'] && !$creator['surname']) {
                $ad['creatorName'] = $creator['name'];
            } else if (!$creator['name'] && $creator['surname']) {
                $ad['creatorName'] = $creator['surname'];
            } else if ($creator['name'] && $creator['surname']) {
                $ad['creatorName'] = $creator['name'] . ' ' . $creator['surname'];
            } else if (!$creator['name'] && !$creator['surname']) {
                $ad['creatorName'] = null;
            }
        }

        if ($user) {
            foreach ($data as $ad) {
                foreach ($user->superWorkFavorites as $item) {
                    if ($item['id'] == $ad['id']) {
                        $ad['isFavorite'] = true;
                    }
                }
            }
        }

        foreach ($data as $ad) {
            if ($ad['yellow_date_time'] >= now()) {
                $ad['color'] = 3;
            } else if ($ad['blue_date_time'] >= now()) {
                $ad['color'] = 2;
            } else if ($ad['purple_date_time'] >= now()) {
                $ad['color'] = 1;
            } else {
                $ad['color'] = 0;
            }
        }

        return $data;
    }

    public function getAdByUserData(SuperworkAdUserRequest $request){
         $user = User::where('id','=',$request->get('creator_id'))
            ->first();
         $ads = Ad::where('creator_id','=',$request->get('creator_id'))
             ->when($request->get('type'),function ($query) use ($request){
                 $query->where('type','=',$request->type);
             })
             ->when($request->sort_by != null,function ($query) use ($request){
                 switch ($request->sort_by){
                     case $request->sort_by == 'date':
                         $query->orderBy('created_at',$request->sort_type == 'asc' ? 'asc' : 'desc' );
                         break;
                     case $request->sort_by == 'views' || $request->sort_by == 'rating':
                         $query->orderBy('views',$request->sort_type == 'asc' ? 'asc' : 'desc' );
                         break;
                     case $request->sort_by == 'calls':
                         $query->orderBy('calls',$request->sort_type == 'asc' ? 'asc' : 'desc' );
                         break;
                 }
             })
             ->with('favorites')
             ->with('creator')
             ->with('city')
             ->with('image')
             ->paginate($request->limit ?? 20);


        $ads->map(function ($ad) use ($user){
            $ad->isFavorite = false;

            $creator = $ad->creator()->first();
            if ($creator['name'] && !$creator['surname']) {
                $ad->creatorName = $creator['name'];
            } else if (!$creator['name'] && $creator['surname']) {
                $ad->creatorName = $creator['surname'];
            } else if ($creator['name'] && $creator['surname']) {
                $ad->creatorName = $creator['name'] . ' ' . $creator['surname'];
            } else if (!$creator['name'] && !$creator['surname']) {
                $ad->creatorName = null;
            }
            if ($user) {
                foreach ($user->superWorkFavorites as $item) {
                    if ($item['id'] == $ad['id']) {
                        $ad->isFavorite = true;
                    }
                }
            }
        });
        $collection = collect($ads);
        $page = $request->page ?? 1;
        $perPage = $request->limit ?? 24;

        $paginate = new LengthAwarePaginator(
            $collection->forPage($page, $perPage),
            $collection->count(),
            $perPage,
            $page,
            ['path' => url("/api/superwork/ad/getPaginationAdUser?creator_id=".$user->id)]
        );

         $owner = User::where('id','=',$request->get('creator_id'))
             ->select('phone','id','name','surname','email','avatar')
             ->first();

        return response()->json([
            'ad' => $paginate,
            'user' => $owner,
        ], 200);
    }


    public function getActiveUserAd(){
        $user = auth('sanctum')->user();

        $active = Ad::where('creator_id','=',Auth::user()->id)
            ->where('expiration_date_time', '>', Carbon::now())
            ->with('city')
            ->with('image')
            ->with('favorites')
            ->with('creator')
            ->get();

        $active->map(function ($ad) use ($user){
            $ad->isFavorite = false;

            $creator = $ad->creator()->first();
            if ($creator['name'] && !$creator['surname']) {
                $ad->creatorName = $creator['name'];
            } else if (!$creator['name'] && $creator['surname']) {
                $ad->creatorName = $creator['surname'];
            } else if ($creator['name'] && $creator['surname']) {
                $ad->creatorName = $creator['name'] . ' ' . $creator['surname'];
            } else if (!$creator['name'] && !$creator['surname']) {
                $ad->creatorName = null;
            }
            if ($user) {
                foreach ($user->superWorkFavorites as $item) {
                    if ($item['id'] == $ad['id']) {
                        $ad->isFavorite = true;
                    }
                }
            }
        });

        $notActive = Ad::where('creator_id','=',Auth::user()->id)
            ->where('expiration_date_time', '<', Carbon::now())
            ->with('city')
            ->with('image')
            ->with('favorites')
            ->with('creator')
            ->get();

        $notActive->map(function ($ad) use ($user){
            $ad->isFavorite = false;

            $creator = $ad->creator()->first();
            if ($creator['name'] && !$creator['surname']) {
                $ad->creatorName = $creator['name'];
            } else if (!$creator['name'] && $creator['surname']) {
                $ad->creatorName = $creator['surname'];
            } else if ($creator['name'] && $creator['surname']) {
                $ad->creatorName = $creator['name'] . ' ' . $creator['surname'];
            } else if (!$creator['name'] && !$creator['surname']) {
                $ad->creatorName = null;
            }
            if ($user) {
                foreach ($user->superWorkFavorites as $item) {
                    if ($item['id'] == $ad['id']) {
                        $ad->isFavorite = true;
                    }
                }
            }
        });
        return response()->json([
            'active' => $active,
            'notActive' => $notActive,
        ], 200);
    }



    public function searchAd(Request $request){
        $user = auth('sanctum')->user();

        $ads =  Ad::when($request->name != null,function ($query) use ($request){
            $query->where('name','LIKE','%'.$request->name.'%');
        })
            ->when($request->city_id != null,function ($query) use ($request){
                $query->where('city_id','=',$request->city_id);
            })
            ->when($request->type != null,function ($query) use ($request){
                $query->where('type','=',$request->type);
            })

            ->when($request->price_from != null,function ($query) use ($request){
                $query->where('price_from','>=',$request->price_from);
            })
            ->when($request->price_to != null,function ($query) use ($request){
                $query->where('price_to','<=',$request->price_to);
            })
            ->where('price_negotiable','=',$request->price_negotiable)
            ->when($request->schedule !=[],function ($query) use ($request){
                $query->whereIn('schedule',$request->schedule);
            })
            ->when($request->category_id !=[],function ($query) use ($request){
                $query->whereIn('category_id',$request->category_id);
            })
            ->when($request->employment !=[],function ($query) use ($request){
                $query->whereIn('employment',$request->employment);
            })
            ->when($request->experience !=[],function ($query) use ($request){
                $query->whereIn('experience',$request->experience);
            })

            ->when($request->sort != null,function ($query) use ($request){
                switch ($request->sort){
                    case $request->sort == 'date':
                        $query->orderBy('created_at',$request->sort_by == 'asc' ? 'asc' : 'desc' );
                        break;
                    case $request->sort == 'views':
                        $query->orderBy('views',$request->sort_by == 'asc' ? 'asc' : 'desc' );
                        break;
                    case $request->sort == 'calls':
                        $query->orderBy('calls',$request->sort_by == 'asc' ? 'asc' : 'desc' );
                        break;
                }
            })
            ->when($request->category_id != null,function ($query) use ($request){
                $query->where('category_id','=',$request->category_id);
            })
            ->where('expiration_date_time', '>', Carbon::now())
            ->with('favorites')
            ->with('creator')
            ->with('city')
            ->with('image')
            ->paginate($request->limit ?? 24);


        $ads->getCollection()->transform(function ($ad, $key) use ($user){
            $ad->isFavorite = false;

            $creator = $ad->creator()->first();
            if ($creator['name'] && !$creator['surname']) {
                $ad->creatorName = $creator['name'];
            } else if (!$creator['name'] && $creator['surname']) {
                $ad->creatorName = $creator['surname'];
            } else if ($creator['name'] && $creator['surname']) {
                $ad->creatorName = $creator['name'] . ' ' . $creator['surname'];
            } else if (!$creator['name'] && !$creator['surname']) {
                $ad->creatorName = null;
            }
            if ($user) {
                foreach ($user->superWorkFavorites as $item) {
                    if ($item['id'] == $ad['id']) {
                        $ad->isFavorite = true;
                    }
                }
            }
            return $ad;
        });

        return  $ads;
    }

}
