<?php

namespace App\Http\Controllers\SuperWork\Category;

use App\Http\Controllers\Controller;
use App\Models\SuperWork\Category;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CategoryController extends Controller
{
    public function index(Request $request)
    {

        if ($request['city_id']) {
            if ($request['type']) {
                $data = Category::withCount(['ads' => function ($query) use ($request) {
                    $query->where('expiration_date_time', '>=', now())->where('city_id', $request['city_id'])->where('type', $request['type']);
                    }])->with('translations');
            } else {
                $data = Category::withCount(['ads' => function ($query) use ($request) {
                    $query->where('expiration_date_time', '>=', now())->where('city_id', $request['city_id']);
                }])->with('translations');
            }
        }
        else {
            if ($request['type']) {
                $data = Category::withCount(['ads' => function ($query) use ($request) {
                    $query->where('expiration_date_time', '>=', now())->where('type', $request['type']);
                }])->with('translations');
            } else {
                $data = Category::withCount('ads')->with('translations');
            }
        }
        $data = $data->get();

        return $data;
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание подкатегорий'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Category::create($request->all());

        return $data;
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание подкатегорий'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Category::find($request->id);
        if ($data) {
            $data->update($request->all());
            $data = 'Success';
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }
        return $data;
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Создание подкатегорий'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Category::destroy($request->id);

        if ($data == 1) {$data = 'Success';} else {$data = 'Error';}

        return $data;
    }
}
