<?php

namespace App\Http\Controllers\SuperWork\Media;

use App\Http\Controllers\Controller;
use App\Models\SuperWork\Media;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class MediaController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $images = Media::where('type', 'image')->get();
        $audio = Media::where('type', 'audio')->get();
        $video = Media::where('type', 'video')->get();

        return response()->json([
            'images' => $images, 'audio' => $audio, 'video' => $video], 200);
    }

    public function store(Request $request)
    {
        $user = Auth::user();



        Media::create($request->except('file'));
        $media = Media::latest()->first();

        if ($request->hasFile('file')) {
            Storage::disk('public')->delete($media['file']);
            switch ($request['type']) {
                case 'image':
                    $directory = 'superwork/media/images';
                    break;
                case 'audio':
                    $directory = 'superwork/media/audio';
                    break;
                case 'video':
                    $directory = 'superwork/media/video';
                    break;
                default:
                    $directory = 'superwork/media/files';
                    break;
            }
            $path = $request['file']->store($directory, ['disk' => 'public']);

            $media->update(['file' => $path]);
        }

        return $media;
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Media::find($request->id);
        if ($data) {
            $data->update($request->except('file'));
            if ($request->hasFile('file')) {
                Storage::disk('public')->delete($data['file']);
                switch ($request['type']) {
                    case 'image':
                        $directory = 'superwork/media/images';
                        break;
                    case 'audio':
                        $directory = 'superwork/media/audio';
                        break;
                    case 'video':
                        $directory = 'superwork/media/video';
                        break;
                    default:
                        $directory = 'superwork/media/files';
                        break;
                }
                $path = $request['file']->store($directory, ['disk' => 'public']);

                $data->update(['file' => $path]);
            }
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }
        return $data;
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        foreach ($request['ids'] as $id) {
            $record = Media::find($id);

            if ((!checkPermissions($user, ['Доступ администратора к базе'])) &&
                !(checkPermissions($user, ['Использование приложения', 2]) && $user['id'] == $record['user_id'])) {
                throw new AuthorizationException();
//                return response()->json([
//                    'message' => 'Forbidden'], 403);
            }

            if ($record) {
                if ($record->adsImage->isNotEmpty()) {
                    foreach ($record->adsImage as $item) {
                        $item->update(['image_id' => null]);
                    }
                }

                if ($record['file']) {
                    Storage::disk('public')->delete($record['file']);
                }
                Media::destroy($record['id']);
            }
        }

        return response()->json(['message' => 'Success'], 200);
    }

    public function publish(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Использование приложения', 2])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        if ($request['files']) {
            foreach ($request['files'] as $file) {
                $media = Media::create($request->except('user_id'));

                Storage::disk('public')->delete($media['file']);
                switch ($request['type']) {
                    case 'image':
                        $directory = 'superwork/media/images';
                        break;
                    case 'audio':
                        $directory = 'superwork/media/audio';
                        break;
                    case 'video':
                        $directory = 'superwork/media/video';
                        break;
                    default:
                        $directory = 'superwork/media/files';
                        break;
                }
                $path = $file->store($directory, ['disk' => 'public']);

                $media->update(['file' => $path]);

                $media->update(['user_id' => $user['id']]);
            }
        } else if ($request['link']) {
            $media = Media::create($request->except('user_id'));

            $media->update(['user_id' => $user['id']]);
        }

        return response()->json(['message' => 'Success'], 200);
    }

    public function getSelf()
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Использование приложения', 2])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $images = Media::where('user_id', $user['id'])->where('type', 'image')->get();
        $audio = Media::where('user_id', $user['id'])->where('type', 'audio')->get();
        $video = Media::where('user_id', $user['id'])->where('type', 'video')->get();

        return response()->json([
            'images' => $images, 'audio' => $audio, 'video' => $video], 200);
    }
}
