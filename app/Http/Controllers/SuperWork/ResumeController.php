<?php

namespace App\Http\Controllers\Superwork;

use App\Http\Controllers\Controller;
use App\Models\Expirience;
use App\Models\LanguageResume;
use App\Models\Skills;
use App\Models\Specialization;
use App\Models\Superwork\AboutCompany;
use App\Models\SuperWork\Category;
use App\Models\Superwork\Resume;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResumeController extends Controller
{
    public function getResume (){

        return response()->json([
            'resume' => Resume::where('user_id','=',Auth::user()->id)
            ->with('city')
            ->first()], 200);
    }

    public function getFilter(){
        return response()->json([
            'expirience' => Expirience::all(),
            'skills' => Skills::all(),
            'language' => LanguageResume::all(),
            'specialization' => Specialization::all(),
            'categories' => Category::all()], 200);
    }


    public function getCompany(){
        return response()->json([
            'company' => AboutCompany::where('user_id','=',Auth::user()->id)
                ->first()], 200);
    }


    public function createResume(Request $request){
        Resume::updateOrCreate(
            [
                'city_id' => $request->city_id,
                'name' => $request->name,
                'surname' => $request->surname,
                'user_id' => Auth::user()->id,
                'other_phone' => $request->other_phone,
                'about_self' => $request->about_self,
                'removal' => $request->removal,
                'birth_day' => $request->birth_day,
                'education' => $request->education,
                'languages' => $request->language,
                'specialty' => $request->specialty,
                'skills' => $request->skills
            ]
        );

        return $this->getResume();
    }

    public function updateResume(Request $request){
        Resume::where('user_id','=',Auth::user()->id)
        ->update(
            [
                'city_id' => $request->city_id,
                'name' => $request->name,
                'surname' => $request->surname,
                'user_id' => Auth::user()->id,
                'other_phone' => $request->other_phone,
                'about_self' => $request->about_self,
                'removal' => $request->removal,
                'birth_day' => $request->birth_day,
                'education' => $request->education,
                'languages' => $request->language,
                'specialty' => $request->specialty,
                'skills' => $request->skills
            ]
        );

        return $this->getResume();
    }


    public function createAboutCompany(Request $request){
            AboutCompany::updateOrCreate(
                [
                    'name' => $request->name,
                    'user_id' => Auth::user()->id,
                    'date_create' => $request->date_create,
                    'about_company' => $request->about_company,
                    'email' => $request->email,
                    'site' => $request->site,
                    'address' => $request->address,
                ]);

        return $this->getCompany();
    }


    public function updateAboutCompany(Request $request){
        AboutCompany::where('user_id','=',Auth::user()->id)
            ->update(
            [
                'name' => $request->name,
                'user_id' => Auth::user()->id,
                'date_create' => $request->date_create,
                'about_company' => $request->about_company,
                'email' => $request->email,
                'site' => $request->site,
                'address' => $request->address,
            ]);

        return $this->getCompany();
    }
}
