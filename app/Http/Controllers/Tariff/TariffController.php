<?php

namespace App\Http\Controllers\Tariff;

use App\Http\Controllers\Controller;
use App\Models\Tariff;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TariffController extends Controller
{
    public function index(Request $request)
    {
        $tariffs = Tariff::all();

        switch ($request['application_id']) {
            case 1:
                $data['agentPrice'] = $tariffs[0]['price'];
                $data['colorPrice'] = $tariffs[1]['price'];
                $data['topPrice'] = $tariffs[2]['price'];
                $data['partPrice'] = $tariffs[3]['price'];
                $data['agentPercent'] = $tariffs[4]['percent'];
                $data['managerPercent'] = $tariffs[49]['percent'];
                for ($i = 0; $i < 41; $i++) {
                    $data['extendPrices'][$i] = ['price' => $tariffs[$i + 5]['price'], 'subcategory_id' => $tariffs[$i + 5]['subcategory_id']];
                }
                $data['minimalWithdrawalPrice'] = $tariffs[47]['price'];
                break;
            case 2:
                $data['agentPrice'] = $tariffs[85]['price'];
                $data['beginnerPrice'] = $tariffs[82]['price'];
                $data['advancedPrice'] = $tariffs[83]['price'];
                $data['vipPrice'] = $tariffs[84]['price'];
                $data['agentPercent'] = $tariffs[86]['percent'];
                $data['managerPercent'] = $tariffs[87]['percent'];
                for ($i = 0; $i < 28; $i++) {
                    $data['extendPrices'][$i] = ['price' => $tariffs[$i + 53]['price'], 'category_id' => $tariffs[$i + 53]['superwork_category_id']];
                }
                $data['minimalWithdrawalPrice'] = $tariffs[50]['price'];
                break;
            default:
                $data['agentPrice'] = $tariffs[0]['price'];
                $data['colorPrice'] = $tariffs[1]['price'];
                $data['topPrice'] = $tariffs[2]['price'];
                $data['partPrice'] = $tariffs[3]['price'];
                $data['agentPercent'] = $tariffs[4]['percent'];
                for ($i = 0; $i < 41; $i++) {
                    $data['extendPrices'][$i] = ['price' => $tariffs[$i + 5]['price'], 'subcategory_id' => $tariffs[$i + 5]['subcategory_id']];
                }
                $data['minimalAdminPercent'] = $tariffs[46]['percent'];
                $data['minimalWithdrawalPrice'] = $tariffs[47]['price'];
                break;
        }

        return $data;
    }

    public function getFull(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Tariff::where('application_id', $request['application_id'])->get();

        return $data;
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Tariff::create($request->all());

        return $data;
    }

    public function update(Request $request)
    {
        $request->validate([
            'price' => 'nullable|string|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'percent' => 'nullable|string|regex:/^[0-9]+(\.[0-9][0-9]?)?$/'
        ]);

        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
        }

        $data = Tariff::find($request->id);

        if ($data) {
            if ($data['price'] !== null) {
                if ($request['price'] === null or empty($request['price'])) {
                    throw new BadRequestHttpException();
                } else {
                    $data->update(['price' => $request['price']]);
                }
            } elseif ($data['percent'] !== null) {
                if ($request['percent'] === null or empty($request['percent'])) {
                    throw new BadRequestHttpException();
                } else {
                    $data->update(['percent' => $request['percent']]);
                }
            } else {
                throw new BadRequestHttpException();
            }
        } else {
            throw new BadRequestHttpException();
        }

        return response()->json(['status' => 'success'], 200);
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Tariff::destroy($request->id);

        if ($data == 1) {$data = 'Success';} else {$data = 'Error';}

        return $data;
    }
}
