<?php

namespace App\Http\Controllers\Transaction;

use App\Exceptions\SimpleException;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class TransactionController extends Controller
{
    public function index(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Просмотр истории транзакций'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Transaction::orderBy('id')->with('user')->with('sourceUser');

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->get();

        return $data;
    }

    public function sum(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Просмотр баланса сервисов'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Transaction::where('id', '!=', null);
        if ($request['date_from']) {
            $data = $data->whereDate('created_at', '>=', $request['date_from']);
        }
        if ($request['date_to']) {
            $data = $data->whereDate('created_at', '<=', $request['date_to']);
        }
        if ($request['application_id']) {
            $data = $data->where('application_id', $request['application_id']);
        }
        $response['income'] = $data->where(function ($q) {
            $q->where('action', 'User increaseBalanceCurrentUser3DS')->orWhere('action', 'User increaseBalanceCurrentUser');
        })->sum('amount');

        $data = Transaction::where('id', '!=', null);
        if ($request['date_from']) {
            $data = $data->whereDate('created_at', '>=', $request['date_from']);
        }
        if ($request['date_to']) {
            $data = $data->whereDate('created_at', '<=', $request['date_to']);
        }
        if ($request['application_id']) {
            $data = $data->where('application_id', $request['application_id']);
        }
        $response['outcome'] = $data->where('action', 'Withdrawal accept')->sum('amount');

        $data = Transaction::where('id', '!=', null);
        if ($request['date_from']) {
            $data = $data->whereDate('created_at', '>=', $request['date_from']);
        }
        if ($request['date_to']) {
            $data = $data->whereDate('created_at', '<=', $request['date_to']);
        }
        if ($request['application_id']) {
            $data = $data->where('application_id', $request['application_id']);
        }
        $response['all'] = $data->sum('amount');

        return $response;
    }

    public function filter(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Просмотр истории транзакций'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        if ($request['type'] == 'income') {
            $data = Transaction::where('action', 'LIKE', '%increaseBalance%')
                ->where('application_id', $request['application_id'])
                ->with('user')->with('sourceUser');
        } else if ($request['type'] == 'outcome') {
            $data = Transaction::where('action', 'Withdrawal accept')
                ->where('application_id', $request['application_id'])
                ->with('user')->with('sourceUser');
        } else {
            $data = Transaction::where('application_id', $request['application_id'])
                ->with('user')->with('sourceUser');
        }

        if ($request['date_from']) {
            $data = $data->whereDate('created_at', '>=', $request['date_from']);
        }
        if ($request['date_to']) {
            $data = $data->whereDate('created_at', '<=', $request['date_to']);
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->orderByDesc('created_at')->get();

        return $data;
    }

    public function revert(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $originalTransaction = Transaction::find($request['transaction_id']);
        $transactionUser = User::find($originalTransaction['user_id']);

        if ($originalTransaction['type'] == 1) {
            $transactionUser->update(['balance' => $transactionUser['balance'] - $originalTransaction['amount']]);
            $data = Transaction::create([
                'amount' => $originalTransaction['amount'],
                'rate' => $originalTransaction['rate'],
                'action' => 'Transaction revert',
                'currency' => $originalTransaction['currency'],
                'type' => 2,
                'comment' => $request['comment'],
                'user_id' => $originalTransaction['user_id'],
                'reverted_transaction_id' => $originalTransaction['id'],
                'application_id' => $originalTransaction['application_id']
            ]);
            switch ($request['application_id']) {
                case 1:
                    Notification::send($transactionUser, new \App\Notifications\Microphone\TransactionRevert(['user' => $transactionUser]));
                    break;
                case 2:
                    Notification::send($transactionUser, new \App\Notifications\SuperWork\TransactionRevert(['user' => $transactionUser]));
                    break;
            }
        } else if ($originalTransaction['type'] == 2) {
            $transactionUser->update(['balance' => $transactionUser['balance'] + $originalTransaction['amount']]);
            $data = Transaction::create([
                'amount' => $originalTransaction['amount'],
                'rate' => $originalTransaction['rate'],
                'action' => 'Transaction revert',
                'currency' => $originalTransaction['currency'],
                'type' => 1,
                'comment' => $request['comment'],
                'user_id' => $originalTransaction['user_id'],
                'reverted_transaction_id' => $originalTransaction['id'],
                'application_id' => $originalTransaction['application_id']
            ]);
            switch ($request['application_id']) {
                case 1:
                    Notification::send($transactionUser, new \App\Notifications\Microphone\TransactionRevert(['user' => $transactionUser]));
                    break;
                case 2:
                    Notification::send($transactionUser, new \App\Notifications\SuperWork\TransactionRevert(['user' => $transactionUser]));
                    break;
            }
        } else {
            throw new SimpleException('No applicable transactions');
//            return response()->json(['message' =>'No applicable transactions'], 400);
        }

        return $data;
    }
}
