<?php

namespace App\Http\Controllers\User;

use App\Exceptions\SimpleException;
use App\Http\Requests\NeedApplicationIdRequest;
use App\Http\Requests\TransactionUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\Application;
use App\Models\Microphone\Ad;
use App\Http\Controllers\Controller;
use App\Models\Part;
use App\Models\Referral;
use App\Models\Role;
use App\Models\Tariff;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Withdrawal;
use App\Services\SearchService;
use App\Services\TransactionService;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use GuzzleHttp;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use function PHPUnit\Framework\isEmpty;

class UserController extends Controller
{

    public $user;
    public $contact;

    public function __construct(UserService $user, SearchService $contact)
    {
        $this->user = $user;
        $this->contact = $contact;
    }

    public function index(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
        }

        $data = User::orderByDesc('id')->with('roles')->with('city');

        $data = $data->whereHas('applications', function ($query) use ($request) {
            $query->where('application_id', $request['application_id']);
        });

        switch ($request['type']) {
            case (1):
                $data = $data->whereHas('roles', function ($query) use ($request) {
                    $query->where('role_id', 2)->orWhere('role_id', 3);
                });
                break;
            case (2):
                $data = $data->whereHas('roles', function ($query) use ($request) {
                    $query->where('role_id', 1)->orWhere('role_id', 4)->orWhere('role_id', 5);
                });
                break;
            case (3):
                $data = $data->whereHas('parts');
                break;
        }

        if ($request['search']) {
            $data = $data
                ->where('login', 'LIKE', '%' . $request['search'] . '%')
                ->orWhere('email', 'LIKE', '%' . $request['search'] . '%')
                ->orWhere('phone', 'LIKE', '%' . $request['search'] . '%')
                ->orWhere('name', 'LIKE', '%' . $request['search'] . '%')
                ->orWhere('surname', 'LIKE', '%' . $request['search'] . '%')
                ->orWhere('patronym', 'LIKE', '%' . $request['search'] . '%')
                ->orWhere('ownership_name', 'LIKE', '%' . $request['search'] . '%')
                ->orWhere('iban', 'LIKE', '%' . $request['search'] . '%')
                ->orWhere('iin', 'LIKE', '%' . $request['search'] . '%');
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->get();

        foreach ($data as $singleUser) {
            $singleUser['income'] = User::find($singleUser['id'])
                ->transactions()
                ->where('type', '1')
                ->where('action', 'LIKE', '%increaseBalance%')
                ->sum('amount');
            $singleUser['outcome'] = User::find($singleUser['id'])
                ->transactions()
                ->where('type', '2')
                ->where('action', 'Money withdrawal')
                ->sum('amount');
            $singleUser['spent'] = User::find($singleUser['id'])
                ->transactions()
                ->where('type', '2')
                ->where('action', '!=', 'Money withdrawal')
                ->sum('amount');
            $singleUser['received'] = User::find($singleUser['id'])
                ->transactions()
                ->where('type', '1')
                ->where('action', 'NOT LIKE', '%increaseBalance%')
                ->sum('amount');

            $referrerStatus = $singleUser->referrerStatuses()->where('application_id', $request['application_id'])->first();
            if ($referrerStatus) {
                $singleUser['agent_expiration'] = $referrerStatus['agent_expiration'];
                $singleUser['agent_status'] = $referrerStatus['agent_status'];
                $singleUser['referral_token'] = $referrerStatus['referral_token'];
                $singleUser['referral_expiration'] = $referrerStatus['referral_expiration'];
            } else {
                $singleUser['agent_expiration'] = null;
                $singleUser['agent_status'] = 0;
                $singleUser['referral_token'] = null;
                $singleUser['referral_expiration'] = null;
            }

            $parts = $singleUser->parts()->where('application_id', $request['application_id']);
            $singleUser['parts_sum'] = $parts->sum('percent');
        }

        return $data;
    }

    public function getUserParts(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Part::where('user_id', $request['user_id'])->where('application_id', $request['application_id']);

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->get();

        return $data;
    }

    public function getUserReferrals(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Referral::where('referrer_id', $request['user_id'])
            ->where('application_id', $request['application_id'])
            ->with('referralUser');

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->get();

        return $data;
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $phoneUtil = PhoneNumberUtil::getInstance();
        $numberPrototype = $phoneUtil->parse($request->phone, 'RU');
        $request['phone'] = $phoneUtil->format($numberPrototype, PhoneNumberFormat::E164);
        if ($request['document_issue_date']) {
            $request['document_issue_date'] = date('Y-m-d', strtotime($request['document_issue_date']));
        }
        $request['part_date'] = str_replace('/', '.', $request['part_date']);
        $request['part_date'] = date('Y-m-d', strtotime($request['part_date']));

        $user = User::withTrashed()->where('phone', $request['phone'])->first();

        if (!$user) {
            $user = User::create($request->except('password'));
        } else {
            if ($user->trashed()) {
                $user->restore();
            }
            $user->update($request->except('password'));
        }


        if ($request['password']) {
            $user->update(['password' => Hash::make($request->password)]);
        }

        if ($request['application_ids']) {
            foreach ($request['application_ids'] as $application) {
                switch ($application) {
                    case 1:
                        if ($user->roles->contains(2) != 1) $user->roles()->attach(2);
                        if ($user->applications->contains(1) != 1) $user->applications()->attach(1);
                        break;
                    case 2:
                        if ($user->roles->contains(6) != 1) $user->roles()->attach(6);
                        if ($user->applications->contains(2) != 1) $user->applications()->attach(2);
                        break;
                }
            }
        }

        if ($request->role_ids) {
            foreach ($request->role_ids as $role) {
                if ($user->roles->contains($role) != 1) $user->roles()->attach($role);
            }
        }

        if ($request['part']) {
            $adminPart = Part::where('user_id', 1)->first();
            $adminPart->update(['percent' => $adminPart['percent'] - $request['part']]);
            Transaction::create(['amount' => 0, 'user_id' => $user['id'], 'action' => 'Part buy', 'type' => 2, 'application_id' => $request['part_application_id']]);

            if ($request['part_date']) {
                Part::create(['percent' => $request['part'], 'user_id' => $user['id'], 'created_at' => $request['part_date'], 'application_id' => $request['part_application_id']]);
            } else {
                Part::create(['percent' => $request['part'], 'user_id' => $user['id'], 'application_id' => $request['part_application_id']]);
            }
        }

        return $user;
    }

    public function update(UpdateUserRequest $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
        }

        $data = User::find($request->id);

        if ($data) {
            $data->update($request->except('password'));
            $data->update(['password' => Hash::make($request->password)]);
            foreach ($data->roles as $item) {
                if ($item['id'] == 1 || $item['id'] == 4) $data->roles()->detach($item['id']);
            }
            $data = User::find($request->id);
            if ($request['role_ids']) {
                foreach ($request->role_ids as $role) {
                    if ($data->roles->contains($role) != 1) $data->roles()->attach($role);
                }
            }
            $data = 'Success';
        } else {
            throw new BadRequestHttpException();
        }
        return $data;
    }

    public function destroy(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $record = User::find($request['id']);

        if ($record) {
            $record->delete();

            $data = response()->json([
                'message' => 'Success'], 200);
        } else {
            throw new BadRequestHttpException();
//            $data = response()->json([
//                'message' => 'Bad request'], 400);
        }

        return $data;
    }

    public function getCurrentUser(Request $request)
    {
        $data = $this->user->getCurrentUser($request);

        return $data;
    }

    public function updateCurrentUser(Request $request)
    {
        $user = Auth::user();

        $phoneUtil = PhoneNumberUtil::getInstance();
        if ($request['additional_phone']) {
            $numberPrototype = $phoneUtil->parse($request['additional_phone'], 'RU');
            $request['additional_phone'] = $phoneUtil->format($numberPrototype, PhoneNumberFormat::E164);
        }
        if ($request['contact_phone']) {
            $numberPrototype = $phoneUtil->parse($request['contact_phone'], 'RU');
            $request['contact_phone'] = $phoneUtil->format($numberPrototype, PhoneNumberFormat::E164);
        }

        $data = User::where('id', '=', Auth::user()->id)
            ->update([
                'phone' => $request->phone ?? Auth::user()->phone,
                'name' => $request->name,
                'surname' => $request->surname,
                'patronym' => $request->patronym,
                'status' => $request->status,
                'youtube' => $request->youtube,
                //'watsapp' =>  $request->watsapp,
                'facebook' => $request->facebook,
                'instagram' => $request->instagram,
                'site' => $request->site,
                'email' => $request->email,
                'address' => $request->address,
                'iin' => $request->iin,
                'bank' => $request->bank,
                'birth_date' => $request->birth_date,
                'city_id' => $request->city_id,
                'iban' => $request->iban,
                'kz_resident' => $request->kz_resident ?? 1,
                'document_type' => $request->document_type,
                'document_issued_by' => $request->document_issued_by,
                'document_issue_date' => $request->document_issue_date,
                'document_number' => $request->document_number,
            ]);
        if ($request->hasFile('avatar')) {
            Storage::disk('public')->delete($user->avatar);
            $path = $request->avatar->store('avatars', ['disk' => 'public']);

            $user->update(['avatar' => $path]);
        }

        if ($data == 1) {
            $user = User::where('id', '=', Auth::user()->id)
                ->select('id', 'name', 'surname', 'avatar', 'status')
                ->first();
            $user->full_link = URL::to('/') . '/' . $user->avatar;
            return $user;
        } else {
            return response()->json('Error update user', 403);
        }

    }

    public function getTransactionsCurrentUser(Request $request)
    {
        $user = Auth::user();

        $data = $user->transactions()
            ->orderByDesc('created_at');

        $data->where('user_id', '=', Auth::user()->id);

        switch ($request->get('time')) {
            case $request->get('time') == 1 || $request->get('time') == null || empty($request->get('time')): //full time
                break;
            case $request->get('time') == 2: //last 3 days
                $data->where('created_at', '>', Carbon::now()->subDays(3));
                break;
            case $request->get('time') == 3: //last week
                $data->where('created_at', '>', Carbon::now()->subDays(7));
                break;
            case $request->get('time') == 4: //last week
                $data->where('created_at', '>', Carbon::now()->subDays(30));
                break;
        }

        switch ($request->get('action')) {
            case $request->get('action') == 1 || $request->get('action') == null || empty($request->get('action')): //все операции
                $dataWithdrawal = Withdrawal::where('user_id', '=', Auth::user()->id)
                    ->where('status', '!=', 1)
                    ->get();
                break;
            case $request->get('action') == 2: //снятие денег
                $data->where('action', '=', 'Withdrawal accept');
                $dataWithdrawal = Withdrawal::where('user_id', '=', Auth::user()->id)
                    ->where('status', '!=', 1)
                    ->get();
                break;
            case $request->get('action') == 3: //пополнение баланса
                $data->where(function ($q) {
                    $q->where('action', 'User increaseBalanceCurrentUser')
                        ->orWhere('action', 'User increaseBalanceCurrentUser3DS');
                });
                break;
            case $request->get('action') == 4: //покупка рекламы
                $data->where('action', '=', 'Ad publish');
                break;
            case $request->get('action') == 5: //заработок агента
                $data->where('action', 'LIKE', '%Referral%');
                break;
            case $request->get('action') == 6: //инвестиционный доход
                $data->where('action', '=', '');
                break;
            case $request->get('action') == 7: //покупка доли
                $data->where('action', 'Part buy');
                break;
            case $request->get('action') == 8: //продажа доли
                $data->where('action', '=', '');
                break;
        }

        if ($request['application_id']) {
            $data = $data->where('application_id', $request['application_id']);
        }

        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->get();

        for ($i = 0; $i < count($data); $i++) {
            $transaction = Transaction::where('id', $data[$i]['id'])->with('sourceUser')->first();
            if ($transaction['sourceUser']) {
                if ($transaction['sourceUser']['name'] && !$transaction['sourceUser']['surname']) {
                    $data[$i]['fullName'] = $transaction['sourceUser']['name'];
                } else if (!$transaction['sourceUser']['name'] && $transaction['sourceUser']['surname']) {
                    $data[$i]['fullName'] = $transaction['sourceUser']['surname'];
                } else if ($transaction['sourceUser']['name'] && $transaction['sourceUser']['surname']) {
                    $data[$i]['fullName'] = $transaction['sourceUser']['name'] . ' ' . $transaction['sourceUser']['surname'];
                } else {
                    $data[$i]['fullName'] = null;
                }
            }
        }
        if ($request->get('action') == 1 || $request->get('action') == null || empty($request->get('action')) || $request->get('action') == 2) {
            $data->map(function ($dataStatus) {
                $dataStatus->status = 1;
            });
        }
        $related = new Collection();
        $related = $related->merge($data);
        if (isset($dataWithdrawal)) {
            $dataWithdrawal->map(function ($withdraw) {
                $withdraw->action = 'Withdrawal accept';
            });
            $related = $related->merge($dataWithdrawal);
        }

        $related->sortByDesc('created_at');

        $paginateRelated = $related->forPage($request->get('pagination_start'), $request->get('pagination_amount'));

        return response()->json($paginateRelated, 202);
    }

    public function getRolesCurrentUser()
    {
        $user = Auth::user();

        $arr = $user->roles()->get()->toArray();
        $data = [];
        foreach ($arr as $item) {
            array_push($data, $item['id']);
        }

        return $data;
    }

    public function addRole(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
        }

        $user = User::find($request['user_id']);
        if ($user->roles->contains($request['role_id']) != 1) {
            $user->roles()->attach($request['role_id']);
            return response()->json('Attach successful', 200);
        } else {
            throw new SimpleException('Unable to attach  this role');
        }
    }

    public function removeRole(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
        }

        $user = User::find($request['user_id']);
        if ($user->roles->contains($request['role_id']) == 1) {
            $user->roles()->detach($request['role_id']);
            return response()->json('Detach successful', 200);
        } else {
            throw new SimpleException('Unable to detach this role');
        }
    }

    public function addApplication(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
        }

        $user = User::find($request['user_id']);
        if ($user->applications->contains($request['application_id']) != 1) {
            $user->applications()->attach($request['application_id']);
            return response()->json('Attach successful', 200);
        } else {
            throw new SimpleException('Unable to attach');
        }
    }

    public function removeApplication(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
        }

        $user = User::find($request['user_id']);
        if ($user->applications->contains($request['application_id']) == 1) {
            $user->applications()->detach($request['application_id']);
            return response()->json('Detach successful', 200);
        } else {
            throw new SimpleException('Unable to detach');
        }
    }

    public function addAd(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
        }

        $user = User::find($request['user_id']);
        if ($user->ads->contains($request['ad_id']) != 1) {
            $user->ads()->attach($request['ad_id']);
            return response()->json('Attach successful', 200);
        } else {
            throw new SimpleException('Unable to attach');
        }
    }

    public function removeAd(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
        }

        $user = User::find($request['user_id']);
        if ($user->ads->contains($request['ad_id']) == 1) {
            $user->ads()->detach($request['ad_id']);
            return response()->json('Detach successful', 200);
        } else {
            throw new SimpleException('Unable to detach');
        }
    }

    public function removeAvatarCurrentUser()
    {
        $user = Auth::user();

        if ($user['avatar'] != null) {
            Storage::disk('public')->delete($user->avatar);

            $user->update(['avatar' => null]);
            $data = response()->json('Success', 200);
        } else {
            throw new SimpleException('No avatar to delete');
        }

        return $data;
    }

    public function increaseBalanceCurrentUser(Request $request)
    {
        if ($request->amount && (int)$request->amount < 300) {
            throw new SimpleException('Введите сумму больше 300.00₸');
        }

        $user = Auth::user();

        $client = new GuzzleHttp\Client();

//        $paymentResult = $client->request('POST', 'https://api.cloudpayments.ru/payments/cards/charge', [
//            'form_params' => [
//                'Amount' => 10,
//                'Currency' => 'RUB',
////                'IpAddress' => $request['ip'],
//                'InvoiceId' => '1234567',
//                'Description' => 'Оплата товаров в example.com',
//                'AccountId' => 'user_x',
//                'Name' => 'CARDHOLDER NAME',
//                'CardCryptogramPacket' => '01492500008719030128SMfLeYdKp5dSQVIiO5l6ZCJiPdel4uDjdFTTz1UnXY+3QaZcNOW8lmXg0H670MclS4lI+qLkujKF4pR5Ri+T/E04Ufq3t5ntMUVLuZ998DLm+OVHV7FxIGR7snckpg47A73v7/y88Q5dxxvVZtDVi0qCcJAiZrgKLyLCqypnMfhjsgCEPF6d4OMzkgNQiynZvKysI2q+xc9cL0+CMmQTUPytnxX52k9qLNZ55cnE8kuLvqSK+TOG7Fz03moGcVvbb9XTg1oTDL4pl9rgkG3XvvTJOwol3JDxL1i6x+VpaRxpLJg0Zd9/9xRJOBMGmwAxo8/xyvGuAj85sxLJL6fA==',
//            ]
//        ]);
//
//        return $paymentResult;

        try {
            $paymentResult = $client->request('POST', 'https://api.cloudpayments.ru/payments/cards/charge', [
                'auth' => [
                    "pk_9ce5b7283ffef43199c46cf1de785",
                    "29984d607d212e9b279932c21437b4ec"
                ],
                'form_params' => [
                    'Amount' => $request['amount'],
                    'Currency' => $request['currency'],
                    'IpAddress' => $request['ip'],
                    'Name' => $request['name'],
                    'CardCryptogramPacket' => $request['cryptogram'],
                ]
            ]);
            $paymentResult = json_decode($paymentResult->getBody(), true);
            if ($paymentResult['Success']) {
                $user = Auth::user();
                $newBalance = $user['balance'] + $paymentResult['Model']['Amount'] * $request['rate'];
                $user->update(['balance' => $newBalance]);
                Transaction::create(['amount' => $paymentResult['Model']['Amount'] * $request['rate'],
                    'user_id' => $user['id'],
                    'action' => 'User increaseBalanceCurrentUser',
                    'type' => 1,
                    'rate' => $request['rate'],
                    'currency' => $paymentResult['Model']['Currency'],
                    'application_id' => $request['application_id']]);

                return $user;
            } else {
                return $paymentResult;
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
//            $code = $e->getCode();
            return response()->json([
                'type' => 'standard_error',
                'message' => $message,
                'success' => false
            ]);
        }
    }

    public function increaseBalanceCurrentUser3DS(Request $request)
    {

        $user = Auth::user();

        $client = new GuzzleHttp\Client();

        try {
            $paymentResult = $client->request('POST', 'https://api.cloudpayments.ru/payments/cards/post3ds', [
                'auth' => [
                    "pk_9ce5b7283ffef43199c46cf1de785",
                    "29984d607d212e9b279932c21437b4ec"
                ],
                'form_params' => [
                    'TransactionId' => $request['TransactionId'],
                    'PaRes' => $request['PaRes'],
                ]
            ]);
            $paymentResult = json_decode($paymentResult->getBody(), true);
            if ($paymentResult['Success']) {
                $user = Auth::user();
                $newBalance = $user['balance'] + $paymentResult['Model']['Amount'] * $request['rate'];
                $user->update(['balance' => $newBalance]);
                Transaction::create(['amount' => $paymentResult['Model']['Amount'] * $request['rate'],
                    'user_id' => $user['id'],
                    'action' => 'User increaseBalanceCurrentUser3DS',
                    'type' => 1,
                    'rate' => $request['rate'],
                    'currency' => $paymentResult['Model']['Currency'],
                    'application_id' => $request['application_id']]);

                return $user;
            } else {
                return $paymentResult;
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
//            $code = $e->getCode();
            return response()->json([
                'type' => 'standard_error',
                'message' => $message,
                'success' => false
            ]);
        }
    }

    public function becomeAgent(Request $request)
    {
        $user = Auth::user();

        $token = (string)Str::uuid();

        $sum = Tariff::where('action', 'Становление агентом')->where('application_id', $request['application_id'])->first()['price'];

        if ($user['balance'] >= $sum) {
            $balance = $user['balance'] - $sum;
            Transaction::create(['amount' => $sum, 'user_id' => $user['id'], 'action' => 'User becomeAgent', 'type' => 2, 'application_id' => $request['application_id']]);
            $user->update(['balance' => $balance]);

            $agentRole = Role::where('name', 'агент')->where('application_id', $request['application_id'])->first()['id'];
            $managerRole = Role::where('name', 'менеджер')->where('application_id', $request['application_id'])->first()['id'];

            if ($user->referrerStatuses()->where('application_id', $request['application_id'])->get()->isEmpty()) {
                $expirationDate = date('Y-m-d', strtotime(now() . '+1 year'));

                Referral::create([
                    'agent_expiration' => $expirationDate,
                    'agent_status' => 1,
                    'referral_token' => $token,
                    'user_id' => $user['id'],
                    'application_id' => $request['application_id']]);
            } else {
                $referrerStatus = Referral::where('user_id', $user['id'])->where('application_id', $request['application_id'])->first();

                if ($referrerStatus['agent_status'] == 0 && $referrerStatus['referrer_id']) {
                    $referrer = User::find($referrerStatus['referrer_id']);
                    if ($referrer->roles->contains($managerRole) == 1) {
                        managerPartPayment($user, $sum, 'Referral User becomeAgent', $request['application_id']);
                    }
                }

                if ($referrerStatus['agent_expiration'] > now()) {
                    $expirationDate = date('Y-m-d', strtotime($referrerStatus['agent_expiration'] . '+1 year'));
                } else {
                    $expirationDate = date('Y-m-d', strtotime(now() . '+1 year'));
                }

                if ($referrerStatus['token']) {
                    $referrerStatus->update([
                        'agent_expiration' => $expirationDate,
                    ]);
                } else {
                    $referrerStatus->update([
                        'agent_expiration' => $expirationDate,
                        'referral_token' => $token,
                    ]);
                }

                if ($referrerStatus['agent_status'] == 0) {
                    $referrerStatus->update([
                        'agent_status' => 1,
                    ]);
                }
            }

            if ($user->roles->contains($agentRole) != 1) {
                $user->roles()->attach($agentRole);
            }

            return response()->json(['message' => 'Success', 'success' => true], 200);
        } else {
            return response()->json(['message' => 'Not enough money on balance', 'success' => false], 400);
        }
    }

    public function makeAgent(Request $request)
    {
        $admin = Auth::user();

        if (!checkPermissions($admin, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
        }

        $user = User::find($request['id']);

        $token = (string)Str::uuid();

        $agentRole = Role::where('name', 'агент')->where('application_id', $request['application_id'])->first()['id'];
        $managerRole = Role::where('name', 'менеджер')->where('application_id', $request['application_id'])->first()['id'];
        $expirationDate = $request['date'];

        if ($user->referrerStatuses()->where('application_id', $request['application_id'])->get()->isEmpty()) {
            if ($request['manager'] == 1) {
                Referral::create([
                    'agent_expiration' => $expirationDate,
                    'agent_status' => 2,
                    'referral_token' => $token,
                    'user_id' => $user['id'],
                    'application_id' => $request['application_id']]);
            } else {
                Referral::create([
                    'agent_expiration' => $expirationDate,
                    'agent_status' => 1,
                    'referral_token' => $token,
                    'user_id' => $user['id'],
                    'application_id' => $request['application_id']]);
            }
        } else {
            $referrerStatus = Referral::where('user_id', $user['id'])->where('application_id', $request['application_id'])->first();

            if ($referrerStatus['token']) {
                $referrerStatus->update([
                    'agent_expiration' => $expirationDate,
                ]);
            } else {
                $referrerStatus->update([
                    'agent_expiration' => $expirationDate,
                    'referral_token' => $token,
                ]);
            }

            if ($request['manager'] == 1) {
                $referrerStatus->update([
                    'agent_status' => 2,
                ]);
            } else {
                $referrerStatus->update([
                    'agent_status' => 1,
                ]);
            }
        }

        if ($user->roles->contains($agentRole) != 1) {
            $user->roles()->attach($agentRole);
        }

        if ($user->roles->contains($managerRole) != 1 && $request['manager'] == 1) {
            $user->roles()->attach($managerRole);
        } else if ($user->roles->contains($managerRole) == 1 && $request['manager'] == 0) {
            $user->roles()->detach($managerRole);
        }

        return response()->json(['message' => 'Success', 'success' => true], 200);
    }

    public function removeAgent(Request $request)
    {
        $admin = Auth::user();

        if (!checkPermissions($admin, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
        }

        $user = User::find($request['id']);
        $referralStatus = Referral::where('user_id', $request['id'])->where('application_id', $request['application_id'])->first();

        $agentRole = Role::where('name', 'агент')->where('application_id', $request['application_id'])->first()['id'];
        $managerRole = Role::where('name', 'менеджер')->where('application_id', $request['application_id'])->first()['id'];

        if ($user->roles->contains($agentRole) == 1) {
            $user->roles()->detach($agentRole);
        }
        if ($user->roles->contains($managerRole) == 1) {
            $user->roles()->detach($managerRole);
        }

        $referralStatus->update(['agent_expiration' => null, 'agent_status' => 0]);

        return response()->json(['message' => 'Success', 'success' => true], 200);
    }

    public static function activateReferral($user, $token)
    {
        $referrerStatus = Referral::where('referral_token', $token)->first();

        if ($referrerStatus) {
            $referralStatus = Referral::where('user_id', $user['id'])->where('application_id', $referrerStatus['application_id'])->first();
            if (!$referralStatus) {
                $referralStatus = Referral::create(['user_id' => $user['id'], 'application_id' => $referrerStatus['application_id']]);
            }

            if ($referralStatus['referrer_id']) {
                throw new SimpleException('Referrer already applied.');
            }

            if ($referrerStatus['user_id'] == $user['id']) {
                throw new SimpleException('Unable to self-bind.');
            }

            if ($referrerStatus['agent_expiration'] >= now()) {
                $referralStatus->update(['referrer_id' => $referrerStatus['user_id'],
                    'referral_expiration' => date('Y-m-d', strtotime(now() . '+1 year'))]);
            } else {
                throw new SimpleException('Unavailable token sent.');
            }
            return $user;
        } else {
            throw new SimpleException('Unavailable token sent.');
        }
    }

    public function count(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
        }

        switch ($request['type']) {
            case ('offline'):
                $data = User::where('last_visit', '<', Carbon::now()->subMinutes(15)->toDateTimeString())->count();
                break;
            case ('online'):
                $data = User::where('last_visit', '>', Carbon::now()->subMinutes(15)->toDateTimeString())->count();
                break;
            case ('lastDay'):
                $data = User::where('last_visit', '>', Carbon::now()->subDay()->toDateTimeString())->count();
                break;
            case ('agents'):
                $data = User::where('agent_expiration', '>', Carbon::now())->count();
                break;
            case ('all'):
                $data = User::count();
                break;
            default:
                $data = User::count();
                break;
        }

        return response()->json($data, 200);
    }

    public function getById(Request $request)
    {
        $data = User::find($request['id']);

        return $data;
    }

    public function getByIdFull(Request $request)
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Доступ администратора к базе'])) {
            throw new AuthorizationException();
        }

        $data = User::where('id', $request['id'])->with('transactions')->first();

        return $data;
    }

    public function getAuthUserReferral(NeedApplicationIdRequest $request)
    {
        $parts = Part::where('user_id', '=', Auth::user()->id)
            ->where('application_id', '=', $request->get('application_id'))
            ->sum('percent');

        $capitalization = Application::where('id', '=', $request->get('application_id'))
            ->first();

        $yourLinkAgent = Referral::where('user_id', '=', Auth::user()->id)
            ->OrderBy('id', 'DESC')
            ->first();

        $referrals = Referral::where('referrer_id', '=', Auth::user()->id)
            ->where('application_id', '=', $request->get('application_id'))
            ->with('referralUser')
            ->paginate($request->limit);

        foreach ($referrals as $referral) {
            $transaction = Transaction::where('user_id', '=', $referral->referrer_id)
                ->where('source_user_id', $referral->user_id)
                ->where('action', 'LIKE', '%Referral%')
                ->where('application_id', $request->get('application_id'))
                ->sum('amount');

            $referral->transaction_to_reffener = $transaction;
        }

        return response()->json([
            'full_transaction_parts' => $referrals->sum('transaction_to_reffener'),
            'agent_expiration' => $yourLinkAgent->agent_expiration,
            'referral_count' => count($referrals),
            'referrals' => $referrals
        ], 202);
    }


    public function getChildrenReferralTransaction(Request $request)
    {
        $request->validate([
            'user_id' => 'required|numeric|exists:users,id',
            'application_id' => 'required|numeric|exists:applications,id',
        ]);

        $childrenUser = User::where('id', '=', $request->get('user_id'))
            ->wherehas('referralChildren', function ($query) use ($request) {
                $query->where('application_id', '=', $request->application_id);
            })
            ->firstOrFail();

        $transactionsReferralUser = Transaction::where('user_id', $childrenUser->referralChildren->referrer_id)
            ->where('source_user_id', $childrenUser->id)
            ->where('application_id', $request['application_id'])
            ->where('action', 'LIKE', '%Referral%')
            ->sum('amount');

        $transactions = Transaction::where('user_id', $childrenUser->referralChildren->referrer_id)
            ->where('source_user_id', $childrenUser->id)
            ->where('application_id', $request['application_id'])
            ->where('action', 'LIKE', '%Referral%')
            ->OrderBy('id', 'DESC')
            ->paginate($request->limit ?? 10);

        return response()->json([
            'user' => $childrenUser,
            'referral_add' => Carbon::parse($childrenUser->referralChildren->created_at)->format('Y-m-d'),
            'full_add_bonus' => $transactionsReferralUser,
            'count_transactions' => count($transactions),
            'transactions' => $transactions,
        ], 202);
    }

    public function getCurrentUserAgentStatus(NeedApplicationIdRequest $request)
    {
        $request->validate([
            'application_id' => 'required|numeric|exists:applications,id'
        ]);

        $user = Auth::user();

        $referrerStatus = $user->referrerStatuses()
            ->where('application_id', $request['application_id'])
            ->first();

        if (!$referrerStatus || $referrerStatus['agent_status'] == 0) {
            return response()->json([
                'is_agent' => false,
                'fee' => 0,
                'agent_expiration' => null
            ], 200);
        }

        $referralsIds = $user->referralsStatuses()->pluck('user_id');

        $fee = $user->transactions()
            ->whereIn('source_user_id', $referralsIds)
            ->where('application_id', $request['application_id'])
            ->pluck('amount')
            ->sum();

        $fee = floatval(number_format($fee, 2, '.', ''));

        if ($referrerStatus['agent_expiration'] < Carbon::now()) {
            return response()->json([
                'is_agent' => false,
                'fee' => $fee,
                'agent_expiration' => $referrerStatus['agent_expiration']
            ], 200);
        } else {
            return response()->json([
                'is_agent' => true,
                'fee' => $fee,
                'agent_expiration' => $referrerStatus['agent_expiration']
            ]);
        }
    }

    public function getCurrentUserContacts(Request $request)
    {
        $request->validate([
            'page' => 'nullable|numeric',
            'limit' => 'nullable|numeric'
        ]);

        $user = Auth::user();

        $contacts = [];
        foreach ($user->contacts as $contact) {
            if ($contact->contact_id != $user->id)
                $contacts[] = $contact->userContacts;
        }

        foreach ($contacts as $contact) {
            $contact->isContact = $this->contact->getNicknameContact($contact) == null ? 0 : 1;
            $contact->name = $this->contact->getNicknameContact($contact) == null ? $contact->name : $this->contact->getNicknameContact($contact);
            $contact->surname = $this->contact->getNicknameContact($contact) == null ? $contact->surneme : NULL;
            $contact->patronym = $this->contact->getNicknameContact($contact) == null ? $contact->surneme : NULL;
        }

        return response()->json(['contacts' => [
            'data' => $contacts,
            'total' => count($contacts)
        ]], 200);

    }


    public function getUserTransaction(TransactionUserRequest $request)
    {
        $transaction = Transaction::where('user_id', '=', Auth::user()->id)
            ->when(!empty($request->time), function ($query) use ($request) {
                switch ($request->get('time')) {
                    case $request->get('time') == 1 || $request->get('time') == null || empty($request->get('time')): //full time
                        break;
                    case $request->get('time') == 2: //last 3 days
                        $query->where('created_at', '>', Carbon::now()->subDays(3));
                        break;
                    case $request->get('time') == 3: //last week
                        $query->where('created_at', '>', Carbon::now()->subDays(7));
                        break;
                    case $request->get('time') == 4: //last week
                        $query->where('created_at', '>', Carbon::now()->subDays(30));
                        break;
                }
            })
            ->when(!empty($request->application_id), function ($query) use ($request) {
                $query->where('application_id', $request->application_id);
            })
            ->when(!empty($request->type), function ($query) use ($request) {

            })
            ->paginate($request->limit ?? 20);
        return response()->json([$transaction], 200);
    }

    public function search(Request $request)
    {
        return User::where('name', 'LIKE', '%' . $request->search . '%')->where('id', '!=', $request->user()->id)->get();
    }


//    public function addChat(Appeal $request)
//    {
//        User::find($request['user_id'])->chats()->attach($request['chat_id']);
//
//        return response()->json('Attach successful', 200);
//    }
//
//    public function removeChat(Appeal $request)
//    {
//        User::find($request['user_id'])->chats()->detach($request['chat_id']);
//
//        return response()->json('Detach successful', 200);
//    }
}
