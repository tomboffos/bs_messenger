<?php

namespace App\Http\Controllers\Withdrawal;

use App\Http\Controllers\Controller;
use App\Models\Tariff;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Withdrawal;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class WithdrawalController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if (!checkPermissions($user, ['Подтверждение вывода средств'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Withdrawal::all();

        return $data;
    }

    public function apply(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Запрос вывода средств'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $tariff = Tariff::where('action', 'Минимальная сумма вывода')->first()['price'];

        if ($user['balance'] >= $request['amount'] && $request['amount'] >= $tariff) {
            $data = Withdrawal::create($request->except('status', 'user_id'));

            $data->update(['user_id' => $user['id'], 'status' => 3]);

            return response()->json(['message' =>'Success', 'success' => true], 200);
        } else {
            return response()->json([
                'type' => 'standard_error',
                'message' =>'Not enough money on balance',
                'success' => false
            ], 400);
//            return response()->json(['message' =>'Not enough money on balance', 'success' => false], 400);
        }
    }

    public function getSelf() {
        $user = Auth::user();

        if (!checkPermissions($user, ['Запрос вывода средств'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Withdrawal::where('user_id', $user['id'])->get();

        return $data;
    }

    public function accept(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Подтверждение вывода средств'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $withdrawal = Withdrawal::find($request['id']);
        $user = User::where('id', $withdrawal['user_id'])->first();
        $sum = $withdrawal['amount'];

        if ($user['balance'] >= $sum) {
            $balance = $user['balance'] - $sum;
            Transaction::create(['amount' => $sum, 'user_id' => $user['id'], 'action' => 'Withdrawal accept', 'type' => 2, 'application_id' => $withdrawal['application_id']]);

            $withdrawal->update(['status' => 1]);

            $user->update(['balance' => $balance]);

            $withdrawal = Withdrawal::where('id',  $request['id'])->first();
            switch ($withdrawal['application_id']) {
                case 1:
                    Notification::send($user, new \App\Notifications\Microphone\WithdrawalAccept(['user' => $user]));
                    break;
                case 2:
                    Notification::send($user, new \App\Notifications\SuperWork\WithdrawalAccept(['user' => $user]));
                    break;
            }

            return response()->json(['message' =>'Success', 'success' => true], 200);
        } else {
            return response()->json([
                'type' => 'standard_error',
                'message' =>'Not enough money on balance',
                'success' => false
            ], 400);
//            return response()->json(['message' =>'Not enough money on balance', 'success' => false], 400);
        }
    }

    public function decline(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Подтверждение вывода средств'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $withdrawal = Withdrawal::find($request['id']);

        $withdrawal->update(['status' => 2, 'comment' => $request['comment']]);

        return $withdrawal;
    }

    public function sum(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Просмотр баланса сервисов'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Withdrawal::where('status', 3)->where('application_id', $request['application_id'])->sum('amount');

        return $data;
    }

    public function filter(Request $request) {
        $user = Auth::user();

        if (!checkPermissions($user, ['Подтверждение вывода средств'])) {
            throw new AuthorizationException();
//            return response()->json([
//                'message' => 'Forbidden'], 403);
        }

        $data = Withdrawal::with('user');

        if ($request['application_id']) {
            $data = $data->where('application_id', $request['application_id']);
        }

        if ($request['status']) {
            $data = $data->where('status', $request['status']);
        }


        if ($request['pagination_amount'] && $request['pagination_start']) {
            $data = $data->skip($request['pagination_start'] - 1)
                ->take($request['pagination_amount']);
        }

        $data = $data->get();

        return $data;
    }
}
