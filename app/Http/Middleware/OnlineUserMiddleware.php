<?php

namespace App\Http\Middleware;

use App\Events\OnlineEvent;
use App\Jobs\OnlineUserJob;
use App\Models\User;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class OnlineUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        User::where('id','=',Auth::user()->id)
            ->update(
                [
                    'last_visit' => Carbon::now()
                ]
            );
        return $next($request);
    }
}
