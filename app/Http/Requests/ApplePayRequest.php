<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApplePayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'receipt-data' => 'required',
            'test' => 'required',
            'amount' => 'required',
            'application_id' => 'required',
        ];
    }
}
