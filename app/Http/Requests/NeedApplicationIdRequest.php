<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NeedApplicationIdRequest extends FormRequest
{
    public function rules()
    {
        return [
            'application_id' => 'required|numeric|exists:applications,id'
        ];
    }
}
