<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSettingsChat extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'admin_media_send' => '',
            'admin_message_send'  => '',
            'admin_change_data' => '',
            'sound'  => '',
            'is_secret' => '',
            'secret_time' => '',
            'transfer_chat_message' => '',
        ];
    }
}
