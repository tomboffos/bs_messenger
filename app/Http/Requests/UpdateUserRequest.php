<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    public function rules()
    {
        return [
            'id' => 'required|numeric|exists:users,id',
            'name' => 'nullable|string',
            'surname' => 'nullable|string',
            'patronym' => 'nullable|string',
            'document_number' => 'nullable|string',
            'document_issue_date' => 'nullable|string',
            'document_issued_by' => 'nullable|string',
            'document_type' => ['nullable', 'numeric', Rule::in(1, 2)],
            'iin' => 'nullable|string',
            'iban' => 'nullable|string',
            'bank' => 'nullable|string',
            'kz_resident' => 'nullable|boolean',
            'phone' => 'nullable|string|unique:users,phone,'.$this['id'],'id',
            'role_ids' => 'nullable|array',
            'role_ids.*' => 'nullable|numeric',
            'login' => 'nullable|string',
            'password' => 'nullable|string',
            'email' => 'nullable|email|unique:users,email,'.$this['id'],'id',
            'gender' => 'nullable|numeric',
            'birth_date' => 'nullable|string',
            'balance' => 'nullable|numeric',
            'microphone_passed_exam' => 'nullable|numeric',
        ];
    }
}
