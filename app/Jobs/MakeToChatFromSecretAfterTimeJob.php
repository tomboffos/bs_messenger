<?php

namespace App\Jobs;

use App\Events\getIndexChat;
use App\Models\Messenger\MessengerChat;
use App\Models\User;
use App\Services\ChatService;
use App\Services\MessageService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MakeToChatFromSecretAfterTimeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $chat;
    public $messageChat;
    public $message;
    public $user;
    public function __construct(MessengerChat $chat,ChatService $messageChat,MessageService $message,User $user)
    {
        $this->chat = $chat;
        $this->messageChat = $messageChat;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->messageChat->setChat($this->chat);

        $this->message->newAction($this->chat, 'SetChat', $this->user);
    }


}
