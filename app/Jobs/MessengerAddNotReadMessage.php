<?php

namespace App\Jobs;

use App\Models\Messenger\MessengerMessage;
use App\Models\Messenger\MessengerMessageReadUser;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class MessengerAddNotReadMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $message;

    public $user;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(MessengerMessage $message,$user)
    {
        $this->message = $message;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        foreach ($this->user as $member){
            MessengerMessageReadUser::firstOrCreate([
                'message_id' => $this->message->id,
                'user_id' => $member->id,
                'chat_id' => $this->message->chat_id,
                'is_read' => 0,
            ]);
        }

    }
}
