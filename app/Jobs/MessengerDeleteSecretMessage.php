<?php

namespace App\Jobs;

use App\Events\DeletedMessageEvent;
use App\Events\getIndexChat;
use App\Events\NewMessage;
use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\MessengerMessage;
use App\Services\MessageService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;


class MessengerDeleteSecretMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public MessengerMessage $message;
    public MessageService $messageChat;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($message,$messageChat)
    {
       $this->message = $message;
       $this->messageChat = $messageChat;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()  {
        $chat = MessengerChat::where('id','=',$this->message->chat_id)
            ->first();
        broadcast(new DeletedMessageEvent($this->message->id,'deletedMessage',$chat->id,null));
    }
}
