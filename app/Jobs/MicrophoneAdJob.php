<?php

namespace App\Jobs;

use App\Models\User;
use App\Notifications\Microphone\AdExpired;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MicrophoneAdJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $ad;

    public function __construct($ad)
    {
        $this->ad = $ad;
    }

    public function handle()
    {
        $user = User::find($this->ad->creator_id);

        if ($user) {
            $user->notify(new AdExpired($this->ad));
        }
    }
}
