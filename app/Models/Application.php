<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $table = 'applications';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $guarded = [
        'id',
    ];

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'users_applications', 'application_id', 'user_id')->withTimestamps();
    }

    public function parts()
    {
        return $this->hasMany('App\Models\Part', 'application_id');
    }

    public function roles()
    {
        return $this->hasMany('App\Models\Role', 'application_id');
    }

    public function permissions()
    {
        return $this->hasMany('App\Models\Permission', 'application_id');
    }

    public function tariffs()
    {
        return $this->hasMany('App\Models\Tariff', 'application_id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction', 'application_id');
    }

    public function withdrawals()
    {
        return $this->hasMany('App\Models\Withdrawal', 'application_id');
    }

    public function playerIds()
    {
        return $this->hasMany('App\Models\PlayerId', 'application_id');
    }
}
