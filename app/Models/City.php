<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $guarded = [
        'id',
    ];

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id');
    }

    public function ads()
    {
        return $this->hasMany('App\Models\Microphone\Ad', 'city_id');
    }

    public function superWorkAds()
    {
        return $this->hasMany('App\Models\SuperWork\Ad', 'city_id');
    }

    public function users()
    {
        return $this->hasMany('App\Models\User', 'city_id');
    }

    public function translations()
    {
        return $this->hasMany('App\Models\CityTranslation', 'city_id');
    }
}
