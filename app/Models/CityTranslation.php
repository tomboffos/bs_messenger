<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CityTranslation extends Model
{
    protected $table = 'cities_translations';

    public function city()
    {
        return $this->belongsTo('App\Models\City', 'city_id');
    }
}
