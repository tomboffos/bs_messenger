<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $guarded = [
        'id',
    ];

    public function cities()
    {
        return $this->hasMany('App\Models\City', 'country_id');
    }

    public function translations()
    {
        return $this->hasMany('App\Models\CountryTranslation', 'country_id');
    }
}
