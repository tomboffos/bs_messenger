<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CountryTranslation extends Model
{
    protected $table = 'countries_translations';

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id');
    }
}
