<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currencies';

    protected $guarded = [
        'id',
    ];

    public function superWorkAds()
    {
        return $this->hasMany('App\Models\SuperWork\Ad', 'city_id');
    }
}
