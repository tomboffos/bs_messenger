<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExpirienceResume extends Model
{
    use HasFactory;

    protected $fillable = ['expirience_id','resume_id'];
}
