<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FranchiseClient extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'patronymic',
        'phone',
        'application_id'
    ];

    public function scopeSearch($q, $search)
    {
        $q->where(
            \DB::raw("CONCAT_WS(' ', last_name, first_name, patronymic, phone)"),
            'like',
            '%' . $search . '%'
        );
    }
}
