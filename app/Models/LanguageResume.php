<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LanguageResume extends Model
{
    use HasFactory;

    protected $fillable = ['language_id','resume_id'];
}
