<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use HasFactory;

    protected $fillable = ['file','user_id','category_id','type','duration','preload_photo'];

    public function media(){
        return $this->belongsTo('App\Models\MediaCategory','category_id','id');
    }

    public function message(){
        return $this->belongsToMany('App\Models\Messenger\MessengerMessage','messenger_media_messages','media_id','message_id');
    }
}
