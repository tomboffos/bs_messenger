<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MediaCategory extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function category(){
        return $this->hasMany('App\Models\Media','id','category_id');
    }
}
