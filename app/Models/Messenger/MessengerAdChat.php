<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerAdChat extends Model
{
    use HasFactory;

    protected $fillable = ['tariff_id','to_period_date','total_price','total_views'];

    public function ad(){
        return $this->hasMany('App\Models\Messenger\MessengerTariffChat','ad_id','id');
    }

    public function tariff(){
        return $this->belongsTo('App\Models\Tariff','tariff_id','id');
    }
}
