<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerBlockUser extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['user_id','block_user_id'];
}
