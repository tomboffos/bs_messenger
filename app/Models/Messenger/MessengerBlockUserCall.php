<?php

namespace App\Models\Messenger;

use App\Models\MessengerUserCategoryOrder;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerBlockUserCall extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['user_id','block_user_id'];

}
