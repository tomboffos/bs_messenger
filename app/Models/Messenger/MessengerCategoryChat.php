<?php

namespace App\Models\Messenger;

use App\Models\MessengerUserCategoryOrder;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerCategoryChat extends Model
{
    use HasFactory;

    protected $fillable = ['name','user_id','avatar','app_chat'];

    public function chat(){
        return $this->belongsToMany('App\Models\Messenger\MessengerChat','messenger_category_user_chats','category_id','chat_id','id');
    }

    public function userChat(){
        return $this->hasMany('App\Models\Messenger\MessengerCategoryUserChat','category_id','id');
    }

    public function users()
    {
        return $this->belongsToMany(
            User::class,
            MessengerUserCategoryOrder::class,
            'category_id',
            'user_id'
        );
    }
}
