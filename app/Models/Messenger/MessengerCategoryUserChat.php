<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerCategoryUserChat extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','chat_id','category_id'];

    public $timestamps= false;


    public function category(){
        return $this->hasOne('App\Models\Messenger\MessengerCategoryChat','id','category_id');
    }

    public function chat(){
        return $this->hasOne('App\Models\Messenger\MessengerChat','id','chat_id');
    }
}
