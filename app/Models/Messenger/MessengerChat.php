<?php

namespace App\Models\Messenger;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerChat extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'avatar', 'description', 'category_id', 'is_secret', 'is_private', 'site', 'email', 'instagram', 'whatsapp', 'facebook', 'youtube', 'is_conference','is_paid'];

    public function categoryChat()
    {
        return $this->belongsTo('App\Models\Messenger\MessengerCategoryChat', 'category_id', 'id');
    }

    public function ad()
    {
        return $this->hasMany('App\Models\Messenger\MessengerTariffChat', 'chat_id', 'id');
    }

    public function member()
    {
        return $this->hasMany('App\Models\Messenger\MessengerChatMember', 'chat_id', 'id');
    }

    public function category()
    {
        return $this->hasMany('App\Models\Messenger\MessengerCategoryUserChat', 'chat_id', 'id');
    }

    public function chatMembers()
    {
        return $this->belongsToMany(
            User::class,
            MessengerChatMember::class,
            'chat_id',
            'user_id');
    }

    public function settings()
    {
        return $this->hasOne('App\Models\Messenger\MessengerChatSettings', 'chat_id', 'id');
    }

    public function message()
    {
        return $this->hasMany('App\Models\Messenger\MessengerMessage', 'chat_id', 'id');
    }

    public function topMessage()
    {
        return $this->belongsTo('App\Models\Messenger\MessengerMessage', 'top_message_id', 'id');
    }


    public function role()
    {
        return $this->hasOne('App\Models\Messenger\MessengerChatRole', 'id', 'chat_id');
    }

    public function chat()
    {
        return $this->hasOne('App\Models\Messenger\MessengerCategoryUserChat', 'chat_id', 'id');
    }

    public function usersSettings()
    {
        return $this->belongsToMany(
            User::class,
            UserSettingsInChat::class,
            'chat_id',
            'user_id');
    }
}
