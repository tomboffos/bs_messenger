<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerChatMember extends Model
{
    use HasFactory;

    protected $fillable = ['chat_id','user_id','role','show_chat'];

    public $timestamps = false;
}
