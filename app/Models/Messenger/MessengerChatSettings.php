<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerChatSettings extends Model
{
    use HasFactory;

    protected $fillable = ['chat_id','admin_media_send','admin_message_send','admin_change_data','sound','transfer_chat_message','is_secret'];
}
