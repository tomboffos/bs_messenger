<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerChatUserSettings extends Model
{
    use HasFactory;

    protected $fillable = ['sound','vibration','user_id','show_phone','show_online'];

}
