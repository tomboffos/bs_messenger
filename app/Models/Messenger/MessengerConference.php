<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerConference extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','name'];


    public function member(){
        return $this->hasMany('App\Models\Messenger\MessengerConferenceMember','conference_id','id');
    }
}
