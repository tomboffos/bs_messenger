<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerConferenceChat extends Model
{
    use HasFactory;
}
