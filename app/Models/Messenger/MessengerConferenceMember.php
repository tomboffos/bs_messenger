<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerConferenceMember extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','conference_id'];
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }
}
