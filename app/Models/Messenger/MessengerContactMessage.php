<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerContactMessage extends Model
{
    use HasFactory;

    protected $fillable = ['message_id','contact_id'];

    public $timestamps = false;

}
