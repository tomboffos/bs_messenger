<?php

namespace App\Models\Messenger;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerContactUser extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','contact_id','nickname'];

    public function userContacts()
    {
        return $this->belongsTo(User::class,'contact_id');
    }

    public $timestamps = false;
}
