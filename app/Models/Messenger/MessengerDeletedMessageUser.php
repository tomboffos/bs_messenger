<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerDeletedMessageUser extends Model
{
    use HasFactory;

    public $timestamps = false;

    public $fillable = ['user_id','message_id'];

}
