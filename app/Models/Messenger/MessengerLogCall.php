<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerLogCall extends Model
{
    use HasFactory;

    protected $fillable = ['from_id','to_id','is_begin','time_call','type_call','messenger_chat_id'];

    public function fromContact(){
        return $this->belongsTo('App\Models\User','from_id','id');
    }

    public function messengerChat()
    {
        return $this->belongsTo(MessengerChat::class);
    }

    public function toContact(){
        return $this->belongsTo('App\Models\User','to_id','id');
    }
}
