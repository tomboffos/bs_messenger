<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerMapCoordinates extends Model
{
    use HasFactory;

    protected $fillable = ['message_id','latitude','longitude','address'];

    public $timestamps = false;
}
