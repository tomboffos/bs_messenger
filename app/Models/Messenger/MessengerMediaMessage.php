<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerMediaMessage extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['message_id','media_id'];

}
