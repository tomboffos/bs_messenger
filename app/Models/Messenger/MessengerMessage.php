<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessengerMessage extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['action','from_id','to_id','chat_id','is_read','text','time_deleted','type'];

    protected $dates = ['deleted_at'];

    public function chat(){
        return $this->belongsTo('App\Models\Messenger\MessengerChat','chat_id','id');
    }

    public function fromContact(){
        return $this->belongsTo('App\Models\User','from_id','id');
    }

    public function toContact(){
        return $this->belongsTo('App\Models\User','to_id','id');
    }

    public function transfer(){
        return $this->belongsToMany('App\Models\Messenger\MessengerMessage','messenger_transfer_messages','message_id','transfer_message_id','id');
    }

    public function contact(){
        return $this->belongsToMany('App\Models\User','messenger_contact_messages','message_id','contact_id','id');
    }


    public function map(){
        return $this->belongsTo('App\Models\Messenger\MessengerMapCoordinates','id','message_id');
    }

    public function file(){
        return $this->belongsToMany('App\Models\Media','messenger_media_messages','message_id','media_id','id');
    }

    public function color(){
        return $this->belongsTo('App\Models\Messenger\MessengerMessageUserColor','chat_id','chat_id');
    }

    public function deletedMessages(){
        return $this->belongsTo('App\Models\Messenger\MessengerDeletedMessageUser','id','message_id');
    }

}
