<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerMessageReadUser extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable =['user_id','message_id','chat_id','is_read'];
}
