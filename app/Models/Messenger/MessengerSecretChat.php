<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerSecretChat extends Model
{
    use HasFactory;
}
