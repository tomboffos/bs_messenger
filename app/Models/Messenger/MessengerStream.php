<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerStream extends Model
{
    use HasFactory;

    protected $fillable = ['name','user_id'];

    public function ownerStream(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }
}
