<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerStreamChat extends Model
{
    use HasFactory;

    protected $fillable = ['text','stream_id','user_id'];

    public function fromContact(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }
}
