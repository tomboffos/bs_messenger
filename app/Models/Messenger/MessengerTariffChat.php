<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerTariffChat extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['ad_id','city_id','chat_id'];

    public function period(){
        return $this->belongsTo('App\Models\Messenger\MessengerAdChat','ad_id','id');
    }

    public function city(){
        return $this->hasMany('App\Models\City','id','city_id');
    }
}
