<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerTransferMessage extends Model
{
    use HasFactory;

    protected $fillable = ['message_id','transfer_message_id'];

    public $timestamps = false;
}
