<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerViewGroups extends Model
{
    use HasFactory;

    protected $fillable = ['chat_id','views'];
}
