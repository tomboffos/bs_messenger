<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserContactPhone extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'phone','nickname'];

    public $timestamps = false;
}
