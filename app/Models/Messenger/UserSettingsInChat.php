<?php

namespace App\Models\Messenger;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSettingsInChat extends Model
{
    use HasFactory;

    protected $table = 'messenger_user_settings_in_chat';

    protected $fillable = ['user_id', 'chat_id', 'sound_on', 'vibration_on'];

    public $timestamps=false;
}
