<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerAdChatInterest extends Model
{
    use HasFactory;

    protected $fillable = [
        'messenger_ad_chat_id',
        'interest_id'
    ];
}
