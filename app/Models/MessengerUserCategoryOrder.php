<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessengerUserCategoryOrder extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'category_id',
        'order'
    ];

    public $timestamps = false;


}
