<?php

namespace App\Models\Microphone;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $table = 'microphone_ads';

    protected $guarded = [
        'id',
    ];

    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'creator_id');
    }

    public function favorites()
    {
        return $this->belongsToMany('App\Models\User', 'microphone_users_ads', 'ad_id', 'user_id')->withTimestamps();
    }

    public function media()
    {
        return $this->belongsToMany('App\Models\Microphone\Media', 'microphone_ads_media', 'ad_id', 'media_id')->withTimestamps();
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Microphone\Review',  'ad_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Microphone\Product', 'microphone_ads_products', 'ad_id', 'product_id')->withTimestamps();
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Models\Microphone\Subcategory', 'subcategory_id');
    }

    public function image()
    {
        return $this->belongsTo('App\Models\Microphone\Media',  'image_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City', 'city_id');
    }
}
