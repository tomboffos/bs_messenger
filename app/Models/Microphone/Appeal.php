<?php

namespace App\Models\Microphone;

use Illuminate\Database\Eloquent\Model;

class Appeal extends Model
{
    protected $table = 'microphone_appeals';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $guarded = [
        'id',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
