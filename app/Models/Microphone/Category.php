<?php

namespace App\Models\Microphone;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'microphone_categories';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $guarded = [
        'id',
    ];

    public function subcategories()
    {
        return $this->hasMany('App\Models\Microphone\Subcategory',  'category_id');
    }

    public function subAds()
    {
        return $this->hasManyThrough(Ad::class, Subcategory::class);
    }

    public function translations()
    {
        return $this->hasMany('App\Models\Microphone\CategoryTranslation', 'category_id');
    }
}
