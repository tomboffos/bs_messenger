<?php

namespace App\Models\Microphone;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    protected $table = 'microphone_categories_translations';

    public function category()
    {
        return $this->belongsTo('App\Models\Microphone\Category', 'category_id');
    }
}
