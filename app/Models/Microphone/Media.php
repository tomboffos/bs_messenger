<?php

namespace App\Models\Microphone;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'microphone_media';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $guarded = [
        'id',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function ads()
    {
        return $this->belongsToMany('App\Models\Microphone\Ad', 'microphone_ads_media', 'media_id', 'ad_id')->withTimestamps();
    }

    public function ads_image()
    {
        return $this->hasMany('App\Models\Microphone\Ad', 'image_id');
    }
}
