<?php

namespace App\Models\Microphone;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'microphone_products';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $guarded = [
        'id',
    ];

    public function ads()
    {
        return $this->belongsToMany('App\Models\Microphone\Ad', 'microphone_ads_products', 'product_id', 'ad_id')->withTimestamps();
    }
}
