<?php

namespace App\Models\Microphone;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'microphone_reviews';

    protected $guarded = [
        'id',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function ad()
    {
        return $this->belongsTo('App\Models\Microphone\Ad', 'ad_id');
    }
}
