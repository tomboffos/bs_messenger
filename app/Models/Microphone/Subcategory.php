<?php

namespace App\Models\Microphone;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $table = 'microphone_subcategories';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $guarded = [
        'id',
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\Microphone\Category', 'category_id');
    }

    public function ads()
    {
        return $this->hasMany('App\Models\Microphone\Ad',  'subcategory_id');
    }

    public function translations()
    {
        return $this->hasMany('App\Models\Microphone\SubcategoryTranslation', 'subcategory_id');
    }
}
