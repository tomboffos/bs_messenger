<?php

namespace App\Models\Microphone;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubcategoryTranslation extends Model
{
    protected $table = 'microphone_subcategories_translations';

    public function subcategory()
    {
        return $this->belongsTo('App\Models\Microphone\Subcategory', 'subcategory_id');
    }
}
