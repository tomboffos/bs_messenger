<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $guarded = [
        'id',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'roles_permissions', 'permission_id', 'role_id')->withTimestamps();
    }
}
