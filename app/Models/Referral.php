<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    protected $table = 'referrals';

    protected $guarded = [
        'id',
    ];

    public function referralUser()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function referrerUser()
    {
        return $this->belongsTo('App\Models\User', 'referrer_id');
    }
}
