<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SkillsResume extends Model
{
    use HasFactory;

    protected $fillable = ['skills_id','resume_id'];

}
