<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecializationResume extends Model
{
    use HasFactory;

    protected $fillable = ['specialization_id','resume_id'];
}
