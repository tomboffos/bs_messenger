<?php

namespace App\Models\Superwork;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutCompany extends Model
{
    use HasFactory;


    protected $fillable = ['name','user_id','date_create','about_company','email','site','address'];
}
