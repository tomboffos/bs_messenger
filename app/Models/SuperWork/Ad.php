<?php

namespace App\Models\SuperWork;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $table = 'superwork_ads';

    protected $guarded = [
        'id',
    ];

    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'creator_id');
    }

    public function favorites()
    {
        return $this->belongsToMany('App\Models\User', 'superwork_users_ads', 'ad_id', 'user_id')->withTimestamps();
    }

    public function image()
    {
        return $this->belongsTo('App\Models\SuperWork\Media',  'image_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\SuperWork\Category', 'category_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City', 'city_id');
    }

    public function companyAbout()
    {
        return $this->belongsTo('App\Models\Superwork\AboutCompany', 'creator_id','user_id');
    }

    public function form()
    {
        return $this->belongsTo('App\Models\Superwork\Resume', 'creator_id','user_id');
    }

    public function currency()
    {
        return $this->belongsTo('App\Models\Currency', 'currency_id');
    }
}
