<?php

namespace App\Models\SuperWork;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'superwork_categories';

    protected $guarded = [
        'id',
    ];

    public function translations()
    {
        return $this->hasMany('App\Models\SuperWork\CategoryTranslation', 'category_id');
    }

    public function ads()
    {
        return $this->hasMany('App\Models\SuperWork\Ad',  'category_id');
    }
}
