<?php

namespace App\Models\SuperWork;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    protected $table = 'superwork_categories_translations';

    protected $guarded = [
        'id',
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\SuperWork\Category', 'category_id');
    }
}
