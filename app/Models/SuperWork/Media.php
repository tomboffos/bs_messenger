<?php

namespace App\Models\SuperWork;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'superwork_media';

    protected $guarded = [
        'id',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function adsImage()
    {
        return $this->hasMany('App\Models\SuperWork\Ad', 'image_id');
    }
}
