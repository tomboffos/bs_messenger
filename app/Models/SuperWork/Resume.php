<?php

namespace App\Models\Superwork;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Resume extends Model
{
    use HasFactory;

    protected $fillable = ['languages','specialty','skills','user_id','name','surname','city_id','other_phone','about_self','removal','birth_day','education'];


    public function city(){
        return $this->belongsTo('App\Models\City', 'city_id');
    }
}
