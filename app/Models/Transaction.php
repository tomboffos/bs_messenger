<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';

    protected $hidden = [
        'updated_at', 'pivot'
    ];

    protected $guarded = [
        'id',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function sourceUser()
    {
        return $this->belongsTo('App\Models\User', 'source_user_id');
    }

    public function revertedTransaction()
    {
        return $this->belongsTo('App\Models\Transaction', 'reverted_transaction_id');
    }
}
