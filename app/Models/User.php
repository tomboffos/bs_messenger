<?php

namespace App\Models;

use App\Models\Messenger\MessengerBlockUserCall;
use App\Models\Messenger\MessengerCategoryChat;
use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\MessengerContactUser;
use App\Models\Messenger\UserSettingsInChat;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'pivot'
    ];

    protected $guarded = [
        'id',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

//    protected $fillable = ['site','youtube','instagram','facebook','whatsapp', 'phone', 'code', 'attempts'];

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'users_roles', 'user_id', 'role_id')->withTimestamps();
    }

    public function media()
    {
        return $this->hasMany('App\Models\Microphone\Media', 'user_id');
    }

    public function documents()
    {
        return $this->hasMany('App\Models\Document', 'user_id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Microphone\Reviews', 'user_id');
    }

    public function ads()
    {
        return $this->hasMany('App\Models\Microphone\Ad', 'creator_id');
    }

    public function favorites()
    {
        return $this->belongsToMany('App\Models\Microphone\Ad', 'microphone_users_ads', 'user_id', 'ad_id')->withTimestamps();
    }

    public function applications()
    {
        return $this->belongsToMany('App\Models\Application', 'users_applications', 'user_id', 'application_id')->withTimestamps();
    }

    public function parts()
    {
        return $this->hasMany('App\Models\Part', 'user_id');
    }

    public function interests()
    {
        return $this->belongsToMany(Interest::class, 'interest_users');
    }

    public function playerIds()
    {
        return $this->hasMany('App\Models\PlayerId', 'user_id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction', 'user_id');
    }

    public function sourceTransactions()
    {
        return $this->hasMany('App\Models\Transaction', 'source_user_id');
    }

    public function referrerStatuses()
    {
        return $this->hasMany('App\Models\Referral', 'user_id');
    }

    public function referralsStatuses()
    {
        return $this->hasMany('App\Models\Referral', 'referrer_id');
    }

    public function appeals()
    {
        return $this->hasMany('App\Models\Microphone\Appeal', 'user_id');
    }

    public function botMessages()
    {
        return $this->hasMany(BotMessage::class)->orderBy('id', 'desc');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City', 'city_id');
    }

    public function withdrawals()
    {
        return $this->hasMany('App\Models\Withdrawal', 'user_id');
    }


    public function oneSignal()
    {
        return $this->hasOne('App\Models\PlayerId', 'user_id', 'id');
    }


    public function settingsUserChat()
    {
        return $this->hasOne('App\Models\Messenger\UserSettingsInChat', 'user_id', 'id');
    }

    //SuperWork

    public function superWorkAds()
    {
        return $this->hasMany('App\Models\SuperWork\Ad', 'creator_id');
    }

    public function superWorkMedia()
    {
        return $this->hasMany('App\Models\SuperWork\Media', 'user_id');
    }

    public function superWorkFavorites()
    {
        return $this->belongsToMany('App\Models\SuperWork\Ad', 'superwork_users_ads', 'user_id', 'ad_id')->withTimestamps();
    }


    public function referralChildren()
    {
        return $this->belongsTo('App\Models\Referral', 'id', 'user_id');
    }

    public function sendNotification()
    {
        $this->notify(new \App\Notifications\Microphone\ReviewPublish($this));
    }

    public function routeNotificationForOneSignal()
    {
//        $user = Auth::user();
//        $tokenName = $user->currentAccessToken()['name'];
//        switch($tokenName) {
//            case 'authToken':
//                $application = 1;
//                break;
//            case 'tokenSuperWork':
//                $application = 2;
//                break;
//            default:
//                $application = 0;
//                break;
//        }
//        return array_column($this->playerIds()->where('application_id', $application)->get()->toArray(), 'player_id');
        $oneSignal = PlayerId::where('user_id', '=', $this->id)
            ->orderByDesc('id')
            ->get()->pluck('player_id');

        return $oneSignal;

    }


    public function member()
    {
        return $this->belongsTo('App\Models\Messenger\MessengerChatMember', 'id', 'user_id');
    }

    public function contact()
    {
        return $this->belongsTo('App\Models\Messenger\MessengerContactUser', 'id', 'contact_id');
    }

    public function contacts()
    {
        return $this->hasMany(MessengerContactUser::class,);
    }

    public function inverseContact()
    {
        return $this->hasMany(MessengerContactUser::class,'contact_id');
    }

    public function deviceTokens()
    {
        return $this->hasMany(DeviceToken::class)->pluck('device_token');
    }

    public function messengerCategories()
    {
        return $this->belongsToMany(
            MessengerCategoryChat::class,
            MessengerUserCategoryOrder::class,
            'user_id',
            'category_id'
        );
    }

    public function chatSettings()
    {
        return $this->belongsToMany(MessengerChat::class, UserSettingsInChat::class, 'user_id', 'chat_id');
    }

    public function blockUser()
    {
        return $this->belongsToMany(
            User::class,
            MessengerBlockUserCall::class,
            'block_user_id',
            'user_id'
        );
    }
}
