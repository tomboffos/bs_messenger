<?php

namespace App\Notifications\Messenger;


use App\Broadcasting\MessengerChannel;

use App\Broadcasting\MicrophoneChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;
use Illuminate\Notifications\Notification;


class NewCall extends Notification implements ShouldBroadcastNow
{
    use Queueable;

    public $user;
    public $avatar;
    public $token;
    public $channel;
    public $type;


    public function __construct($user,$avatar,$token,$channel,$type){
        $this->user = $user;
        $this->avatar = $avatar;
        $this->token = $token;
        $this->channel = $channel;
        $this->type = $type;
    }
    public function via($notifiable)
    {
        return [MessengerChannel::class];
    }

    public function toOneSignal($notifiable)
    {
        $message = OneSignalMessage::create()
            ->setSubject('Incoming call')
            ->setBody($this->user)
            ->setParameter('large_icon',$this->avatar)
            ->setParameter('token',$this->token)
            ->setParameter('channel_name',$this->channel)
            ->setParameter('type',$this->type)
            ->setData('type',$this->type)
            ->setData('token',$this->token)
            ->setData('channel_name',$this->channel);


        return $message;
    }
}
