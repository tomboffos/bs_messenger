<?php

namespace App\Notifications\Messenger;


use App\Broadcasting\MessengerChannel;

use App\Models\Messenger\MessengerChatSettings;
use App\Models\Messenger\MessengerChatUserSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;
use Illuminate\Notifications\Notification;


class NewMessage extends Notification implements ShouldBroadcastNow
{
    use Queueable;

    public $user;

    public $text;

    public $avatar;
    public $chat;
    public function __construct($user,$text,$avatar,$chat){
        $this->user = $user;
        $this->text = $text;
        $this->avatar = $avatar;
        $this->chat = $chat;

    }
    public function via($notifiable)
    {
        return [MessengerChannel::class];
    }

    public function toOneSignal($notifiable)
    {
        $message = OneSignalMessage::create()
            ->setSubject($this->user->name)
            ->setBody($this->text)
            ->setParameter('large_icon',$this->avatar)
            ->setParameter('chat_id',$this->chat->id);


        return $message;
    }
}
