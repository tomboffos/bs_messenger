<?php

namespace App\Notifications\Messenger;


use App\Broadcasting\MessengerChannel;

use App\Broadcasting\MicrophoneChannel;
use App\Broadcasting\SuperWorkChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;
use Illuminate\Notifications\Notification;


class SuperworkNewMessage extends Notification implements ShouldBroadcastNow
{
    use Queueable;

    public $user;

    public $text;

    public $avatar;

    public function __construct($user,$text,$avatar){
        $this->user = $user;
        $this->text = $text;
        $this->avatar = $avatar;

    }
    public function via($notifiable)
    {
        return [SuperWorkChannel::class];
    }

    public function toOneSignal($notifiable)
    {
        $message = OneSignalMessage::create()
            ->setSubject($this->user->name)
            ->setBody($this->text)
            ->setParameter('large_icon',$this->avatar);


        return $message;
    }
}
