<?php

namespace App\Notifications\Microphone;

use App\Broadcasting\MicrophoneChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;

class ReviewPublish extends Notification
{
    use Queueable;
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function via($notifiable)
    {
        return [MicrophoneChannel::class];
    }

    public function toOneSignal($notifiable)
    {
        switch ($this->data['user']['language_id']) {
            case 1:
                return OneSignalMessage::create()
                    ->setSubject("Получен отзыв")
                    ->setBody('Русский');
            case 2:
                return OneSignalMessage::create()
                    ->setSubject("Получен отзыв")
                    ->setBody('Английский');
            case 3:
                return OneSignalMessage::create()
                    ->setSubject("Получен отзыв")
                    ->setBody('Казахский');
            case 4:
                return OneSignalMessage::create()
                    ->setSubject("Получен отзыв")
                    ->setBody('Арабский');
            case 5:
                return OneSignalMessage::create()
                    ->setSubject("Получен отзыв")
                    ->setBody('Китайский');
            default:
                return OneSignalMessage::create()
                    ->setSubject("Получен отзыв")
                    ->setBody('Русский');
        }
    }
}
