<?php

namespace App\Notifications\SuperWork;

use App\Broadcasting\SuperWorkChannel;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalMessage;

class AdExpired extends Notification
{
    use Queueable;

    const MESSAGE = 'Ваше объявление удалено в архив.';
    private $ad;

    public function __construct($ad)
    {
        $this->ad = $ad;
    }

    public function via($notifiable)
    {
        return [SuperWorkChannel::class];
    }

    public function toOneSignal($notifiable)
    {
        $user = User::find($this->ad->creator_id);

        switch ($user->language_id) {
            case 1:
                return OneSignalMessage::create()
                    ->setSubject(self::MESSAGE)
                    ->setBody('Русский');
            case 2:
                return OneSignalMessage::create()
                    ->setSubject(self::MESSAGE)
                    ->setBody('Английский');
            case 3:
                return OneSignalMessage::create()
                    ->setSubject(self::MESSAGE)
                    ->setBody('Казахский');
            case 4:
                return OneSignalMessage::create()
                    ->setSubject(self::MESSAGE)
                    ->setBody('Арабский');
            case 5:
                return OneSignalMessage::create()
                    ->setSubject(self::MESSAGE)
                    ->setBody('Китайский');
            default:
                return OneSignalMessage::create()
                    ->setSubject(self::MESSAGE)
                    ->setBody('Русский');
        }
    }
}
