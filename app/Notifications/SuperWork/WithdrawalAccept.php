<?php

namespace App\Notifications\SuperWork;

use App\Broadcasting\SuperWorkChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalMessage;

class WithdrawalAccept extends Notification
{
    use Queueable;
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function via($notifiable)
    {
        return [SuperWorkChannel::class];
    }

    public function toOneSignal($notifiable)
    {
        switch ($this->data['user']['language_id']) {
            case 1:
                return OneSignalMessage::create()
                    ->setSubject("Подтверждение вывода средств")
                    ->setBody('Русский');
            case 2:
                return OneSignalMessage::create()
                    ->setSubject("Подтверждение вывода средств")
                    ->setBody('Английский');
            case 3:
                return OneSignalMessage::create()
                    ->setSubject("Подтверждение вывода средств")
                    ->setBody('Казахский');
            case 4:
                return OneSignalMessage::create()
                    ->setSubject("Подтверждение вывода средств")
                    ->setBody('Арабский');
            case 5:
                return OneSignalMessage::create()
                    ->setSubject("Подтверждение вывода средств")
                    ->setBody('Китайский');
            default:
                return OneSignalMessage::create()
                    ->setSubject("Подтверждение вывода средств")
                    ->setBody('Русский');
        }
    }
}
