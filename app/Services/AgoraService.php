<?php

namespace App\Services;



use App\Agora\RtcTokenBuilder as RtcTokenBuilderAlias;
use App\Http\Requests\ContinueBroadcastByPlanRequest;
use App\Http\Requests\MessageStreamRequest;
use App\Http\Requests\NewConferenceRequest;
use App\Http\Requests\NewStreamRequest;
use App\Models\Messenger\MessengerConference;
use App\Models\Messenger\MessengerConferenceMember;
use App\Models\Messenger\MessengerStream;
use App\Models\Messenger\MessengerStreamChat;
use App\Models\Tariff;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AgoraService
{

    public $call;
    public $chat;
    public function __construct(TransactionService $transaction,ChatService $chat){
        $this->transaction = $transaction;
        $this->chat = $chat;
    }


    public function agoraInitTokenApp(Request $request){
        $appID = env('AGORA_ID');
        $appCertificate = env('AGORA_KEY');
        $channelName = $request->channelName;
        $user = Auth::user()->name;
        $role = RtcTokenBuilderAlias::RoleAttendee;
        $expireTimeInSeconds = 3600;
        $currentTimestamp = Carbon::now()->timestamp;
        $privilegeExpiredTs = $currentTimestamp + $expireTimeInSeconds;

        return  RtcTokenBuilderAlias::buildTokenWithUserAccount($appID, $appCertificate, $channelName, $user, $role, $privilegeExpiredTs);
    }


    public function continueVideo(User $user,Tariff $plan){
        if ($user->balance > $plan->price){
            $this->transaction->minusBalanceUser(Auth::user(), $plan->price);
            return response()->json([
                'continue'
            ], 200);
        }else{
            return response()->json([
                'Not money'
            ], 403);
        }
    }

    public function newStream(NewStreamRequest $request){
            MessengerStream::create(
                [
                    'name' => $request->name,
                    'user_id' => Auth::user()->id,
                ]
            );
           return $this->getLastStreamUser();

    }

    public function getLastStreamUser(){
        return MessengerStream::where('user_id','=',Auth::user()->id)
            ->with('ownerStream',function ($query){
                $query->select('id','phone','name','surname','avatar');
            })
            ->orderByDesc('id')
            ->first();
    }

    public function sendMessageStream(MessageStreamRequest $request){
        MessengerStreamChat::create(
            [
                'text' => $request->text,
                'user_id' => Auth::user()->id,
                'stream_id' => $request->stream_id,
            ]
        );
        return $this->getLastUserStreamMessage();
    }

    public function getLastUserStreamMessage(){
          return MessengerStreamChat::where('user_id','=',Auth::user()->id)
            ->with('fromContact',function ($query){
                $query->select('id','phone','name','surname','avatar');
            })
            ->orderByDesc('id')
            ->first();
    }

    public function createConference(NewConferenceRequest $request){
        MessengerConference::create(
            [
                'user_id'  => Auth::user()->id,
                'name' => $request->name
            ]
        );

         $this->addContactToConference($request);
         $resultChatForConference =  $this->chat->create($request);
         $resultLastConference=  $this->getLastConference(Auth::user());

         return ['conference' => $resultLastConference,'chat_conference' => $resultChatForConference];
    }

    public function getLastConference(User $user){
        return MessengerConference::where('user_id','=',$user->id)
            ->with('member.user',function ($query){
                $query->select('id','name','surname','phone');
            })
            ->orderByDesc('id')
            ->first();
    }

    public function addContactToConference($request){
        $contacts = explode ( ',',$request->contact);
        $conference = $this->getLastConference(Auth::user());
        MessengerConferenceMember::firstOrCreate(
            [
                'conference_id' => $conference->id,
                'user_id' => Auth::user()->id
            ]
        );
        foreach ($contacts as $contact){
            MessengerConferenceMember::firstOrCreate(
                [
                    'conference_id' => $conference->id,
                    'user_id' => $contact
                ]
            );

        }
    }
}
