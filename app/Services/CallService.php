<?php

namespace App\Services;

use App\Http\Controllers\Messenger\ChatController;
use App\Http\Requests\CallRequest;
use App\Models\Messenger\MessengerBlockUserCall;
use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\MessengerChatMember;
use App\Models\Messenger\MessengerLogCall;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CallService
{
    public $chat;
    public $settings;


    public function __construct(ChatService $chat,  SettingsService $settings)
    {
        $this->chat = $chat;
        $this->settings = $settings;
    }
    public function newLogCall(Request $request){
        MessengerLogCall::create(
            [
                'from_id' => Auth::user()->id,
                'to_id' => $request->to_id,
                'is_begin' => 0,
                'type_call' => $request->type,
                'messenger_chat_id' => $request->messenger_chat_id
            ]
        );
        return $this->getLastCallUser();
    }

    public function updateLogCall(Request $request,MessengerLogCall $call){
        MessengerLogCall::where('id','=',$call->id)
            ->update(
                [
                    'is_begin' => 1,
                ]
            );

    }

    public function getLogCallUser(Request $request){
        $logCalls = MessengerLogCall::where(function($q) {
            $q->where('from_id','=',Auth::user()->id);
        })->orWhere(function($q)  {
            $q->where('to_id','=',Auth::user()->id);
        })
            ->orderByDesc('id')
            ->with('fromContact',function ($query){
                $query->select('id','phone','name','surname','avatar');
            })
            ->with('messengerChat')
            ->with('toContact',function ($query){
                $query->select('id','phone','name','surname','avatar');
            })
            ->paginate($request->limit ?? 20);
        foreach ($logCalls as $call)
            $call->messengerChat = $this->show($call->messengerChat);
        return $logCalls;

    }

    public function show(MessengerChat $chat)
    {
        $current_chat = $this->chat->showChat($chat);
        $memberInChat = $this->chat->getMembersChat($chat);
        if ($chat->is_private == 1) {
            $user = $this->chat->getUserInPrivateChat($chat);
        }
        return [
            'ad' => $this->chat->getStatisticAdChat($chat),
            'chat' => $current_chat,
            'media' => $this->chat->getMediaChat($chat),
            'settings' => $this->settings->getSettingsChat($chat),
            'count_member' => $memberInChat->count(),
            'role' => $this->chat->getRoleUserChat($chat, Auth::user()),
            'user_in_private_chat' => $user ?? null,
            'members' => $memberInChat
        ];
    }


    public function getMissCall(){
        return MessengerLogCall::where('to_id','=',Auth::user()->id)
            ->where('is_begin','=',1)
            ->count();
    }

    public function getBlockUserList($request){
        return User::whereHas('blockUser',function ($query){
            $query->where('user_id','=',Auth::user()->id);
        })
            ->select('id','name','surname','avatar','phone')
            ->paginate($request->limit ?? 20);
    }

    public function addBlockUser(User $blockUser){
        return MessengerBlockUserCall::firstOrCreate([
            'user_id' => Auth::user()->id,
            'block_user_id' => $blockUser->id
        ]);
    }


    public function deleteBlockUser(User $blockUser){
        return MessengerBlockUserCall::where('user_id','=',Auth::user()->id)
            ->where( 'block_user_id','=',$blockUser->id)
            ->delete();
    }

    public function checkBlockUser(User $blockUser){
        $user = MessengerBlockUserCall::where('user_id','=',$blockUser->id)
            ->where('block_user_id',Auth::user()->id)
            ->first();

        if (!empty($user->user_id)){
            return true;
        }else{
            return false;
        }
    }

    public function checkBlockUserByProfile(User $blockUser){
        $user =  MessengerBlockUserCall::where('user_id','=',Auth::user()->id)
            ->where('block_user_id',$blockUser->id)
            ->first();

        if (!empty($user->user_id)){
            return true;
        }else{
            return false;
        }
    }

    public function getLastCallUser(){
        return MessengerLogCall::where('from_id','=',Auth::user()->id)
            ->with('fromContact')
            ->with('toContact')
            ->orderByDesc('id')
            ->first();
    }

    public function endCall(CallRequest $call){
        $log = MessengerLogCall::where('id','=',$call->call)
            ->first();
        $now = Carbon::now();
        $timeCall = Carbon::parse($now)->timestamp - strtotime($log->created_at) ;
        MessengerLogCall::where('id','=',$call->call)
            ->update(
                [
                    'time_call' => $timeCall,
                ]
            );
        return $this->getLogCallUser($call);
    }

    public function checkBlockUserInPrivateChat(MessengerChat  $chat){
        $memberInChat = MessengerChatMember::where('user_id','!=',Auth::user()->id)
                ->where('chat_id','=',$chat->id)
                ->first();

        if (!empty($memberInChat->user_id)){
            $user = User::where('id','=',$memberInChat->user_id)
                ->first();
            return $this->checkBlockUser($user);
        }else{
            return false;
        }

    }

}
