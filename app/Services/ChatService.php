<?php

namespace App\Services;


use App\Events\getIndexChat;
use App\Events\NewMessage;
use App\Exceptions\SimpleException;
use App\Http\Requests\CreateCategoryChatRequest;
use App\Http\Requests\CreateChatRequest;
use App\Http\Requests\TimeSecretChatRequest;
use App\Http\Requests\UpdateChat;
use App\Models\Media;
use App\Models\Messenger\MessengerAdChat;
use App\Models\Messenger\MessengerCategoryChat;
use App\Models\Messenger\MessengerCategoryUserChat;
use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\MessengerChatMember;
use App\Models\Messenger\MessengerChatSettings;
use App\Models\Messenger\MessengerMessage;
use App\Models\Messenger\MessengerMessageUserColor;
use App\Models\Messenger\MessengerTariffChat;
use App\Models\Messenger\MessengerViewGroups;
use App\Models\Messenger\UserSettingsInChat;
use App\Models\MessengerUserCategoryOrder;
use App\Models\User;
use DateTime;
use App\Services\SearchService;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class ChatService
{

    public $media;
    public $message;
    public $user;
    public $contact;

    public function __construct(MediaService $media = null, MessageService $message = null, UserService $user = null, SearchService $contact = null)
    {
        $this->user = $user;
        $this->media = $media;
        $this->message = $message;
        $this->contact = $contact;
    }

    public function getUserChatCategory()
    {
        $user = Auth::user();

        $category = $user->messengerCategories()
            ->withPivot(['order'])
            ->orderBy('pivot_order', 'asc')
            ->get();


        /*$category = MessengerCategoryChat::where('app_chat','=',1)
            ->orWhere('user_id','=',Auth::user()->id)
            ->get();*/
        $category->map(function ($chat) {
            $chat->no_read_message = $this->message->getUserCategoryNotReadMessage($chat);
            $getCountChat = MessengerChat::whereHas('member', function ($query) {
                $query->where('user_id', '=', Auth::user()->id);
            })
                ->whereHas('chat', function ($query) use ($chat) {
                    $query->where('category_id', '=', $chat->id);
                })
                ->get();

            $chat->full_link = URL::to('/') . '/' . $chat->avatar;
            $chat->total_chat = $getCountChat->count();
        });
        return $category;
    }

    public function createCategory(CreateCategoryChatRequest $request)
    {
        if ($request->hasFile('file')) {
            $pathAvatar = $this->media->saveImage($request->file, '/messenger/chat/avatar');
        }
        $user = Auth::user();

        $messengerCategory = MessengerCategoryChat::create([
            'name' => $request->name,
            'avatar' => $pathAvatar ?? null,
            'user_id' => $user->id,
            'app_chat' => 0,
        ]);

        $lastCategoryPivot = MessengerUserCategoryOrder::orderBy('order', 'desc')->get()->last();

        $user->messengerCategories()->attach($messengerCategory->id,
            ['order' => $lastCategoryPivot ? $lastCategoryPivot->order : 1]
        );

        if (!empty($request->transfer)) {
            $chats = explode(',', $request->transfer);
            foreach ($chats as $chat) {
                $this->transferChatUserToOtherCategory($chat, $messengerCategory->id);
            }
        }


        return $this->getUserChatCategory();
    }

    public function showCategoryChat(MessengerCategoryChat $category, int $lastChatId = null, int $limit = null)
    {
        $chats = MessengerChat::whereHas('category', function ($query) use ($category) {
            $query->where('category_id', '=', $category->id);
            $query->where('user_id', '=', Auth::user()->id);
        })
            ->get();
        $chats->map(function ($chat) use ($category) {
            $chat->category_chat = $category;
            $chat->category_chat->full_link = URL::to('/') . '/' . $chat->category_chat->avatar;
            $chat->last_message = $this->message->getLastMessageChat($chat->id);
            $chat->last_message_id = $chat->last_message->id ?? null;
            $chat->no_read_message = $this->message->getUserChatNotReadMessage($chat);
        });
        $chats = $chats->sortByDesc(function ($item) {
            return $item->last_message_id;
        })->values();

        if ($lastChatId) {
            foreach ($chats as $key => $value) {
                $chats->forget($key);
                if ($value['id'] === $lastChatId) {
                    break;
                }
            }
        }

        $hasMoreResults = false;

        if ($chats->count()) {
            $hasMoreResults = !!($chats->count() - $chats->take($limit ?? 10)->count());
        }

        return [
            'chats' => $chats->take($limit ?? 10)->values(),
            'hasMoreResults' => $hasMoreResults,
            'count_chats' => $this->showCategoryChatCount($category)
        ];

    }

    public function showCategoryChatCount(MessengerCategoryChat $category)
    {
        return MessengerCategoryChat::whereHas('userChat', function ($query) use ($category) {
            $query->where('user_id', '=', Auth::user()->id);
            $query->where('category_id', '=', $category->id);
        })
            ->with('chat')
            ->where('id', '=', $category->id)
            ->count();
    }

    public function deleteCategoryChat(MessengerCategoryChat $category)
    {
        MessengerCategoryUserChat::where('category_id', '=', $category->id)
            ->delete();

        return MessengerCategoryChat::where('id', '=', $category->id)
            ->delete();
    }

    public function showChat(MessengerChat $chat)
    {
        if ($chat->is_private == 1) {
            $contact = $this->getMessengerPrivateChatMember($chat);

            if (!empty($contact->user_id)) {
                $user = User::where('id', '=', $contact->user_id)
                    ->first();
                $chat->avatar = $user->avatar;
                $chat->name = $this->contact->getNicknameContact($user) == null ? $user->name : $this->contact->getNicknameContact($user);
                $chat->isOnline = $this->user->checkOnlineUser($user);
            }

        }
        $chat->full_link = URL::to('/') . '/' . $chat->avatar;
        $chat->admin_id = $this->getAdminChatId($chat);
        $chat->description = $chat->description == "null" ? NULL : $chat->description;
        $chat->description = $chat->description == null ? NULL : $chat->description;
        return $chat;
    }

    public function getMembersChat(MessengerChat $chat)
    {

        $members = User::whereHas('member', function ($query) use ($chat) {
            $query->where('chat_id', '=', $chat->id);
        })
            ->select('id', 'name', 'phone', 'last_visit', 'avatar')
            ->get();

        $members->map(function ($member) {
            $member->full_link = URL::to('/') . '/' . $member->avatar;
            $member->isOnline = $this->user->checkOnlineUser($member);
            $member->name = $this->contact->getNicknameContact($member) != null ? $this->contact->getNicknameContact($member) : $member->name;

        });
        return $members;
    }


    public function getMediaChat(MessengerChat $chat)
    {
        $media = Media::whereHas('message.chat', function ($query) use ($chat) {
            $query->where('chat_id', '=', $chat->id);
        })
            ->where('type', '=', 'video')
            ->orWhere('type', '=', 'video');
        $documents = Media::whereHas('message.chat', function ($query) use ($chat) {
            $query->where('chat_id', '=', $chat->id);
        })
            ->where('type', '=', 'document');
        $audio = Media::whereHas('message.chat', function ($query) use ($chat) {
            $query->where('chat_id', '=', $chat->id);
        })
            ->where('type', '=', 'audio');
        return ['media' => $media->count(),
            'document' => $documents->count(),
            'audio' => $audio->count(),
        ];
    }


    public function leaveChat(MessengerChat $chat)
    {
        MessengerChatMember::where('chat_id', '=', $chat->id)
            ->where('user_id', '=', Auth::user()->id)
            ->delete();
    }


    public function create($request)
    {

        if ($request->hasFile('file')) {
            $pathAvatar = $this->media->saveImage($request->file, '/messenger/chat/avatar');
        }


        if (($request->is_private == 1)) {
            if ($this->checkPrivateExistChat($request->contact) != false) {
                return $this->checkPrivateExistChat($request->contact);
            }
        }

        $newChat = MessengerChat::create([
            'name' => $request->name ?? 'private',
            'avatar' => $request->hasFile('file') ? $pathAvatar : null,
            'description' => $request->description ?? NULL,
            'is_secret' => $request->is_secret ?? 0,
            'email' => $request->email ?? NULL,
            'youtube' => $request->youtube ?? NULL,
            'instagram' => $request->instagram ?? NULL,
            'is_private' => $request->is_private ?? 0,
            'site' => $request->site ?? NULL,
            'whatsapp' => $request->whatsapp ?? NULL,

        ]);

        MessengerChatMember::create([
            'chat_id' => $newChat->id,
            'user_id' => Auth::user()->id,
            'role' => 'admin',
            'show_chat' => $request->is_private == 0 ? 1 : 0,
        ]);


        $chat = $this->getLastChatUser();

        if (isset($request->contact)) {
            $this->addContactToChat($chat, $request);
        } else {
            $this->message->newAction($newChat, 'chatCreated', null);
        }
        MessengerChatSettings::firstOrCreate([
            'chat_id' => $chat->id,
            'admin_media_send' => 0,
            'admin_message_send' => 0,
            'admin_change_data' => 1,
            'transfer_chat_message' => 1,
            'sound' => 1,
        ]);

        if ($chat->is_private == 0) {
            $this->callGetIndexChannel($chat, "CreateChat");
        }
        if ($chat->is_private == 1) {
            $members = $this->getMembersChat($chat);
            foreach ($members as $user) {
                if (!empty($user->id)) {
                    if ($user->id != Auth::user()->id) {
                        $chat->name = $this->contact->getNicknameContact($user) === NULL ? $user->name : $this->contact->getNicknameContact($user);
                    }
                }
            }
        }
        return $chat;
    }


    public function showMembersChat(MessengerChat $chat, Request $request)
    {
        $users = User::whereHas('member', function ($query) use ($chat) {
            $query->where('chat_id', '=', $chat->id);
        })
            ->select('id', 'name', 'phone', 'last_visit', 'avatar')
            ->get();

        $users->map(function ($user) {
            $user->full_link = URL::to('/') . '/' . $user->avatar;
        });
        $collection = collect($users);
        $page = $request->page ?? 1;
        $perPage = $request->limit ?? 20;

        return $paginate = new LengthAwarePaginator(
            $collection->forPage($page, $perPage),
            $collection->count(),
            $perPage,
            $page,
            ['path' => url("/api/messenger/chat/$chat->id/full-members")]
        );
    }

    public function updateChat(MessengerChat $chat, UpdateChat $request)
    {
        if ($request->has('file')) {
            Storage::disk('public')->delete($request->file);
            $path = $request->file->store('messenger/' . $request->type, ['disk' => 'public']);
        }
        return MessengerChat::where('id', '=', $chat->id)
            ->update(['avatar' => $path ?? $chat->avatar,
                'name' => $request->name,
                'description' => empty($request->description) ? $chat->description : $request->description,
            ]);
    }

    public function addContactToChat(MessengerChat $chat, $request)
    {
        $contacts = explode(',', $request->contact);
        $chatMember = array();
        foreach ($contacts as $contact) {
            $this->newUserChat($contact, $chat);
            $user = User::where('id', '=', $contact)
                ->first();
            array_push($chatMember, $user->name);
            $this->addColorUserToChat($chat, $user);
            $this->message->newAction($chat, 'Ad User', $contact);
            $message = $this->message->getLastUserMessage($chat);
            $this->message->callNewMessageEvent($message, 'AddUserAction');
        }

        if ($chat->is_private == 1) {
            MessengerChat::where('id', '=', $chat->id)
                ->update(
                    [
                        'name' => $chatMember[0] ?? Auth::user()->name == null ? Auth::user()->phone : Auth::user()->name,
                    ]
                );
        }

    }


    public function newUserChat($contact, MessengerChat $chat)
    {

        MessengerChatMember::firstOrCreate([
            'user_id' => $contact,
            'chat_id' => $chat->id,
            'show_chat' => 0,
            'role' => $chat->is_private == 1 ? 'admin' : 'member',
        ]);

        UserSettingsInChat::create([
            'user_id' => $contact,
            'chat_id' => $chat->id,
            'sound_on' => 1,
            'vibration_on' => 1,
        ]);
    }


    public function deleteContactFromChat(MessengerChat $chat, $request)
    {
        $contacts = explode(',', $request->contact);
        foreach ($contacts as $contact) {
            $this->deleteUserChat($contact, $chat);
            $this->message->newAction($chat, 'Kick User', $contact);
            $message = $this->message->getLastUserMessage($chat);
            $this->message->callNewMessageEvent($message, 'DeleteUserAction');
        }

    }


    public function deleteUserChat($contact, MessengerChat $chat)
    {
        MessengerChatMember::where('user_id', '=', $contact)
            ->where('chat_id', '=', $chat->id)
            ->delete();
    }


    public function getAllUserChat()
    {
        $chats = MessengerChat::where(function ($query) {
            $query->where([
                'is_paid' => true,
                'is_conference' => true
            ])->orWhere([
                'is_conference' => false
            ]);
        })->whereHas('member', function ($query) {
            $query->where('user_id', '=', Auth::user()->id);
            $query->where('show_chat', '=', 1);
        })
            ->get();

        $chats->map(function ($chat) use ($chats) {
            if ($chat->is_private == 1) {
                $contact = $this->getMessengerPrivateChatMember($chat);
                if (!empty($contact->user_id)) {
                    $user = User::where('id', '=', $contact->user_id)
                        ->first();
                    $chat->name = $this->contact->getNicknameContact($user) == null ? $user->name : $this->contact->getNicknameContact($user);
                    $chat->full_link = URL::to('/') . '/' . $user->avatar;
                } else {
                    $chat->full_link = null;
                }
            } else {
                $chat->full_link = URL::to('/') . '/' . $chat->avatar;
            }
            $chat->last_message = $this->message->getLastMessageChat($chat->id);
            $chat->last_message_id = $chat->last_message->id ?? null;
            $chat->no_read_message = $this->message->getUserChatNotReadMessage($chat);
            $chat->category_chat = $this->getCategoryChat($chat, Auth::user());
            $chat->settings = $this->getSettingsChat($chat);
        });

        $chats = $chats->sortByDesc(function ($item) {
            return $item->last_message_id;
        })->values();

        return $chats;
    }


    public function getAllUserChatWithPaginate(int $lastChatId = null, int $limit = null, int $type = null)
    {
        $chats = MessengerChat::whereHas('member', function ($query) {
            $query->where('user_id', '=', Auth::user()->id);
            $query->where('show_chat', '=', 1);
        })
            ->when($type != null, function ($query) use ($type) {
                $query->where('is_private', '=', $type);
            })
            ->get();

        $chats->map(function ($chat) use ($chats) {
            $chat->last_message = $this->message->getLastMessageChat($chat->id);
            $chat->admin_id = $this->getAdminChatId($chat);
            if ($chat->is_private == 1) {
                $contact = $this->getMessengerPrivateChatMember($chat);
                if (!empty($contact->user_id)) {
                    $user = User::where('id', '=', $contact->user_id)
                        ->first();
                    if (!empty($user->phone)) {
                        $chat->name = $this->contact->getNicknameContact($user) == null ? $user->name : $this->contact->getNicknameContact($user);
                        $chat->full_link = URL::to('/') . '/' . $user->avatar;
                    }
                } else {
                    $chat->full_link = null;
                }
            } else {
                $chat->full_link = URL::to('/') . '/' . $chat->avatar;
            }
            if ($chat->last_message) {
                $fromContact = User::findOrFail($chat->last_message->from_id);
                $chat->last_message->from_name = $fromContact->name;
                $chat->last_message->from_surname = $fromContact->surname;
                $chat->last_message->from_patronymic = $fromContact->patronym;
                $chat->category_chat = $this->getCategoryChat($chat, Auth::user());
                $chat->settings = $this->getSettingsChat($chat);
            }

            $chat->last_message_id = $chat->last_message->id ?? null;
            $chat->no_read_message = $this->message->getUserChatNotReadMessage($chat);
        });

        $chats = $chats->sortByDesc(function ($item) {
            return $item->last_message_id;
        })->values();

        if ($lastChatId) {
            foreach ($chats as $key => $value) {
                $chats->forget($key);
                if ($value['id'] === $lastChatId) {
                    break;
                }
            }
        }

        $hasMoreResults = false;

        if ($chats->count()) {
            $hasMoreResults = !!($chats->count() - $chats->take($limit ?? 10)->count());
        }

        return ['chats' => $chats->take($limit ?? 10)->values(), 'hasMoreResults' => $hasMoreResults];
    }

    public function transferChatUserToOtherCategory($chatId, $newCategory)
    {
        $category = MessengerCategoryUserChat::where('user_id', '=', Auth::user()->id)
            ->where('chat_id', '=', $chatId)
            ->first();
        if (!empty($category->user_id)) {
            MessengerCategoryUserChat::where('user_id', '=', Auth::user()->id)
                ->where('chat_id', '=', $chatId)
                ->update([
                    'category_id' => $newCategory == 0 ? null : $newCategory,
                ]);
        } else {
            MessengerCategoryUserChat::firstOrCreate([
                'category_id' => $newCategory == 0 ? null : $newCategory,
                'chat_id' => $chatId,
                'user_id' => Auth::user()->id

            ]);
        }

    }

    public function getLastCategoryUser()
    {
        return MessengerCategoryChat::where('user_id', '=', Auth::user()->id)
            ->OrderbyDesc('id')
            ->first();
    }

    public function getLastChatUser()
    {
        $member = MessengerChatMember::where('user_id', '=', Auth::user()->id)
            ->OrderbyDesc('chat_id')
            ->first();
        $chat = MessengerChat::where('id', '=', $member->chat_id)
            ->whereHas('member', function ($query) {
                $query->where('user_id', '=', Auth::user()->id);
            })
            ->first();
        if ($chat->is_private == 1) {
            $contact = $this->getMessengerPrivateChatMemberByUser($chat, Auth::user());
            if (!empty($contact->user_id)) {
                $contactUser = User::where('id', '=', $contact->user_id)
                    ->first();
                $chat->name = $this->contact->getNicknameContactInterlocutor(Auth::user(), $contactUser) == null ? $contactUser->name : $this->contact->getNicknameContactInterlocutor(Auth::user(), $contactUser);
                $chat->full_link = URL::to('/') . '/' . $contactUser->avatar;
            } else {
                $chat->full_link = null;
            }
        }
        return $chat;
    }


    public function updateCategory($request, MessengerCategoryChat $category)
    {
        if ($request->has('file')) {
            Storage::disk('public')->delete($request->file);
            $path = $request->file->store('messenger/category' . $request->type, ['disk' => 'public']);
        }
        if (isset($request->transfer)) {
            $transfers = explode(',', $request->transfer);
            foreach ($transfers as $transfer) {
                $this->transferChatUserToOtherCategory($transfer, $category->id);
            }
        }
        return MessengerCategoryChat::where('id', '=', $category->id)
            ->update(['avatar' => isset($path) ? $path : $category->avatar,
                'name' => $request->name
            ]);
    }

    public function getSettingsChat(MessengerChat $chat)
    {
        return MessengerChatSettings::where('chat_id', '=', $chat->id)
            ->first();
    }

    public function joinToChat(MessengerChat $chat)
    {

        MessengerChatMember::firstOrCreate([
            'user_id' => Auth::user()->id,
            'chat_id' => $chat->id
        ]);
        UserSettingsInChat::firstOrCreate([
            'user_id' => Auth::user()->id,
            'chat_id' => $chat->id,
            'sound_on' => 1,
            'vibration_on' => 1,
        ]);
        $this->addColorUserToChat($chat, Auth::user());
    }


    public function actionTimeToSecretChat(TimeSecretChatRequest $request, MessengerChat $chat, $action)
    {
        MessengerChat::where('id', '=', $chat->id)
            ->update([
                'time_deleted' => $request->time_deleted,
            ]);
        $this->message->newAction($chat, $action, null);
        $message = $this->message->getLastUserMessage($chat);

        return $message;
    }

    public function getUserLastChats()
    {
        $chats = MessengerChat::whereHas('member', function ($query) {
            $query->where('user_id', '=', Auth::user()->id);
        })
            ->with('categoryChat')
            ->with('settings')
            ->limit(5)
            ->get();
        $chats->map(function ($chat) {
            $chat->last_message = $this->message->getLastMessageChat($chat->id);
            if ($chat->is_private == 1) {
                $contact = $this->getMessengerPrivateChatMember($chat);
                if (!empty($contact->user_id)) {
                    $user = User::where('id', '=', $contact->user_id)
                        ->first();
                    $chat->name = $this->contact->getNicknameContact($user) == null ? $user->name : $this->contact->getNicknameContact($user);
                    $chat->full_link = URL::to('/') . '/' . $user->avatar;
                } else {
                    $chat->full_link = null;
                }
            } else {
                $chat->full_link = URL::to('/') . '/' . $chat->avatar;
            }
        });

        return $chats;
    }

    public function reorderUserChatCategories($orders, $user)
    {
        foreach ($orders as $key => $value) {

            Validator::validate([
                'id' => $key,
                'order' => $value
            ], [
                'id' => 'required|numeric|exists:messenger_user_category_orders,category_id',
                'order' => 'required|numeric'
            ]);

            $key = (int)$key;
            $value = (int)$value;

            $categories = $user->messengerCategories()
                ->withPivot(['order'])
                ->orderBy('pivot_order', 'asc')
                ->get();

            if (!$categories->count()) {
                throw new SimpleException('There is no any categories.');
            }

            $category = $categories->where('id', $key)->first();

//            $oldOrder = $category->pivot->order;

//            if ($oldOrder === $value) {
//                return $categories;
//            }

            $category->pivot->update(['order' => $value]);

//            if ($oldOrder < $value) {
//                $prevCategories = $categories
//                                    ->where('pivot.order', '<=', $value)
//                                    ->where('id', '!=', $category->id);
//
//                foreach ($prevCategories as $prevCategory) {
//                    $order = $prevCategory->pivot->order;
//
//                    if ($order >= $oldOrder) {
//                        $prevCategory->pivot->update(['order' => $order - 1]);
//                    }
//                }
//            } else {
//                $nextCategories = $categories
//                                    ->where('pivot.order', '>=', $value)
//                                    ->where('id', '!=', $category->id);
//
//                foreach ($nextCategories as $nextCategory) {
//                    $order = $nextCategory->pivot->order;
//
//                    if ($order <= $oldOrder) {
//                        $nextCategory->pivot->update(['order' => $order + 1]);
//                    }
//                }
//            }
        }
    }

    public function setChat(MessengerChat $chat)
    {
        MessengerChat::where('id', '=', $chat->id)
            ->update(
                [
                    'is_secret' => 0,
                ]
            );
    }


    public function addColorUserToChat(MessengerChat $chat, $user)
    {
        $lastColorChat = $this->getLastColorInChat($chat);
        if (empty($lastColorChat->id)) {
            MessengerMessageUserColor::firstOrCreate(
                [
                    'chat_id' => $chat->id,
                    'user_id' => $user->id ?? Auth::user()->id,
                    'color' => 1
                ]
            );
        } else {
            if ($lastColorChat->color == 8) {
                MessengerMessageUserColor::firstOrCreate(
                    [
                        'chat_id' => $chat->id,
                        'user_id' => $user->id ?? Auth::user()->id,
                        'color' => 1
                    ]
                );
            } else {
                MessengerMessageUserColor::firstOrCreate(
                    [
                        'chat_id' => $chat->id,
                        'user_id' => $user->id ?? Auth::user()->id,
                        'color' => intval($lastColorChat->color) + 1
                    ]
                );
            }

        }
    }

    public function deleteColorUserChat(MessengerChat $chat)
    {
        return MessengerMessageUserColor::where('chat_id', '=', $chat->id)
            ->where('user_id', '=', Auth::user()->id)
            ->delete();
    }

    public function getLastColorInChat(MessengerChat $chat)
    {
        return MessengerMessageUserColor::where('chat_id', '=', $chat->id)
            ->orderByDesc('id')
            ->first();
    }

    public function getMessengerPrivateChatMember(MessengerChat $chat)
    {
        $user = MessengerChatMember::where('chat_id', '=', $chat->id)
            ->where('user_id', '!=', Auth::user()->id)
            ->first();
        if (!empty($user->user_id)) {
            $findedUser = User::where('id', '=', $user->user_id)
                ->first();
            $user->isOnline = $this->user->checkOnlineUser($findedUser);
            return $user;
        } else {
            return null;
        }

    }

    public function callGetIndexChannel($chat, $type)
    {

        $members = $this->getMembersChat($chat);
        $last_message = $this->message->getLastMessageChat($chat->id);
        foreach ($members as $user) {
            if ($chat->is_private == 1) {
                $contact = $this->getMessengerPrivateChatMemberByUser($chat, $user);
                if (!empty($contact->user_id)) {
                    $contactUser = User::where('id', '=', $contact->user_id)
                        ->first();
                    $chat->name = $this->contact->getNicknameContactInterlocutor($user, $contactUser) == null ? $contactUser->name : $this->contact->getNicknameContactInterlocutor($user, $contactUser);
                    $chat->full_link = URL::to('/') . '/' . $contactUser->avatar;
                } else {
                    $chat->full_link = null;
                }
            }

            $category = $this->getCategoryChat($chat, $user);
            $settings = $this->getSettingsChat($chat);
            $noReadMessage = $this->getCountUnreadMessageUserInChat($chat, $user);
            $chat->description = $chat->description == "null" ? NULL : $chat->description;
            $chat->description = $chat->description == null ? NULL : $chat->description;
            broadcast(new getIndexChat($chat, $user, $last_message, $category, $settings, $noReadMessage, $type, $chat->name));
        }
    }

    public function getUserLastChat(User $user)
    {
        $chats = MessengerChat::whereHas('member', function ($query) use ($user) {
            $query->where('user_id', '=', $user->id);
        })
            ->with('categoryChat')
            ->with('settings')
            ->get();

        $chats->map(function ($chat) use ($chats) {
            if ($chat->is_private == 1) {
                $contact = $this->getMessengerPrivateChatMember($chat);
                if (!empty($contact->user_id)) {
                    $user = User::where('id', '=', $contact->user_id)
                        ->first();
                    if (!empty($user->phone)) {
                        $chat->name = $this->contact->getNicknameContact($user) == null ? $user->name : $this->contact->getNicknameContact($user);
                        $chat->full_link = URL::to('/') . '/' . $user->avatar;
                    }
                } else {
                    $chat->full_link = null;
                }
            } else {
                $chat->full_link = URL::to('/') . '/' . $chat->avatar;
            }
            $chat->last_message = $this->message->getLastMessageChat($chat->id);
            $chat->last_message_id = $chat->last_message->id ?? null;
            $chat->no_read_message = $this->message->getUserChatNotReadMessage($chat);
        });

        $chats = $chats->sortByDesc(function ($item) {
            return $item->last_message_id;
        })->values();

        return $chats->take(1);
    }

    public function getChatByPrivateType(int $type)
    {
        $chats = MessengerChat::whereHas('member', function ($query) {
            $query->where('user_id', '=', Auth::user()->id);
        })
            ->where('is_private', '=', $type)
            ->with('categoryChat')
            ->with('settings')
            ->get();

        $chats->map(function ($chat) use ($chats) {
            if ($chat->is_private == 1) {
                $contact = $this->getMessengerPrivateChatMember($chat);
                if (!empty($contact->user_id)) {
                    $user = User::where('id', '=', $contact->user_id)
                        ->first();
                    if (!empty($user->phone)) {
                        $chat->name = $this->contact->getNicknameContact($user) == null ? $user->name : $this->contact->getNicknameContact($user);
                        $chat->full_link = URL::to('/') . '/' . $user->avatar;
                    }
                } else {
                    $chat->full_link = null;
                }
            } else {
                $chat->full_link = URL::to('/') . '/' . $chat->avatar;
            }
            $chat->last_message = $this->message->getLastMessageChat($chat->id);
            $chat->last_message_id = $chat->last_message->id ?? null;
            $chat->no_read_message = $this->message->getUserChatNotReadMessage($chat);
        });

        $chats = $chats->sortByDesc(function ($item) {
            return $item->last_message_id;
        })->values();

        return $chats;
    }

    public function checkUserAdminRoleChat(MessengerChat $chat)
    {
        $chatRole = MessengerChatMember::where('chat_id', '=', $chat->id)
            ->where('user_id', '=', Auth::user()->id)
            ->first();
        if (empty($chatRole->role)) {
            return false;
        } else {
            if ($chatRole->role == 'admin') {
                return true;
            } else {
                return false;
            }
        }
    }


    public function getRoleUserChat(MessengerChat $chat, User $user)
    {
        $role = MessengerChatMember::where('chat_id', '=', $chat->id)
            ->where('user_id', '=', $user->id)
            ->first();
        if (empty($role->role)) {
            return null;
        } else {
            return $role->role;
        }
    }

    public function getCategoryChat(MessengerChat $chat, User $user)
    {
        $category = MessengerCategoryUserChat::where('chat_id', '=', $chat->id)
            ->where('user_id', '=', $user->id)
            ->first();
        if (empty($category->category_id)) {
            return null;
        } else {
            return MessengerCategoryChat::where('id', '=', $category->category_id)
                ->first();
        }
    }


    public function getCountUnreadMessageUserInChat(MessengerChat $chat, User $user)
    {
        return MessengerMessage::where('chat_id', '=', $chat->id)
            ->where('from_id', '!=', $user->id)
            ->where('is_read', '=', 0)
            ->count();
    }


    public function getLastChatByUser(User $user)
    {
        return MessengerChat::whereHas('member', function ($query) use ($user) {
            $query->where('user_id', '=', $user->id);
        })
            ->OrderbyDesc('id')
            ->first();
    }

    public function checkPrivateExistChat($user)
    {
        $chat = MessengerChat::where('is_private', '=', 1)
            ->whereHas('member', function ($query) use ($user) {
                $query->where('user_id', Auth::user()->id);
            })
            ->whereHas('member', function ($query) use ($user) {
                $query->where('user_id', $user);
            })
            ->first();

        if (!empty($chat->id)) {
            if ($chat->is_private == 1) {
                $contact = $this->getMessengerPrivateChatMember($chat);
                if (!empty($contact->user_id)) {
                    $user = User::where('id', '=', $contact->user_id)
                        ->first();
                    if (!empty($user->phone)) {
                        $chat->name = $this->contact->getNicknameContact($user) == null ? $user->name : $this->contact->getNicknameContact($user);
                        $chat->full_link = URL::to('/') . '/' . $user->avatar;
                    }
                } else {
                    $chat->full_link = null;
                }
            } else {
                $chat->full_link = URL::to('/') . '/' . $chat->avatar;
            }
            return $chat;
        } else {
            return false;
        }
    }


    public function getAdminChatId(MessengerChat $chat)
    {
        $user = MessengerChatMember::where('chat_id', '=', $chat->id)
            ->where('role', '=', 'admin')
            ->first();
        if (!empty($user->user_id)) {
            return $user->user_id;
        } else {
            return null;
        }
    }


    public function getUserInPrivateChat(MessengerChat $chat)
    {
        $user = MessengerChatMember::where('chat_id', '=', $chat->id)
            ->where('user_id', '!=', Auth::user()->id)
            ->first();
        if (!empty($user->user_id)) {
            $result = User::where('id', '=', $user->user_id)
                ->select('id', 'phone', 'name', 'surname', 'youtube', 'email', 'site', 'whatsapp', 'facebook', 'instagram')
                ->first();
            $result->full_link = URL::to('/') . '/' . $result->avatar;
            return $result;
        } else {
            return null;
        }
    }

    public function addAdminUser($chat, $request)
    {
        $contacts = explode(',', $request->contact);
        foreach ($contacts as $contact) {
            MessengerChatMember::where('user_id', '=', $contact)
                ->where('chat_id', '=', $chat->id)
                ->update(
                    [
                        'role' => 'admin'
                    ]
                );
        }
    }


    public function deleteAdminUser($chat, $request)
    {
        $contacts = explode(',', $request->contact);
        foreach ($contacts as $contact) {
            MessengerChatMember::where('user_id', '=', $contact)
                ->where('chat_id', '=', $chat->id)
                ->update(
                    [
                        'role' => 'member'
                    ]
                );
        }
    }

    public function getPrivateChatwithUser($user)
    {

    }

    public function getInterlocutorInPrivateChat(MessengerChat $chat, $currentUser)
    {
        $user = MessengerChatMember::where('chat_id', '=', $chat->id)
            ->where('user_id', '!=', $currentUser->id)
            ->first();
        if (!empty($user->user_id)) {
            $result = User::where('id', '=', $user->user_id)
                ->select('id', 'phone', 'name', 'surname', 'youtube', 'email', 'site', 'whatsapp', 'facebook', 'instagram')
                ->first();
            $result->full_link = URL::to('/') . '/' . $result->avatar;
            return $result;
        } else {
            return null;
        }
    }

    public function getMessengerPrivateChatMemberByUser($chat, $getUser)
    {
        $user = MessengerChatMember::where('chat_id', '=', $chat->id)
            ->where('user_id', '!=', $getUser->id)
            ->first();
        if (!empty($user->user_id)) {
            return $user;
        } else {
            return null;
        }
    }


    public function getUserPublicChatAdmin(User $user)
    {
        $chats = MessengerChat::whereHas('member', function ($query) use ($user) {
            $query->where('user_id', '=', $user->id);
            $query->where('role', '=', 'admin');
            $query->where('show_chat', '=', 1);
        })
            ->with('ad')
            ->where('is_private', '=', 0)
            ->get();
        $chats->map(function ($chat) {
            $chat->full_link = URL::to('/') . '/' . $chat->avatar;
        });
        return $chats;
    }


    public function getAdChat()
    {
        $chats = MessengerChat::whereHas('ad.period', function ($query) {
            $query->where('to_period_date', '>', Carbon::now());
        })
            ->inRandomOrder()
            ->paginate($request->limit ?? 24);
        $chats->map(function ($chat) {
            $chat->full_link = URL::to('/') . '/' . $chat->avatar;
        });
        return $chats;
    }


    public function getStatisticAdChat($chat)
    {
        $checkChatAd = MessengerTariffChat::where('chat_id', '=', $chat->id)
            ->first();
        if (!empty($checkChatAd->ad_id)) {
            $getCurrentStatistic = MessengerViewGroups::where('chat_id', '=', $checkChatAd->chat_id)
                ->first();
            if (!empty($getCurrentStatistic->views)) {
                $total = MessengerAdChat::where('id', '=', $checkChatAd->ad_id)
                    ->first();
                $views = $getCurrentStatistic->views;
            } else {
                $views = 0;
            }

            return [
                'current_views' => $views,
                'total_views' => $total->total_views ?? 10000,
            ];
        } else {
            return null;
        }
    }

    public function incrementView($chat)
    {
        $checkChatAd = MessengerTariffChat::where('chat_id', '=', $chat->id)
            ->first();
        if (!empty($checkChatAd->ad_id)) {
            $checkViewsChat = MessengerViewGroups::where('chat_id', '=', $checkChatAd->chat_id)
                ->first();
            if (!empty($checkViewsChat->views)) {
                $views = $checkViewsChat;
            } else {
                $views = MessengerViewGroups::create([
                    'chat_id' => $chat->id,
                    'views' => 0,
                ]);
            }

            DB::transaction(function () use ($views) {
                $views->increment('views', 1);
            });
        }
    }
}
