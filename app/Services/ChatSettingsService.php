<?php


namespace App\Services;


use App\Models\Messenger\MessengerChat;

class ChatSettingsService
{
    public function switchSound(MessengerChat $chat, int $userId, int $soundOn)
    {
        $chatSettings = $chat->usersSettings()
            ->where('id', $userId)
            ->withPivot('sound_on')
            ->first();

        $chatSettings->pivot->sound_on = $soundOn;
        $chatSettings->pivot->save();

        return $chatSettings->pivot;
    }

    public function switchVibration(MessengerChat $chat, int $userId, int $vibrationOn)
    {
        $chatSettings = $chat->usersSettings()
            ->where('id', $userId)
            ->withPivot('vibration_on')
            ->first();

        $chatSettings->pivot->vibration_on = $vibrationOn;
        $chatSettings->pivot->save();

        return $chatSettings->pivot;
    }
}
