<?php


namespace App\Services;


use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class FirebaseService
{
    static public function send($tokens, $data = [], $title = null, $text = null)
    {
        Log::error('tokens ' . json_encode($tokens));
        Log::error('data ' . json_encode($data));


        $response = Http::withHeaders([
            'Authorization' => "key=AAAAHcpu-oQ:APA91bHZaHLTBC9lpps8WcavXEHkrs-cR0DHZRonwEtlW4OOPi2ywTbhd0P1FX25XGs152gxfN-8rIVQQP-5pQ9Fan5dYSSVxwgka5hHmUXWvEXmIZXWLyLYMk-rn86OG7lVeyydtxzm",
            'Content-Type' => 'application/json',
        ])->post('https://fcm.googleapis.com/fcm/send', [
            "registration_ids" => $tokens,
            "message_id" => Str::random(15),
            'priority' => 'high',
            'sound' => true,
            'badge' => true,
            'alert' => true,
            'content_available' => true,
            'notification' => [
                'title' => $title,
                'body' => $text,
                'data' => $data,
                "content_available" => true
            ],
            'data' => $data,
            'apns' => [
                'payload' => [
                    'aps' => [
                        'content_available' => true
                    ]
                ]
            ]
        ]);
        Log::error('logging ' . $response->body());
        Log::error('status code ' . $response->status());

        return $response->successful();

    }
}
