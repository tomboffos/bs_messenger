<?php

namespace App\Services;

use App\Http\Requests\CreateMediaCategory;
use App\Http\Requests\UpdateFileRequest;
use App\Models\Media;
use App\Models\MediaCategory;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Pawlox\VideoThumbnail\VideoThumbnail;


class MediaService
{

    public function getAllMediaCategory(){
        return MediaCategory::withCount('media')
            ->get();
    }

    public function getMedAppMediaCategory(){  //будущее медапп Медиа грамоты и т.д.
        return MediaCategory::withCount('media')
            //->where('name')
            ->get();
    }


    public function create(CreateMediaCategory $category){
        return MediaCategory::create([
            'name' =>$category->name,
        ]);
    }

    public function show(MediaCategory $category){
        return $category->media();
    }

    public function update(CreateMediaCategory $request,MediaCategory $category){

        return MediaCategory::where('id','=',$category->id)
        ->update([
            'name' => $request->name,
        ]);
    }

    public function delete(MediaCategory $category){
        return MediaCategory::where('id','=',$category->id)
        ->delete();
    }

   public function getMediaFull(){
       $media = Media::all();

       $media->map(function ($mediaFullLink){
           $mediaFullLink->full_link = URL::to('/').'/'.$mediaFullLink->file;
       });

       return $media;
   }

    public function getMediaUser(){
        $media = Media::where('user_id','=',Auth::user()->id)
            ->get();
        $media->map(function ($mediaFullLink){
            $mediaFullLink->full_link = URL::to('/').'/'.$mediaFullLink->file;
        });

        return $media;
    }


    public function newFile($file,$type)
    {
        switch ($type){
            case $type == 'image' :
                $this->savePath($file,'messenger/image',$type);
                break;
            case $type == 'audio' :
                $this->savePath($file,'messenger/audio',$type);
                break;
            case $type == 'video' :
                $this->savePath($file,'messenger/video',$type);
                break;
        }

    }

    public function saveImage($file,$directory){
        return $file->store($directory, ['disk' => 'public']);
    }

    public function savePath($file,$directory,$type){
        $path = $file->store($directory, ['disk' => 'public']);
        if ($type == 'audio' || $type == 'video'){
            $getID3 = new \getID3;
            $file = $getID3->analyze($path);
        }

        if ($type == 'video'){
            $fileName = Carbon::now().'.jpg';
            $video = new VideoThumbnail();
            $video->createThumbnail($path, 'messenger/thumbs/', $fileName, 2, 640, 480);

        }
        Media::create([
            'file' => $path,
            'type' => $type,
            'user_id' => Auth::user()->id,
            'category_id' => null,
            'duration' => $type == 'image' ? null :  intval($file['playtime_seconds']),
            'preload_photo' => $type == 'video' ? 'messenger/thumbs/'.$fileName : null,
        ]);
    }

    public function deleteMedia($media){
        $mediaFile = Media::where('id','=',$media)
            ->first();
        if (!empty($mediaFile)){
            Storage::disk('public')->delete($mediaFile->file);
        }
         Media::where('id','=',$media)
            ->delete();
    }


    public function showMedia(Media $media){
        $media->full_link = URL::to('/').'/'.$media->file;

        return $media;
    }

    public function updateMedia(Media $media,UpdateFileRequest $request)
    {
        Storage::disk('public')->delete($media->file);
        $path = $request->file->store('messenger/' . $media->type, ['disk' => 'public']);
        return Media::where('id', '=', $media->id)
            ->update(['file' => $path,]);
    }

    public function getLastMediaFileUser(){
        return Media::where('user_id','=',Auth::user()->id)
            ->orderByDesc('id')
            ->first();
    }
}
