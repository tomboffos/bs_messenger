<?php

namespace App\Services;


use App\Events\getIndexChat;
use App\Events\NewMessage;
use App\Http\Requests\DeleteMessageRequest;
use App\Http\Requests\SendMessageRequest;
use App\Http\Requests\SetTopMessageRequest;
use App\Http\Requests\UpdateMessageTextRequest;
use App\Jobs\DeleteSecretMessageJob;
use App\Models\Messenger\MessengerCategoryChat;
use App\Models\Messenger\MessengerCategoryUserChat;
use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\MessengerChatMember;
use App\Models\Messenger\MessengerDeletedMessageUser;
use App\Models\Messenger\MessengerMapCoordinates;
use App\Models\Messenger\MessengerMediaMessage;
use App\Models\Messenger\MessengerMessage;
use App\Models\Messenger\MessengerMessageReadUser;
use App\Models\Messenger\MessengerMessageUserColor;
use App\Models\Messenger\MessengerTransferMessage;
use App\Models\Messenger\MessengerContactMessage;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;


class MessageService
{
    public $media;
    public $contact;
    public $user;

    public function __construct(MediaService $media,SearchService $contact,UserService $user){
        $this->media = $media;
        $this->contact = $contact;
        $this->user = $user;
    }


    public function getMessageChat(MessengerChat $chat, int $lastMessageId=null, int $limit=null, string $type) {
        $noReadMesssage =  MessengerMessage::where('chat_id','=',$chat->id)
            ->where('from_id','!=', Auth::user()->id)
            ->where('is_read','=',0)
            ->get();

        foreach ($noReadMesssage as $message){
            if ($message->time_deleted != null){
                $this->callNewMessageEvent($message,'StartTimerSecretMessage');
            }
        }

        $lastNotReadMessage = $this->getLastMessageNotReadUser($chat);
        if (!empty($lastNotReadMessage->from_id)){
            $this->callNewMessageEvent($lastNotReadMessage,'MessageRead');
            $members =  User::whereHas('member',function ($query) use($chat){
                $query->where('chat_id','=',$chat->id);
            })
                ->select('id','name','phone','last_visit','avatar')
                ->get();
            $last_message = $this->getLastMessageChat($chat->id);
            foreach ($members as $user){
                if ($chat->is_private == 1) {
                    $contact = $this->getMessengerPrivateChatMemberByUser($chat,$user);
                    if (!empty($contact->user_id)) {
                        $contactUser = User::where('id', '=', $contact->user_id)
                            ->first();
                        $chat->name = $this->contact->getNicknameContactInterlocutor($user,$contactUser) == null ? $contactUser->name : $this->contact->getNicknameContactInterlocutor($user,$contactUser);
                        $chat->full_link = URL::to('/') . '/' . $contactUser->avatar;
                    } else {
                        $chat->full_link = null;
                    }
                }
                broadcast(new getIndexChat($chat,$user,$last_message,null,null,null,'MessageRead',$chat->name));
            }
           // $this->chat->callGetIndexChannel($chat,"MessageRead");
        }
        MessengerMessage::where('chat_id','=',$chat->id)
            ->where('from_id','!=', Auth::user()->id)
            ->update(['is_read' => true]);
        MessengerMessageReadUser::where('chat_id','=',$chat->id)
            ->where('user_id','=',Auth::user()->id)
            ->update(['is_read' => 1]);

        $query = MessengerMessage::where('chat_id', $chat->id)->whereDoesntHave('deletedMessages',function ($q) {
                $q->where('user_id', Auth::id());
            });

        if ($lastMessageId) {
            $query->where('id', $type === 'top' ? '<' : '>', $lastMessageId);
        }

        $messages = $query->with('file')
            ->with('transfer')
            ->with('map')
            ->with('fromContact',function ($query){
                $query->select('id','phone','name','surname','avatar');
            })
            ->with('toContact',function ($query){
                $query->select('id','phone','name','surname','avatar');
            })
            ->with('transfer.fromContact',function ($query){
                $query->select('id','phone','name','surname','avatar');
            })
            ->with('transfer.toContact',function ($query){
                $query->select('id','phone','name','surname','avatar');
            })
            ->with('contact',function ($query){
                $query->select('id','phone','name','surname','avatar');
            })
            ->orderBy('id', $type === 'top' ? 'desc' : 'asc')
            ->limit($limit ?? 20)
            ->get();

        $messages->map(function ($message){
           $color = MessengerMessageUserColor::where('chat_id','=',$message->chat_id)
                    ->where('user_id','=',$message->from_id)
                    ->first();
            empty($color->color) ? $message->color = 1 :
            $message->color = $color->color;

            $message->file->map(function ($file){
               $file->full_link = URL::to('/').'/'.$file->file;
               $file->full_preload = URL::to('/').'/'.$file->preload_photo;
            });
        });

        $messages = $messages->sortByDesc('id')->values();

        $hasMoreResults = false;
        if ($messages->count()) {
            $hasMoreResults = !!MessengerMessage::where('id', $type === 'top' ? '<' : '>', $messages->last()->id)
                ->count();
        }

        return ['messages' => $messages, 'hasMoreResults' => $hasMoreResults];
    }

    public function getChatMessagesBySearch(MessengerChat $chat, MessengerMessage $message)
    {
        MessengerMessage::where('chat_id', $chat->id)
            ->where('from_id', '!=', Auth::id())
            ->update(['is_read' => true]);
        MessengerMessageReadUser::where('chat_id', $chat->id)
            ->where('user_id', Auth::id())
            ->update(['is_read' => 1]);

        $messages = MessengerMessage::where('chat_id', $chat->id)
            ->where('id', '<', $message->id + 10)
            ->with('file')
            ->with('transfer')
            ->with('map')
            ->with('contact',function ($query){
                $query->select('id','phone','name','surname','avatar');
            })
            ->with('fromContact',function ($query){
                $query->select('id','phone','name','surname','avatar');
            })
            ->with('transfer.fromContact',function ($query){
                $query->select('id','phone','name','surname','avatar');
            })
            ->orderBy('id', 'desc')
            ->limit(20)
            ->get();

        $messages->map(function ($message){
            $color = MessengerMessageUserColor::where('chat_id', $message->chat_id)
                ->where('user_id','=',$message->from_id)
                ->first();
            empty($color->color) ? $message->color = 1 :
                $message->color = $color->color;
        });

        $hasMoreResultsTop = false;
        $hasMoreResultsBot = false;

        if ($messages->count()) {
            $hasMoreResultsTop = !!MessengerMessage::where('id', '<', $messages->last()->id)->count();
            $hasMoreResultsBot = !!MessengerMessage::where('id', '>', $messages->first()->id)->count();
        }

        return [
            'messages' => $messages,
            'hasMoreResultsTop' => $hasMoreResultsTop,
            'hasMoreResultsBot' => $hasMoreResultsBot
        ];
    }

    public function newAction(MessengerChat $chat, $action, $user){
        MessengerMessage::create([
            'action' => $action,
            'from_id' => Auth::user()->id,
            'to_id' => $user,
            'chat_id' => $chat->id,
            'is_read' => 0,
        ]);
    }


    public function sendMessage(SendMessageRequest $request,MessengerChat $chat){
        switch ($request){
            case $request->hasFile('file'):
                $type = 'file';
                break;
            case isset($request->latitude):
                $type = 'map';
                break;
            case isset ($request->contact_id):
                $type = 'contact';
                break;
            default:
                $type = null;
                break;
        }

        MessengerMessage::create([
            'from_id' => Auth::user()->id,
            'to_id' => $request->contact_id ?? null,
            'text' => $request->text,
            'date_message' => Carbon::now(),
            'is_read' => 0,
            'time_deleted' => $request->time_deleted ?? null,
            'chat_id' => $chat->id,
            'type' => $type,
        ]);
        $lastUserMessage = MessengerMessage::where('from_id','=',Auth::user()->id)
            ->where('chat_id','=',$chat->id)
            ->with('color')
            ->with('transfer')
            ->with('transfer.fromContact')
            ->with('transfer.toContact')
            ->with('toContact')
            ->with('map')
            ->with('file')
            ->orderByDesc('id')
            ->first();

        if (isset($request->forward)){

            $forwardMessages = explode ( ',',$request->forward);
            foreach ($forwardMessages as $message){
                MessengerTransferMessage::create([
                    'message_id' => $lastUserMessage->id,
                    'transfer_message_id' => $message
                ]);
            }
        }
        if ($request->hasFile('file')){
            foreach ($request->file as $file){
                $this->media->newFile($file,$request->type);
                $media = $this->media->getLastMediaFileUser();
                MessengerMediaMessage::create(
                    [
                        'message_id' => $lastUserMessage->id,
                        'media_id'  => $media->id
                    ]
                );
            }
        }
        if ($request->hasFile('fileDevice')){
            foreach ($request->fileDevice as $file){
                $this->media->newFile($file,$request->type);
                $media = $this->media->getLastMediaFileUser();
                MessengerMediaMessage::create(
                    [
                        'message_id' => $lastUserMessage->id,
                        'media_id'  => $media->id
                    ]
                );
            }
        }
        if ($request->get('latitude') != null && $request->get('longitude') != null){
                MessengerMapCoordinates::create(
                    [
                        'message_id' => $lastUserMessage->id,
                        'latitude'  => $request->get('latitude'),
                        'longitude'  => $request->get('longitude'),
                        'address'  => $request->get('address') ?? null,
                    ]
                );
        }

        if ($request->get('contact_id') != null){
            MessengerContactMessage::create(
                [
                    'message_id' => $lastUserMessage->id,
                    'contact_id'  => $request->get('contact_id'),
                ]
            );
        }
         $result = $this->getLastUserMessage($chat);
         $color = $this->getUserColorInChat($chat);
         empty($color->color) ? $result->color = 1 : $result->color = $color->color;


        return $result;
    }

    public function setTopMessage (SetTopMessageRequest $request,MessengerChat $chat){
        $chat = MessengerChat::where('id','=',$chat->id)
            ->first();
        $chat->top_message_id = $request->message_id;
        $chat->save();
        $message =MessengerMessage::where('id','=',$chat->top_message_id)
            ->with('color')
            ->with('transfer')
            ->with('transfer.fromContact')
            ->with('transfer.toContact')
            ->with('toContact')
            ->with('contact',function ($query){
                $query->select('id','phone','name','surname','avatar');
            })
            ->with('map')
            ->with('file')
            ->first();
        return ['message' => $message ,'chat' => $chat];
    }

    public function unsetTopMessage (MessengerChat $chat){
        MessengerChat::where('id','=',$chat->id)
            ->update([
                'top_message_id' => null
            ]);
    }


    public function getTopMessage(MessengerChat $chat){
        return $chat->topMessage;
    }

    public function update(MessengerMessage $message,UpdateMessageTextRequest $request){
        return MessengerMessage::where('id','=',$message->id)
            ->update(
                [
                    'text' => $request->text
                ]
            );
    }
    public function delete(MessengerMessage $message,DeleteMessageRequest $request){
        if (!empty($request->for_me) || $request->for_me == 0){
            return MessengerMessage::where('id','=',$message->id)
                ->restore();
        }else{
            $file = MessengerMediaMessage::where('message_id','=',$message->id)
                ->first();

            MessengerMessageReadUser::where('message_id','=',$message->id)
                ->delete();

            if (!empty($file->message_id)){
                MessengerMediaMessage::where('message_id','=',$message->id)
                    ->delete();
            }
            $forward = MessengerTransferMessage::where('message_id','=',$message->id)
                ->orWhere('transfer_message_id','=',$message->id)
                ->first();
            if (!empty($forward->message_id)){
                MessengerTransferMessage::where('message_id','=',$message->id)
                    ->orWhere('transfer_message_id','=',$message->id)
                    ->delete();
            }
            return MessengerMessage::where('id','=',$message->id)
                ->delete();

        }
    }

    public function getLastUserMessage(MessengerChat $chat){
        $message =  MessengerMessage::where('from_id','=',Auth::user()->id)
                ->where('chat_id','=',$chat->id)
                ->with('file')
                ->with('transfer')
                ->with('transfer.file')
                ->with('transfer.map')
                ->with('map')
                ->with('contact',function ($query){
                    $query->select('id','phone','name','surname','avatar');
                })
                ->with('fromContact',function ($query){
                    $query->select('id','phone','name','surname','avatar');
                })
                ->with('transfer.fromContact',function ($query){
                    $query->select('id','phone','name','surname','avatar');
                })
                ->orderByDesc('id')
                ->first();

        if (!empty($message->file->id)){
            $message->file->full_link =  URL::to('/').'/'.$message->file->avatar;
        }
        if (!empty($message->fromContact->id)){
            $message->fromContact->full_link =  URL::to('/').'/'.$message->fromContact->avatar;
        }

        return  $message;
    }

    public function getLastMessageChat($chatId){
        return MessengerMessage::where('chat_id', $chatId)
            ->orderBy('id', 'desc')
            ->first();
    }

    public function getUserCategoryNotReadMessage(MessengerCategoryChat $category){
        $total = 0;
        $categoriesList = MessengerCategoryUserChat::where('category_id','=',$category->id)
            ->where('user_id','=',Auth::user()->id)
            ->get();

        foreach ($categoriesList as $list){
            $chat = MessengerChat::where('id','=',$list->chat_id)
                ->first();
            if ($this->getUserChatNotReadMessage($chat) > 0){
                $total++;
            }
        }

        return $total;
    }

    public function getUserChatNotReadMessage(MessengerChat $chat){
        return  MessengerMessage::where('chat_id','=',$chat->id)
            ->where('from_id','!=', Auth::user()->id)
            ->where('is_read','=',0)
            ->count();
    }

    public function getUserColorInChat(MessengerChat $chat){
        return MessengerMessageUserColor::where('chat_id','=',$chat->id)
            ->where('user_id','=',Auth::user()->id)
            ->first();
    }
    public function getMessageTopChat(MessengerChat $chat){
        return MessengerMessage::where('id','=',$chat->top_message_id)
            ->firstOrFail();
    }

    public function deleteSecretChat($message,$eventMessage = null){
        $message = MessengerMessage::findOrFail($message->id);
        MessengerTransferMessage::where('message_id','=',$message->id)
            ->delete();
        MessengerMediaMessage::where('message_id','=',$message->id)
            ->delete();
        $message->delete();
        $this->forceDelete($message);
    }

    public function forceDelete($message){
        $project = MessengerMessage::where('id', $message->id)->withTrashed()->first();
        $project->forceDelete();
    }

    public function callNewMessageEvent(MessengerMessage $message,$type){
        if ($type == 'StartTimerSecretMessage'){
            DeleteSecretMessageJob::dispatch($message,$this)->delay(\Carbon\Carbon::now()->addSeconds($message->time_deleted));
        }
        $chat  = MessengerChat::where('id','=',$message->chat_id)
            ->first();
        $color = $this->getUserColorInChat($chat);
        empty($color->color) ? $result = 1 : $result = $color->color;
        broadcast(new NewMessage($message,$type,$result));
    }


    public function safeDelete(MessengerMessage $message,User $user){
        MessengerDeletedMessageUser::firstOrCreate(
            [
                'message_id' => $message->id,
                'user_id' => $user->id
            ]
        );
    }


    public function getLastMessageNotReadUser(MessengerChat $chat){
        return MessengerMessage::where('from_id','!=',Auth::user()->id)
            ->where('chat_id','=',$chat->id)
            ->where('is_read','=',0)
            ->orderByDesc('id')
            ->first();
    }


    public function getMessengerPrivateChatMember(MessengerChat $chat){
        $user = MessengerChatMember::where('chat_id','=',$chat->id)
            ->where('user_id','!=',Auth::user()->id)
            ->first();
        if (!empty($user->user_id)){
            $findedUser = User::where('id','=',$user->user_id)
                ->first();
            $user->isOnline = $this->user->checkOnlineUser($findedUser);
            return $user;
        }else{
            return null;
        }

    }

    public function getMessengerPrivateChatMemberByUser($chat,$getUser){
        $user = MessengerChatMember::where('chat_id','=',$chat->id)
            ->where('user_id','!=',$getUser->id)
            ->first();
        if (!empty($user->user_id)){
            return $user;
        }else{
            return null;
        }
    }
}
