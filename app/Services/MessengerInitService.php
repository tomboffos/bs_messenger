<?php

namespace App\Services;

use App\Http\Requests\CreateMediaCategory;
use App\Http\Requests\NewFileRequest;
use App\Http\Requests\UpdateFileRequest;
use App\Models\City;
use App\Models\Media;
use App\Models\MediaCategory;
use App\Models\Messenger\MessengerCategoryChat;
use App\Models\Messenger\MessengerCategoryUserChat;
use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\MessengerChatMember;
use App\Models\Messenger\MessengerChatSettings;
use App\Models\Messenger\MessengerChatUserSettings;
use App\Models\MessengerUserCategoryOrder;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class MessengerInitService
{
    public function initUserSettings(User $user){

        $settings = MessengerChatUserSettings::where('user_id','=',$user->id)
            ->get();

        if (isset($settings)){
            MessengerChatUserSettings::firstOrCreate(
                [
                    'sound' => 1,
                    'vibration' => 1,
                    'show_phone' => 1,
                    'show_online' => 1,
                    'user_id' => $user->id,
                ]
            );
        }
    }

    public function pushToCityChat(User $user){
        $city = City::where('id','=',$user->city_id)
            ->first();
        if (!empty($city)){
            $chatCategory = MessengerCategoryChat::where('name','=','Город')
                ->first();
            if (empty($chatCategory->name)){
                MessengerCategoryChat::create(
                    [
                        'name' => 'Город',
                        'avatar' => '/',
                        'app_chat' => 1,
                    ]
                );
            }
            $chat = MessengerCategoryUserChat::where('user_id','=',$user->id)
                ->whereHas('chat',function ($query) use ($city){
                    $query->where('name','=',$city->name);
                })
                ->first();

            if (empty($chat->category_id)){
                MessengerChat::firstOrCreate(
                    [
                        'name' => $city->name,
                        'avatar' => null,
                    ]
                );

                $chat = MessengerChat::where('name','=',$city->name)
                    ->first();
                MessengerChatSettings::firstOrCreate(
                    [
                        'chat_id' => $chat->id,
                        'admin_media_send'   => '0',
                        'admin_message_send'   => '0',
                        'admin_change_data'   => '1',
                        'transfer_chat_message'   => '1',
                        'sound'   => '0',
                        'is_secret'   => '0',
                    ]
                );
                MessengerCategoryUserChat::firstOrCreate(
                    [
                        'user_id' => $user->id,
                        'category_id' => $chatCategory->id,
                        'chat_id' => $chat->id,
                    ]
                );
                MessengerChatMember::firstOrCreate(
                    [
                        'chat_id' => $chat->id,
                        'user_id' => $user->id,
                        'role' => 'member',
                        'show_chat' => 1,
                    ]
                );
            }
        }
    }


    public function OrderCategoryInit(User $user){
        $categories = MessengerCategoryChat::where('app_chat','=',1)
            ->orWhere('user_id','=',$user->id)
            ->get();
        $order = 1;

        foreach ($categories as $category){
            MessengerUserCategoryOrder::firstOrCreate(
                [
                    'category_id' => $category->id,
                    'user_id' => $user->id,
                    'order' => $order++,
                ]
            );
        }
    }

}
