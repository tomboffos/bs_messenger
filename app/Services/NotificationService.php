<?php

namespace App\Services;


use App\Models\PlayerId;


class NotificationService
{
    function sendMessage( $header,$message,$avatar,$chat) {
        $oneSignal = PlayerId::where('user_id','=',$header->id)
            ->orderByDesc('id')
            ->get()->pluck('player_id')->toArray();

        $fields = array(
            'app_id' => '7b554f28-7075-49ef-91a9-dc65ad83486',
            'include_player_ids' => $oneSignal,
            'contents' => array("en" =>$message),
            'headings' => array("en"=>$header->name),
            'largeIcon' => $avatar,
            'chat_id' => $chat->id,
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic OTFhNjQzOTQtOGQyNy00ODljLTlkZjEtNmQxNjZhNDBhOWU4'));
                                       curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                                       curl_setopt($ch, CURLOPT_HEADER, FALSE);
                                       curl_setopt($ch, CURLOPT_POST, TRUE);
                                       curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                                       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                                       $response = curl_exec($ch);
                                       curl_close($ch);
    }
}
