<?php

namespace App\Services;

use App\Http\Requests\CreateMediaCategory;
use App\Http\Requests\NewFileRequest;
use App\Http\Requests\SearchMessageRequest;
use App\Http\Requests\UpdateFileRequest;
use App\Models\Media;
use App\Models\MediaCategory;
use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\MessengerContactUser;
use App\Models\Messenger\MessengerMessage;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class SearchService
{

    public function searchUserByPhoneOrName($request){

        $users =  User::where('phone', 'LIKE',$request->search . '%')
            ->orWhere('name', 'LIKE',$request->search . '%')
            ->where('id', '!=', Auth::id())
            ->select('id','name','phone','last_visit','avatar')
            ->paginate($request->limit ?? 20);
        $users->getCollection()->transform(function ($user) {
            $user->full_link = URL::to('/').'/'.$user->file;
            $user->name = $this->getNicknameContact($user) == null ? $user->name : $this->getNicknameContact($user);
            $user->surname = $this->getNicknameContact($user) == null ? $user->surname : NULL;
            return $user;
        });


          return $users;
    }



    public function getUserContact(Request $request){
       $users =  User::whereHas('inverseContact',function ($query){
            $query->where('user_id','=',Auth::user()->id);
        })
            ->select('id','phone','avatar','name','surname','last_visit')
            ->get();
        $users->map(function ($user){
            $user->full_link = URL::to('/').'/'.$user->avatar;
            $user->isContact = $this->getNicknameContact($user) == null ? 0 : 1;
            $user->name = $this->getNicknameContact($user) == null ? $user->name : $this->getNicknameContact($user);
            $user->surname = $this->getNicknameContact($user) == null ? $user->surname : NULL;
        });

        $collection = collect($users->sortByDesc('isContact'));
        $page = $request->page ?? 1;
        $perPage = $request->limit ?? 20;

        return  $paginate = new LengthAwarePaginator(
            $collection->forPage($page, $perPage),
            $collection->count(),
            $perPage,
            $page,
            ['path' => url('api/messenger/user/contact')]
        );
    }

    public function searchMessage(SearchMessageRequest $request){
        return MessengerMessage::where('chat_id','=',$request->chat_id)
            ->where('text', 'LIKE', '%' . $request->search . '%')
            ->paginate($request->limit);

    }


    public function searchMessageAllChat(SearchMessageRequest $request){
        $messages =  MessengerMessage::whereHas('chat.member',function ($query){
            $query->where('user_id','=',Auth::user()->id);
        })
            ->where('text', 'LIKE', '%' . $request->search . '%')
            ->with('fromContact',function ($query){
                $query->select('id','phone','name','surname','avatar');
            })
            ->with('chat')
            ->with('chat',function ($query){
                $query->select('id','name');
            })
           ->paginate($request->limit);

        return $messages;
    }


    public function searchNotPrivateGroupChat(SearchMessageRequest $request){
        return MessengerChat::where('name', 'LIKE', '%' . $request->search . '%')
            ->where('is_private','=',0)
            ->inRandomOrder()
            ->limit(10)
            ->get();
    }


    public function getNicknameContact($user){
        if ($user == null){
            return null;
        }
        $contact = MessengerContactUser::where('user_id','=',Auth::user()->id)
            ->where('contact_id','=',$user->id)
            ->first();
        if (!empty($contact->user_id)){
            return $contact->nickname;
        }else{
            return null;
        }

    }



    public function getNicknameContactInterlocutor($user,$interlocutor){
        $contact = MessengerContactUser::where('user_id','=',$user->id)
            ->where('contact_id','=',$interlocutor->id)
            ->first();
        if (!empty($contact->user_id)){
            return $contact->nickname;
        }else{
            return null;
        }

    }
}
