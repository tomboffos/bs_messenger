<?php

namespace App\Services;


use App\Http\Requests\BlockUserRequest;
use App\Http\Requests\UpdateCategoryChatRequest;
use App\Http\Requests\UpdateSettingsChat;
use App\Http\Requests\UpdateUserSettingsRequest;
use App\Jobs\MakeToChatFromSecretAfterTimeJob;
use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\MessengerChatSettings;
use App\Models\Messenger\MessengerChatUserSettings;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class SettingsService{

    public $chat;

    public $message;

    public function __construct(ChatService $chat,MessageService $message){
        $this->chat = $chat;
        $this->message = $message;
    }
    public function getSettingsChat(MessengerChat $chat){
        return MessengerChatSettings::where('chat_id','=',$chat->id)
            ->first();
    }


    public function updateSettingsChat(MessengerChat $chat,UpdateSettingsChat $request){
        if (isset($request->secret_time)){
            MakeToChatFromSecretAfterTimeJob::dispatch($chat,$this->chat,$this->message,Auth::user())->delay(Carbon::now()->addSeconds($request->secret_time));
        }

        MessengerChat::where('id','=',$chat->id)
            ->update(
                [
                    'is_secret' => isset($request->is_secret) ? $request->is_secret  : 0 ,
                    'whatsapp' => isset($request->whatsapp) ? $request->whatsapp  :  $chat->whatsapp ,
                    'youtube' => isset($request->youtube) ? $request->youtube  : $chat->youtube ,
                    'facebook' => isset($request->facebook) ? $request->facebook  :  $chat->facebook ,
                    'instagram' => isset($request->instagram) ? $request->instagram  :  $chat->instagram ,
                    'site' => isset($request->site) ? $request->site  :  $chat->site ,
                    'email' => isset($request->email) ? $request->email  :  $chat->email ,
                ]
            );
        $settings = MessengerChatSettings::where('chat_id','=',$chat->id)
            ->first();
        if (($chat->is_secret == 0) && ($request->is_secret == 1)){
            $this->message->newAction($chat,'SetSecretChat',null);
            $message = $this->message->getLastMessageChat($chat->id);
            $this->message->callNewMessageEvent($message,'NewMessage');
            $this->chat->callGetIndexChannel($chat,"SetSecretChat");
        }
        if (($chat->is_secret == 1) && ($request->is_secret == 0)){
            $this->message->newAction($chat,'UnsetSecretChat',null);
            $message = $this->message->getLastMessageChat($chat->id);
            $this->message->callNewMessageEvent($message,'NewMessage');
            $this->chat->callGetIndexChannel($chat,"UnsetSecretChat");
        }
        return MessengerChatSettings::where('chat_id','=',$chat->id)
            ->update([
                'admin_media_send' => isset($request->admin_media_send) ? $request->admin_media_send : $settings->admin_media_send ,
                'admin_message_send' => isset($request->admin_message_send) ? $request->admin_message_send : $settings->admin_message_send ,
                'admin_change_data' => isset($request->admin_change_data) ? $request->admin_change_data : $settings->admin_change_data ,
                'transfer_chat_message' => isset($request->transfer_chat_message) ? $request->transfer_chat_message : $settings->transfer_chat_message ,
                'sound' => isset($request->sound) ? $request->sound : $settings->sound ,
                'is_secret' => isset($request->is_secret) ? $request->is_secret : $settings->is_secret ,
            ]);


    }


    public function getSettingUser(){
        return MessengerChatUserSettings::where('user_id','=',Auth::user()->id)
            ->first();
    }

    public function updateSettingUser(UpdateUserSettingsRequest $request){
        MessengerChatUserSettings::where('user_id','=',Auth::user()->id)
            ->update([
                'sound' => $request->sound,
                'vibration' => $request->vibration,
                'show_phone' => $request->show_phone,
                'show_online' => $request->show_online,
            ]);

    }

}
