<?php

namespace App\Services;


use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class TransactionService
{

    public function addLogTransaction($action,$amount,$user,$type,$application){
      return  Transaction::create(
            [
                'user_id' => $user->id,
                'amount' => $amount,
                'action' => $action,
                'type'  => $type,
                'application_id'  => $application,
            ]
        );
    }

    public function addBalanceUser(User $user,$amount){
        return DB::transaction(function () use ($user,$amount) {
            $user->increment('balance', $amount);
        });
    }


    public function minusBalanceUser(User $user,$amount){
        return DB::transaction(function () use ($user,$amount) {
            $user->decrement('balance', $amount);
        });
    }

}
