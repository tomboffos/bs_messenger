<?php

namespace App\Services;



use App\Models\Application;
use App\Models\Microphone\Ad;
use App\Models\Referral;
use App\Models\Role;
use App\Models\User;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class UserService
{
    public function getCurrentUser($request){
        $user = Auth::user();

        $user->update(['last_visit' => now()]);

        $referrerStatus = $user->referrerStatuses()->where('application_id', $request['application_id'])->first();
        $referrer = null;
        if ($referrerStatus) {
            $referrer = $referrerStatus->referrerUser()->first();
            if ($referrer) {
                $referrer['agent_status'] = $referrer->referrerStatuses()->where('application_id', $request['application_id'])->first()['agent_status'];
            }
        }
        $referralsStatuses = $user->referralsStatuses()->where('application_id', $request['application_id'])->get();
        $referrals = [];
        foreach ($referralsStatuses as $referral) {
            array_push($referrals, $referral->referralUser);
        }

        $data = User::where('id', $user['id'])
            ->with('parts', function ($query) use ($request) {
                $query->where('application_id', $request['application_id']);
            })
            ->with('city')
            ->withCount('documents')
            ->first();
        $data['referrer'] = $referrer;
        $data['referrals'] = $referrals;
        $data->sum_parts_percent = $data->parts->sum('percent');
        $applicationCapitalization = Application::where('id','=',$request['application_id'])
            ->first();
        $balanceParts = $applicationCapitalization->capitalization * $data->sum_parts_percent;
        $data->balance_parts  = floatval(number_format($balanceParts, 4, '.', ''));

        $referralsSum = $user->transactions()
            ->where('action', 'LIKE', 'Referral%')
            ->where('application_id', $request['application_id'])
            ->sum('amount');

        foreach ($referrals as $referral) {
            $referral['referralSum'] = $user->transactions()
                ->where('action', 'LIKE', 'Referral%')
                ->where('source_user_id', $referral['id'])
                ->sum('amount');
        }

        $data['referralsSum'] = $referralsSum;
        $data['phone_number'] = $data['phone'];

        if ($referrerStatus) {
            $data['agent_expiration'] = $referrerStatus['agent_expiration'];
            $data['agent_status'] = $referrerStatus['agent_status'];
            $data['referral_token'] = $referrerStatus['referral_token'];
            $data['referral_expiration'] = $referrerStatus['referral_expiration'];
        } else {
            $data['agent_expiration'] = null;
            $data['agent_status'] = 0;
            $data['referral_token'] = null;
            $data['referral_expiration'] = null;
        }

        $ads = Ad::where('creator_id', $user['id'])
            ->where(function ($query) {
                $query
                    ->where('subcategory_id', 1)
                    ->orWhere('subcategory_id', 11)
                    ->orWhere('subcategory_id', 4)
                    ->orWhere('subcategory_id', 9)
                    ->orWhere('subcategory_id', 10)
                    ->orWhere('subcategory_id', 25);
            })
            ->get();
        $data['full_link'] = URL::to('/').'/'.$user->avatar;
        if (!$ads->isEmpty()) {
            $data['isOrganizer'] = true;
        } else {
            $data['isOrganizer'] = false;
        }

        return $data;
    }


    public function becomeAgent($request){
        $user = Auth::user();

        $token = (string)Str::uuid();

        $agentRole = Role::where('name', 'агент')->where('application_id', $request['application_id'])->first()['id'];
        $managerRole = Role::where('name', 'менеджер')->where('application_id', $request['application_id'])->first()['id'];

        if ($user->referrerStatuses()->where('application_id', $request['application_id'])->get()->isEmpty()) {
            $expirationDate = date('Y-m-d', strtotime(now() . '+1 year'));

            Referral::create([
                'agent_expiration' => $expirationDate,
                'agent_status' => 1,
                'referral_token' => $token,
                'user_id' => $user['id'],
                'application_id' => $request['application_id']]);
        } else {
            $referrerStatus = Referral::where('user_id', $user['id'])->where('application_id', $request['application_id'])->first();

            if ($referrerStatus['agent_status'] == 0 && $referrerStatus['referrer_id']) {
                $referrer = User::find($referrerStatus['referrer_id']);
                if ($referrer->roles->contains($managerRole) == 1) {
                    managerPartPayment($user, $request->amount, 'Referral User becomeAgent', $request['application_id']);
                }
            }

            if ($referrerStatus['agent_expiration'] > now()) {
                $expirationDate = date('Y-m-d', strtotime($referrerStatus['agent_expiration'] . '+1 year'));
            } else {
                $expirationDate = date('Y-m-d', strtotime(now() . '+1 year'));
            }

            if ($referrerStatus['token']) {
                $referrerStatus->update([
                    'agent_expiration' => $expirationDate,
                ]);
            } else {
                $referrerStatus->update([
                    'agent_expiration' => $expirationDate,
                    'referral_token' => $token,
                ]);
            }

            if ($referrerStatus['agent_status'] == 0) {
                $referrerStatus->update([
                    'agent_status' => 1,
                ]);
            }
        }

        if ($user->roles->contains($agentRole) != 1) {
            $user->roles()->attach($agentRole);
        }
    }


    public function checkOnlineUser(User $user = null){
        if ($user == null){
            return 0;
        }
        $date = new DateTime;
        $date->modify('-10 minutes');
        $formatted_date = $date->format('Y-m-d H:i:s');
        if ($user->last_visit > $formatted_date){
            return 1;
        }else{
            return 0;
        }


    }
}
