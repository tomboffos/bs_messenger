<?php

use App\Models\Tariff;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Facades\Notification;

function checkPermissions($user, array $permissions, int $application_id = null) {
    $allow = false;

    foreach ($permissions as $permission) {
        foreach ($user->roles as $role) {
            if ($application_id) {
                if (($role->permissions->contains('name', $permission) >= 1) &&
                    ($role->permissions->contains('application_id', $application_id) >= 1)) $allow = true;
            } else {
                if ($role->permissions->contains('name', $permission) >= 1) $allow = true;
            }
        }
    }

    if ($allow) {
        return true;
    } else {
        return false;
    }
}
//    Пример использования:
//        $user = Auth::user();
//
//        if (!checkPermissions($user, ['банить'])) {
//            return response()->json([
//                'message' => 'Forbidden'], 403);
//        }

function agentPartPayment($user, $sum, $action, $application) {
    $referrerStatus = $user->referrerStatuses()->where('application_id', $application)->first();
    if ($referrerStatus) {
        if ($referrerStatus['referrer_id'] && $referrerStatus['referral_expiration'] >= now() && $sum > 0) {
            $tariffAgent = Tariff::where('action', 'Агентская доля')->where('application_id', $application)->first()['percent'] * 0.01;
            if ($referrerStatus['referrer_id']) {
                $agent = User::find($referrerStatus['referrer_id']);
                $agentReferrerStatus = $agent->referrerStatuses()->where('application_id', $application)->first();
                if ($agentReferrerStatus['agent_status'] == 1 || $agentReferrerStatus['agent_status'] == 2) {
                    $agentBalance = $agent['balance'] + $sum * $tariffAgent;
                    Transaction::create(['amount' => $sum * $tariffAgent,
                        'user_id' => $agent['id'],
                        'action' => $action, 'type' => 1,
                        'source_user_id' => $user['id'],
                        'application_id' => $application]);
                    $agent->update(['balance' => $agentBalance]);
                    switch ($application) {
                        case 1:
                            Notification::send($agent, new \App\Notifications\Microphone\ReferralPay(['user' => $agent]));
                            break;
                        case 2:
                            Notification::send($agent, new \App\Notifications\SuperWork\ReferralPay(['user' => $agent]));
                            break;
                    }
                }
            }
        }
    }
}

function managerPartPayment($user, $sum, $action, $application) {
    $referrerStatus = $user->referrerStatuses()->where('application_id', $application)->first();
    if ($referrerStatus) {
        if ($referrerStatus['referrer_id'] && $referrerStatus['referral_expiration'] >= now() && $sum > 0) {
            $tariffManager = Tariff::where('action', 'Менеджерская доля')->where('application_id', $application)->first()['percent'] * 0.01;
            if ($referrerStatus['referrer_id']) {
                $agent = User::find($referrerStatus['referrer_id']);
                $agentReferrerStatus = $agent->referrerStatuses()->where('application_id', $application)->first();
                if ($agentReferrerStatus['agent_status'] == 2) {
                    $agentBalance = $agent['balance'] + $sum * $tariffManager;
                    Transaction::create(['amount' => $sum * $tariffManager,
                        'user_id' => $agent['id'],
                        'action' => $action, 'type' => 1,
                        'source_user_id' => $user['id'],
                        'application_id' => $application]);
                    $agent->update(['balance' => $agentBalance]);
                    switch ($application) {
                        case 1:
                            Notification::send($agent, new \App\Notifications\Microphone\ReferralPay(['user' => $agent]));
                            break;
                        case 2:
                            Notification::send($agent, new \App\Notifications\SuperWork\ReferralPay(['user' => $agent]));
                            break;
                    }
                }
            }
        }
    }
}

function investorsPartPayment($user, $sum, $action, $application) {
    $referrerStatus = $user->referrerStatuses()->where('application_id', $application)->first();
    if ($referrerStatus) {
        if ($referrerStatus['referrer_id'] && $referrerStatus['referral_expiration'] >= now() && $sum > 0) {
            $tariffManager = Tariff::where('action', 'Менеджерская доля от инвестора')->where('application_id', $application)->first()['percent'] * 0.01;
            if ($referrerStatus['referrer_id']) {
                $agent = User::find($referrerStatus['referrer_id']);
                $agentReferrerStatus = $agent->referrerStatuses()->where('application_id', $application)->first();
                if ($agentReferrerStatus['agent_status'] == 2) {
                    $agentBalance = $agent['balance'] + $sum * $tariffManager;
                    Transaction::create(['amount' => $sum * $tariffManager,
                        'user_id' => $agent['id'],
                        'action' => $action, 'type' => 1,
                        'source_user_id' => $user['id'],
                        'application_id' => $application]);
                    $agent->update(['balance' => $agentBalance]);
                    switch ($application) {
                        case 1:
                            Notification::send($agent, new \App\Notifications\Microphone\ReferralPay(['user' => $agent]));
                            break;
                        case 2:
                            Notification::send($agent, new \App\Notifications\SuperWork\ReferralPay(['user' => $agent]));
                            break;
                    }
                }
            }
        }
    }
}