<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('login')->nullable()->default(null);
            $table->string('password')->nullable()->default(null);
            $table->string('code')->nullable()->default(null);
            $table->string('email')->unique()->nullable()->default(null);
            $table->string('name')->nullable()->default(null);
            $table->string('surname')->nullable()->default(null);
            $table->string('patronym')->nullable()->default(null);
            $table->timestamp('email_verified_at')->nullable()->default(null);
            $table->boolean('gender')->nullable()->default(null);
            $table->date('birth_date')->nullable()->default(null);
            $table->string('phone')->unique()->nullable()->default(null);
            $table->string('avatar')->nullable()->default(null);
            $table->bigInteger('city_id')->unsigned()->nullable()->default(null);
            $table->foreign('city_id')->references('id')->on('cities');
            $table->bigInteger('language_id')->unsigned()->nullable()->default(null);
            $table->foreign('language_id')->references('id')->on('languages');
            $table->string('address')->nullable()->default(null);
            $table->double('balance', 16, 2)->default(0);
            $table->date('agent_expiration')->nullable()->default(null);
            $table->integer('agent_status')->default(0);
//            $table->bigInteger('currency_id')->unsigned()->nullable()->default(null);
//            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->boolean('microphone_passed_exam')->nullable()->default(null);
            $table->bigInteger('referrer_id')->unsigned()->nullable()->default(null);
            $table->foreign('referrer_id')->references('id')->on('users');
            $table->string('referral_token')->unique()->nullable()->default(null);
            $table->date('referral_expiration')->nullable()->default(null);
            $table->string('status')->nullable()->default(null);
            $table->integer('views')->nullable()->default(0);
            $table->integer('calls')->nullable()->default(0);
            $table->string('iban')->nullable()->default(null);
            $table->string('iin')->nullable()->default(null);
            $table->string('document_number')->nullable()->default(null);
            $table->date('document_issue_date')->nullable()->default(null);
            $table->string('document_issued_by')->nullable()->default(null);
            $table->integer('document_type')->nullable()->default(null);
            $table->boolean('kz_resident')->default(false);
            $table->dateTime('last_visit')->nullable()->default(null);
            $table->integer('attempts')->nullable()->default(0);
            $table->boolean('deleted')->default(false);
            $table->boolean('trial_used')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
