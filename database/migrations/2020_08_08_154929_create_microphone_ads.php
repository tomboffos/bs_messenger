<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMicrophoneAds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('microphone_ads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable()->default(null);
            $table->longText('description')->nullable()->default(null);
            $table->float('rating')->default(0);
            $table->string('phone')->nullable()->default(null);
            $table->string('address')->nullable()->default(null);
            $table->dateTime('colored_date_time')->nullable()->default(null);
            $table->dateTime('top_date_time')->nullable()->default(null);
            $table->dateTime('expiration_date_time')->nullable()->default(null);
            $table->bigInteger('subcategory_id')->unsigned()->nullable()->default(null);
            $table->foreign('subcategory_id')->references('id')->on('microphone_subcategories');
            $table->bigInteger('creator_id')->unsigned()->nullable()->default(null);
            $table->foreign('creator_id')->references('id')->on('users');
            $table->bigInteger('image_id')->unsigned()->nullable()->default(null);
            $table->foreign('image_id')->references('id')->on('microphone_media');
            $table->bigInteger('city_id')->unsigned()->nullable()->default(null);
            $table->foreign('city_id')->references('id')->on('cities');
            $table->integer('views')->nullable()->default(0);
            $table->integer('calls')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('microphone_ads');
    }
}
