<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMicrophoneReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('microphone_reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('body')->nullable()->default(null);
            $table->float('rating')->nullable()->default(null);
            $table->bigInteger('ad_id')->unsigned()->nullable()->default(null);
            $table->foreign('ad_id')->references('id')->on('microphone_ads');
            $table->bigInteger('user_id')->unsigned()->nullable()->default(null);
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('microphone_reviews');
    }
}
