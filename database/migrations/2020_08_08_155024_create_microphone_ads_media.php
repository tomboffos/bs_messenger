<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMicrophoneAdsMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('microphone_ads_media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ad_id')->unsigned()->nullable()->default(null);
            $table->foreign('ad_id')->references('id')->on('microphone_ads');
            $table->bigInteger('media_id')->unsigned()->nullable()->default(null);
            $table->foreign('media_id')->references('id')->on('microphone_media');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('microphone_ads_media');
    }
}
