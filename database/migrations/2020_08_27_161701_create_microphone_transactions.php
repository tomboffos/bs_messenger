<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMicrophoneTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('microphone_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('amount', 16, 2)->default(0);
            $table->float('rate')->nullable()->default(null);
            $table->string('action')->nullable()->default(null);
            $table->string('currency')->nullable()->default(null);
            $table->integer('type')->default(0);
            $table->string('comment')->nullable()->default(null);
            $table->bigInteger('user_id')->unsigned()->nullable()->default(null);
            $table->foreign('user_id')->references('id')->on('users');
            $table->bigInteger('source_user_id')->unsigned()->nullable()->default(null);
            $table->foreign('source_user_id')->references('id')->on('users');
            $table->bigInteger('reverted_transaction_id')->unsigned()->nullable()->default(null);
            $table->foreign('reverted_transaction_id')->references('id')->on('microphone_transactions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('microphone_transactions');
    }
}
