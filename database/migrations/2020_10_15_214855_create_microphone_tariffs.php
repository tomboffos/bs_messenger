<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMicrophoneTariffs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('microphone_tariffs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('action')->nullable()->default(null);
            $table->double('price', 16, 2)->nullable()->default(null);
            $table->double('percent', 12, 8)->nullable()->default(null);
            $table->bigInteger('subcategory_id')->unsigned()->nullable()->default(null);
            $table->foreign('subcategory_id')->references('id')->on('microphone_subcategories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('microphone_tariffs');
    }
}
