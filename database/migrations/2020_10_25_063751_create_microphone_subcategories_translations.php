<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMicrophoneSubcategoriesTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('microphone_subcategories_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subcategory_id')->unsigned()->nullable()->default(null);
            $table->foreign('subcategory_id')->references('id')->on('microphone_subcategories');
            $table->bigInteger('language_id')->unsigned()->nullable()->default(null);
            $table->foreign('language_id')->references('id')->on('languages');
            $table->string('name')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('microphone_subcategories_translations');
    }
}
