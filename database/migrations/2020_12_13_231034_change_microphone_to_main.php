<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeMicrophoneToMain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //ФИКС СЛОМАВШИХСЯ СВЯЗЕЙ В СУЩЕСТВУЮЩЕЙ БАЗЕ
        Schema::table('microphone_ads_media', function ($table) {
            $table->foreign('ad_id')->references('id')->on('microphone_ads');
            $table->foreign('media_id')->references('id')->on('microphone_media');
        });
        Schema::table('microphone_ads_products', function ($table) {
            $table->foreign('ad_id')->references('id')->on('microphone_ads');
            $table->foreign('product_id')->references('id')->on('microphone_products');
        });
        Schema::table('microphone_appeals', function ($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('city_id')->references('id')->on('cities');
        });
        Schema::table('microphone_categories_translations', function ($table) {
            $table->foreign('category_id')->references('id')->on('microphone_categories');
            $table->foreign('language_id')->references('id')->on('languages');
        });
        //КОНЕЦ ФИКСА

        Schema::rename('microphone_parts', 'parts');
        Schema::rename('microphone_roles', 'roles');
        Schema::rename('microphone_permissions', 'permissions');
        Schema::rename('microphone_roles_permissions', 'roles_permissions');
        Schema::rename('microphone_users_roles', 'users_roles');
        Schema::rename('microphone_tariffs', 'tariffs');
        Schema::rename('microphone_transactions', 'transactions');

        Schema::table('parts', function ($table) {
            $table->bigInteger('application_id')->after('user_id')->unsigned()->nullable()->default(null);
            $table->foreign('application_id')->references('id')->on('applications');
        });
        Schema::table('permissions', function ($table) {
            $table->bigInteger('application_id')->after('name')->unsigned()->nullable()->default(null);
            $table->foreign('application_id')->references('id')->on('applications');
        });
        Schema::table('player_ids', function ($table) {
            $table->bigInteger('application_id')->after('user_id')->unsigned()->nullable()->default(null);
            $table->foreign('application_id')->references('id')->on('applications');
        });
        Schema::table('roles', function ($table) {
            $table->bigInteger('application_id')->after('name')->unsigned()->nullable()->default(null);
            $table->foreign('application_id')->references('id')->on('applications');
        });
        Schema::table('tariffs', function ($table) {
            $table->bigInteger('application_id')->after('subcategory_id')->unsigned()->nullable()->default(null);
            $table->foreign('application_id')->references('id')->on('applications');
        });
        Schema::table('transactions', function ($table) {
            $table->bigInteger('application_id')->after('reverted_transaction_id')->unsigned()->nullable()->default(null);
            $table->foreign('application_id')->references('id')->on('applications');
        });
        Schema::table('withdrawals', function ($table) {
            $table->bigInteger('application_id')->after('user_id')->unsigned()->nullable()->default(null);
            $table->foreign('application_id')->references('id')->on('applications');
        });
        DB::table('parts')->update(['application_id' => 1]);
        DB::table('permissions')->update(['application_id' => 1]);
        DB::table('player_ids')->update(['application_id' => 1]);
        DB::table('roles')->update(['application_id' => 1]);
        DB::table('tariffs')->update(['application_id' => 1]);
        DB::table('transactions')->update(['application_id' => 1]);
        DB::table('withdrawals')->update(['application_id' => 1]);

        DB::table('applications')->insert(['id' => 2, 'name' => 'SuperWork', 'capitalization' => 5161881760]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('applications')->delete(2);

        Schema::table('parts', function ($table) {
            $table->dropForeign(['application_id']);
            $table->dropColumn('application_id');
        });
        Schema::table('permissions', function ($table) {
            $table->dropForeign(['application_id']);
            $table->dropColumn('application_id');
        });
        Schema::table('player_ids', function ($table) {
            $table->dropForeign(['application_id']);
            $table->dropColumn('application_id');
        });
        Schema::table('roles', function ($table) {
            $table->dropForeign(['application_id']);
            $table->dropColumn('application_id');
        });
        Schema::table('tariffs', function ($table) {
            $table->dropForeign(['application_id']);
            $table->dropColumn('application_id');
        });
        Schema::table('transactions', function ($table) {
            $table->dropForeign(['application_id']);
            $table->dropColumn('application_id');
        });
        Schema::table('withdrawals', function ($table) {
            $table->dropForeign(['application_id']);
            $table->dropColumn('application_id');
        });

        Schema::rename('parts', 'microphone_parts');
        Schema::rename('roles', 'microphone_roles');
        Schema::rename('permissions', 'microphone_permissions');
        Schema::rename('roles_permissions', 'microphone_roles_permissions');
        Schema::rename('users_roles', 'microphone_users_roles');
        Schema::rename('tariffs', 'microphone_tariffs');
        Schema::rename('transactions', 'microphone_transactions');

        //ФИКС СЛОМАВШИХСЯ СВЯЗЕЙ В СУЩЕСТВУЮЩЕЙ БАЗЕ
        Schema::table('microphone_ads_media', function ($table) {
            $table->dropForeign(['ad_id']);
            $table->dropForeign(['media_id']);
        });
        Schema::table('microphone_ads_products', function ($table) {
            $table->dropForeign(['ad_id']);
            $table->dropForeign(['product_id']);
        });
        Schema::table('microphone_appeals', function ($table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['city_id']);
        });
        Schema::table('microphone_categories_translations', function ($table) {
            $table->dropForeign(['category_id']);
            $table->dropForeign(['language_id']);
        });
        //КОНЕЦ ФИКСА
    }
}
