<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuperworkAds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('superwork_ads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable()->default(null);
            $table->longText('description')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);
            $table->string('address')->nullable()->default(null);
            $table->dateTime('vip_date_time')->nullable()->default(null);
            $table->dateTime('top_date_time')->nullable()->default(null);
            $table->dateTime('expiration_date_time')->nullable()->default(null);
            $table->dateTime('purple_date_time')->nullable()->default(null);
            $table->dateTime('blue_date_time')->nullable()->default(null);
            $table->dateTime('yellow_date_time')->nullable()->default(null);
            $table->integer('type')->default(0);
            $table->integer('schedule')->default(0);
            $table->integer('employment')->default(0);
            $table->integer('experience')->default(0);
            $table->integer('price_from')->nullable()->default(null);
            $table->integer('price_to')->nullable()->default(null);
            $table->boolean('price_negotiable')->default(false);
            $table->integer('views')->nullable()->default(0);
            $table->integer('calls')->nullable()->default(0);
            $table->double('latitude', 15, 6)->nullable()->default(null);
            $table->double('longitude', 15, 6)->nullable()->default(null);
            $table->bigInteger('currency_id')->unsigned()->nullable()->default(null);
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->bigInteger('category_id')->unsigned()->nullable()->default(null);
            $table->foreign('category_id')->references('id')->on('superwork_categories');
            $table->bigInteger('creator_id')->unsigned()->nullable()->default(null);
            $table->foreign('creator_id')->references('id')->on('users');
            $table->bigInteger('image_id')->unsigned()->nullable()->default(null);
            $table->foreign('image_id')->references('id')->on('superwork_media');
            $table->bigInteger('city_id')->unsigned()->nullable()->default(null);
            $table->foreign('city_id')->references('id')->on('cities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('superwork_ads');
    }
}
