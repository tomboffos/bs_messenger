<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTariffs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tariffs', function ($table) {
            $table->bigInteger('superwork_category_id')->after('subcategory_id')->unsigned()->nullable()->default(null);
            $table->foreign('superwork_category_id')->references('id')->on('superwork_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tariffs', function ($table) {
            $table->dropForeign(['superwork_category_id']);
            $table->dropColumn('superwork_category_id');
        });
    }
}
