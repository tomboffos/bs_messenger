<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferrals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referrals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('agent_expiration')->nullable()->default(null);
            $table->integer('agent_status')->default(0);
            $table->bigInteger('referrer_id')->unsigned()->nullable()->default(null);
            $table->foreign('referrer_id')->references('id')->on('users');
            $table->string('referral_token')->unique()->nullable()->default(null);
            $table->date('referral_expiration')->nullable()->default(null);
            $table->bigInteger('user_id')->unsigned()->nullable()->default(null);
            $table->foreign('user_id')->references('id')->on('users');
            $table->bigInteger('application_id')->unsigned()->nullable()->default(null);
            $table->foreign('application_id')->references('id')->on('applications');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referrals');
    }
}
