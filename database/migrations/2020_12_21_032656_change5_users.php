<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class Change5Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = json_decode(DB::Table('users')->get(), true);
        foreach ($users as $item) {
            if ($item['agent_status'] == 1 || $item['referrer_id']) {
                DB::table('referrals')->insert([
                    'agent_expiration' => $item['agent_expiration'],
                    'agent_status' => $item['agent_status'],
                    'referrer_id' => $item['referrer_id'],
                    'referral_token' => $item['referral_token'],
                    'referral_expiration' => $item['referral_expiration'],
                    'user_id' => $item['id'],
                    'application_id' => 1,
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }
        }

        Schema::table('users', function ($table) {
            $table->dropColumn('agent_expiration');
            $table->dropColumn('agent_status');
            $table->dropForeign(['referrer_id']);
            $table->dropColumn('referrer_id');
            $table->dropColumn('referral_token');
            $table->dropColumn('referral_expiration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->date('agent_expiration')->after('balance')->nullable()->default(null);
            $table->integer('agent_status')->after('agent_expiration')->default(0);
            $table->bigInteger('referrer_id')->after('agent_status')->unsigned()->nullable()->default(null);
            $table->foreign('referrer_id')->references('id')->on('users');
            $table->string('referral_token')->after('referrer_id')->unique()->nullable()->default(null);
            $table->date('referral_expiration')->after('referral_token')->nullable()->default(null);
        });

        $referrals = json_decode(DB::Table('referrals')->get(), true);
        foreach ($referrals as $item) {
            DB::table('users')->where('id', $item['user_id'])->update([
                'agent_expiration' => $item['agent_expiration'],
                'agent_status' => $item['agent_status'],
                'referrer_id' => $item['referrer_id'],
                'referral_token' => $item['referral_token'],
                'referral_expiration' => $item['referral_expiration']
            ]);
        }
    }
}
