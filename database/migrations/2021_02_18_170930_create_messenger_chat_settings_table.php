<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessengerChatSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messenger_chat_settings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('chat_id')->unsigned();
            $table->tinyInteger('admin_media_send');
            $table->tinyInteger('admin_message_send');
            $table->tinyInteger('admin_change_data');
            $table->tinyInteger('sound');
            $table->foreign('chat_id')
                ->references('id')
                ->on('messenger_chats');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messenger_chat_settings');
    }
}
