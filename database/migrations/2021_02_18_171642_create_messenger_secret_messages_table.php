<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessengerSecretMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messenger_secret_messages', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('from_id')->unsigned();
            $table->bigInteger('to_id')->nullable();
            $table->text('text')->nullable();
            $table->string('action')->nullable();
            $table->bigInteger('chat_id')->unsigned();
            $table->tinyInteger('is_read');
            $table->string('time_deleted')->nullable();
            $table->tinyInteger('false_transfer');
            $table->foreign('from_id')
                ->references('id')
                ->on('users');
            $table->foreign('chat_id')
                ->references('id')
                ->on('messenger_secret_chats');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messenger_secret_messages');
    }
}
