<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessengerMediaSecretMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messenger_media_secret_messages', function (Blueprint $table) {
            $table->bigInteger('media_id')->unsigned();
            $table->bigInteger('message_id')->unsigned();
            $table->foreign('media_id')
                ->references('id')
                ->on('media');
            $table->foreign('message_id')
                ->references('id')
                ->on('messenger_secret_messages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messenger_media_secret_messages');
    }
}
