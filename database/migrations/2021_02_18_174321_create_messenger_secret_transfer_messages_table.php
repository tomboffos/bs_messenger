<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessengerSecretTransferMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messenger_secret_transfer_messages', function (Blueprint $table) {
            $table->bigInteger('message_id')->unsigned();
            $table->bigInteger('transfer_message_id')->unsigned();
            $table->foreign('message_id')
                ->references('id')
                ->on('messenger_secret_messages');
            $table->foreign('transfer_message_id')
                ->references('id')
                ->on('messenger_secret_messages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messenger_secret_transfer_messages');
    }
}
