<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessengerCategoryUserChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messenger_category_user_chats', function (Blueprint $table) {
           $table->bigInteger('user_id')->unsigned();
           $table->unsignedBigInteger('category_id');
           $table->bigInteger('chat_id')->unsigned();
            $table->foreign('chat_id')
                ->references('id')
                ->on('messenger_chats');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messenger_category_user_chats');
    }
}
