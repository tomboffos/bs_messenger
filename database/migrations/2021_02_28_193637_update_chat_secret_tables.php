<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateChatSecretTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::dropIfExists('messenger_media_secret_messages');
        Schema::dropIfExists('messenger_secret_transfer_messages');
        Schema::dropIfExists('messenger_secret_messages');
        Schema::dropIfExists('messenger_secret_chats');


        Schema::table('messenger_chats', function (Blueprint $table) {
            $table->tinyInteger('is_secret')->default(0);
        });
        Schema::table('messenger_messages', function (Blueprint $table) {
            $table->integer('time_deleted')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
