<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessengerMessageReadUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messenger_message_read_users', function (Blueprint $table) {
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('chat_id')->unsigned();
            $table->bigInteger('message_id')->unsigned();
            $table->tinyInteger('is_read')->default(0);
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->foreign('chat_id')
                ->references('id')
                ->on('messenger_chats');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messenger_message_read_users');
    }
}
