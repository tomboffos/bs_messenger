<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessengerMapCoordinatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messenger_map_coordinates', function (Blueprint $table) {
            $table->bigInteger('message_id')->unsigned();
            $table->float('latitude');
            $table->float('longitude');
            $table->foreign('message_id')
                ->references('id')
                ->on('messenger_messages')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messenger_map_coordinates');
    }
}
