<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateChatSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messenger_chat_settings', function (Blueprint $table) {
            $table->tinyInteger('transfer_chat_message')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messenger_chat_settings', function (Blueprint $table) {
            $table->dropColumn('transfer_chat_message');
        });
    }
}
