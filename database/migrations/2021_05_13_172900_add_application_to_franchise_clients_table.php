<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddApplicationToFranchiseClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('franchise_clients', function (Blueprint $table) {
            $table->unsignedBigInteger('application_id')->after('phone');
            $table->foreign('application_id')
                ->references('id')
                ->on('applications')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('franchise_clients', function (Blueprint $table) {
            $table->dropForeign('franchise_clients_application_id_foreign');
            $table->dropColumn('application_id');
        });
    }
}
