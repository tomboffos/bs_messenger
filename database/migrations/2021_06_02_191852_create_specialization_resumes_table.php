<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecializationResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specialization_resumes', function (Blueprint $table) {
            $table->bigInteger('specialization_id')->unsigned();
            $table->bigInteger('resume_id')->unsigned();
            $table->foreign('specialization_id')
                ->references('id')
                ->on('specializations')
                ->onDelete('cascade');
            $table->foreign('resume_id')
                ->references('id')
                ->on('resumes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specialization_resumes');
    }
}
