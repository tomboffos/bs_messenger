<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpirienceResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expirience_resumes', function (Blueprint $table) {
            $table->bigInteger('expirience_id')->unsigned();
            $table->bigInteger('resume_id')->unsigned();
            $table->foreign('expirience_id')
                ->references('id')
                ->on('expiriences')
                ->onDelete('cascade');
            $table->foreign('resume_id')
                ->references('id')
                ->on('resumes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expirience_resumes');
    }
}
