<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguageResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('language_resumes', function (Blueprint $table) {
            $table->bigInteger('language_id')->unsigned();
            $table->bigInteger('resume_id')->unsigned();
            $table->foreign('language_id')
                ->references('id')
                ->on('languages_resume')
                ->onDelete('cascade');
            $table->foreign('resume_id')
                ->references('id')
                ->on('resumes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('language_resumes');
    }
}
