<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessengerViewGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messenger_view_groups', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('chat_id');
            $table->integer('views')->default(0);
            $table->timestamps();
            $table->foreign('chat_id')
                ->references('id')
                ->on('messenger_chats')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messenger_view_groups');
    }
}
