<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessengerTariffChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messenger_tariff_chats', function (Blueprint $table) {
          $table->unsignedBigInteger('ad_id');
          $table->unsignedBigInteger('city_id');
          $table->unsignedBigInteger('chat_id');
            $table->foreign('chat_id')
                ->references('id')
                ->on('messenger_chats')
                ->onDelete('cascade');
            $table->foreign('ad_id')
                ->references('id')
                ->on('messenger_ad_chats')
                ->onDelete('cascade');
            $table->foreign('city_id')
                ->references('id')
                ->on('cities')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messenger_tariff_chats');
    }
}
