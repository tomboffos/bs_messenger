<?php

namespace Database\Seeders;

use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\MessengerChatSettings;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ChatSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $allChat = MessengerChat::all();
        DB::table('messenger_chat_settings')->truncate();
        foreach ($allChat as $chat){
            MessengerChatSettings::firstOrCreate([
                'chat_id' => $chat->id,
                'admin_media_send' => 0,
                'admin_message_send' => 0,
                'admin_change_data' => 1,
                'transfer_chat_message' => 1,
                'sound' => 1,
            ]);
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
