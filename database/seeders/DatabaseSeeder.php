<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersSeeder::class,
            ApplicationsSeeder::class,
            UsersApplicationsSeeder::class,
            RolesSeeder::class,
            PermissionsSeeder::class,
            UsersRolesSeeder::class,
            RolesPermissionsSeeder::class,
            MediaSeeder::class,
            ProductsSeeder::class,
            CategoriesSeeder::class,
            CategoriesTranslationsSeeder::class,
            SubcategoriesSeeder::class,
            SubcategoriesTranslationsSeeder::class,
            AdsSeeder::class,
            ReviewsSeeder::class,
            AdsMediaSeeder::class,
            AdsProductsSeeder::class,
            UsersAdsSeeder::class,
            PartsSeeder::class,
            TariffsSeeder::class,
            CurrenciesSeeder::class
        ]);
    }
}

class UsersSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'login' => 'Admin',
            'password' => Hash::make('0000'),
            'email' => 'admin@example.com',
            'name' => 'Админ',
            'surname' => 'Админов',
            'patronym' => 'Админович',
            'gender' => 1,
            'birth_date' => '2000-01-01',
            'phone' => '+72345678901',
            'address' => 'ул. Улица, дом 1, кв. 10',
            'balance' => 1000,
            'microphone_passed_exam' => 1,
            'views' => 200,
            'calls' => 100,
            'last_visit' => now(),
            'deleted' => 0,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('users')->insert([
            'login' => 'Reklamodatel',
            'password' => Hash::make('0000'),
            'email' => 'reklamodatel@example.com',
            'name' => 'Рекламодатель',
            'gender' => 1,
            'birth_date' => '2000-01-02',
            'phone' => '+71234567890',
            'address' => 'ул. Улица, дом 2, кв. 11',
            'balance' => 0,
            'microphone_passed_exam' => 1,
            'views' => 100,
            'calls' => 50,
            'last_visit' => now(),
            'deleted' => 0,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('users')->insert([
            'login' => 'Reklamodatel2',
            'password' => Hash::make('0000'),
            'email' => 'reklamodatel2@example.com',
            'name' => 'Рекламодатель',
            'gender' => 1,
            'birth_date' => '2000-01-03',
            'phone' => '+71234567891',
            'address' => 'ул. Улица, дом 3, кв. 12',
            'balance' => 0,
            'microphone_passed_exam' => 1,
            'views' => 50,
            'calls' => 23,
            'last_visit' => now(),
            'deleted' => 0,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}

class ApplicationsSeeder extends Seeder
{
    public function run()
    {
        DB::table('applications')->insert([
            'name' => 'Microphone',
            'capitalization' => 51345678,
        ]);
    }
}

class UsersApplicationsSeeder extends Seeder
{
    public function run()
    {
        DB::table('users_applications')->insert([
            'user_id' => 1,
            'application_id' => 1,
        ]);
    }
}

class RolesSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_roles')->insert([
            'name' => 'администратор',
        ]);
        DB::table('microphone_roles')->insert([
            'name' => 'рекламодатель',
        ]);
        DB::table('microphone_roles')->insert([
            'name' => 'агент',
        ]);
        DB::table('microphone_roles')->insert([
            'name' => 'бухгалтер',
        ]);
    }
}

class PermissionsSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_permissions')->insert([
            'name' => 'Создание заявок',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Просмотр заявок',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Участие в чатах',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Создание объявлений, услуг, медиафайлов',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Создание отзывов',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Покупка, продажа долей',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Запрос вывода средств',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Создание заявки агента',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Привязка к агенту по реферальной ссылке',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Привязка к рекламодателям по реферальной ссылке',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Подтверждение вывода средств',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Получение IBAN',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Просмотр баланса сервисов',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Одобрение, отклонение, активация, деактивация агента по заявке',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Пополнение счёта любому пользователю',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Просмотр истории транзакций',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Задавание стоимости доли',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Создание подкатегорий',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Добавление объявлений в избранное',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Просмотр стран',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Просмотр городов',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Редактирование информации в профиле',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_permissions')->insert([
            'name' => 'Доступ администратора к базе',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}

class UsersRolesSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_users_roles')->insert([
            'user_id' => 1,
            'role_id' => 1,
        ]);
        DB::table('microphone_users_roles')->insert([
            'user_id' => 2,
            'role_id' => 2,
        ]);
        DB::table('microphone_users_roles')->insert([
            'user_id' => 3,
            'role_id' => 2,
        ]);
    }
}

class RolesPermissionsSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 1,
            'permission_id' => 13,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 1,
            'permission_id' => 14,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 1,
            'permission_id' => 15,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 1,
            'permission_id' => 16,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 1,
            'permission_id' => 17,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 1,
            'permission_id' => 18,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 1,
            'permission_id' => 23,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 2,
            'permission_id' => 1,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 2,
            'permission_id' => 2,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 2,
            'permission_id' => 3,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 2,
            'permission_id' => 4,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 2,
            'permission_id' => 5,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 2,
            'permission_id' => 6,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 2,
            'permission_id' => 7,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 2,
            'permission_id' => 8,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 2,
            'permission_id' => 9,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 2,
            'permission_id' => 19,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 2,
            'permission_id' => 22,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 3,
            'permission_id' => 10,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 4,
            'permission_id' => 11,
        ]);
        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 4,
            'permission_id' => 12,
        ]);
    }
}

class MediaSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/FSKsnxT9du9MSYw3CZy2mMiupfNkVYJZPT9BdKTQ.png',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/ROFy1mDuEtzR9Lepl4m5c0HER1KKTSY1u7wGjxU5.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/qZiB3NxrGkwAmK3pYEgfTxudpUqWUkBirRjof3Rk.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/CSGiS9aSaka2kEJX9uGjGMd27HN5krINPliTilbD.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/MlRyZsHn9E2a3aWxzhDHiYgQfqS39m0KLf3Wc6EV.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/noLBdNm03W2Zqb4iahYktpCtMhpxWdsp9ZQ4vA7Q.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/lf0RREHq0v3VHXtit419begsff8rl2eUCCOWJevE.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/7quWMjFPr52ebs7RBYPm1kmHOR6jGA8z4q5QQw74.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/uGU60tU9n9tAUeIWsJBSiThDWtSdsaU4lcKxSeKg.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/5gYiatpaBDSQO0MwKzWk2BHORdzfX0JmObvpHcOk.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/L5yqhXaD4ECpEZ5cUpFma9LeOkVq9z0ZNxuxCyVQ.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/r37HG466TFG0leDrxREhawAIuBjwU9mGQiRhjqSe.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/QaPRaUyk2197sf2hmKEs5M4GrzoBSs84bB7kddqX.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/dAjDV4UMvetj4oZqpC35cEkvLmITG1sIMftigtJA.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/fFiDrw7dbZiMXySiKORuuzsNwbWUa1Uxg1kXswHA.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/r3aFqeDD81UpfwVhpx2ewH0HZRyvYBJozNZRN1IO.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/NUUGZfmPZ2p3KK6c2ZkW8ljzz9cmvdh4SWabqtyM.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/40AtTeYTXOKhkYh3Enx2J3bFeaNRRzxN5QoiPihI.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/Sp3e65yqGouZmsVcQP8UOfICdsewLgo7mkCVaeC8.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/OpKo61Uxrndb7ZtlsSI2fJDRAlUcMgBCkvR66oEx.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/1ogzMdhBuw2uteNXYXrYGg1C8bwKQQDBOKFk7qqp.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/OpJRSyYCLbSuKTKPCZeSySLq4UsXPz3iJ8juAR3G.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/V1YZ1MS02WWHIoSzTgwzKbmCKq4yJi2AOs2VOcqM.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/OjYnPcMJyhRa29h0nRR17Nfk3F6rpnI2dVKxtgTw.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/rR9tRwnMGluKD7XzkOoCViimrP2i9xRxGECBitV8.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/grdRni5SFUqwhrwgtJSTcK1N3hQF7NciW3W99Kya.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/oIMbm2txg4VqVrGHlGC3eNw7REyzKRQkZPfyA5Pj.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/aHY52X4hugKrcdlRetc1EVwRS8VTzVjjWhqy7URH.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/13XOnSe3p7jGVcotXlq1tValveTP5O3FjmSRehYO.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/W7NhqX0MTJtESObr4DFtQGNb50Lq1mITxVsaEXjZ.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/aIOx6ptDA64R9GDKTUABEUcKWuSjmd8cCOEQMhLc.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/85LNFNXBq0BgcWE2muTJGCy9azjOt24A64N9x15Y.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/GtkLZBwnQ4z6ej6hEhAqaJ3gUcUFFs4sqpROx5p7.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/PoNlyB1MHyWkiNtBqMj9BxkHveZYfI8emXAcrkFm.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/weVolpgpetA19sKTGpg2eWnDeICYxzYEiIODxOra.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/qZslRtLPrbL5woYcWXZXOpqs0lPFGJLYfQu1Cbo9.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/FlMZujF8KkSjMBnSUg0ouJEhuyDhJECLD6iBXOcH.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/GOt7nxdiVVGkyPXbN8TEZKu65jJbt5v1ffjfRnVN.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/mFWBhKnrrTJ6aSYY4fud8rUypG909xVM0KmLVgcw.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/NnpBLJ9nYfoFHX4wCcbqbNtTKS8csXUiUlKGKwQ9.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/ROwNd1E9Su1ok2Al5zvVXnZYthTWjrKaV4fvWWku.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/qNCRyHevHho7BwRuVwNo3VhRJr6GJUe2C7XiWcDZ.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/ucCinG09loSjXZsX5HQJoiGiKdASOb41A3CvrdOe.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/VPqFms0pMwIDMQZOCAOmtjkUAVEa956A8dYBwOKf.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/OGazOcYLhanYbetMfNuSgOzA91qrPMJ5E7tqmIl3.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/zn3RUTu1kvIkTLnfqjtaPktGjORAaZjPEOBRPFxj.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/MHMl6LAhYemEauzYCJOZ9mmVEcTImEDjBU7ePTp6.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/FxlEe1K8zeO5ZOJWdS9isx6CYp3ARPvDffXvZOBK.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/pH00a1K4rRL4WKvgMngNA26UMsr3QgnLqxadTiU3.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/ZChU3LMXJGovskbFSR9E6wJJ1UdBV6JNpAuDz00r.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/etoCuDd9p6fw405R0LoZmArJkkVQ4LTOMkvqkF1Q.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/Q0rHTfmJMYQ5MG56RT64nLAfVlwOSJcjRvU0A7e2.png',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/q6pg2Kg30G3LNArTwWpOxSfvz5BrDg4UxKOKtYep.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/wH7PysOT7TXYfCPoYt4B94g4uFEuY4CWA0oPjH1F.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/gRYv0zkqSLP9gjDbtBEYaHuPbioUQFJmYcfeP6gr.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/ccEhGg7AgD6ZhimIxb4L4PtG8DPs4KwzpyR9EzAQ.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/i2gVjaVeUr3WVoK4wSrl6ngAqDu3hxDfeJvHy0h3.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/FULsU0hvGotuFj1gIuDByKu8uJR86gpHLppLDkzs.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/DI5Ux22iU5WqgqC0qyDTD47bhHzP7cShVAWORHxu.jpeg',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'image',
            'file' => 'media/images/Ap3Xh4Ypd1nDKT11YrFQGVSAPGUP3xQP6EN5CGTP.jpeg',
            'user_id' => 2,
        ]);

        DB::table('microphone_media')->insert([
            'type' => 'video',
            'link' => 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
            'user_id' => 2,
        ]);
        DB::table('microphone_media')->insert([
            'type' => 'video',
            'file' => 'media/video/L04nHUBuGZcuPwCNVA69jwWWwYGp3zlJnrRXrCQ8.mp4',
            'user_id' => 2,
        ]);

        DB::table('microphone_media')->insert([
            'type' => 'audio',
            'file' => 'media/audio/31kjhviDZyNuqYIf26LstkGcMV91HVSu89iZKPRY.mp3',
            'user_id' => 2,
        ]);
    }
}

class ProductsSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_products')->insert([
            'name' => 'Шарики',
            'price' => 200,
            'user_id' => 2,
        ]);
    }
}

class CategoriesSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_categories')->insert([
            'name' => 'Организаторы',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories')->insert([
            'name' => 'Услуги',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories')->insert([
            'name' => 'Рестораны/Кафе',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories')->insert([
            'name' => 'Одежда',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories')->insert([
            'name' => 'Прокат',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories')->insert([
            'name' => 'Разное',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}

class CategoriesTranslationsSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_categories_translations')->insert([
            'name' => 'Organizers',
            'category_id' => 1,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'Services',
            'category_id' => 2,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'Restaurants/Cafe',
            'category_id' => 3,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'Clothing',
            'category_id' => 4,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'Rent',
            'category_id' => 5,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'Miscellaneous',
            'category_id' => 6,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'Ұйымдастырушылар',
            'category_id' => 1,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'Қызмет көрсету',
            'category_id' => 2,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'Мейрамханалар/Кафелер',
            'category_id' => 3,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'Киім',
            'category_id' => 4,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'Жалға алу',
            'category_id' => 5,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'Әртүрлі',
            'category_id' => 6,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'المنظمون',
            'category_id' => 1,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'الخدمات',
            'category_id' => 2,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'المطاعم/الكافيتيربا',
            'category_id' => 3,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'الكافيتيربا',
            'category_id' => 4,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'التأجير',
            'category_id' => 5,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => 'منوعات',
            'category_id' => 6,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => '组织者',
            'category_id' => 1,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => '服务',
            'category_id' => 2,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => '餐厅/咖啡厅',
            'category_id' => 3,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => '服装',
            'category_id' => 4,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => '出租',
            'category_id' => 5,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_categories_translations')->insert([
            'name' => '其他',
            'category_id' => 6,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}

class SubcategoriesSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_subcategories')->insert([
            'name' => 'Организаторы',
            'category_id' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Певцы/Музыканты',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Звезды',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Ведущие/Тамада',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Шоу программы',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Официанты',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Пригласительные',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Свет и звук',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Ди-джеи',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Танцевальные коллективы',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Аниматоры',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Изготовление тортов',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Спа-центры',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Школы танцев',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Школы вокала',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Магазины подарков',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Флористы',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Магазины фейерверков',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Услуги оформления',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Салоны красоты',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Фото/видео съемка',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Студия звукозаписи',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Разное',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Халал',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Рестораны',
            'category_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Банкетные залы',
            'category_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Клубы и бары',
            'category_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Караоке',
            'category_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Мужская одежда',
            'category_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Женская одежда',
            'category_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Свадебные платья',
            'category_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Мужские костюмы',
            'category_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Женские платья',
            'category_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Женская обувь',
            'category_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Мужская обувь',
            'category_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Детская одежда',
            'category_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Прокат фото/видео',
            'category_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Прокат автомобилей',
            'category_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Прокат одежды',
            'category_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Световое оборудование',
            'category_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Прокат пресс-стен',
            'category_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories')->insert([
            'name' => 'Разное',
            'category_id' => 6,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}

class SubcategoriesTranslationsSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Organizers',
            'subcategory_id' => 1,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Singers/Musicians',
            'subcategory_id' => 2,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Stars',
            'subcategory_id' => 3,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Host/Toastmaster',
            'subcategory_id' => 4,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Show programs',
            'subcategory_id' => 5,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Waiters',
            'subcategory_id' => 6,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Invitations',
            'subcategory_id' => 7,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Light and sound',
            'subcategory_id' => 8,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'DJs',
            'subcategory_id' => 9,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Dance groups',
            'subcategory_id' => 10,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Animators',
            'subcategory_id' => 11,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Making cakes',
            'subcategory_id' => 12,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Spa centers',
            'subcategory_id' => 13,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Dance schools',
            'subcategory_id' => 14,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Vocal schools',
            'subcategory_id' => 15,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Gift shops',
            'subcategory_id' => 16,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Florists',
            'subcategory_id' => 17,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Fireworks shops',
            'subcategory_id' => 18,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Decoration services',
            'subcategory_id' => 19,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Beauty salons',
            'subcategory_id' => 20,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Photo/video shooting',
            'subcategory_id' => 21,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Recording studio',
            'subcategory_id' => 22,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Miscellaneous',
            'subcategory_id' => 23,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Halal',
            'subcategory_id' => 24,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Restaurants',
            'subcategory_id' => 25,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Banquet halls',
            'subcategory_id' => 26,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Clubs and bars',
            'subcategory_id' => 27,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Karaoke',
            'subcategory_id' => 28,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Men\'s clothing',
            'subcategory_id' => 29,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Women\'s clothing',
            'subcategory_id' => 30,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Wedding Dresses',
            'subcategory_id' => 31,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Men\'s suits',
            'subcategory_id' => 32,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Women\'s dresses',
            'subcategory_id' => 33,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Women\'s shoes',
            'subcategory_id' => 34,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Men\'s shoes',
            'subcategory_id' => 35,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Children\'s clothing',
            'subcategory_id' => 36,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Photo/video rental',
            'subcategory_id' => 37,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Car rental',
            'subcategory_id' => 38,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Clothes rental',
            'subcategory_id' => 39,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Lighting equipment',
            'subcategory_id' => 40,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Press wall rental',
            'subcategory_id' => 41,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Miscellaneous',
            'subcategory_id' => 42,
            'language_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);



        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Ұйымдастырушылар',
            'subcategory_id' => 1,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Әншілер/Музыканттар',
            'subcategory_id' => 2,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Жұлдыздар',
            'subcategory_id' => 3,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Жүргізушілер/Асабалар',
            'subcategory_id' => 4,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Шоу бағдарламалар',
            'subcategory_id' => 5,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Даяшылар',
            'subcategory_id' => 6,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Шақырулар',
            'subcategory_id' => 7,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Жарық және дыбыс',
            'subcategory_id' => 8,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Ди-джейлер',
            'subcategory_id' => 9,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Би ұжымдары',
            'subcategory_id' => 10,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Аниматорлар',
            'subcategory_id' => 11,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Торттар дайындау',
            'subcategory_id' => 12,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Спа-орталықтар',
            'subcategory_id' => 13,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Би мектептері',
            'subcategory_id' => 14,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Вокал мектептері',
            'subcategory_id' => 15,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Сыйлықтар дүкендері ',
            'subcategory_id' => 16,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Флористика',
            'subcategory_id' => 17,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'От-шашулар дүкендері ',
            'subcategory_id' => 18,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Безендіру қызметтері',
            'subcategory_id' => 19,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Сұлулық салондары',
            'subcategory_id' => 20,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Фото/бейне түсірілім',
            'subcategory_id' => 21,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Дыбыс жазу студиясы',
            'subcategory_id' => 22,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Әртүрлі',
            'subcategory_id' => 23,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Халал',
            'subcategory_id' => 24,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Мейрамханалар',
            'subcategory_id' => 25,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Банкет залдары',
            'subcategory_id' => 26,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Клубтар мен барлар',
            'subcategory_id' => 27,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Караоке',
            'subcategory_id' => 28,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Ерлер киімі',
            'subcategory_id' => 29,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Әйелдер киімі',
            'subcategory_id' => 30,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Той көйлектері',
            'subcategory_id' => 31,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Ерлерге арналған костюмдер',
            'subcategory_id' => 32,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Әйелдер көйлектері',
            'subcategory_id' => 33,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Әйелдер аяқ-киімі',
            'subcategory_id' => 34,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Ерлер аяқ-киімі',
            'subcategory_id' => 35,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Балалар киімі',
            'subcategory_id' => 36,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Фото/видео жалға алу',
            'subcategory_id' => 37,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Автокөлікті жалға алу',
            'subcategory_id' => 38,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Жалға киім',
            'subcategory_id' => 39,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Жарық жабдықтары',
            'subcategory_id' => 40,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Жалға пресс-қабырғалар',
            'subcategory_id' => 41,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'Әртүрлі',
            'subcategory_id' => 42,
            'language_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);



        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'المنظمون',
            'subcategory_id' => 1,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'المطربون/الموسيقيون',
            'subcategory_id' => 2,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'النجوم',
            'subcategory_id' => 3,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'أعضاء الفرقة/تامادا',
            'subcategory_id' => 4,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'عرض البرنامج',
            'subcategory_id' => 5,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'النوادل',
            'subcategory_id' => 6,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'الدعوات',
            'subcategory_id' => 7,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'الضوء والصوت',
            'subcategory_id' => 8,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'دي.جي',
            'subcategory_id' => 9,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'فرق الرقص',
            'subcategory_id' => 10,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'المصممون',
            'subcategory_id' => 11,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'تحضير قوالب',
            'subcategory_id' => 12,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'المراكز الصحية',
            'subcategory_id' => 13,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'مدارس الرقص',
            'subcategory_id' => 14,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'مدارس الدروس الصوتية',
            'subcategory_id' => 15,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'متاجر الهدايا',
            'subcategory_id' => 16,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'بائعو الزهور',
            'subcategory_id' => 17,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'محلات الألعاب النارية',
            'subcategory_id' => 18,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'خدمات التسجيل',
            'subcategory_id' => 19,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'صالونات التجميل',
            'subcategory_id' => 20,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'تصوير صورة/فيديو',
            'subcategory_id' => 21,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'استوديوهات التسجيل الصوتي',
            'subcategory_id' => 22,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'منوعات',
            'subcategory_id' => 23,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'حلال',
            'subcategory_id' => 24,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'المطاعم',
            'subcategory_id' => 25,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'قاعات الولائم',
            'subcategory_id' => 26,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'النوادي والحانات',
            'subcategory_id' => 27,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'كاراووكي',
            'subcategory_id' => 28,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'الألبسة الرجالية',
            'subcategory_id' => 29,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'الألبسة النسائية',
            'subcategory_id' => 30,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'فساتين الأعراس',
            'subcategory_id' => 31,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'بدلات رجالية',
            'subcategory_id' => 32,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'أثواب نسائية',
            'subcategory_id' => 33,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'الأحذية النسائية',
            'subcategory_id' => 34,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'الأحذية الرجالية',
            'subcategory_id' => 35,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'ألبسة الأطفال',
            'subcategory_id' => 36,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'تأجير صور/فيديو',
            'subcategory_id' => 37,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'تأجير السيارات',
            'subcategory_id' => 38,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'تأجير الألبسة',
            'subcategory_id' => 39,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'معدات الإضاءة',
            'subcategory_id' => 40,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'تأجير منصات الصحافة',
            'subcategory_id' => 41,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => 'منوعات',
            'subcategory_id' => 42,
            'language_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);



        DB::table('microphone_subcategories_translations')->insert([
            'name' => '组织者',
            'subcategory_id' => 1,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '歌手/音乐家',
            'subcategory_id' => 2,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '明星',
            'subcategory_id' => 3,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '主持人/司仪',
            'subcategory_id' => 4,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '节目表演',
            'subcategory_id' => 5,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '服务员',
            'subcategory_id' => 6,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '请柬',
            'subcategory_id' => 7,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '照明与音响',
            'subcategory_id' => 8,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '调音师',
            'subcategory_id' => 9,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '舞蹈团体',
            'subcategory_id' => 10,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '穿戏装演员',
            'subcategory_id' => 11,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '做蛋糕',
            'subcategory_id' => 12,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '水疗中心',
            'subcategory_id' => 13,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '舞蹈学校',
            'subcategory_id' => 14,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '歌唱艺术学校',
            'subcategory_id' => 15,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '礼品商店',
            'subcategory_id' => 16,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '花卉学家',
            'subcategory_id' => 17,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '烟火店',
            'subcategory_id' => 18,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '办理服务',
            'subcategory_id' => 19,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '美容沙龙',
            'subcategory_id' => 20,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '照片/视频拍摄',
            'subcategory_id' => 21,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '录音播放室',
            'subcategory_id' => 22,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '其他',
            'subcategory_id' => 23,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '清真',
            'subcategory_id' => 24,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '餐厅',
            'subcategory_id' => 25,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '宴会厅',
            'subcategory_id' => 26,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '俱乐部和酒吧',
            'subcategory_id' => 27,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '卡拉OK',
            'subcategory_id' => 28,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '男装',
            'subcategory_id' => 29,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '女装',
            'subcategory_id' => 30,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '婚纱礼服',
            'subcategory_id' => 31,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '男士西装',
            'subcategory_id' => 32,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '女士服装',
            'subcategory_id' => 33,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '女士靴鞋',
            'subcategory_id' => 34,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '男士靴鞋',
            'subcategory_id' => 35,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '儿童服装',
            'subcategory_id' => 36,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '照相/录像出租',
            'subcategory_id' => 37,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '汽车租赁',
            'subcategory_id' => 38,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '服装租赁',
            'subcategory_id' => 39,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '照明设备',
            'subcategory_id' => 40,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '媒体墙出租',
            'subcategory_id' => 41,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_subcategories_translations')->insert([
            'name' => '其他',
            'subcategory_id' => 42,
            'language_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}

class AdsSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_ads')->insert([
            'name' => 'Айгерим Ерланова',
            'description' => 'Организатор',
            'rating' => 4,
            'phone' => '+71234567890',
            'address' => 'ул. Улица, дом 3',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'colored_date_time' => '2021-09-26 16:14:28',
            'top_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 1,
            'creator_id' => 2,
            'image_id' => 21,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Олжас Кулаханов',
            'description' => '"AIO" corporation департамент развития',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'colored_date_time' => '2019-09-26 16:14:28',
            'top_date_time' => '2019-09-26 16:14:28',
            'subcategory_id' => 1,
            'creator_id' => 2,
            'image_id' => 1,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Зухра Кожамкулова',
            'description' => 'Организация мероприятий и праздников на высоком уровне',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 1,
            'creator_id' => 2,
            'image_id' => 22,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Марат Мырзакадыров',
            'description' => 'Организатор мероприятий',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 1,
            'creator_id' => 2,
            'image_id' => 2,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Гульшат Абдалиева',
            'description' => 'Организация мероприятий на высоком уровне',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 1,
            'creator_id' => 2,
            'image_id' => 23,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Динара Бекбасарова',
            'description' => 'Организация жизни В шоколаде',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 1,
            'creator_id' => 2,
            'image_id' => 24,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Александр Щетинин',
            'description' => 'Изготовление пригласительных и печатной продукции Алматы по доступным ценам',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 1,
            'creator_id' => 2,
            'image_id' => 3,
            'city_id' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Samal Asain',
            'description' => 'Любое торжество, любого масштаба с нами пройдет на высшем уровне. От встречи гостей до финала. Разработаем сценарий по вашему желанию. Подарим яркие эмоции, стильные образы. Исполнители казахстанской и российской эстрады! Мы рассмотрим любой бюджет и будем рады сотрудничеству!',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 1,
            'creator_id' => 2,
            'image_id' => 4,
            'city_id' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Сера Мурат',
            'description' => '',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 1,
            'creator_id' => 2,
            'image_id' => 5,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Бадыр Асланов',
            'description' => '🐯🐯🦁🐯🦁🐯🦁🦁',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 1,
            'creator_id' => 2,
            'image_id' => 6,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);



        DB::table('microphone_ads')->insert([
            'name' => 'Айкын Толепберген',
            'description' => 'Айкын Толепберген - участник первого сезона SuperStar.kz (аналог American Idol), ныне успешный казахстанский певец.',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 2,
            'creator_id' => 2,
            'image_id' => 7,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Sax',
            'description' => 'Яркий дуэт придаст любому мероприятию свою индивидуальность.',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 2,
            'creator_id' => 2,
            'image_id' => 43,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Live Band',
            'description' => 'Live-band "Premium" - это уникальный проект, целью которого является обеспечение качественного музыкального оформления любого типа мероприятий.',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 2,
            'creator_id' => 2,
            'image_id' => 8,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Берiкол Ахметов',
            'description' => 'БЕТАШАР.',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 2,
            'creator_id' => 2,
            'image_id' => 9,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Даурен Кыдыров',
            'description' => 'Концерттик багдарламаларда',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 2,
            'creator_id' => 2,
            'image_id' => 10,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Нурым Куаныш',
            'description' => 'Барлык сурактар бойынша',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 2,
            'creator_id' => 2,
            'image_id' => 11,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Жанкожа Аяпов',
            'description' => 'Домбырамен, баянмен',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 2,
            'creator_id' => 2,
            'image_id' => 12,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Айгуль Иманбаева',
            'description' => 'Айгул Иманбаева 1977 ж Жамбыл об.',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 2,
            'creator_id' => 2,
            'image_id' => 25,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Индира Расылхан',
            'description' => 'Мерекелiк кештермен, концерт жонiнде',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 2,
            'creator_id' => 2,
            'image_id' => 26,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Макпал Диханбаева',
            'description' => '',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 2,
            'creator_id' => 2,
            'image_id' => 27,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Ведущий showman',
            'description' => 'Ведущий на юбилеи Ведущий на концерт Ведущий на новый год',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 4,
            'creator_id' => 2,
            'image_id' => 13,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Шоумен, ведущий',
            'description' => 'Творческий подход к любимому делу и огромный опыт ведения мероприятий любого уровня',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 4,
            'creator_id' => 2,
            'image_id' => 14,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Ведущий Болат',
            'description' => 'Ведущий Болат Срымов Шоумен, иногда телеведущий.',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 4,
            'creator_id' => 2,
            'image_id' => 15,
            'city_id' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_ads')->insert([
            'name' => 'Сергей Гарун',
            'description' => 'Хотите веселую и незабываемую свадьбу? Запоминающийся юбилей? Корпоратив, который ваши сотрудники запомнят навсегда?',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 4,
            'creator_id' => 2,
            'image_id' => 16,
            'city_id' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);



        DB::table('microphone_ads')->insert([
            'name' => 'Farabi',
            'description' => 'Более 100 блюд от поваров прошедших обучение у обладателей мишленовских звезд. Собственная парковка на 100 мест не на проезжей части. У нас 2 новых красивых банкетных зала, со специализированной сценой и отдельным танцполом. Ресторан Банкетный зал',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 26,
            'creator_id' => 2,
            'image_id' => 41,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);



        DB::table('microphone_ads')->insert([
            'name' => 'Yerbol',
            'description' => '',
            'expiration_date_time' => '2021-09-26 16:14:28',
            'subcategory_id' => 39,
            'creator_id' => 2,
            'image_id' => 42,
            'city_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}

class ReviewsSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_reviews')->insert([
            'body' => 'Организатор не очень',
            'rating' => 3,
            'ad_id' => 1,
            'user_id' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_reviews')->insert([
            'body' => 'Отличный организатор!',
            'rating' => 5,
            'ad_id' => 1,
            'user_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}

class AdsMediaSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_ads_media')->insert([
            'ad_id' => 1,
            'media_id' => 60,
        ]);
        DB::table('microphone_ads_media')->insert([
            'ad_id' => 1,
            'media_id' => 59,
        ]);
        DB::table('microphone_ads_media')->insert([
            'ad_id' => 1,
            'media_id' => 58,
        ]);
        DB::table('microphone_ads_media')->insert([
            'ad_id' => 1,
            'media_id' => 61,
        ]);
        DB::table('microphone_ads_media')->insert([
            'ad_id' => 1,
            'media_id' => 62,
        ]);
        DB::table('microphone_ads_media')->insert([
            'ad_id' => 1,
            'media_id' => 63,
        ]);
    }
}

class AdsProductsSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_ads_products')->insert([
            'ad_id' => 1,
            'product_id' => 1,
        ]);
    }
}

class UsersAdsSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_users_ads')->insert([
            'user_id' => 1,
            'ad_id' => 1,
        ]);
    }
}

class PartsSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_parts')->insert([
            'percent' => 100,
            'user_id' => 1,
        ]);
    }
}

class TariffsSeeder extends Seeder
{
    public function run()
    {
        DB::table('microphone_tariffs')->insert([
            'action' => 'Становление агентом',
            'price' => 7000,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Покраска',
            'price' => 2000,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Топ',
            'price' => 3000,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Минимальная сумма вклада',
            'price' => 1000,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Агентская доля',
            'percent' => 35,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1200,
            'subcategory_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1500,
            'subcategory_id' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 300,
            'subcategory_id' => 6,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 800,
            'subcategory_id' => 7,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 8,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 800,
            'subcategory_id' => 9,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 400,
            'subcategory_id' => 10,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 400,
            'subcategory_id' => 11,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 12,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1500,
            'subcategory_id' => 13,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 500,
            'subcategory_id' => 14,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 500,
            'subcategory_id' => 15,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 16,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 17,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 2000,
            'subcategory_id' => 18,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 19,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 20,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 21,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 500,
            'subcategory_id' => 22,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 23,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 24,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 2000,
            'subcategory_id' => 25,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 2000,
            'subcategory_id' => 26,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 27,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1500,
            'subcategory_id' => 28,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 29,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 30,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 31,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 32,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 33,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 34,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 35,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 36,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 37,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 2000,
            'subcategory_id' => 38,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 400,
            'subcategory_id' => 39,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 40,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1500,
            'subcategory_id' => 41,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Объявление',
            'price' => 1000,
            'subcategory_id' => 42,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Минимальный процент админа',
            'percent' => 51,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Минимальная сумма вывода',
            'price' => 1000,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('microphone_tariffs')->insert([
            'action' => 'Максимальная сумма вклада',
            'price' => 21000000,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}

class CurrenciesSeeder extends Seeder
{
    public function run()
    {
        DB::table('currencies')->insert([
            'code' => 'KZT',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'AUD',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'AZN',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'AMD',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'BYN',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'BRL',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'HUF',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'HKD',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'GEL',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'DKK',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'AED',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'USD',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'EUR',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'INR',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'IRR',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'CAD',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'CNY',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'KWD',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'KGS',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'MYR',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'MXN',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'MDL',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'NOK',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'PLN',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'SAR',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'RUB',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'XDR',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'SGD',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'TJS',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'THB',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'TRY',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'UZS',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'UAH',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'GBP',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'CZK',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'SEK',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'CHF',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'ZAR',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'KRW',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('currencies')->insert([
            'code' => 'JPY',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
