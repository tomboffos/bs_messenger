<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Country;
use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Facades\Log;

class LocalizationCitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $cityEnglish = DB::table('net_city')
            ->leftJoin('cities', 'net_city.name_en', '=', 'cities.name')
            ->where('cities.country_id','=',17)
            ->get();


        foreach ($cityEnglish as $city){
            City::where('name','=',$city->name_en)
                ->update([
                   'name' => $city->name_ru
                ]);
        }


    }


}
