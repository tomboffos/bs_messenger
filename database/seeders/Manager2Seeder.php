<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Manager2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tariffs')->insert([
            [
                'action' => 'Менеджерская доля от инвестора',
                'percent' => 5,
                'application_id' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Менеджерская доля от инвестора',
                'percent' => 5,
                'application_id' => 2,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
