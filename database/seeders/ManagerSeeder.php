<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ManagerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('microphone_roles')->insert([
            'id' => 5,
            'name' => 'менеджер',
        ]);

        DB::table('microphone_permissions')->insert([
            'name' => 'Получение доли с агентов',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('microphone_roles_permissions')->insert([
            'role_id' => 5,
            'permission_id' => 24,
        ]);

        DB::table('microphone_tariffs')->insert([
            'action' => 'Менеджерская доля',
            'percent' => 10,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
