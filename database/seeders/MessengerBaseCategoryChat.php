<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MessengerBaseCategoryChat extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('messenger_category_chats')->insert([[
            'id' => 1,
            'name' => 'Super-work',
            'avatar' => '/',
            'app_chat' => 1,
            'user_id' => null,
        ],
            [
                'id' => 2,
                'name' => 'Microphon',
                'avatar' => '/',
                'app_chat' => 1,
                'user_id' => null,
            ],
            [
                'id' => 3,
                'name' => 'Svistok',
                'avatar' => '/',
                'app_chat' => 1,
                'user_id' => null,
            ],
        ]);
    }
}
