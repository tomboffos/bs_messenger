<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MessengerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            MessengerApplicationSeeder::class,
        ]);
    }
}

class MessengerApplicationSeeder extends Seeder
{
    public function run()
    {
        DB::table('applications')->insert([
            [
                'id' => 3,
                'name' => 'Messenger',
                'capitalization' => 10000000,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}