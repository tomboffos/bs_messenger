<?php

namespace Database\Seeders;

use App\Models\Messenger\MessengerCategoryChat;
use App\Models\MessengerUserCategoryOrder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MessnegerCategoryChat extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('messenger_category_chats')->truncate();
        $messengeruserLogIn = MessengerUserCategoryOrder::all();

        DB::table('messenger_user_category_orders')->truncate();
        DB::table('messenger_category_chats')->insert([[
            'id' => 1,
            'name' => 'Super-Work',
            'avatar' => 'messenger/image/TC0slDgDwvJAYJBhNkevDW3b60ItHSUUSaWYukCJ.png',
            'app_chat' => 1,
            'user_id' => null,
        ],
            [
                'id' => 2,
                'name' => 'Microphon',
                'avatar' => 'messenger/image/th9R3b5P14SdlyCxckyrziehhBzaEBAhvJVi6rse.png',
                'app_chat' => 1,
                'user_id' => null,
            ],
            [
                'id' => 3,
                'name' => 'Svistok',
                'avatar' => 'messenger/image/cyKFe0dGrTHMN9TmpbwaqM5liTNyli3CSaYiin0V.png',
                'app_chat' => 1,
                'user_id' => null,
            ],
            [
                'id' => 4,
                'name' => 'MedApp',
                'avatar' => 'messenger/image/zJJFB5UBfs0fV3fuVyGoo5EcVVdt3OnJpXLx9Rhz.png',
                'app_chat' => 1,
                'user_id' => null,
            ],
            [
                'id' => 5,
                'name' => 'Tour&Sport',
                'avatar' => 'messenger/image/jgM57EHFaEjFeGtfS5jNEHiEFyBq1O61QooytZ2f.png',
                'app_chat' => 1,
                'user_id' => null,
            ],
            [
                'id' => 6,
                'name' => 'Город',
                'avatar' => null,
                'app_chat' => 1,
                'user_id' => null,
            ],
        ]);
        $allCategory = MessengerCategoryChat::all();
        foreach ($messengeruserLogIn->unique('user_id') as $user){
            $order = 1;
            foreach ($allCategory as $category){
                MessengerUserCategoryOrder::firstOrCreate(
                    [
                        'user_id' => $user->user_id,
                        'category_id' => $category->id,
                        'order' => $order++,
                    ]
                );
            }
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
