<?php

namespace Database\Seeders;

use App\Models\Messenger\MessengerChat;
use App\Models\Messenger\MessengerChatMember;
use Illuminate\Database\Seeder;

class RoleAdminInPrivateChatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $chats = MessengerChat::where('is_private','=',1)
            ->get();

        foreach ($chats as $chat){
            MessengerChatMember::where('chat_id','=',$chat->id)
                ->update([
                    'role' => 'admin',
                ]);
        }
    }
}
