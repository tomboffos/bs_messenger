<?php

namespace Database\Seeders;

use App\Models\Messenger\MessengerChatMember;
use App\Models\Messenger\UserSettingsInChat;
use Illuminate\Database\Seeder;

class SettingsInChatWhoMember extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $members = MessengerChatMember::all();

        foreach ($members as $member){
            UserSettingsInChat::firstOrCreate([
                'user_id' => $member->user_id,
                'chat_id' => $member->chat_id,
                'sound_on' => 1,
                'vibration_on' => 1,
            ]);
        }
    }
}
