<?php

namespace Database\Seeders;

use App\Models\Tariff;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class SuperWorkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            SuperWorkCategoriesSeeder::class,
            SuperWorkTariffsSeeder::class,
            SuperWorkPartsSeeder::class,
            SuperWorkRolesSeeder::class,
            SuperWorkPermissionsSeeder::class,
            SuperWorkRolesPermissionsSeeder::class
        ]);
    }
}

class SuperWorkCategoriesSeeder extends Seeder
{
    public function run()
    {
        DB::table('superwork_categories')->insert([
            [
                'name' => 'Административный персонал',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Банки/страхование',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Бары/рестораны/кафе',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Гостиницы/туризм/отдых',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Государственная служба/некоммерческая деятельность',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Добыча сырья',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Домашний персонал',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Интернет/IT/компьютеры/телеком',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Культура/искусство/развлечения',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Красота/фитнес/спорт',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Маркетинг/реклама/дизайн',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Медицина/фармацевтика',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Начало карьеры/стажировка/практика',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Недвижимость',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'HR/управление персоналом/тренинги',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Образование/наука',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Охрана/безопасность',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Производство/энергетика',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Работа за рубежом',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Розничная торговля/продажи/закупки',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Сельское хозяйство/агробизнес/лесное хозяйство',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Сервис и быт',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Сетевой маркетинг',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Строительство/архитектура',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'СТО/автомойки/сервисное обслуживание',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Транспорт/логистика',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Топ-менеджмент/руководство высшего звена',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Финансы/бухгалтерия/экономика',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Другие сферы деятельности',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}

class SuperWorkTariffsSeeder extends Seeder
{
    public function run()
    {
        DB::table('tariffs')->insert([
            [
                'action' => 'Минимальная сумма вклада',
                'price' => 2000,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Максимальная сумма вклада',
                'price' => 50000,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Минимальный процент админа',
                'price' => null,
                'percent' => 20,
                'application_id' => 2,
                'superwork_category_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 2,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 3,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 4,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 5,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 6,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 7,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 8,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 9,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 10,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 11,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 12,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 13,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 14,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 15,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 16,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 17,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 18,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 19,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 20,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 21,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 22,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 23,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 24,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 25,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 26,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 27,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 28,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Объявление',
                'price' => 150,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => 29,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Начальный',
                'price' => 700,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Продвинутый',
                'price' => 1300,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'VIP',
                'price' => 4500,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Становление агентом',
                'price' => 7000,
                'percent' => null,
                'application_id' => 2,
                'superwork_category_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Агентская доля',
                'price' => null,
                'percent' => 35,
                'application_id' => 2,
                'superwork_category_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'action' => 'Менеджерская доля',
                'price' => null,
                'percent' => 10,
                'application_id' => 2,
                'superwork_category_id' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}

class SuperWorkPartsSeeder extends Seeder
{
    public function run()
    {
        DB::table('parts')->insert([
            [
            'percent' => 100,
            'user_id' => 1,
            'application_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
            ],
        ]);
    }
}

class SuperWorkRolesSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'рекламодатель',
                'application_id' => 2,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'агент',
                'application_id' => 2,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'менеджер',
                'application_id' => 2,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}

class SuperWorkPermissionsSeeder extends Seeder
{
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name' => 'Использование приложения',
                'application_id' => 2,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Привязка к рекламодателям по реферальной ссылке',
                'application_id' => 2,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Получение доли с агентов',
                'application_id' => 2,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}

class SuperWorkRolesPermissionsSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles_permissions')->insert([
            [
                'role_id' => 6,
                'permission_id' => 25,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'role_id' => 6,
                'permission_id' => 3,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'role_id' => 6,
                'permission_id' => 6,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'role_id' => 6,
                'permission_id' => 7,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'role_id' => 6,
                'permission_id' => 8,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'role_id' => 6,
                'permission_id' => 9,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'role_id' => 6,
                'permission_id' => 22,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'role_id' => 7,
                'permission_id' => 26,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'role_id' => 8,
                'permission_id' => 27,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}