<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TariffForMessenger extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tariffs')->insert([[
            'id' => 103,
            'action' => 'Начальный',
            'price' => '700',
            'percent' => null,
            'subcategory_id' => null,
            'superwork_category_id' => null,
            'application_id' => 3,
        ],
            [
                'id' => 104,
                'action' => 'Продвинутый',
                'price' => '1300',
                'percent' => null,
                'subcategory_id' => null,
                'superwork_category_id' => null,
                'application_id' => 3,
            ],
            [
                'id' => 105,
                'action' => 'VIP',
                'price' => '1500',
                'percent' => null,
                'subcategory_id' => null,
                'superwork_category_id' => null,
                'application_id' => 3,
            ],
        ]);
    }

}
