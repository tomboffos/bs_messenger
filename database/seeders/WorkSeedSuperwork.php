<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class WorkSeedSuperwork extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('skills')
            ->insert(
                [
                    'name' => ''
                ],
            );


        \DB::table('specializations')
            ->insert([
                [
                    'name' => ''
                ],
                ]
            );


        \DB::table('expiriences')
            ->insert([
                [
                    'name' => '1 год'
                ],
                [
                    'name' => '1-3 года'
                ],
                [
                    'name' => '3-6 лет'
                ],
                [
                    'name' => '6+ лет'
                ],
                    ]);

        \DB::table('languages_resume')
            ->insert([
                [
                    'name' => 'Русский'
                ],
                [
                    'name' => 'Казахский'
                ],
                [
                    'name' => 'Немецкий'
                ],
                [
                    'name' => 'Китайский'
                ],
                [
                    'name' => 'Английский'
                ],
                [
                    'name' => 'Французский'
                ],
            ]);
    }
}
