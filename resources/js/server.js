var app = require('express')();
const  fs = require('fs');
const certificate = fs.readFileSync("/etc/letsencrypt/live/aio-test-vps.kulenkov-group.kz/fullchain.pem", 'utf8')
const privateKey = fs.readFileSync("/etc/letsencrypt/live/aio-test-vps.kulenkov-group.kz/privkey.pem", 'utf8')
const credentials = {
    key: privateKey,
    cert: certificate,
    passphrase: process.env.PASSPHRASE
}
var server = require('https').createServer(credentials, app);
var io = require('socket.io')(server, {
    cors: {
        origin: "https://aio-test-vps.kulenkov-group.kz",
        methods: ["GET", "POST"],
        allowedHeaders: ["Access-Control-Allow-Origin:*"],
        credentials: true,
        transports: ['websocket', 'polling'],
    },
    allowEIO3: true,
});
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.set('port', process.env.PORT || 6001);
app.set('host', process.env.HOST || '0.0.0.0');

server.listen(app.get('port'), app.get('host'), () => {
    console.log("Express server listening IP: " + app.get('host'));
});

const  log = require('fs');

io.on('connection', socket => {
    socket.on('sayHi', data => {
        console.log(data.message);
    });
})
.on('comms', function(content) {
        console.log('Client is ready!');
    log.writeFile("/var/www/html/aio_api/storage/socket.log", content );
    })
    .on('get.index', function(content) {
        console.log('Client is ready!');
        log.writeFile("/var/www/html/aio_api/storage/socket.log", content );
    });
