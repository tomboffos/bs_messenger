<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://widget.cloudpayments.ru/bundles/cloudpayments"></script>
</head>
<body>
<div class="flex-center position-ref full-height">
    <button id="checkout">Оплата</button>
    <script>
        let widget = new cp.CloudPayments();
        pay = function () {
            widget.pay('charge', // или 'charge'
                { //options
                    publicId: 'pk_936b3d7eb3be5e367bdcbe623c16f', //id из личного кабинета
                    description: 'Оплата товаров в example.com', //назначение
                    amount: 100, //сумма
                    currency: 'RUB', //валюта
                    skin: "mini", //дизайн виджета (необязательно)
                    data: {
                        myProp: 'myProp value'
                    }
                },
                {
                    onSuccess: function (options) { // success
                        e.preventDefault(); // Want to stay on the page, not go through normal form
                        // Might be easier to make this a NON submit button - then we can use the rest below and not have to js submit()
                        // Grab any extra info via data.
                        let item_type = $(this).data('item-type');
                        let name = $(this).val();
                        $.ajax({
                            url: "{{url('user/create/')}}",
                            type: "POST",
                            data: {
                                item_type: item_type,
                                name: name
                            },
                            success: function (name) {
                                alert(message);
                                // Or, if you want a better looking alert, you can use a library like SWAL:
                                swal("Success!", "New user created with a name of: "+name, "success");
                            },
                            error: function () {
                                swal("Error", "Unable to bring up the dialog.", "error");
                            }
                        });

                        // let xhr = new XMLHttpRequest();
                        // xhr.open("POST", 'https://apimicrophone.phoenixds.kz/api/user/increaseBalance', true);
                        // xhr.setRequestHeader('Content-Type', 'application/json');
                        // xhr.send(JSON.stringify({
                        //     amount: '10000',
                        //     user_id: '2',
                        // }));
                        window.location.replace("https://apimicrophone.phoenixds.kz/payment_success");
                        //действие при успешной оплате
                    },
                    onFail: function (reason, options) { // fail
                        window.location.replace("https://apimicrophone.phoenixds.kz/payment_failure");
                        //действие при неуспешной оплате
                    },
                    onComplete: function (paymentResult, options) { //Вызывается как только виджет получает от api.cloudpayments ответ с результатом транзакции.
                        // window.location.replace("https://apimicrophone.phoenixds.kz/payment");
                        //например вызов вашей аналитики Facebook Pixel
                    }
                }
            )
        };

        pay();

        $('#checkout').click(pay);
    </script>
</div>
</body>
</html>
