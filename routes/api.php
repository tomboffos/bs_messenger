<?php

use App\Http\Controllers\Api\User\Messenger\DocumentController;
use App\Http\Controllers\Bot\BotMessageController;
use App\Http\Controllers\Messenger\ContactController;
use App\Http\Controllers\Messenger\MessageController;
use App\Http\Controllers\Microphone\Franchise\FranchiseController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/createCode', 'Auth\AuthController@createCode');
Route::post('/login', 'Auth\AuthController@login');

Route::middleware(['auth:sanctum'])->group(function () {
    Route::resource('/interest', 'Api\Ad\InterestController');
    Route::resource('/bot-message', 'Bot\BotMessageController');
    Route::get('/superwork/ad/filter', 'Superwork\ResumeController@getFilter');
    Route::get('/superwork/get-resume', 'Superwork\ResumeController@getResume');
    Route::get('/superwork/get-about-company', 'Superwork\ResumeController@getCompany');
    Route::post('/superwork/resume', 'Superwork\ResumeController@createResume');
    Route::post('/superwork/resume/update', 'Superwork\ResumeController@updateResume');
    Route::post('/superwork/about-company', 'Superwork\ResumeController@createAboutCompany');
    Route::post('/superwork/about-company/update', 'Superwork\ResumeController@updateAboutCompany');

    Route::post('/user/getCurrentUser', 'User\UserController@getCurrentUser');
    Route::post('/user/updateCurrentUser', 'User\UserController@updateCurrentUser');
    Route::get('/user/getRolesCurrentUser', 'User\UserController@getRolesCurrentUser');
    Route::post('/user/increaseBalanceCurrentUser', 'User\UserController@increaseBalanceCurrentUser');
    Route::post('/user/increaseBalanceCurrentUser3DS', 'User\UserController@increaseBalanceCurrentUser3DS');
    Route::post('/user/becomeAgent', 'User\UserController@becomeAgent');
    Route::get('/user/referral-list', 'User\UserController@getAuthUserReferral');
    Route::get('/user/children-referral/transactions', 'User\UserController@getChildrenReferralTransaction');

//    Route::post('/user/activateReferral', 'User\UserController@activateReferral');
    Route::get('/user/removeAvatarCurrentUser', 'User\UserController@removeAvatarCurrentUser');
    Route::post('/user/getTransactionsCurrentUser', 'User\UserController@getTransactionsCurrentUser');
    Route::post('/user/getByIdFull', 'User\UserController@getByIdFull');

//    Route::post('/part/buy', 'Part\PartController@buy');
//    Route::post('/part/calculate', 'Part\PartController@calculate');

    Route::post('/microphone/appeal/publish', 'Microphone\Appeal\AppealController@publish');
    Route::post('/microphone/appeal/getAvailable', 'Microphone\Appeal\AppealController@getAvailable');
    Route::post('/microphone/appeal/getSelf', 'Microphone\Appeal\AppealController@getSelf');

    Route::post('/microphone/ad/getSelf', 'Microphone\Ad\AdController@getSelf');
    Route::post('/microphone/ad/publish', 'Microphone\Ad\AdController@publish');
    Route::post('/microphone/ad/extend', 'Microphone\Ad\AdController@extend');
    Route::post('/microphone/ad/makeColored', 'Microphone\Ad\AdController@makeColored');
    Route::post('/microphone/ad/makeTop', 'Microphone\Ad\AdController@makeTop');
    Route::post('/microphone/ad/addFavorite', 'Microphone\Ad\AdController@addFavorite');
    Route::post('/microphone/ad/removeFavorite', 'Microphone\Ad\AdController@removeFavorite');
    Route::post('/microphone/ad/getFavorites', 'Microphone\Ad\AdController@getFavorites');
    Route::post('/microphone/ad/getByIdWithFavorite', 'Microphone\Ad\AdController@getByIdWithFavorite');

    Route::post('/microphone/media/publish', 'Microphone\Media\MediaController@publish');
    Route::get('/microphone/media/getSelf', 'Microphone\Media\MediaController@getSelf');

    Route::post('/microphone/product/publish', 'Microphone\Product\ProductController@publish');
    Route::get('/microphone/product/getSelf', 'Microphone\Product\ProductController@getSelf');

    Route::post('/withdrawal/apply', 'Withdrawal\WithdrawalController@apply');
    Route::get('/withdrawal/getSelf', 'Withdrawal\WithdrawalController@getSelf');

    Route::post('/microphone/review/publish', 'Microphone\Review\ReviewController@publish');


    Route::post('/apple-pay', 'Payment\PaymentController@applePay');

    Route::post('/microphone/ad', 'Microphone\Ad\AdController@index');
    Route::post('/microphone/ad/store', 'Microphone\Ad\AdController@store');
    Route::post('/microphone/ad/update', 'Microphone\Ad\AdController@update');
    Route::post('/microphone/ad/destroy', 'Microphone\Ad\AdController@destroy');
    Route::post('/microphone/ad/addMedia', 'Microphone\Ad\AdController@addMedia');
    Route::post('/microphone/ad/removeMedia', 'Microphone\Ad\AdController@removeMedia');
    Route::post('/microphone/ad/addProduct', 'Microphone\Ad\AdController@addProduct');
    Route::post('/microphone/ad/removeProduct', 'Microphone\Ad\AdController@removeProduct');

    Route::get('/application', 'Application\ApplicationController@index');
    Route::post('/application/store', 'Application\ApplicationController@store');
    Route::post('/application/update', 'Application\ApplicationController@update');
    Route::post('/application/destroy', 'Application\ApplicationController@destroy');

    Route::get('/microphone/appeal', 'Microphone\Appeal\AppealController@index');
    Route::post('/microphone/appeal/store', 'Microphone\Appeal\AppealController@store');
    Route::post('/microphone/appeal/update', 'Microphone\Appeal\AppealController@update');
    Route::post('/microphone/appeal/destroy', 'Microphone\Appeal\AppealController@destroy');

    Route::post('/microphone/category/store', 'Microphone\Category\CategoryController@store');
    Route::post('/microphone/category/update', 'Microphone\Category\CategoryController@update');
    Route::post('/microphone/category/destroy', 'Microphone\Category\CategoryController@destroy');

    Route::get('/microphone/media', 'Microphone\Media\MediaController@index');
    Route::post('/microphone/media/store', 'Microphone\Media\MediaController@store');
    Route::post('/microphone/media/update', 'Microphone\Media\MediaController@update');
    Route::post('/microphone/media/destroy', 'Microphone\Media\MediaController@destroy');

//    Route::get('/part', 'Part\PartController@index');
//    Route::post('/part/store', 'Part\PartController@store');
//    Route::post('/part/update', 'Part\PartController@update');
//    Route::post('/part/destroy', 'Part\PartController@destroy');
//    Route::post('/part/pay', 'Part\PartController@pay');

    Route::get('/permission', 'Permission\PermissionController@index');
    Route::post('/permission/store', 'Permission\PermissionController@store');
    Route::post('/permission/update', 'Permission\PermissionController@update');
    Route::post('/permission/destroy', 'Permission\PermissionController@destroy');

    Route::get('/role', 'Role\RoleController@index');
    Route::post('/role/store', 'Role\RoleController@store');
    Route::post('/role/update', 'Role\RoleController@update');
    Route::post('/role/destroy', 'Role\RoleController@destroy');
    Route::post('/role/addPermission', 'Role\RoleController@addPermission');
    Route::post('/role/removePermission', 'Role\RoleController@removePermission');

    Route::post('/microphone/subcategory/store', 'Microphone\Subcategory\SubcategoryController@store');
    Route::post('/microphone/subcategory/update', 'Microphone\Subcategory\SubcategoryController@update');
    Route::post('/microphone/subcategory/destroy', 'Microphone\Subcategory\SubcategoryController@destroy');

    Route::get('/microphone/product', 'Microphone\Product\ProductController@index');
    Route::post('/microphone/product/store', 'Microphone\Product\ProductController@store');
    Route::post('/microphone/product/update', 'Microphone\Product\ProductController@update');
    Route::post('/microphone/product/destroy', 'Microphone\Product\ProductController@destroy');

    Route::get('/microphone/review', 'Microphone\Review\ReviewController@index');
    Route::post('/microphone/review/store', 'Microphone\Review\ReviewController@store');
    Route::post('/microphone/review/update', 'Microphone\Review\ReviewController@update');
    Route::post('/microphone/review/destroy', 'Microphone\Review\ReviewController@destroy');

    Route::get('/city', 'City\CityController@index');
    Route::post('/city/import', 'City\CityController@import');
    Route::post('/city/store', 'City\CityController@store');
    Route::post('/city/update', 'City\CityController@update');
    Route::post('/city/destroy', 'City\CityController@destroy');

    Route::get('/withdrawal', 'Withdrawal\WithdrawalController@index');
    Route::post('/withdrawal/accept', 'Withdrawal\WithdrawalController@accept');
    Route::post('/withdrawal/decline', 'Withdrawal\WithdrawalController@decline');
    Route::post('/withdrawal/sum', 'Withdrawal\WithdrawalController@sum');
    Route::post('/withdrawal/filter', 'Withdrawal\WithdrawalController@filter');

    Route::post('/transaction', 'Transaction\TransactionController@index');
    Route::post('/transaction/sum', 'Transaction\TransactionController@sum');
    Route::post('/transaction/filter', 'Transaction\TransactionController@filter');
    Route::post('/transaction/revert', 'Transaction\TransactionController@revert');

    Route::post('/tariff/getFull', 'Tariff\TariffController@getFull');
    Route::post('/tariff/store', 'Tariff\TariffController@store');
    Route::post('/tariff/update', 'Tariff\TariffController@update');
    Route::post('/tariff/destroy', 'Tariff\TariffController@destroy');

    Route::get('/currency', 'Currency\CurrencyController@index');
    Route::post('/currency/store', 'Currency\CurrencyController@store');
    Route::post('/currency/update', 'Currency\CurrencyController@update');
    Route::post('/currency/destroy', 'Currency\CurrencyController@destroy');

    Route::get('/document', 'Document\DocumentController@index');
    Route::post('/document/store', 'Document\DocumentController@store');
    Route::post('/document/update', 'Document\DocumentController@update');
    Route::post('/document/destroy', 'Document\DocumentController@destroy');
    Route::post('/document/publish', 'Document\DocumentController@publish');
    Route::get('/document/getSelf', 'Document\DocumentController@getSelf');
    Route::post('/document/getByUser', 'Document\DocumentController@getByUser');

    Route::post('/user', 'User\UserController@index');
    Route::post('/user/getUserParts', 'User\UserController@getUserParts');
    Route::post('/user/getUserReferrals', 'User\UserController@getUserReferrals');
    Route::post('/user/store', 'User\UserController@store');
    Route::post('/user/update', 'User\UserController@update');
    Route::post('/user/destroy', 'User\UserController@destroy');
    Route::post('/user/addRole', 'User\UserController@addRole');
    Route::post('/user/removeRole', 'User\UserController@removeRole');
    Route::post('/user/addApplication', 'User\UserController@addApplication');
    Route::post('/user/removeApplication', 'User\UserController@removeApplication');
    Route::post('/user/addAd', 'User\UserController@addAd');
    Route::post('/user/removeAd', 'User\UserController@removeAd');
//    Route::post('/user/acceptAgent', 'User\UserController@acceptAgent');
//    Route::post('/user/declineAgent', 'User\UserController@declineAgent');
    Route::post('/user/count', 'User\UserController@count');
//    Route::post('/user/addChat', 'User\UserController@addChat');
//    Route::post('/user/removeChat', 'User\UserController@removeChat');
    Route::post('/user/makeAgent', 'User\UserController@makeAgent');
    Route::post('/user/removeAgent', 'User\UserController@removeAgent');
    Route::get('/user/get-current-user-agent-status', [UserController::class, 'getCurrentUserAgentStatus']);

    Route::post('/playerId/store', 'PlayerId\PlayerIdController@store');
    Route::post('/playerId/destroy', 'PlayerId\PlayerIdController@destroy');
    Route::get('/playerId/getSelf', 'PlayerId\PlayerIdController@getSelf');
    Route::post('/playerId/replace', 'PlayerId\PlayerIdController@replace');

    //SuperWork

    Route::post('/superwork/ad/getSelf', 'SuperWork\Ad\AdController@getSelf');
    Route::post('/superwork/ad/publish', 'SuperWork\Ad\AdController@publish');
    Route::post('/superwork/ad/extend', 'SuperWork\Ad\AdController@extend');
    Route::post('/superwork/ad/addFavorite', 'SuperWork\Ad\AdController@addFavorite');
    Route::post('/superwork/ad/removeFavorite', 'SuperWork\Ad\AdController@removeFavorite');
    Route::post('/superwork/ad/getFavorites', 'SuperWork\Ad\AdController@getFavorites');

    Route::post('/superwork/ad', 'SuperWork\Ad\AdController@index');
    Route::post('/superwork/ad/store', 'SuperWork\Ad\AdController@store');
    Route::post('/superwork/ad/update', 'SuperWork\Ad\AdController@update');
    Route::post('/superwork/ad/destroy', 'SuperWork\Ad\AdController@destroy');

    Route::get('/superwork/getActiveUserAd', 'SuperWork\Ad\AdController@getActiveUserAd');


    Route::post('/superwork/category/store', 'SuperWork\Category\CategoryController@store');
    Route::post('/superwork/category/update', 'SuperWork\Category\CategoryController@update');
    Route::post('/superwork/category/destroy', 'SuperWork\Category\CategoryController@destroy');

    Route::post('/superwork/media/publish', 'SuperWork\Media\MediaController@publish');
    Route::get('/superwork/media/getSelf', 'SuperWork\Media\MediaController@getSelf');

    Route::get('/superwork/media', 'SuperWork\Media\MediaController@index');
    Route::post('/superwork/media/store', 'SuperWork\Media\MediaController@store');
    Route::post('/superwork/media/update', 'SuperWork\Media\MediaController@update');
    Route::post('/superwork/media/destroy', 'SuperWork\Media\MediaController@destroy');

    //messenger
    Route::get('/media/category/', 'Media\MediaCategoryController@index');
    Route::post('/media/category/', 'Media\MediaCategoryController@create');
    Route::put('/media/category/{category}', 'Media\MediaCategoryController@update');
    Route::get('/media/category/{category}', 'Media\MediaCategoryController@show');
    Route::delete('/media/category/{category}', 'Media\MediaCategoryController@delete');

    Route::get('/media/', 'Media\MediaController@index');
    Route::get('/media/{media}', 'Media\MediaController@show');
    Route::post('/media/{media}', 'Media\MediaController@update');
    Route::post('/media/', 'Media\MediaController@create');
    Route::post('/delete-media', 'Media\MediaController@delete');
    Route::group(['prefix' => 'messenger', 'middleware' => ['online']], function () {
        Route::resource('/document', 'Api\User\Messenger\DocumentController');
        Route::resource('/device-token','Api\Messenger\DeviceTokenController');
        Route::resource('/conference', 'Api\User\ConferenceController');
        Route::get('/document/share/{document}/{user}','Api\User\Messenger\DocumentController@shareWithOther');
        Route::group(['prefix' => 'category'], function () {
            Route::get('', 'Messenger\ChatCategoryController@index');
            Route::post('', 'Messenger\ChatCategoryController@create');
            Route::post('new', 'Messenger\ChatCategoryController@create');
            Route::get('{category}', 'Messenger\ChatCategoryController@show');
            Route::post('{category}', 'Messenger\ChatCategoryController@update');
            Route::delete('{category}', 'Messenger\ChatCategoryController@delete');
            Route::post('chat/transfer', 'Messenger\ChatController@transferChatUserToOtherCategory');
            Route::post('actions/reorder', 'Messenger\ChatCategoryController@reorder');
        });
        Route::get('last', 'Messenger\ChatController@lastChat');
        Route::group(['prefix' => 'chat'], function () {
            Route::get('admin-chat', 'Messenger\ChatController@getAdminChats');
            Route::get('current-ad', 'Messenger\ChatController@getCurrentAdChat');
            Route::get('tariff', 'Messenger\SettingsController@getTariff');
            Route::post('new-ad', 'Messenger\ChatController@newAdChat');
            Route::post('stop-ad', 'Messenger\ChatController@stopAdChat');

            Route::post('add-admin', 'Messenger\ChatController@addAdminRoleChat');
            Route::post('delete-admin', 'Messenger\ChatController@deleteAdminRoleChat');
            Route::post('forward', 'Messenger\MessageController@sendMessage');
            Route::get('{chat}', 'Messenger\ChatController@show');
            Route::post('', 'Messenger\ChatController@create');
            Route::post('settings/{chat}', 'Messenger\ChatController@updateSettingsChat');
            Route::post('{chat}', 'Messenger\ChatController@update');
            Route::post('join/{chat}', 'Messenger\ChatController@joinToChat');
            Route::post('leave/{chat}', 'Messenger\ChatController@leaveFromChat');
            Route::get('{chat}/full-members', 'Messenger\ChatController@showFullMembers');
            Route::post('{chat}/add-contacts', 'Messenger\ChatController@addContactToChat');
            Route::post('{chat}/kick-contacts', 'Messenger\ChatController@deleteContactFromChat');
            Route::get('{chat}/get-message', 'Messenger\MessageController@getChatMessage');
            Route::get('{chat}/get-messages-by-search/{message}',
                'Messenger\MessageController@getChatMessagesBySearch');
            Route::post('{chat}/send-message', 'Messenger\MessageController@sendMessage');
            Route::post('{chat}/set-top_message', 'Messenger\MessageController@setTopMessage');
            Route::post('{chat}/unset-top_message', 'Messenger\MessageController@unsetTopMessage');
            Route::post('set-time-deleted', 'Messenger\ChatController@addTimeToSecretChat');

            Route::post('{chat}/settings/switch-sound', 'Messenger\ChatSettingsController@switchSound');
            Route::post('{chat}/settings/switch-vibration', 'Messenger\ChatSettingsController@switchVibration');
        });

        Route::get('get-contact', 'Messenger\ContactController@searchContact');

        Route::post('settings/user', 'Messenger\SettingsController@updateSettings');
        Route::get('settings/user', 'Messenger\SettingsController@getSettings');

        Route::get('index', 'Messenger\ChatController@getIndex');

        Route::group(['prefix' => 'user'], function () {
            Route::get('profile', 'Messenger\ProfileController@getUser');
            Route::get('chat', 'Messenger\ChatController@getUserChat');
            Route::post('contact', 'Messenger\ContactController@parseUserContact');
            Route::get('contact', 'Messenger\ContactController@getUserContact');
            Route::post('bind-contact', 'Messenger\ContactController@bindContacts');
            Route::get('get-contacts', 'User\UserController@getCurrentUserContacts');
            Route::get('search', 'User\UserController@search');
        });

        Route::post('message/{message}', 'Messenger\MessageController@updateMessage');
        Route::post('delete-message', [MessageController::class, 'deleteChatMessage']);

        Route::get('message/search', 'Messenger\MessageController@searchMessageInChat');
        Route::get('message/search/all', 'Messenger\MessageController@searchMessageAllChat');
        Route::post('message-read', 'Messenger\MessageController@readMessage');

        Route::group(['prefix' => 'call'], function () {
            Route::get('user-call', 'Messenger\AgoraController@getLogCallUser');
            Route::get('user-call/delete/{call}', 'Messenger\AgoraController@deleteCall');
            Route::get('user-call/delete-all-calls','Messenger\AgoraController@deleteAll');
            Route::post('new-call', 'Messenger\AgoraController@newCall');
            Route::post('new-group-call', 'Messenger\AgoraController@makeCallConference');

            Route::post('accept-call', 'Messenger\AgoraController@agoraOneOnOneCall');
            Route::post('end-call', 'Messenger\AgoraController@endCall');
        });

        Route::group(['prefix' => 'conference'], function () {
            Route::post('create', 'Messenger\AgoraController@newConference');
        });

        Route::get('get-block-user', 'Messenger\AgoraController@getBlockUserList');
        Route::post('block-user', 'Messenger\ChatSettingsController@blockUser');
        Route::post('unblock-user', 'Messenger\ChatSettingsController@unblockUser');

        Route::group(['prefix' => 'video'], function () {
            Route::post('continue-video', 'Messenger\AgoraController@continueVideoCall');
        });

        Route::group(['prefix' => 'stream'], function () {
            Route::post('new-stream', 'Messenger\AgoraController@newStream');
            Route::post('send-message', 'Messenger\AgoraController@newMessageStream');
            Route::post('end-stream', 'Messenger\AgoraController@endStream');
        });
    });

    Route::get('get-franchise-clients-list', 'Microphone\Franchise\FranchiseController@index');
    Route::post('franchise-purchase', 'Microphone\Franchise\FranchiseController@store');
    Route::delete('/franchise-purchase/{franchiseClient}', 'Microphone\Franchise\FranchiseController@delete');

    Route::post('report/chat', 'Messenger\ReportController@reportChat');
    Route::post('report/message', 'Messenger\ReportController@reportMessage');
    Route::post('report/user', 'Messenger\ReportController@reportUser');
    Route::post('agora/token', 'Messenger\AgoraController@token');
});

Route::post('/microphone/ad/getByCategory', 'Microphone\Ad\AdController@getByCategory');
Route::post('/microphone/ad/getBySubcategory', 'Microphone\Ad\AdController@getBySubcategory');
Route::post('/microphone/ad/getByUser', 'Microphone\Ad\AdController@getByUser');
Route::post('/microphone/ad/getById', 'Microphone\Ad\AdController@getById');
Route::post('/microphone/ad/search', 'Microphone\Ad\AdController@search');
Route::post('/microphone/ad/increaseCalls', 'Microphone\Ad\AdController@increaseCalls');
Route::post('/microphone/ad/top', 'Microphone\Ad\AdController@top');

Route::post('/microphone/category', 'Microphone\Category\CategoryController@index');

Route::get('/country', 'Country\CountryController@index');

Route::post('/city/getById', 'City\CityController@getById');
Route::post('/city/getByCountry', 'City\CityController@getByCountry');

Route::post('/microphone/review/getByAd', 'Microphone\Review\ReviewController@getByAd');

Route::get('/microphone/subcategory', 'Microphone\Subcategory\SubcategoryController@index');

Route::post('/tariff', 'Tariff\TariffController@index');

Route::post('/user/getById', 'User\UserController@getById');

Route::post('/part/minimalPercent', 'Part\PartController@minimalPercent');

//SuperWork

Route::post('/superwork/ad/getByCategory', 'SuperWork\Ad\AdController@getByCategory');
Route::post('/superwork/ad/getByUser', 'SuperWork\Ad\AdController@getByUser');
Route::post('/superwork/ad/getById', 'SuperWork\Ad\AdController@getById');
Route::post('/superwork/ad/search', 'SuperWork\Ad\AdController@search');
Route::post('/superwork/ad/increaseCalls', 'SuperWork\Ad\AdController@increaseCalls');
Route::post('/superwork/ad/increaseViews', 'SuperWork\Ad\AdController@increaseViews');
Route::post('/superwork/ad/top', 'SuperWork\Ad\AdController@top');

Route::post('/superwork/category', 'SuperWork\Category\CategoryController@index');


Route::get('/superwork/ad/getPaginationAdUser', 'SuperWork\Ad\AdController@getAdByUserData');
Route::get('/superwork/ad/searchAd', 'SuperWork\Ad\AdController@searchAd');
Route::get('/city/search', 'City\CityController@search');
