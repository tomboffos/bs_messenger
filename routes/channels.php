<?php

use App\Models\Messenger\MessengerChat;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});


Broadcast::channel('messages.{id}', function ($id) {
    return ['id' => $id];
});

Broadcast::channel('deleted.message.{id}', function ($id) {
    return ['id' => $id];
});


Broadcast::channel('messages.stream.{id}', function ($id) {
    return ['id' => $id];
});

Broadcast::channel('messages.edit.{id}', function ($id) {
    return true;
});

Broadcast::channel('get.index.{id}', function ($id) {
    return ['id' => $id];
});

Broadcast::channel('get.index.call.{id}', function ($id) {
    return ['id' => $id];
});

Broadcast::channel('profile.chat.{id}', function (MessengerChat $chat, $id) {
    return (int) $chat->id === (int) $id;
});

Broadcast::channel('online.{id}', function ($id) {
       User::where('id','=',$id)
           ->update(
               [
                   'last_visit' => Carbon::now()
               ]
           );
       return ['id' => $id,'last_visit' => Carbon::now()];
});
