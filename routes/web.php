<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return response()->file('../public/bs_messenger.pdf');
});

Route::get('/payment', function () {
    return view('payment');
});

Route::get('/payment_success', function () {
    return view('payment_success');
});

Route::get('/payment_failure', function () {
    return view('payment_failure');
});
