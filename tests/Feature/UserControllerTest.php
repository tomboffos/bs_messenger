<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::find(104720);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetCurrentUserAgentStatus()
    {
        // Client errors
        $response = $this->actingAs($this->user)->getJson('/api/user/get-current-user-agent-status');

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['application_id']);


        // Successful response
        $response = $this->actingAs($this->user)
            ->getJson('/api/user/get-current-user-agent-status?application_id=2');

        $response->assertJsonMissingValidationErrors(['application_id']);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'is_agent',
            'fee',
            'agent_expiration'
        ]);

        $this->assertEquals('boolean', gettype($response['is_agent']));
        $this->assertEquals('double', gettype($response['fee']));
        $this->assertEquals('string', gettype($response['agent_expiration']));
    }

    public function testGetAuthUserReferral()
    {
        // Client errors
        $response = $this->actingAs($this->user)->getJson('/api/user/referral-list');

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['application_id']);


        // Successful response
        $response = $this->actingAs($this->user)
            ->getJson('/api/user/referral-list?application_id=1');

        $response->assertSuccessful();
        $response->assertJsonStructure([
            'full_transaction_parts',
            'agent_expiration',
            'referral_count',
            'referrals'
        ]);

        $this->assertEquals('double', gettype($response['full_transaction_parts']));
        $this->assertEquals('string', gettype($response['agent_expiration']));
        $this->assertEquals('integer', gettype($response['referral_count']));
        $this->assertEquals('array', gettype($response['referrals']));
    }

    public function testGetChildrenReferralTransaction()
    {
        // Client errors
        $response = $this->actingAs($this->user)
            ->getJson('/api/user/children-referral/transactions');

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['user_id', 'application_id']);

        $response = $this->actingAs($this->user)
            ->getJson('/api/user/children-referral/transactions?user_id=1');

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['application_id']);

        $response = $this->actingAs($this->user)
            ->getJson('/api/user/children-referral/transactions?application_id=1');

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['user_id']);


        // Successful response
        $response = $this->actingAs($this->user)
            ->getJson('/api/user/children-referral/transactions?user_id=1&application_id=1');

        $response->assertSuccessful();
        $response->assertJsonStructure([
            'user',
            'referral_add',
            'full_add_bonus',
            'count_transactions',
            'transactions'
        ]);

        $this->assertEquals('array', gettype($response['user']));
        $this->assertEquals('string', gettype($response['referral_add']));
        $this->assertEquals('integer', gettype($response['full_add_bonus']));
        $this->assertEquals('integer', gettype($response['count_transactions']));
        $this->assertEquals('array', gettype($response['transactions']['data']));
    }
}
