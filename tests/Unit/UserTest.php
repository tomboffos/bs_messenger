<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Illuminate\Support\Facades\Log;
class UserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

   /**
     * A basic functional test example.
     *
     * @return void
     */
    public function getUserProfile()
    {
        $response = $this->json('POST','/api/user/getCurrentUser', ['application_id' => '1']);
        
        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);
    }

    public function getReferralList(){
       $response = $this->json('GET','/api/user/referral-list?application_id=2');
        
        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);
    }

    public function getReferralTransaction(){
          $response = $this->json('GET','/api/user/children-referral/transactions?application_id=2&user_id=105361');
        
        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);
    }


    public function getUserTransactionWithoutParams(){
          $response = $this->json('POST','/api/user/getTransactionsCurrentUser',['application_id' => '1']);
        
        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);
    }


     public function getUserTransactionWithTimeWithoutAction(){
          $response = $this->json('POST','/api/user/getTransactionsCurrentUser',['application_id' => '1','time' => 2]);
        
        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);
    }


     public function getUserTransactionWithActionWithoutTime(){
          $response = $this->json('POST','/api/user/getTransactionsCurrentUser',['application_id' => '1','action' => 2]);
        
        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);
    }
}
